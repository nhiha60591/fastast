<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            // You can remove this line anytime.
            $theme->setTitle('Fastast');

            // Breadcrumb template.
            // $theme->breadcrumb()->setTemplate('
            //     <ul class="breadcrumb">
            //     @foreach ($crumbs as $i => $crumb)
            //         @if ($i != (count($crumbs) - 1))
            //         <li><a href="{{ $crumb["url"] }}">{!! $crumb["label"] !!}</a><span class="divider">/</span></li>
            //         @else
            //         <li class="active">{!! $crumb["label"] !!}</li>
            //         @endif
            //     @endforeach
            //     </ul>
            // ');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            // You may use this event to set up your assets.
            $theme->asset()->usePath()->add('bootstrap-css', 'bootstrap/css/bootstrap.min.css');
            $theme->asset()->usePath()->add('bootstrap-theme-css', 'bootstrap/css/bootstrap-theme.min.css');
            $theme->asset()->usePath()->add('slick-css', 'slick/slick.css');
            $theme->asset()->usePath()->add('slick-theme-css', 'slick/slick-theme.css');
            $theme->asset()->usePath()->add('bootstrap-select-css', 'css/bootstrap-select.min.css');
            $theme->asset()->usePath()->add('font-awesome-css', 'font-awesome/css/font-awesome.min.css');
            $theme->asset()->usePath()->add('bootstrap-datetimepicker', 'plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');
            $theme->asset()->usePath()->add('fastast-fonts', 'css/fastast-fonts.css');
            $theme->asset()->usePath()->add('style', 'css/style.css');

            $theme->asset()->usePath()->add('jquery', 'js/jquery-1.11.3.min.js');
            $theme->asset()->usePath()->add('jquery-validation', 'plugins/jquery-validation/dist/jquery.validate.min.js');
            $theme->asset()->usePath()->add('settings', 'js/settings.js', array('jquery'));
            $theme->asset()->usePath()->add('bootstrap', 'bootstrap/js/bootstrap.min.js');
            $theme->asset()->usePath()->add('slick-js', 'slick/slick.min.js');
            $theme->asset()->usePath()->add('bootstrap-select', 'js/bootstrap-select.min.js');
            $theme->asset()->usePath()->add('moment-js', 'plugins/moment/moment.js');
            $theme->asset()->usePath()->add('bootstrap-datetimepicker-js', 'plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');
            $theme->asset()->container('footer')->add('google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDyOHNsZJs2Nq9a8bHDwZe9GbqGI5ra3oo&libraries=places&callback=initMap');
            $theme->asset()->container('footer')->usePath()->add('map-marker-label', 'js/google/markerwithlabel.js');
            $theme->asset()->container('footer')->usePath()->add('infobox', 'js/google/infobox.js');
            $theme->asset()->container('footer')->add('tinymce-editor', '//cdn.tinymce.com/4/tinymce.min.js', array('jquery'));
            $theme->asset()->container('footer')->usePath()->add('scripts', 'js/scripts.js', array('jquery', 'tinymce-editor'));
            //$theme->asset()->container('footer')->usePath()->add('list-shipping-api', 'js/list-shipping.js');
            // $theme->asset()->add('jquery', 'vendor/jquery/jquery.min.js');
            // $theme->asset()->add('jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', array('jquery'));

            // Partial composer.
            // $theme->partialComposer('header', function($view)
            // {
            //     $view->with('auth', Auth::user());
            // });
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => array(

            'default' => function($theme)
            {
                // $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            }

        )

    )

);