<!DOCTYPE html>
<html>
<head>
    <title>{!! Theme::get('title') !!}</title>
    <meta charset="utf-8">
    <meta name="keywords" content="{!! Theme::get('keywords') !!}">
    <meta name="description" content="{!! Theme::get('description') !!}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    {!! Theme::asset()->styles() !!}
    {!! Theme::asset()->scripts() !!}
    {!! Theme::partial('scripts') !!}
</head>
<body>
{!! Theme::partial('header') !!}

<div class="container">
    {!! Theme::content() !!}
</div>

{!! Theme::partial('footer') !!}

{!! Theme::asset()->container('footer')->scripts() !!}
</body>
</html>