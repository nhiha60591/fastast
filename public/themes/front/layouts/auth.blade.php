<!DOCTYPE html>
<html>
<head>
    <title>{!! Theme::get('title') !!}</title>
    <meta charset="utf-8">
    <meta name="keywords" content="{!! Theme::get('keywords') !!}">
    <meta name="description" content="{!! Theme::get('description') !!}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! Theme::asset()->styles() !!}
    {!! Theme::asset()->scripts() !!}
    {!! Theme::partial('scripts') !!}
</head>
<body class="auth">
{!! Theme::partial('auth-header') !!}

{!! Theme::content() !!}

{!! Theme::partial('auth-footer') !!}

{!! Theme::asset()->container('footer')->scripts() !!}
</body>
</html>