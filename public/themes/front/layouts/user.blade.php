<!DOCTYPE html>
<html>
<head>
    <title>{!! Theme::get('title') !!}</title>
    <meta charset="utf-8">
    <meta name="keywords" content="{!! Theme::get('keywords') !!}">
    <meta name="description" content="{!! Theme::get('description') !!}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! Theme::asset()->styles() !!}
    {!! Theme::asset()->scripts() !!}
    {!! Theme::partial('scripts') !!}
</head>
<body class="user">
<div class="hidden">
    <span class="create-marker-icon"><img src="{!! Theme::asset()->url('img/map-marker.png') !!}"></span>
    <span class="marker-pin-icon"><img src="{!! Theme::asset()->url('img/map-pins.png') !!}"></span>
</div>
{!! Theme::partial('header') !!}

<div class="container-fluid no-padding">
    <div class="row user-tab-menu">
        <div class="col-lg-12">
            @if(Auth::user()->isSuperUser())
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <a href="{{ url('user-admin/manage-shipping') }}" class="{!! Theme::get('active') =='admin-manage-shipping' ? 'active ' : '' !!}btn btn-primary btn-lg">Manage Shipping</a>
                    </div>
                    <div class="btn-group dropdown" role="group">
                        <a href="{{ url('user/manage-listing') }}" data-toggle="dropdown" class="dropdown-toggle {!! Theme::get('active') =='admin-listing' ? 'active ' : '' !!}btn btn-primary btn-lg">Manage Listing <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('user-admin/manage-listing-shipping') }} " class="{!! Theme::get('subActive') =='listing-shipping' ? 'sub-active' : '' !!}">List of shipping</a></li>
                            <li><a href="{{ url('user-admin/manage-listing-package') }}" class="{!! Theme::get('subActive') =='listing-package' ? 'sub-active' : '' !!}">List of package</a></li>
                        </ul>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('user/chat-message') }}" class="{!! Theme::get('active') =='admin-chat-message' ? 'active ' : '' !!}btn btn-primary btn-lg">Chat/Message</a>

                    </div>
                    <div class="btn-group dropdown" role="group">
                        <a href="{{ url('user-admin/payment-view') }}" data-toggle="dropdown" class="dropdown-toggle {!! Theme::get('active') =='admin-payment' ? 'active ' : '' !!}btn btn-primary btn-lg">Payment <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('user-admin/payment-statement-view') }} " class="{!! Theme::get('subActive') =='statement-view' ? 'sub-active' : '' !!}">All statement view</a></li>
                            <li><a href="{{ url('user-admin/payment-transaction-fees') }}" class="{!! Theme::get('subActive') =='transaction-fees' ? 'sub-active' : '' !!}">Transaction fees</a></li>
                            <li><a href="{{ url('user-admin/payment-membership-fees') }}" class="{!! Theme::get('subActive') =='membership-fees' ? 'sub-active' : '' !!}">Membership fees</a></li>
                            <li><a href="{{ url('user-admin/payment-advertisement-fees') }}" class="{!! Theme::get('subActive') =='advertisement-fees' ? 'sub-active' : '' !!}">Advertisement fees</a></li>
                        </ul>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('user-admin/manage-account') }}" class="{!! Theme::get('active') =='admin-manage-account' ? 'active ' : '' !!}btn btn-primary btn-lg">Manage account</a>
                    </div>
                </div>
            @else
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <a href="{{ url('user/manage-shipping') }}" class="{!! Theme::get('active') =='manage-shipping' ? 'active ' : '' !!}btn btn-primary btn-lg">Manage Shipping</a>
                    </div>
                    <div class="btn-group dropdown" role="group">
                        <a href="{{ url('user/manage-listing') }}" data-toggle="dropdown" class="dropdown-toggle {!! Theme::get('active') =='listing' ? 'active ' : '' !!}btn btn-primary btn-lg">Manage Listing <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('user/manage-listing-shipping') }}" class="{!! Theme::get('subActive') =='manage-listing-shipping' ? 'sub-active' : '' !!}">List of shipping rate</a></li>
                            <li><a href="{{ url('user/manage-listing-package') }}" class="{!! Theme::get('subActive') =='manage-listing-package' ? 'sub-active' : '' !!}">Package need quote</a></li>
                        </ul>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('user/chat-message') }}" class="{!! Theme::get('active') =='chat' ? 'active ' : '' !!}btn btn-primary btn-lg">Chat/Message</a>

                    </div>
                    <div class="btn-group dropdown" role="group">
                        <a href="{{ url('user/payment') }}" data-toggle="dropdown" class="dropdown-toggle {!! Theme::get('active') =='payment' ? 'active ' : '' !!}btn btn-primary btn-lg">Payment<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('user/all-statement-view') }}" class="{!! Theme::get('subActive') =='all-statement-view' ? 'sub-active' : '' !!}">All statement view</a></li>
                            <li><a href="{{ url('user/transaction-view') }}" class="{!! Theme::get('subActive') =='transaction-view' ? 'sub-active' : '' !!}">Transaction view</a></li>
                            <li><a href="{{ url('user/membership-fees') }}" class="{!! Theme::get('subActive') =='membership-fees' ? 'sub-active' : '' !!}">Membership fees/card</a></li>
                        </ul>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('user/update-profile') }}" class="{!! Theme::get('active') =='profile' ? 'active ' : '' !!}btn btn-primary btn-lg">Update Profile</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {!! Theme::content() !!}
</div>

{!! Theme::partial('footer') !!}
{!! Theme::asset()->container('footer')->scripts() !!}
</body>
</html>