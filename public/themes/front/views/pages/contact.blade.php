<div class="container">
<h1 class="page-title">{{ $page->title }}</h1>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
    <div class="row">
        <div class="col-md-8" id="main-content">
        <div class="page-content">{!! $page->content !!}</div>
        <div class="contact-form">
            {!! Form::open(array('method' => 'POST', 'id' => $page->slug, 'files'=>true, 'role'=>'form', 'class'=> 'form')) !!}
            <div class="form-group{{ $errors->has('your_name') ? ' has-error' : '' }}">
                {!! Form::label( 'your_name', trans( 'user.full-name' ) ) !!}
                {!! Form::text( 'your_name', '', array( 'class' => 'form-control', 'placeholder' => trans( 'user.full-name' ), 'require'=>'require' ) ) !!}
                @if ($errors->has('your_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('your_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label( 'email', trans( 'user.email' ) ) !!}
                {!! Form::text( 'email', '', array( 'class' => 'form-control', 'placeholder' => trans( 'user.email' ), 'require'=>'require' ) ) !!}
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                {!! Form::label( 'message', trans( 'user.message' ) ) !!}
                {!! Form::textarea( 'message', '', array( 'class' => 'form-control', 'placeholder' => trans( 'user.message' ), 'require'=>'require' ) ) !!}
                @if ($errors->has('message'))
                    <span class="help-block">
                        <strong>{{ $errors->first('message') }}</strong>
                    </span>
                @endif
            </div>
            <p class="text-right btns"><input type="submit" class="btn btn-save" value="{{ trans('user.send') }}"></p>
            {!! Form::close(); !!}
        </div>
        </div>
    </div>
</div>