<div class="container">
    <h1 class="page-title">{{ $page->title }}</h1>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-8" id="main-content">
            <div class="page-content">{!! $page->content !!}</div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>