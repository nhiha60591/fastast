{{--<script type="text/javascript">--}}
    {{--$(document).ready(function() {--}}
        {{--$(".select2").select2();--}}
    {{--});--}}
{{--</script>--}}
<div class="hidden">
    <!-- Chuyển phát nhanh tài liệu/văn bản -->
    <!-- Trong nước -->
    <div id="shipping-rate-1-1">
        <?php 
            $deliveryType = 1; //Vận chuyển trong vòng 6 giờ
            $weightList = array('0.05','0.1','0.25','0.5','1.0','1.5','2.0');
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==1?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            {{--<select name="shippingMath" id="shipping-math" class="form-control select-arrow select2" multiple="multiple">--}}
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000">Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000" selected>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-7">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{$weightList[0]}}">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][rate]" placeholder="0.00" class="form-control shipping-rate-input">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                        <span class="require">(*)</span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i=1;$i<(count($weightList)-1);$i++)
                            <tr id="shipping-rate-item-{{$i}}">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit select-arrow">
                                            <option value="0" selected>(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                        <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                            </tr>
                            @endfor
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][unit]" class="form-control last-unit select-arrow">
                                            <option value="0" selected>(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                        <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <div class="form-group text-right" id="over-last-item-0">
                                        ><span class="over-text">{{end($weightList)}}</span>
                                        <span class="weight-unit unit-weight">kg</span>
                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[over][rate]" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5" selected>Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Chuyển phát nhanh tài liệu/văn bản -->
    <!-- Ngoài nước -->
    <div id="shipping-rate-1-2">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
            $weightList = array('0.5','1.0','1.5','2.0','2.5','3.0');
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000">Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000" selected>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{$weightList[0]}}">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][rate]" placeholder="0.00" class="form-control shipping-rate-input">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                        <span class="require">(*)</span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i=1;$i<(count($weightList)-1);$i++)
                            <tr id="shipping-rate-item-{{$i}}">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit select-arrow">
                                            <option value="0" selected>(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                        <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                            </tr>
                            @endfor
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][unit]" class="form-control last-unit select-arrow">
                                            <option value="0" selected>(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                        <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <div class="form-group text-right" id="over-last-item-0">
                                        ><span class="over-text">{{end($weightList)}}</span>
                                        <span class="weight-unit unit-weight">kg</span>
                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[over][rate]" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5" selected>Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Chuyển phát đường bộ -->
    <div id="shipping-rate-2-1">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
            $weightList = array('5.0','10.0','20.0','50.0','100.0','200.0');
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{$weightList[0]}}">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][rate]" placeholder="0.00" class="form-control shipping-rate-input">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                        <span class="require">(*)</span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i=1;$i<(count($weightList)-1);$i++)
                            <tr id="shipping-rate-item-{{$i}}">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                        <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                            </tr>
                            @endfor
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][unit]" class="form-control last-unit select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                        <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <div class="form-group text-right" id="over-last-item-0">
                                        ><span class="over-text">{{end($weightList)}}</span>
                                        <span class="weight-unit unit-weight">kg</span>
                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[over][rate]" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển bằng xe tải nặng/container -->
    <!-- Giá theo khối lượng (VAT) -->
    <div id="shipping-rate-6-3">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
            $weightList = array('10.0','50.0','100.0','150.0');
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{$weightList[0]}}">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][rate]" placeholder="0.00" class="form-control shipping-rate-input">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                        <span class="require">(*)</span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i=1;$i<(count($weightList)-1);$i++)
                            <tr id="shipping-rate-item-{{$i}}">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                        <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                            </tr>
                            @endfor
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][unit]" class="form-control last-unit select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                        <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <div class="form-group text-right" id="over-last-item-0">
                                        ><span class="over-text">{{end($weightList)}}</span>
                                        <span class="weight-unit unit-weight">kg</span>
                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[over][rate]" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển bằng xe tải nặng/container -->
    <!-- Giá theo chuyến -->
    <div id="shipping-rate-6-4">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.load-capacity') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.rate-per-trip-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.loading-per-trip') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][trip_type]" class="form-control select-arrow">
                                            <option value="20DC">Container 20'DC</option>
                                            <option value="40DC">Container 40'DC</option>
                                            <option value="40HC">Container 40'HC</option>
                                            <option value="8">8 ton</option>
                                            <option value="9-10">9-10 ton</option>
                                            <option value="12-15">12-15 ton</option>
                                            <option value="15-17">15-17 ton</option>
                                            <option value="17-20">17-20 ton</option>
                                            <option value="20-25">20-25 ton</option>
                                            <option value="25-30">25-30 ton</option>
                                            <option value=">30">>30 ton</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][trip_rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[last][rate_type]" value="trip">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển hàng nhẹ bằng ôtô -->
    <!-- Giá theo khối lượng (VAT) -->
    <div id="shipping-rate-4-3">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
            $weightList = array('10.0','50.0','100.0','150.0');
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{$weightList[0]}}">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][rate]" placeholder="0.00" class="form-control shipping-rate-input">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                        <span class="require">(*)</span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i=1;$i<(count($weightList)-1);$i++)
                            <tr id="shipping-rate-item-{{$i}}">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                        <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                            </tr>
                            @endfor
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][unit]" class="form-control last-unit select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                        <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <div class="form-group text-right" id="over-last-item-0">
                                        ><span class="over-text">{{end($weightList)}}</span>
                                        <span class="weight-unit unit-weight">kg</span>
                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[over][rate]" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển hàng nhẹ bằng ôtô -->
    <!-- Giá theo chuyến -->
    <div id="shipping-rate-4-4">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.load-capacity') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.rate-per-trip-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.loading-per-trip') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][trip_type]" class="form-control select-arrow">
                                            <option value="500">500 kilogram</option>
                                            <option value="750">750 kilogram</option>
                                            <option value="1000">1.0 ton</option>
                                            <option value="1250">1.25 ton</option>
                                            <option value="1500">1.5 ton</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][trip_rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[last][rate_type]" value="trip">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển bằng xe tải nhẹ -->
    <!-- Giá theo khối lượng (VAT) -->
    <div id="shipping-rate-5-3">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
            $weightList = array('10.0','50.0','100.0','150.0');
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{$weightList[0]}}">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][rate]" placeholder="0.00" class="form-control shipping-rate-input">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                        <span class="require">(*)</span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i=1;$i<(count($weightList)-1);$i++)
                            <tr id="shipping-rate-item-{{$i}}">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                        <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                            </tr>
                            @endfor
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][unit]" class="form-control last-unit select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                        <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <div class="form-group text-right" id="over-last-item-0">
                                        ><span class="over-text">{{end($weightList)}}</span>
                                        <span class="weight-unit unit-weight">kg</span>
                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[over][rate]" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1" selected>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển bằng xe tải nhẹ -->
    <!-- Giá theo chuyến -->
    <div id="shipping-rate-5-4">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.load-capacity') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.rate-per-trip-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.loading-per-trip') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][trip_type]" class="form-control select-arrow">
                                            <option value="500">500 kilogram</option>
                                            <option value="750">750 kilogram</option>
                                            <option value="1000">1.0 ton</option>
                                            <option value="1250">1.25 ton</option>
                                            <option value="1500">1.5 ton</option>
                                            <option value="2000">2.0 ton</option>
                                            <option value="2500">2.5 ton</option>
                                            <option value="3000">3.0 ton</option>
                                            <option value="5000">5.0 ton</option>
                                            <option value="6500">6.5 ton</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][trip_rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[last][rate_type]" value="trip">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển nhà, văn phòng -->
    <!-- Giá theo chuyến -->
    <div id="shipping-rate-3-1">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.load-capacity') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.rate-per-trip-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.loading-per-trip') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][trip_type]" class="form-control select-arrow">
                                            <option value="500">500 kilogram</option>
                                            <option value="750">750 kilogram</option>
                                            <option value="1000">1.0 ton</option>
                                            <option value="1250">1.25 ton</option>
                                            <option value="1500">1.5 ton</option>
                                            <option value="2000">2.0 ton</option>
                                            <option value="2500">2.5 ton</option>
                                            <option value="3000">3.0 ton</option>
                                            <option value="5000">5.0 ton</option>
                                            <option value="6500">6.5 ton</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][trip_rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[last][rate_type]" value="trip">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển hàng không -->
    <!-- Trong nước -->
    <div id="shipping-rate-7-1">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
            $weightList = array('0.5','1.0','1.5','2.0');
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==1?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000">Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000" selected>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{$weightList[0]}}">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][rate]" placeholder="0.00" class="form-control shipping-rate-input">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                        <span class="require">(*)</span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i=1;$i<(count($weightList)-1);$i++)
                            <tr id="shipping-rate-item-{{$i}}">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit select-arrow">
                                            <option value="0" selected>(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                        <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                            </tr>
                            @endfor
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][unit]" class="form-control last-unit select-arrow">
                                            <option value="0" selected>(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                        <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <div class="form-group text-right" id="over-last-item-0">
                                        ><span class="over-text">{{end($weightList)}}</span>
                                        <span class="weight-unit unit-weight">kg</span>
                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[over][rate]" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5" selected>Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển hàng không -->
    <!-- Ngoài nước -->
    <div id="shipping-rate-7-2">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
            $weightList = array('0.5','1.0','1.5','2.0');
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000">Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000" selected>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{$weightList[0]}}">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][rate]" placeholder="0.00" class="form-control shipping-rate-input">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                        <span class="require">(*)</span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i=1;$i<(count($weightList)-1);$i++)
                            <tr id="shipping-rate-item-{{$i}}">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit select-arrow">
                                            <option value="0" selected>(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                        <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                            </tr>
                            @endfor
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][unit]" class="form-control last-unit select-arrow">
                                            <option value="0" selected>(*)</option>
                                            <option value="0.5">Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                        <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <div class="form-group text-right" id="over-last-item-0">
                                        ><span class="over-text">{{end($weightList)}}</span>
                                        <span class="weight-unit unit-weight">kg</span>
                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[over][rate]" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="0.5" selected>Per 0.5kg(*)</option>
                                            <option value="1">Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển đường biển -->
    <!-- LCL -->
    <div id="shipping-rate-8-5">
        <?php 
            $deliveryType = 12; //Vận chuyển trong vòng 1 - 2 ngày
            $weightList = array('1.0','2.0','3.0','4.0');
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000">Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000" selected>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{$weightList[0]}}">
                                            <div class="input-group-addon unit-weight">cbm</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[first][rate]" placeholder="0.00" class="form-control shipping-rate-input">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                        <input type="hidden" name="shippingRate[first][rate_type]" value="cbmlcl">
                                        <span class="require">(*)</span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i=1;$i<(count($weightList)-1);$i++)
                            <tr id="shipping-rate-item-{{$i}}">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">cbm</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="2" selected>per cbm(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                        <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="cbmlcl">
                                    </div>
                                </td>
                                <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                            </tr>
                            @endfor
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                            <div class="input-group-addon unit-weight">cbm</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][unit]" class="form-control last-unit select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="2" selected>per cbm(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                        <input type="hidden" name="shippingRate[last][rate_type]" value="cbmlcl">
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <div class="form-group text-right" id="over-last-item-0">
                                        ><span class="over-text">{{end($weightList)}}</span>
                                        <span class="weight-unit unit-weight">cbm</span>
                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[over][rate]" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select select-arrow">
                                            <option value="0">(*)</option>
                                            <option value="2" selected>per cbm(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                        <input type="hidden" name="shippingRate[over][rate_type]" value="cbmlcl">
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển đường biển -->
    <!-- FCL -->
    <div id="shipping-rate-8-6">
        <?php 
            $deliveryType = 12; //Vận chuyển trong vòng 1 - 2 ngày
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control select-arrow" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control select-arrow">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.load-capacity') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.rate-per-trip-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.loading-per-trip') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="shipping-rate-item-0">
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[0][trip_type]" class="form-control select-arrow">
                                            <option value="20DC">Container 20'DC</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[0][type]" value="0">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[0][trip_rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[0][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[0][rate_type]" value="cbmfcl">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <!-- <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button> -->
                                </td>
                            </tr>
                            <tr class="shipping-rate-item-1">
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[1][trip_type]" class="form-control select-arrow">
                                            <option value="40DC">Container 40'DC</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[1][type]" value="1">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[1][trip_rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[1][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[1][rate_type]" value="cbmfcl">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <!-- <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button> -->
                                </td>
                            </tr>
                            <tr class="shipping-rate-item-2">
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[2][trip_type]" class="form-control select-arrow">
                                            <option value="40HC">Container 40'HC</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[2][type]" value="2">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[2][trip_rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" required name="shippingRate[2][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[2][rate_type]" value="cbmfcl">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <!-- <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button> -->
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <div class="from-to-box-item" id="from-to-box-item-default" data-id="default">
        <div class="col-md-10 col-md-offset-1 col-sm-12">
            <div class="row">
                <div class="shipping-service-box col-md-5">
                    <h4 class="location-title">{{ trans('front.from') }}</h4>
                    <div class="form-group">
                        <input type="hidden" name="address[default][from_long]" id="address-from-long-default" value="0">
                        <input type="hidden" name="address[default][from_lat]" id="address-from-lat-default" value="0">
                        <input type="hidden" name="address[default][from_location]" id="address-from-location-0" value="0">
                        <input type="text" required name="address[default][from]" id="address-from-default" placeholder="Ex: Hồ Chí Minh city, Vietnam…" class="form-control google-maps-input">
                    </div>
                    <label id="address-from-default-error" class="error" for="address-from-default"></label>
                </div>
                <div class="col-md-1">
                    <h4 class="hidden">&nbsp;</h4>
                    <div class="form-group text-center">
                        <img src="{!! Theme::asset()->url('img/form-arrow-left-right.png') !!}" class="img-responsive" alt="" />
                    </div>
                </div>
                <div class="shipping-service-types col-md-5">
                    <div class="row">
                        <div class="col-md-8"><h4 class="location-title">{{ trans('front.to') }}</h4></div>
                        <div class="col-md-4 text-right"><h4 class="location-title">{{ trans('front.two-direct') }}</h4></div>
                    </div>
                    <div class="form-group">
                        <div class="input-group from-and-to">
                            <input type="hidden" name="address[default][to_long]" id="address-to-long-default" value="0">
                            <input type="hidden" name="address[default][to_lat]" id="address-to-lat-default" value="0">
                            <input type="hidden" name="address[default][to_location]" id="address-to-location-default" value="0">
                            <input type="text" required class="form-control google-maps-input" id="address-to-default" name="address[default][to]" placeholder="Ex: Ha Noi city, Vietnam…" />
                            <div class="input-group-addon">
                                <label>
                                    <input type="checkbox" class="form-control" name="address[default][two_way]" value="1" checked />
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <label id="address-to-default-error" class="error" for="address-to-default"></label>
                    </div>
                </div>
                <div class="col-md-1 actions">
                    <h4 class="remove-btn hidden">&nbsp;</h4>
                    <i class="remove-btn"></i>
                    <a href="" class="btn btn-default add-more add-more-shipping-address">{{ trans('front.add') }}+</a>
                </div>
            </div>

            <!---More file for specail-->
            <div class="row more-field hidden">
                <div class="shipping-service-box col-md-5">
                    <h4>{{ trans('front.number-trip') }}</h4>
                    <div class="form-group">
                        <input type="text" name="address[default][trip]" placeholder="" value="{{ trans('front.many') }}" class="form-control bg-yellow">
                    </div>
                </div>
                <div class="col-md-1">
                    <!-- <h4>&nbsp;</h4>
                    <div class="form-group text-center">
                        <img src="{!! Theme::asset()->url('img/form-arrow-left-right.png') !!}" class="img-responsive" alt="" />
                    </div> -->
                </div>
                <div class="shipping-service-types col-md-5">
                    <div class="row">
                        <div class="col-md-12"><h4>{{ trans('front.date-go') }}</h4></div>
                    </div>
                    <div class="form-group">
                        <input type="date" name="address[default][dateGo]" placeholder="" class="form-control bg-yellow">
                        <!-- <div class="from-and-to">
                            <select name="address[0][dateGo]" class="form-control bg-yellow select-arrow" placeholder="">
                                <option value="">Mổi ngày</option>
                                <option value="">2 Ngày/Tuần</option>
                                <option value="">3 Ngày/Tuần</option>
                                <option value="" class="" id=""></option>
                            </select>
                        </div> -->
                    </div>
                </div>
                <div class="col-md-1">
                </div>
            </div>
        </div>
    </div><!-- END #from-to-box-item-0 -->
    
</div>
<div class="full-container">
    <div class="container-fluid container-create">
        {!! Form::open(array('url' => 'shipping/post-shipping',  'method' => 'POST', 'id' => 'post-shipping', 'role'=>'form','files' => true, 'class'=> 'form')) !!}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1 class="title">{{ trans('front.upload-shipping') }}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h2>1. {{ trans('front.shipping-services') }}</h2>
                </div>
                <div class="col-md-10 col-md-offset-1 col-sm-12">
                    <div class="">
                        <div class="shipping-service-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>{{ trans('front.delivery-service') }} <span class="red-color">(*)</span></h4>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group shipping-services{{ $errors->has('service') ? ' has-error' : '' }}">
                                                <select id="shipping-service" class="shipping-service" name="service" data-show-icon="true" required>
                                                    <!-- <option value="" data-content="{{ trans('front.all-services') }} <img src='{!! Theme::asset()->url('img/icon-service-1.png') !!}' class='pull-right' />">{{ trans('front.all-services') }}</option> -->
                                                    @if( $services )
                                                        @foreach( $services as $service )
                                                            <option value="{{ $service->id }}" data-servicetype="{{ $service->listType() }}" data-content="{{ $service->translation($currentLanguage->id, 'name') }} <img src='{{ image_url($service->image) }}' class='pull-right' />">{{ $service->translation($currentLanguage->id, 'name') }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('service'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('service') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group text-center arrow-right">
                                                <img src="{!! Theme::asset()->url('img/form-arrow-right.png') !!}" class="img-responsive" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group shipping-service-type{{ $errors->has('serviceType') ? ' has-error' : '' }}">
                                                <select name="serviceType" id="select-service-type" class="form-control select-service-type select-arrow">
                                                    @if( $serviceTypes )
                                                        @foreach( $serviceTypes as $serviceType )
                                                            <option value="{{ $serviceType->id }}" id="service-id-{{ $serviceType->id }}" data-type="{{ $serviceType->type }}">{{ $serviceType->translation($currentLanguage->id, 'name') }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('serviceType'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('serviceType') }}</strong>
                                                    </span>
                                                @endif
                                                <div class="after-service-type">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>    
                                </div>
                                <div class="col-md-1"></div>
                                <!-- <div class="col-md-5">
                                    <div class="input-licence">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-sm-5 pull-left" for="email">License plate number <span class="red-color">(*)</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" required class="form-control" id="licenseNumber-text" required placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-sm-5 pull-left" for="email">Insurance Number <span  class="red-color">(*)</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" required class="form-control" id="insuranceNumber-text" required placeholder="">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-sm-5 pull-left" for="email">Driven license number <span  class="red-color">(*)</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" required class="form-control" id="driverLicenseNumber-text" required placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                    <div class="form-horizontal">
                                        <div class="form-group att-icon">
                                            <input type="file" class="form-control hidden" id="licenseNumber" name="licenseNumber" placeholder="">
                                            <span class="glyphicon glyphicon-paperclip" data-id="licenseNumber"></span>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group att-icon">
                                            <input type="file" class="form-control hidden" id="insuranceNumber" name="insuranceNumber" placeholder="">
                                            <span class="glyphicon glyphicon-paperclip"  data-id="insuranceNumber"></span>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group att-icon">
                                            <input type="file" class="form-control hidden" id="driverLicenseNumber" name="driverLicenseNumber" placeholder="">
                                            <span class="glyphicon glyphicon-paperclip" data-id="driverLicenseNumber"></span>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="from-to-box">
                    <div class="from-to-box-item" id="from-to-box-item-0" data-id="0">
                        <div class="col-md-10 col-md-offset-1 col-sm-12">
                            <div class="row">
                                <div class="shipping-service-box col-md-5">
                                    <h4 class="location-title">{{ trans('front.from') }}</h4>
                                    <div class="form-group">
                                        <input type="hidden" name="address[0][from_long]" id="address-from-long-0" value="0">
                                        <input type="hidden" name="address[0][from_lat]" id="address-from-lat-0" value="0">
                                        <input type="hidden" name="address[0][from_location]" id="address-from-location-0" value="0">
                                        <input type="text" required name="address[0][from]" id="address-from-0" placeholder="Ex: Hồ Chí Minh city, Vietnam…" class="form-control google-maps-input">
                                    </div>
                                    <label id="address-from-0-error" class="error" for="address-from-0"></label>
                                </div>
                                <div class="col-md-1">
                                    <h4>&nbsp;</h4>
                                    <div class="form-group text-center">
                                        <img src="{!! Theme::asset()->url('img/form-arrow-left-right.png') !!}" class="img-responsive" alt="" />
                                    </div>
                                </div>
                                <div class="shipping-service-types col-md-5">
                                    <div class="row">
                                        <div class="col-md-8"><h4 class="location-title">{{ trans('front.to') }}</h4></div>
                                        <div class="col-md-4 text-right"><h4 class="location-title">{{ trans('front.two-direct') }}</h4></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group from-and-to">
                                            <input type="hidden" name="address[0][to_long]" id="address-to-long-0" value="0">
                                            <input type="hidden" name="address[0][to_lat]" id="address-to-lat-0" value="0">
                                            <input type="hidden" name="address[0][to_location]" id="address-to-location-0" value="0">
                                            <input type="text" required class="form-control google-maps-input" id="address-to-0" name="address[0][to]" placeholder="Ex: Ha Noi city, Vietnam…" />
                                            <div class="input-group-addon">
                                                <label>
                                                    <input type="checkbox" class="form-control" name="address[0][two_way]" value="1" checked />
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <label id="address-to-0-error" class="error" for="address-to-0"></label>
                                    </div>
                                </div>
                                <div class="col-md-1 actions">
                                    <h4 class="remove-btn">&nbsp;</h4>
                                    <i class="remove-btn"></i>
                                    <a href="" class="btn btn-default add-more add-more-shipping-address">{{ trans('front.add') }}+</a>
                                </div>
                            </div>

                            <!---More file for specail-->
                            <div class="row more-field hidden">
                                <div class="shipping-service-box col-md-5">
                                    <h4>{{ trans('front.number-trip') }}</h4>
                                    <div class="form-group">
                                        <input type="text" name="address[0][trip]" placeholder="" value="{{ trans('front.many') }}" class="form-control bg-yellow">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <!-- <h4>&nbsp;</h4>
                                    <div class="form-group text-center">
                                        <img src="{!! Theme::asset()->url('img/form-arrow-left-right.png') !!}" class="img-responsive" alt="" />
                                    </div> -->
                                </div>
                                <div class="shipping-service-types col-md-5">
                                    <div class="row">
                                        <div class="col-md-12"><h4>{{ trans('front.date-go') }}</h4></div>
                                    </div>
                                    <div class="form-group">
                                        <input type="date" name="address[0][dateGo]" placeholder="" class="form-control bg-yellow">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                </div>
                            </div>
                        </div>
                    </div><!-- END #from-to-box-item-0 -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h2>2. {{ trans('front.shipping-rate') }}</h2>
                </div>
                <!-- <div class="col-md-5 col-sm-12">
                    <h4 class="text-center">{{ trans('front.shipping-delivery') }}</h4>
                </div> -->
            </div>
            <div id="shipping-rate-full-content">
                <!-- FULL CONTENT GET FROM HIDDEN -->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h2>3. {{ trans('front.surcharge') }}</h2>
                </div>
                <div class="col-md-10 col-md-offset-1 surcharge">
                    <div class="row">
                        <div class="col-md-10">
                            <table class="table col-4">
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="vatSurcharge">VAT <span class="require">(*)</span></label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="vatSurcharge" id="vatSurcharge" required class="form-control" placeholder="0.00" />
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td rowspan="3" colspan="2">
                                        {!!  trans('front.total-shipping-cost') !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="fuelSurcharge">{{ trans('front.fuelSurcharge') }}</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="fuelSurcharge" id="fuelSurcharge" class="form-control" placeholder="0.00" />
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="fragileSurcharge">{{ trans('front.fragileSurcharge') }}</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="fragileSurcharge" id="fragileSurcharge" class="form-control" placeholder="0.00">
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="loadingUnloadingSurcharge">{{ trans('front.loadingUnloadingSurcharge') }}</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="loadingUnloadingSurcharge" id="loadingUnloadingSurcharge" class="form-control" placeholder="0.00">
                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-3 col-md-offset-1 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="fuelSurcharge">{{ trans('front.fuelSurcharge') }} <span class="require">(*)</span></label>
                    </div>
                    <div class="form-group">
                        <label for="fragileSurcharge">{{ trans('front.fragileSurcharge') }} <span class="require">(*)</span></label>
                    </div>
                    <div class="form-group">
                        <label for="loadingUnloadingSurcharge">{{ trans('front.loadingUnloadingSurcharge') }} <span class="require">(*)</span></label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" required name="fuelSurcharge" id="fuelSurcharge" class="form-control" placeholder="0.00" />
                            <div class="input-group-addon">%</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" required name="fragileSurcharge" id="fragileSurcharge" class="form-control" placeholder="0.00">
                            <div class="input-group-addon">%</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" required name="loadingUnloadingSurcharge" id="loadingUnloadingSurcharge" class="form-control" placeholder="0.00">
                            <div class="input-group-addon unit-currency">US$</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    {!!  trans('front.total-shipping-cost') !!}
                </div> -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>4. {!! trans('front.package-requirement') !!}</h2>
                </div>
                <div class="col-md-10 col-md-offset-1 surcharge">
                    <div class="row">
                        <div class="col-md-10">
                            <table class="table col-4">
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="maxLength">Chiều dài lớn nhất</label>
                                            <input type="text" name="maxLength" id="maxLength" class="form-control" placeholder="Không yêu cầu" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="maxWidth">Chiều rộng lớn nhất</label>
                                            <input type="text" name="maxWidth" id="maxWidth" class="form-control" placeholder="Không yêu cầu" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="maxHeight">Chiều cao lớn nhất</label>
                                            <input type="text" name="maxHeight" id="maxHeight" class="form-control" placeholder="Không yêu cầu" />
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="minWeight">Khối lượng tối thiểu</label>
                                            <input type="text" name="minWeight" id="minWeight" class="form-control" placeholder="Không yêu cầu" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="maxWeight">Khối lượng tối đa</label>
                                            <input type="text" name="maxWeight" id="maxWeight" class="form-control" placeholder="Không yêu cầu" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="maxValue">Giá trị gói hàng không quá</label>
                                            <input type="text" name="maxValue" id="maxValue" class="form-control" placeholder="Không yêu cầu" />
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="packageDeliveryDate">Thời gian chuyển hàng</label>
                                            <select name="packageDeliveryDate" id="packageDeliveryDate" class="form-control select-arrow">
                                                <option value="24/24">24/24 giờ</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="packageDeliveryDate">Thời gian giao hàng</label>
                                            <select name="packageDeliveryDate" id="packageDeliveryDate" class="form-control select-arrow">
                                                <option value="24/24">24/24 giờ</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="package-type">
                                                <img src="{!! Theme::asset()->url('img/door-to-door.png') !!}" alt="">
                                                <p>{{ trans('front.door-to-door') }}</p>
                                                <label>
                                                    <input type="checkbox" name="doorToDoor" checked value="1">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-3 col-md-offset-1 col-sm-5 col-xs-12">
                    <div class="form-group">
                        <label for="maxLength">Chiều dài lớn nhất</label>
                        <input type="text" required name="maxLength" id="maxLength" class="form-control" placeholder="Không yêu cầu" />
                    </div>
                    <div class="form-group">
                        <label for="minWeight">Khối lượng tối thiểu</label>
                        <input type="text" required name="minWeight" id="minWeight" class="form-control" placeholder="Không yêu cầu" />
                    </div>
                    <div class="form-group">
                        <label for="packageDeliveryDate">Thời gian chuyển hàng</label>
                        <select name="packageDeliveryDate" id="packageDeliveryDate" class="form-control">
                            <option value="24/24">24/24 giờ</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 col-sm-5 col-xs-12">
                    <div class="form-group">
                        <label for="maxWidth">Chiều rộng lớn nhất</label>
                        <input type="text" required name="maxWidth" id="maxWidth" class="form-control" placeholder="Không yêu cầu" />
                    </div>
                    <div class="form-group">
                        <label for="maxWeight">Khối lượng tối đa</label>
                        <input type="text" required name="maxWeight" id="maxWeight" class="form-control" placeholder="Không yêu cầu" />
                    </div>
                    <div class="form-group">
                        <label for="packageDeliveryDate">Thời gian giao hàng</label>
                        <select name="packageDeliveryDate" id="packageDeliveryDate" class="form-control">
                            <option value="24/24">24/24 giờ</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 col-sm-5 col-xs-12">
                    <div class="form-group">
                        <label for="maxHeight">Chiều cao lớn nhất</label>
                        <input type="text" required name="maxHeight" id="maxHeight" class="form-control" placeholder="Không yêu cầu" />
                    </div>
                    <div class="form-group">
                        <label for="maxValue">Giá trị gói hàng không quá</label>
                        <input type="text" required name="maxValue" id="maxValue" class="form-control" placeholder="Không yêu cầu" />
                    </div>
                    <div class="form-group">
                        <div class="checkbox-box">
                            <div class="checkbox-img"><img src="{!! Theme::asset()->url('img/door-to-door.png') !!}" /></div>
                            <div class="checkbox-label">{{ trans('front.door-to-door') }}</div>
                            <div class="checkbox-input">
                                <label>
                                    <input type="checkbox" name="doorToDoor" value="1">
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- <div class="row">
                <div class="col-lg-12">
                    <h2>6. {{ trans('front.guarantee-policy') }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="form-group">
                        <textarea name="guarantee_policy" id="shipping-content" class="form-control">{{ Auth()->user()->guarantee_policy }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h2>  {{ trans('front.requirement') }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="form-group">
                        <textarea name="requirement" id="shipping-requirement" class="form-control"></textarea>
                    </div>
                </div>
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="form-group">
                        <label for="shipping-image">Image</label>
                        <input type="file" name="shipping_image" id="shipping-image" class="form-control" />
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="col-md-12 text-center btn-groups">
                    <input type="submit" class="btn btn-cancel" value="{{ trans('front.cancel') }}">
                    <input type="submit" class="btn btn-preview" value="{{ trans('front.preview') }}">
                    <input type="submit" class="btn btn-upload" value="{{ trans('front.upload') }}">
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<div id="map" style="display: none;"></div>
<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#shipping-service').selectpicker();
    //$("#post-shipping").validate();
    $('.form-group.att-icon .glyphicon').click(function(){
        var id = $(this).attr('data-id');
        $("#"+id).click();
    });
    $('.form-group.att-icon input[type="file"]').change(function(e){
        var id = $(this).attr('id');

        $.each(event.target.files, function(key, value){
            console.log(value.name);
            $("#"+id+"-text").val(value.name);
        });
    });
});
</script>