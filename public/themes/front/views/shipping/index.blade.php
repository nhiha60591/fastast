<div class="page-header find-package">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <form name="find-package" class="form-inline find-package-form" action="" method="post">
                    <div class="form-group">
                        <label>
                            <input type="radio" value="express" name="packageService" checked />
                            <img src="{!! Theme::asset()->url('img/icon-service-1.png') !!}" alt="" class="service-item">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="radio" value="express" name="packageService" />
                            <img src="{!! Theme::asset()->url('img/icon-service-2.png') !!}" alt="" class="service-item">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="radio" value="express" name="packageService" />
                            <img src="{!! Theme::asset()->url('img/icon-service-3.png') !!}" alt="" class="service-item">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="radio" value="express" name="packageService" />
                            <img src="{!! Theme::asset()->url('img/icon-service-4.png') !!}" alt="" class="service-item">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="radio" value="express" name="packageService" />
                            <img src="{!! Theme::asset()->url('img/icon-service-5.png') !!}" alt="" class="service-item">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="radio" value="express" name="packageService" />
                            <img src="{!! Theme::asset()->url('img/icon-service-6.png') !!}" alt="" class="service-item">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="radio" value="express" name="packageService" />
                            <img src="{!! Theme::asset()->url('img/icon-service-7.png') !!}" alt="" class="service-item">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="radio" value="express" name="packageService" />
                            <img src="{!! Theme::asset()->url('img/icon-service-8.png') !!}" alt="" class="service-item">
                        </label>
                    </div>
                    <div class="form-group">
                        <select name="shipperType" class="form-control">
                            <option value="0">All Shipper</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="shipperTime" class="form-control">
                            <option value="0">Within 24 hours</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="date" class="form-control" value="2015-06-15" name="shipperDate" />
                    </div>
                    <div class="form-group">
                        <select name="shipperRelevance" class="form-control">
                            <option value="0">Relevance</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <img src="{!! Theme::asset()->url('img/find-package-button.png') !!}">
                        <input type="submit" class="btn btn-default find-package-btn" value="Find Package" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="list-shipping-box">
    <div class="col-lg-7 col-sm-12">
        <h3><span>{{ trans('front.rate_per_weight') }}</span></h3>
        <form class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-2">
                    <label for="">Package size</label>
                </div>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="inputWith" placeholder="With(cm)">
                </div>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="inputLength" placeholder="Length(cm)">
                </div>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="inputHeight" placeholder="Height(cm)">
                </div>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="inputQuantity" placeholder="Quantity: 1">
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-default">+</button>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-2">
                    <label for="">
                        Total gross
                        weight
                    </label>
                </div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputGross">
                </div>
                <div class="col-sm-2">
                    kg
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="col-sm-4">
                            <div class="package-type">
                                <img src="{!! Theme::asset()->url('img/door-to-door.png') !!}" alt="">
                                <p>Door-to-door service</p>
                                <input type="checkbox" name="packageType" value="door-to-door">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="package-type">
                                <img src="{!! Theme::asset()->url('img/fragile-package.png') !!}" alt="">
                                <p>Fragile Package</p>
                                <input type="checkbox" name="packageType" value="fragile-package">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="package-type">
                                <img src="{!! Theme::asset()->url('img/loading-unloading.png') !!}" alt="">
                                <p>Loading & Unloading</p>
                                <input type="checkbox" name="packageType" value="loading-unloading">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="collapse" id="collapseAdvancedSearch">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam earum minus modi provident. A aspernatur atque deserunt doloremque et fugiat laborum maiores quas, quia sit soluta vitae voluptatem. Consequuntur, esse.</p>
            </div>
            <div class="form-group text-center">
                <input type="submit" class="btn btn-default" value="Find Shipper">
                <a role="button" data-toggle="collapse" href="#collapseAdvancedSearch" aria-expanded="false" class="btn pull-right" aria-controls="collapseAdvancedSearch">More options >></a>
            </div>
        </form>
        <div class="list-shipping row">
            <div class="row">
                <div class="col-lg-4">
                    <div class="shipping-item">
                        <div class="shipping-image">
                            <img src="{!! Theme::asset()->url('img/shipping-image-1.png') !!}" class="img-responsive" alt="Kehner">
                            <a class="shipping-image-detail" href="">Detail</a>
                            <div class="shipping-user-avatar">
                                <a href="">
                                    <img src="{!! Theme::asset()->url('img/shipping-avatar.jpg') !!}" class="img-responsive" alt="Kehner">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="shipping-user-info col-lg-12">
                                <div class="shipping-user-name">
                                    <h4>Kehner</h4>
                                    <div class="shipping-user-rates">
                                        <div class="shipping-user-rate" style="width: 50%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="shipping-info col-lg-12">
                                <div class="col-lg-12">
                                    <p>Max: <strong>unlimited</strong></p>
                                    <p>Min: <strong>unlimited</strong></p>
                                </div>
                            </div>
                        </div>
                    </div><!-- END .shipping-item -->
                </div><!-- END .col-lg-4 -->
                <div class="col-lg-4">
                    <div class="shipping-item">
                        <div class="shipping-image">
                            <img src="{!! Theme::asset()->url('img/shipping-image-1.png') !!}" class="img-responsive" alt="Kehner">
                            <div class="guaranteed">
                                <img src="{!! Theme::asset()->url('img/guaranteed.png') !!}" class="img-responsive" alt="Kehner">
                            </div>
                            <a class="shipping-image-detail" href="">Detail</a>
                        </div>
                        <div class="row">
                            <div class="shipping-user-info col-lg-12">
                                <div class="shipping-user-avatar">
                                    <a href="">
                                        <img src="{!! Theme::asset()->url('img/shipping-avatar.jpg') !!}" class="img-responsive" alt="Kehner">
                                    </a>
                                </div>
                                <div class="shipping-user-name">
                                    <h4>Kehner</h4>
                                    <div class="shipping-user-rates">
                                        <div class="shipping-user-rate" style="width: 50%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="shipping-info col-lg-12">
                                <div class="col-lg-12">
                                    <p>Max: <strong>1500kg</strong></p>
                                    <p>Min: <strong>50kg</strong></p>
                                </div>
                            </div>
                        </div>
                    </div><!-- END .shipping-item -->
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div><!-- END .col-lg-4 -->
                <div class="col-lg-4">
                    <div class="shipping-item">
                        <div class="shipping-image">
                            <img src="{!! Theme::asset()->url('img/shipping-image-1.png') !!}" class="img-responsive" alt="Kehner">
                            <div class="guaranteed">
                                <img src="{!! Theme::asset()->url('img/guaranteed.png') !!}" class="img-responsive" alt="Kehner">
                            </div>
                            <a class="shipping-image-detail" href="">Detail</a>
                        </div>
                        <div class="row">
                            <div class="shipping-user-info col-lg-12">
                                <div class="shipping-user-avatar">
                                    <a href="">
                                        <img src="{!! Theme::asset()->url('img/shipping-avatar.jpg') !!}" class="img-responsive" alt="Kehner">
                                    </a>
                                </div>
                                <div class="shipping-user-name">
                                    <h4>Kehner</h4>
                                    <div class="shipping-user-rates">
                                        <div class="shipping-user-rate" style="width: 50%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="shipping-info col-lg-12">
                                <div class="col-lg-12">
                                    <p>Max: <strong>unlimited</strong></p>
                                    <p>Min: <strong>unlimited</strong></p>
                                </div>
                            </div>
                        </div>
                    </div><!-- END .shipping-item -->
                </div><!-- END .col-lg-4 -->
            </div><!-- END .row -->
        </div><!-- END .list-shipping -->
    </div>
    <div class="col-lg-5 col-sm-12">
        <div id="map"></div>
        <script type="text/javascript">
            var map, latLng, homeLatLng;
            function initMap() {
                latLng = new google.maps.LatLng( 10.754919, 106.643336);
                homeLatLng = new google.maps.LatLng(10.753238, 106.639430);
                var locations = [
                    ['Bondi Beach', 10.754919, 106.643336, 4],
                    ['Coogee Beach', 10.753238, 106.639430, 3],
                    ['Cronulla Beach', 10.8056949,106.6321204, 2],
                    ['Manly Beach', 10.777406, 106.680844, 1]
                ];

                map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: new google.maps.LatLng(10.8056949,106.6321204),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
            }
        </script>
    </div>
</div>
<div id="kenny" style="display: none;">
    <div class="box_title">
        <div class="map-shipping-image">
            <img src="{!! Theme::asset()->url('img/shipping-image-1.png') !!}" class="img-responsive" alt="Kehner">
            <div class="map-guaranteed">
                <img src="{!! Theme::asset()->url('img/guaranteed.png') !!}" class="img-responsive" alt="Kehner">
            </div>
            <a class="map-shipping-image-detail" href="">Detail</a>
        </div>
        <div class="map-shipping-info">
            <div class="row">
                <p class="col-lg-12">
                    <span class="pull-left">Max: <strong>2500 kg</strong></span>
                    <span class="pull-right">Min: <strong>50 kg</strong></span>
                </p>
            </div>
        </div>
    </div>
    <span class='trig_arrow'></span>
</div>