<figure class="container-fluid banner shipping-detail">
    {{ $shipping->user->banner }}
    <div class="row">
        <img class="img-responsive" src="{{ image_url(!empty($shipping->user->banner) ? $shipping->user->banner : 'images/banner-default.jpg') }}" alt="">

        <div class="shipper">
            <div class="shipper-avatar">
                <img  class="person img-responsive" src="{{ image_url(!empty($shipping->user->avatar) ? $shipping->user->avatar : 'images/avatar-default.jpg') }}">
            </div>
            <div class="shipper-info">
                <h2>{{ $shipping->user->full_name }} </h2>
                <p class="rating">

                </p>
            </div>
        </div>
    </div>
</figure>
<main class="container">
    <div class="row row1">
        <div class="col-md-7 ">
            <div class="row">
                <div class="road col-md-8 col-lg-offset-3">
                    <h3>{{ $shipping->service->translation($currentLanguage->id, 'name') }}</h3>
                    <p>{{ trans('front.shipping-time') }}: {{ $shipping->delivery->translation($currentLanguage->id, 'name') }}</p>
                </div>
            </div>

           <div class="maping">
               <div class="ways">
                   <?php $searchParams = session('searchParams'); ?>
                   @if( $shipping->ways->count() > 0)
                       @if( @$searchParams['from'] && @$searchParams['to'] && !empty($searchParams['from']) && !empty($searchParams['to']) )
                           <?php $ways = $shipping->ways()->where('from', 'LIKE', "%{$searchParams['from']}%")->where('to', 'LIKE', "%{$searchParams['to']}%")->get(); ?>
                           @foreach($ways as $way)
                               <div class="ways-item way-{{ $shipping->service->id }}">
                                   <p class="from">{{ trans('front.from') }}: <br>
                                       <strong>{{ $way->from }}</strong>
                                   </p>

                                   <img class="img-responsive" src="{!! Theme::asset()->url('img/'.$shipping->getWayImage().'.png') !!}" alt="">
                                   <p class="to">{{ trans('front.to') }}: <br>
                                       <strong>{{ $way->to }}</strong>
                                   </p>
                               </div>
                           @endforeach
                       @else
                           @foreach($shipping->ways as $way)
                               <div class="ways-item way-{{ $shipping->service->id }}">
                                   <p class="from">{{ trans('front.from') }}: <br>
                                       <strong>{{ $way->from }}</strong>
                                   </p>

                                   <img class="img-responsive" src="{!! Theme::asset()->url('img/'.$shipping->getWayImage().'.png') !!}" alt="">
                                   <p class="to">{{ trans('front.to') }}: <br>
                                       <strong>{{ $way->to }}</strong>
                                   </p>
                               </div>
                           @endforeach
                       @endif
                   @endif
               </div>
           </div>
        </div>
        <div class="col-md-5 total ">
            <div class="top row">
                <div class="price col-md-6">
                    <p>{{ trans('front.totalShippingCost') }}</p>
                    <h4 class="price"><span class="unit-currency">VND</span> {{ $shipping->costOnMaps(session('packageSize'), session('type')) }}</h4>
                </div>
                <div class="send-packet col-md-6">
                    <a class="btn btn-send-package" href="{{ url('package/send-package', $shipping->id) }}">{{ trans('front.send-package') }}</a>
                </div>
            </div>
            <div class="detail">
                <p>{{ trans('front.rate-includes') }}</p>
                <ul class="include">
                    <li><p>{{ trans('front.freight-rate-include-vat') }}</p></li>
                    <li><p>{{ trans('front.fragile-package-surcharge-if-any') }}</p></li>
                    <li><p>{{ trans('front.loading-unloading-surcharge-if-any') }}</p></li>
                    <li><p>{{ trans('front.fuel-surcharge') }}</p></li>
                    <li><p>{{ trans('front.service-fees') }}</p></li>
                </ul>
                <p>{!! trans('front.shipping-show-other') !!}</p>
            </div>

        </div>
    </div><!---End .packet-row1-->
    <div class="row row2 package-detail">
        <div class="col-md-10 col-lg-offset-1">
            <div class="row one">
                <div class="col-md-10 col-md-offset-1 inline-block">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="package-requirement-item">
                                <div class="col-xs-8">
                                    <p class="title">{{ trans('front.package-size') }}</p>
                                    <p>{{ trans('front.weight') }} {{ !empty($shipping->max_weight) ? ' <= '.$shipping->max_weight : trans('front.unlimited') }}</p>
                                    <p>{{ trans('front.width') }}   {{ !empty($shipping->max_width) ? ' <= '.$shipping->max_width : trans('front.unlimited') }}  </p>
                                    <p>{{ trans('front.height') }}  {{ !empty($shipping->max_height) ? ' <= '.$shipping->max_height : trans('front.unlimited') }}</p>
                                </div>
                                <div class="col-xs-4">
                                    <div class="icon icon-icon-fastast-05"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="package-requirement-item">
                                <div class="col-xs-8">
                                    <p class="title">{{ trans('front.minimum-weight') }}:</p>
                                    <p class="text-capitalize">{{ !empty($shipping->min_weight) ? ' '.$shipping->min_weight : trans('front.unlimited') }}</p>
                                </div>
                                <div class="col-xs-4">
                                    <div class="icon icon-icon-fastast-19"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="package-requirement-item">
                                <div class="col-xs-8">
                                    <p class="title">{{ trans('front.maximum-weight') }}:</p>
                                    <p class="text-capitalize">{{ !empty($shipping->max_weight) ? ' '.$shipping->max_weight : trans('front.unlimited') }}</p>
                                </div>
                                <div class="col-xs-4">
                                    <div class="icon icon-icon-fastast-19"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row two">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="package-requirement-item">
                            <div class="col-xs-12"><p class="title">{{ trans('front.door-toDoor') }}</p></div>
                            <div class="col-xs-4">
                                <p class="text-uppercase">{{ $shipping->door_to_door == 'yes' ? trans('front.yes') : trans('front.no') }}</p>
                            </div>
                            <div class="col-xs-8">
                                <div class="icon icon-icon-fastast-16"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="package-requirement-item">
                            <div class="col-xs-12"><p class="title">{{ trans('front.fuel-surcharge') }}</p></div>
                            <div class="col-xs-4">
                                <p class="text-uppercase">{{ $shipping->fuel_surcharge == 'yes' ? $shipping->fuel_surcharge : '0' }}%</p>
                            </div>
                            <div class="col-xs-8">
                                <div class="icon icon-icon-fastast-20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="package-requirement-item">
                            <div class="col-xs-12"><p class="title">{{ trans('front.fragilePackage') }}</p></div>
                            <div class="col-xs-4">
                                <p class="text-uppercase">{{ $shipping->fragile_surcharge == 'yes' ? $shipping->fragile_surcharge : '0' }}%</p>
                            </div>
                            <div class="col-xs-8">
                                <div class="icon icon-icon-fastast-17"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="package-requirement-item">
                            <div class="col-xs-12"><p class="title">{{ trans('front.loading-unloading') }}</p></div>
                            <div class="col-xs-4">
                                <p class="text-uppercase">{{ $shipping->loading_unloading_surcharge == 'yes' ? $shipping->loading_unloading_surcharge : '0' }}<br>
                                <span class="text-capitalize">per kg</span>
                                </p>
                            </div>
                            <div class="col-xs-8">
                                <div class="icon icon-icon-fastast-18"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!---End .row2-->
    <div class="row row3">
        <div class="col-md-12">
            @if( !empty( $shipping->description ) )
                {!! $shipping->description !!}
            @else
                @if( !empty( $shipping->user->company_introduction ) )
                    <div class="company-introduction service-detail">
                        <h3 class="shipping-show-title text-capitalize">{!! trans('front.company-introduction') !!}</h3>
                        {!! $shipping->user->company_introduction !!}
                    </div>
                @endif
                @if( !empty( $shipping->user->our_service ) )
                    <div class="introduction service-detail">
                        <h3 class="shipping-show-title text-capitalize">{{ trans('front.our-service') }}</h3>
                        {!! $shipping->user->our_service !!}
                    </div>
                @endif
                @if( !empty( $shipping->user->surcharges ) )
                    <div class="surcharges service-detail">
                        <h3 class="shipping-show-title text-capitalize">{{ trans('front.other-surcharges') }}</h3>
                        {!! $shipping->user->surcharges !!}
                    </div>
                @endif
                @if( !empty( $shipping->user->requirement ) )
                    <div class="requirements service-detail">
                        <h3 class="shipping-show-title text-capitalize">{{ trans('front.package_requirements') }}</h3>
                        {!! $shipping->user->requirement !!}
                    </div>
                @endif
                @if( !empty( $shipping->user->guarantee_policy ) )
                    <div class="guarantee service-detail">
                        <h3 class="shipping-show-title">{{ trans('front.guarantee_policy') }}</h3>
                        {!! $shipping->user->guarantee_policy !!}
                    </div>
                @endif
            @endif
        </div>
    </div>
    <div class="row row4">
        <h3>Orther surcharges</h3>
        <table>
            <thead>
            <tr>
                <th>Description</th>
                <th>Surcharge (VND)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Remote Area Service</td>
                <td>VND 500,000.00 or VND 8,400.00/kilo if higher</td>
            </tr>
            <tr>
                <td>Remote Area Pickup</td>
                <td>VND 500,000.00 or VND 8,400.00/kilo if higher</td>
            </tr>
            <tr>
                <td>Lithium Batteries Section II (PI967, PI970)</td>
                <td>No charge</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="row row4">
        <div class="text-center">
            <img class="img-responsive text-center" src="{!! Theme::asset()->url('img/img-show-shipping.png') !!}" alt="">
        </div>
    </div>
</main>