<div class="hidden">
    <!-- Chuyển phát nhanh tài liệu/văn bản -->
    <!-- Trong nước -->
    <div id="shipping-rate-1-1">
        <?php 
            $deliveryType = 1; //Vận chuyển trong vòng 6 giờ
            $weightList = array('0.05','0.1','0.25','0.5','1.0','1.5','2.0');
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control" multiple="multiple"multiple="multiple">
                                <option value="3000"{!! selected($shipping->math, 3000) !!}>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000"{!! selected($shipping->math, 5000) !!}>Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000"{!! selected($shipping->math, 6000) !!}>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            <?php $min = $shipping->getRateItems('min')->first(); ?>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{ $min->weight }}">
                                            <div class="input-group-addon unit-weight">kg</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[first][rate]" placeholder="0.00" value="{{ $min->rate }}" class="form-control shipping-rate-input">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                        <span class="require">(*)</span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @if( $shipping->rates->count() )
                                <?php $i=1; ?>
                                @foreach($shipping->getRateItems() as $rate )
                                    <tr id="shipping-rate-item-{{$i}}">
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{ $rate->weight }}" class="form-control shipping-weight-input last-weight">
                                                <div class="input-group-addon unit-weight">kg</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" value="{{ $rate->rate }}" class="form-control shipping-rate-input last-rate">
                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                <option value="0"{!! selected(0, $rate->unit) !!}>(*)</option>
                                                <option value="0.5"{!! selected(0.5, $rate->unit) !!}>Per 0.5kg(*)</option>
                                                <option value="1"{!! selected(1, $rate->unit) !!}>Per kg(*)</option>
                                            </select>
                                            <input type="hidden" name="shippingRate[{{$i}}][type]" value="{!! $rate->type !!}">
                                            <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="{!! $rate->rate_type !!}">
                                        </div>
                                    </td>
                                    <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                </tr>
                                @endforeach
                                <?php $last = $shipping->getRateItems('last')->first(); ?>
                                <tr class="shipping-rate-item-last">
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{ $last->weight }}" class="form-control shipping-weight-input last-weight">
                                                <div class="input-group-addon unit-weight">kg</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[last][rate]" placeholder="0.00" value="{{ $last->rate }}" class="form-control shipping-rate-input last-rate">
                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                <option value="0"{!! selected(0, $last->unit) !!}>(*)</option>
                                                <option value="0.5"{!! selected(0.5, $last->unit) !!}>Per 0.5kg(*)</option>
                                                <option value="1"{!! selected(1, $last->unit) !!}>Per kg(*)</option>
                                            </select>
                                            <input type="hidden" name="shippingRate[last][type]" value="last">
                                            <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                        </div>
                                    </td>
                                    <td class="actions">
                                        <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                    </td>
                                </tr>
                            @else
                                @for($i=1;$i<(count($weightList)-1);$i++)
                                <tr id="shipping-rate-item-{{$i}}">
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                                <div class="input-group-addon unit-weight">kg</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                <option value="0" selected>(*)</option>
                                                <option value="0.5">Per 0.5kg(*)</option>
                                                <option value="1">Per kg(*)</option>
                                            </select>
                                            <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                            <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                        </div>
                                    </td>
                                    <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                </tr>
                                @endfor
                                <tr class="shipping-rate-item-last">
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                                <div class="input-group-addon unit-weight">kg</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                <option value="0" selected>(*)</option>
                                                <option value="0.5">Per 0.5kg(*)</option>
                                                <option value="1">Per kg(*)</option>
                                            </select>
                                            <input type="hidden" name="shippingRate[last][type]" value="last">
                                            <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                        </div>
                                    </td>
                                    <td class="actions">
                                        <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                            <tfoot>
                            <?php $over = $shipping->getRateItems('over')->first(); ?>
                            <tr>
                                <td>
                                    <div class="form-group text-right" id="over-last-item-0">
                                        ><span class="over-text">{{ $over->weight }}</span>
                                        <span class="weight-unit unit-weight">kg</span>
                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[over][rate]" value="{{ $over->rate }}" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select">
                                            <option value="0"{!! selected(0, $over->unit) !!}>(*)</option>
                                            <option value="0.5"{!! selected(0.5, $over->unit) !!}>Per 0.5kg(*)</option>
                                            <option value="1"{!! selected(1, $over->unit) !!}>Per kg(*)</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Chuyển phát nhanh tài liệu/văn bản -->
    <!-- Ngoài nước -->
    <div id="shipping-rate-1-2">
        <?php
                $deliveryType = 1; //Vận chuyển trong vòng 6 giờ
                $weightList = array('0.05','0.1','0.25','0.5','1.0','1.5','2.0');
            ?>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12">
                    <div class="row">
                        <div class="shipping-delivery-box col-md-5">
                            <div class="form-group">
                                <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                    @if( $deliveries->count() > 0)
                                        @foreach($deliveries as $delivery)
                                            <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="shippingMath" id="shipping-math" class="form-control">
                                    <option value="3000"{!! selected($shipping->math, 3000) !!}>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                    <option value="5000"{!! selected($shipping->math, 5000) !!}>Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                    <option value="6000"{!! selected($shipping->math, 6000) !!}>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="shipping-guidance col-md-5">
                            <h3>{{ trans('front.guidance') }}</h3>
                            <p>{{ trans('front.guidance-text-1') }}</p>
                            <p>{{ trans('front.guidance-text-2') }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                    <div class="row">
                        <div class="col-md-10">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                    <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                    <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                    <th style="width: 25%;"></th>
                                </tr>
                                <?php $min = $shipping->getRateItems('min')->first(); ?>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{ $min->weight }}">
                                                <div class="input-group-addon unit-weight">kg</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[first][rate]" placeholder="0.00" value="{{ $min->rate }}" class="form-control shipping-rate-input">
                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                            <input type="hidden" name="shippingRate[first][type]" value="min">
                                            <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                            <span class="require">(*)</span>
                                        </div>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                @if( $shipping->rates->count() )
                                    <?php $i=1; ?>
                                    @foreach($shipping->getRateItems() as $rate )
                                        <tr id="shipping-rate-item-{{$i}}">
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{ $rate->weight }}" class="form-control shipping-weight-input last-weight">
                                                    <div class="input-group-addon unit-weight">kg</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" value="{{ $rate->rate }}" class="form-control shipping-rate-input last-rate">
                                                    <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                    <option value="0"{!! selected(0, $rate->unit) !!}>(*)</option>
                                                    <option value="0.5"{!! selected(0.5, $rate->unit) !!}>Per 0.5kg(*)</option>
                                                    <option value="1"{!! selected(1, $rate->unit) !!}>Per kg(*)</option>
                                                </select>
                                                <input type="hidden" name="shippingRate[{{$i}}][type]" value="{!! $rate->type !!}">
                                                <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="{!! $rate->rate_type !!}">
                                            </div>
                                        </td>
                                        <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                    </tr>
                                    @endforeach
                                    <?php $last = $shipping->getRateItems('last')->first(); ?>
                                    <tr class="shipping-rate-item-last">
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{ $last->weight }}" class="form-control shipping-weight-input last-weight">
                                                    <div class="input-group-addon unit-weight">kg</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[last][rate]" placeholder="0.00" value="{{ $last->rate }}" class="form-control shipping-rate-input last-rate">
                                                    <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                    <option value="0"{!! selected(0, $last->unit) !!}>(*)</option>
                                                    <option value="0.5"{!! selected(0.5, $last->unit) !!}>Per 0.5kg(*)</option>
                                                    <option value="1"{!! selected(1, $last->unit) !!}>Per kg(*)</option>
                                                </select>
                                                <input type="hidden" name="shippingRate[last][type]" value="last">
                                                <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                            </div>
                                        </td>
                                        <td class="actions">
                                            <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                        </td>
                                    </tr>
                                @else
                                    @for($i=1;$i<(count($weightList)-1);$i++)
                                    <tr id="shipping-rate-item-{{$i}}">
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                                    <div class="input-group-addon unit-weight">kg</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                    <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                    <option value="0" selected>(*)</option>
                                                    <option value="0.5">Per 0.5kg(*)</option>
                                                    <option value="1">Per kg(*)</option>
                                                </select>
                                                <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                                <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                            </div>
                                        </td>
                                        <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                    </tr>
                                    @endfor
                                    <tr class="shipping-rate-item-last">
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                                    <div class="input-group-addon unit-weight">kg</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                    <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                    <option value="0" selected>(*)</option>
                                                    <option value="0.5">Per 0.5kg(*)</option>
                                                    <option value="1">Per kg(*)</option>
                                                </select>
                                                <input type="hidden" name="shippingRate[last][type]" value="last">
                                                <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                            </div>
                                        </td>
                                        <td class="actions">
                                            <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                                <tfoot>
                                <?php $over = $shipping->getRateItems('over')->first(); ?>
                                <tr>
                                    <td>
                                        <div class="form-group text-right" id="over-last-item-0">
                                            ><span class="over-text">{{ $over->weight }}</span>
                                            <span class="weight-unit unit-weight">kg</span>
                                            <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="shippingRate[over][rate]" value="{{ $over->rate }}" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select">
                                                <option value="0"{!! selected(0, $over->unit) !!}>(*)</option>
                                                <option value="0.5"{!! selected(0.5, $over->unit) !!}>Per 0.5kg(*)</option>
                                                <option value="1"{!! selected(1, $over->unit) !!}>Per kg(*)</option>
                                            </select>
                                            <input type="hidden" name="shippingRate[over][type]" value="over">
                                            <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                        </div>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Chuyển phát đường bộ -->
    <div id="shipping-rate-2-1">
        <?php
                    $deliveryType = 1; //Vận chuyển trong vòng 6 giờ
                    $weightList = array('0.05','0.1','0.25','0.5','1.0','1.5','2.0');
                ?>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-12">
                        <div class="row">
                            <div class="shipping-delivery-box col-md-5">
                                <div class="form-group">
                                    <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                        @if( $deliveries->count() > 0)
                                            @foreach($deliveries as $delivery)
                                                <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="shippingMath" id="shipping-math" class="form-control">
                                        <option value="3000"{!! selected($shipping->math, 3000) !!}>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                        <option value="5000"{!! selected($shipping->math, 5000) !!}>Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                        <option value="6000"{!! selected($shipping->math, 6000) !!}>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="shipping-guidance col-md-5">
                                <h3>{{ trans('front.guidance') }}</h3>
                                <p>{{ trans('front.guidance-text-1') }}</p>
                                <p>{{ trans('front.guidance-text-2') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                        <div class="row">
                            <div class="col-md-10">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                        <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                        <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                        <th style="width: 25%;"></th>
                                    </tr>
                                    <?php $min = $shipping->getRateItems('min')->first(); ?>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{ $min->weight }}">
                                                    <div class="input-group-addon unit-weight">kg</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[first][rate]" placeholder="0.00" value="{{ $min->rate }}" class="form-control shipping-rate-input">
                                                    <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                                <input type="hidden" name="shippingRate[first][type]" value="min">
                                                <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                                <span class="require">(*)</span>
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if( $shipping->rates->count() )
                                        <?php $i=1; ?>
                                        @foreach($shipping->getRateItems() as $rate )
                                            <tr id="shipping-rate-item-{{$i}}">
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{ $rate->weight }}" class="form-control shipping-weight-input last-weight">
                                                        <div class="input-group-addon unit-weight">kg</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" value="{{ $rate->rate }}" class="form-control shipping-rate-input last-rate">
                                                        <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                        <option value="0"{!! selected(0, $rate->unit) !!}>(*)</option>
                                                        <option value="0.5"{!! selected(0.5, $rate->unit) !!}>Per 0.5kg(*)</option>
                                                        <option value="1"{!! selected(1, $rate->unit) !!}>Per kg(*)</option>
                                                    </select>
                                                    <input type="hidden" name="shippingRate[{{$i}}][type]" value="{!! $rate->type !!}">
                                                    <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="{!! $rate->rate_type !!}">
                                                </div>
                                            </td>
                                            <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                        </tr>
                                        @endforeach
                                        <?php $last = $shipping->getRateItems('last')->first(); ?>
                                        <tr class="shipping-rate-item-last">
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{ $last->weight }}" class="form-control shipping-weight-input last-weight">
                                                        <div class="input-group-addon unit-weight">kg</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[last][rate]" placeholder="0.00" value="{{ $last->rate }}" class="form-control shipping-rate-input last-rate">
                                                        <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                        <option value="0"{!! selected(0, $last->unit) !!}>(*)</option>
                                                        <option value="0.5"{!! selected(0.5, $last->unit) !!}>Per 0.5kg(*)</option>
                                                        <option value="1"{!! selected(1, $last->unit) !!}>Per kg(*)</option>
                                                    </select>
                                                    <input type="hidden" name="shippingRate[last][type]" value="last">
                                                    <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                </div>
                                            </td>
                                            <td class="actions">
                                                <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                            </td>
                                        </tr>
                                    @else
                                        @for($i=1;$i<(count($weightList)-1);$i++)
                                        <tr id="shipping-rate-item-{{$i}}">
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                                        <div class="input-group-addon unit-weight">kg</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                        <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                        <option value="0" selected>(*)</option>
                                                        <option value="0.5">Per 0.5kg(*)</option>
                                                        <option value="1">Per kg(*)</option>
                                                    </select>
                                                    <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                                    <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                                </div>
                                            </td>
                                            <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                        </tr>
                                        @endfor
                                        <tr class="shipping-rate-item-last">
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                                        <div class="input-group-addon unit-weight">kg</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                        <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                        <option value="0" selected>(*)</option>
                                                        <option value="0.5">Per 0.5kg(*)</option>
                                                        <option value="1">Per kg(*)</option>
                                                    </select>
                                                    <input type="hidden" name="shippingRate[last][type]" value="last">
                                                    <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                </div>
                                            </td>
                                            <td class="actions">
                                                <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <?php $over = $shipping->getRateItems('over')->first(); ?>
                                    <tr>
                                        <td>
                                            <div class="form-group text-right" id="over-last-item-0">
                                                ><span class="over-text">{{ $over->weight }}</span>
                                                <span class="weight-unit unit-weight">kg</span>
                                                <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[over][rate]" value="{{ $over->rate }}" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                                    <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select">
                                                    <option value="0"{!! selected(0, $over->unit) !!}>(*)</option>
                                                    <option value="0.5"{!! selected(0.5, $over->unit) !!}>Per 0.5kg(*)</option>
                                                    <option value="1"{!! selected(1, $over->unit) !!}>Per kg(*)</option>
                                                </select>
                                                <input type="hidden" name="shippingRate[over][type]" value="over">
                                                <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển bằng xe tải nặng/container -->
    <!-- Giá theo khối lượng (VAT) -->
    <div id="shipping-rate-6-3">
        <?php 
                    $deliveryType = 1; //Vận chuyển trong vòng 6 giờ
                    $weightList = array('0.05','0.1','0.25','0.5','1.0','1.5','2.0');
                ?>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-12">
                        <div class="row">
                            <div class="shipping-delivery-box col-md-5">
                                <div class="form-group">
                                    <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                        @if( $deliveries->count() > 0)
                                            @foreach($deliveries as $delivery)
                                                <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="shippingMath" id="shipping-math" class="form-control">
                                        <option value="3000"{!! selected($shipping->math, 3000) !!}>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                        <option value="5000"{!! selected($shipping->math, 5000) !!}>Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                        <option value="6000"{!! selected($shipping->math, 6000) !!}>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="shipping-guidance col-md-5">
                                <h3>{{ trans('front.guidance') }}</h3>
                                <p>{{ trans('front.guidance-text-1') }}</p>
                                <p>{{ trans('front.guidance-text-2') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                        <div class="row">
                            <div class="col-md-10">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                        <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                        <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                        <th style="width: 25%;"></th>
                                    </tr>
                                    <?php $min = $shipping->getRateItems('min')->first(); ?>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{ $min->weight }}">
                                                    <div class="input-group-addon unit-weight">kg</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[first][rate]" placeholder="0.00" value="{{ $min->rate }}" class="form-control shipping-rate-input">
                                                    <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                                <input type="hidden" name="shippingRate[first][type]" value="min">
                                                <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                                <span class="require">(*)</span>
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if( $shipping->rates->count() )
                                        <?php $i=1; ?>
                                        @foreach($shipping->getRateItems() as $rate )
                                            <tr id="shipping-rate-item-{{$i}}">
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{ $rate->weight }}" class="form-control shipping-weight-input last-weight">
                                                        <div class="input-group-addon unit-weight">kg</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" value="{{ $rate->rate }}" class="form-control shipping-rate-input last-rate">
                                                        <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                        <option value="0"{!! selected(0, $rate->unit) !!}>(*)</option>
                                                        <option value="0.5"{!! selected(0.5, $rate->unit) !!}>Per 0.5kg(*)</option>
                                                        <option value="1"{!! selected(1, $rate->unit) !!}>Per kg(*)</option>
                                                    </select>
                                                    <input type="hidden" name="shippingRate[{{$i}}][type]" value="{!! $rate->type !!}">
                                                    <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="{!! $rate->rate_type !!}">
                                                </div>
                                            </td>
                                            <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                        </tr>
                                        @endforeach
                                        <?php $last = $shipping->getRateItems('last')->first(); ?>
                                        <tr class="shipping-rate-item-last">
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{ $last->weight }}" class="form-control shipping-weight-input last-weight">
                                                        <div class="input-group-addon unit-weight">kg</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[last][rate]" placeholder="0.00" value="{{ $last->rate }}" class="form-control shipping-rate-input last-rate">
                                                        <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                        <option value="0"{!! selected(0, $last->unit) !!}>(*)</option>
                                                        <option value="0.5"{!! selected(0.5, $last->unit) !!}>Per 0.5kg(*)</option>
                                                        <option value="1"{!! selected(1, $last->unit) !!}>Per kg(*)</option>
                                                    </select>
                                                    <input type="hidden" name="shippingRate[last][type]" value="last">
                                                    <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                </div>
                                            </td>
                                            <td class="actions">
                                                <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                            </td>
                                        </tr>
                                    @else
                                        @for($i=1;$i<(count($weightList)-1);$i++)
                                        <tr id="shipping-rate-item-{{$i}}">
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                                        <div class="input-group-addon unit-weight">kg</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                        <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                        <option value="0" selected>(*)</option>
                                                        <option value="0.5">Per 0.5kg(*)</option>
                                                        <option value="1">Per kg(*)</option>
                                                    </select>
                                                    <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                                    <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                                </div>
                                            </td>
                                            <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                        </tr>
                                        @endfor
                                        <tr class="shipping-rate-item-last">
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                                        <div class="input-group-addon unit-weight">kg</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                        <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                        <option value="0" selected>(*)</option>
                                                        <option value="0.5">Per 0.5kg(*)</option>
                                                        <option value="1">Per kg(*)</option>
                                                    </select>
                                                    <input type="hidden" name="shippingRate[last][type]" value="last">
                                                    <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                </div>
                                            </td>
                                            <td class="actions">
                                                <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <?php $over = $shipping->getRateItems('over')->first(); ?>
                                    <tr>
                                        <td>
                                            <div class="form-group text-right" id="over-last-item-0">
                                                ><span class="over-text">{{ $over->weight }}</span>
                                                <span class="weight-unit unit-weight">kg</span>
                                                <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="shippingRate[over][rate]" value="{{ $over->rate }}" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                                    <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select">
                                                    <option value="0"{!! selected(0, $over->unit) !!}>(*)</option>
                                                    <option value="0.5"{!! selected(0.5, $over->unit) !!}>Per 0.5kg(*)</option>
                                                    <option value="1"{!! selected(1, $over->unit) !!}>Per kg(*)</option>
                                                </select>
                                                <input type="hidden" name="shippingRate[over][type]" value="over">
                                                <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển bằng xe tải nặng/container -->
    <!-- Giá theo chuyến -->
    <div id="shipping-rate-6-4">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.load-capacity') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.rate-per-trip-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.loading-per-trip') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $last = $shipping->getRateItems('last'); ?>
                            <tr class="shipping-rate-item-last">
                                <td>

                                    <div class="form-group">
                                        <select name="shippingRate[last][trip_type]" class="form-control last-unit">
                                            <option value="20DC" {!! selected('20DC', @$last->trip_type) !!}>Container 20'DC</option>
                                            <option value="40DC" {!! selected('40DC', @$last->trip_type) !!}>Container 40'DC</option>
                                            <option value="40HC" {!! selected('40HC', @$last->trip_type) !!}>Container 40'HC</option>
                                            <option value="8" {!! selected('8', @$last->trip_type) !!}>8 ton</option>
                                            <option value="9-10" {!! selected('9-10', @$last->trip_type) !!}>9-10 ton</option>
                                            <option value="12-15" {!! selected('12-15', @$last->trip_type) !!}>12-15 ton</option>
                                            <option value="15-17" {!! selected('15-17', @$last->trip_type) !!}>15-17 ton</option>
                                            <option value="17-20" {!! selected('17-20', @$last->trip_type) !!}>17-20 ton</option>
                                            <option value="20-25" {!! selected('20-25', @$last->trip_type) !!}>20-25 ton</option>
                                            <option value="25-30" {!! selected('25-30', @$last->trip_type) !!}>25-30 ton</option>
                                            <option value=">30" {!! selected('>30', @$last->trip_type) !!}>>30 ton</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[last][trip_rate]" value="{{ @$last->trip_rate }}" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[last][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[last][rate_type]" value="trip">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển hàng nhẹ bằng ôtô -->
    <!-- Giá theo khối lượng (VAT) -->
    <div id="shipping-rate-4-3">
        <?php 
                            $deliveryType = 1; //Vận chuyển trong vòng 6 giờ
                            $weightList = array('0.05','0.1','0.25','0.5','1.0','1.5','2.0');
                        ?>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-sm-12">
                                <div class="row">
                                    <div class="shipping-delivery-box col-md-5">
                                        <div class="form-group">
                                            <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                                @if( $deliveries->count() > 0)
                                                    @foreach($deliveries as $delivery)
                                                        <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select name="shippingMath" id="shipping-math" class="form-control">
                                                <option value="3000"{!! selected($shipping->math, 3000) !!}>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                                <option value="5000"{!! selected($shipping->math, 5000) !!}>Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                                <option value="6000"{!! selected($shipping->math, 6000) !!}>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="shipping-guidance col-md-5">
                                        <h3>{{ trans('front.guidance') }}</h3>
                                        <p>{{ trans('front.guidance-text-1') }}</p>
                                        <p>{{ trans('front.guidance-text-2') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                                <div class="row">
                                    <div class="col-md-10">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                                <th style="width: 25%;"></th>
                                            </tr>
                                            <?php $min = $shipping->getRateItems('min')->first(); ?>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{ $min->weight }}">
                                                            <div class="input-group-addon unit-weight">kg</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[first][rate]" placeholder="0.00" value="{{ $min->rate }}" class="form-control shipping-rate-input">
                                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                                        <span class="require">(*)</span>
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if( $shipping->rates->count() )
                                                <?php $i=1; ?>
                                                @foreach($shipping->getRateItems() as $rate )
                                                    <tr id="shipping-rate-item-{{$i}}">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{ $rate->weight }}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" value="{{ $rate->rate }}" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                                <option value="0"{!! selected(0, $rate->unit) !!}>(*)</option>
                                                                <option value="0.5"{!! selected(0.5, $rate->unit) !!}>Per 0.5kg(*)</option>
                                                                <option value="1"{!! selected(1, $rate->unit) !!}>Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[{{$i}}][type]" value="{!! $rate->type !!}">
                                                            <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="{!! $rate->rate_type !!}">
                                                        </div>
                                                    </td>
                                                    <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                                </tr>
                                                @endforeach
                                                <?php $last = $shipping->getRateItems('last')->first(); ?>
                                                <tr class="shipping-rate-item-last">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{ $last->weight }}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][rate]" placeholder="0.00" value="{{ $last->rate }}" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                                <option value="0"{!! selected(0, $last->unit) !!}>(*)</option>
                                                                <option value="0.5"{!! selected(0.5, $last->unit) !!}>Per 0.5kg(*)</option>
                                                                <option value="1"{!! selected(1, $last->unit) !!}>Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[last][type]" value="last">
                                                            <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions">
                                                        <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                                    </td>
                                                </tr>
                                            @else
                                                @for($i=1;$i<(count($weightList)-1);$i++)
                                                <tr id="shipping-rate-item-{{$i}}">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                                <option value="0" selected>(*)</option>
                                                                <option value="0.5">Per 0.5kg(*)</option>
                                                                <option value="1">Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                                            <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                                </tr>
                                                @endfor
                                                <tr class="shipping-rate-item-last">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                                <option value="0" selected>(*)</option>
                                                                <option value="0.5">Per 0.5kg(*)</option>
                                                                <option value="1">Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[last][type]" value="last">
                                                            <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions">
                                                        <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            <?php $over = $shipping->getRateItems('over')->first(); ?>
                                            <tr>
                                                <td>
                                                    <div class="form-group text-right" id="over-last-item-0">
                                                        ><span class="over-text">{{ $over->weight }}</span>
                                                        <span class="weight-unit unit-weight">kg</span>
                                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[over][rate]" value="{{ $over->rate }}" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select">
                                                            <option value="0"{!! selected(0, $over->unit) !!}>(*)</option>
                                                            <option value="0.5"{!! selected(0.5, $over->unit) !!}>Per 0.5kg(*)</option>
                                                            <option value="1"{!! selected(1, $over->unit) !!}>Per kg(*)</option>
                                                        </select>
                                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển hàng nhẹ bằng ôtô -->
    <!-- Giá theo chuyến -->
    <div id="shipping-rate-4-4">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control">
                                <option value="3000"{!! selected($shipping->math, 3000) !!}>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000"{!! selected($shipping->math, 5000) !!}>Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000"{!! selected($shipping->math, 6000) !!}>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.load-capacity') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.rate-per-trip-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.loading-per-trip') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $last = $shipping->getRateItems('last'); ?>
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][trip_type]" class="form-control last-unit">
                                            <option value="500"{!! selected(500, @$last->trip_type) !!}>500 kilogram</option>
                                            <option value="750"{!! selected(750, @$last->trip_type) !!}>750 kilogram</option>
                                            <option value="1000"{!! selected(1000, @$last->trip_type) !!}>1.0 ton</option>
                                            <option value="1250"{!! selected(1250, @$last->trip_type) !!}>1.25 ton</option>
                                            <option value="1500"{!! selected(1500, @$last->trip_type) !!}>1.5 ton</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[last][trip_rate]" placeholder="0.00" value="{{ @$last->trip_rate }}" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[last][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[last][rate_type]" value="trip">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển bằng xe tải nhẹ -->
    <!-- Giá theo khối lượng (VAT) -->
    <div id="shipping-rate-5-3">
        <?php 
                            $deliveryType = 1; //Vận chuyển trong vòng 6 giờ
                            $weightList = array('0.05','0.1','0.25','0.5','1.0','1.5','2.0');
                        ?>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-sm-12">
                                <div class="row">
                                    <div class="shipping-delivery-box col-md-5">
                                        <div class="form-group">
                                            <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                                @if( $deliveries->count() > 0)
                                                    @foreach($deliveries as $delivery)
                                                        <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select name="shippingMath" id="shipping-math" class="form-control">
                                                <option value="3000"{!! selected($shipping->math, 3000) !!}>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                                <option value="5000"{!! selected($shipping->math, 5000) !!}>Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                                <option value="6000"{!! selected($shipping->math, 6000) !!}>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="shipping-guidance col-md-5">
                                        <h3>{{ trans('front.guidance') }}</h3>
                                        <p>{{ trans('front.guidance-text-1') }}</p>
                                        <p>{{ trans('front.guidance-text-2') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                                <div class="row">
                                    <div class="col-md-10">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                                <th style="width: 25%;"></th>
                                            </tr>
                                            <?php $min = $shipping->getRateItems('min')->first(); ?>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{ $min->weight }}">
                                                            <div class="input-group-addon unit-weight">kg</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[first][rate]" placeholder="0.00" value="{{ $min->rate }}" class="form-control shipping-rate-input">
                                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                                        <span class="require">(*)</span>
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if( $shipping->rates->count() )
                                                <?php $i=1; ?>
                                                @foreach($shipping->getRateItems() as $rate )
                                                    <tr id="shipping-rate-item-{{$i}}">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{ $rate->weight }}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" value="{{ $rate->rate }}" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                                <option value="0"{!! selected(0, $rate->unit) !!}>(*)</option>
                                                                <option value="0.5"{!! selected(0.5, $rate->unit) !!}>Per 0.5kg(*)</option>
                                                                <option value="1"{!! selected(1, $rate->unit) !!}>Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[{{$i}}][type]" value="{!! $rate->type !!}">
                                                            <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="{!! $rate->rate_type !!}">
                                                        </div>
                                                    </td>
                                                    <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                                </tr>
                                                @endforeach
                                                <?php $last = $shipping->getRateItems('last')->first(); ?>
                                                <tr class="shipping-rate-item-last">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{ $last->weight }}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][rate]" placeholder="0.00" value="{{ $last->rate }}" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                                <option value="0"{!! selected(0, $last->unit) !!}>(*)</option>
                                                                <option value="0.5"{!! selected(0.5, $last->unit) !!}>Per 0.5kg(*)</option>
                                                                <option value="1"{!! selected(1, $last->unit) !!}>Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[last][type]" value="last">
                                                            <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions">
                                                        <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                                    </td>
                                                </tr>
                                            @else
                                                @for($i=1;$i<(count($weightList)-1);$i++)
                                                <tr id="shipping-rate-item-{{$i}}">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                                <option value="0" selected>(*)</option>
                                                                <option value="0.5">Per 0.5kg(*)</option>
                                                                <option value="1">Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                                            <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                                </tr>
                                                @endfor
                                                <tr class="shipping-rate-item-last">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                                <option value="0" selected>(*)</option>
                                                                <option value="0.5">Per 0.5kg(*)</option>
                                                                <option value="1">Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[last][type]" value="last">
                                                            <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions">
                                                        <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            <?php $over = $shipping->getRateItems('over')->first(); ?>
                                            <tr>
                                                <td>
                                                    <div class="form-group text-right" id="over-last-item-0">
                                                        ><span class="over-text">{{ $over->weight }}</span>
                                                        <span class="weight-unit unit-weight">kg</span>
                                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[over][rate]" value="{{ $over->rate }}" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select">
                                                            <option value="0"{!! selected(0, $over->unit) !!}>(*)</option>
                                                            <option value="0.5"{!! selected(0.5, $over->unit) !!}>Per 0.5kg(*)</option>
                                                            <option value="1"{!! selected(1, $over->unit) !!}>Per kg(*)</option>
                                                        </select>
                                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển bằng xe tải nhẹ -->
    <!-- Giá theo chuyến -->
    <div id="shipping-rate-5-4">
        <?php 
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.load-capacity') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.rate-per-trip-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.loading-per-trip') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $last = $shipping->getRateItems('last'); ?>
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][trip_type]" class="form-control last-unit">

                                            <option value="500"{!! selected(500, @$last->trip_type) !!}>500 kilogram</option>
                                            <option value="750"{!! selected(750, @$last->trip_type) !!}>750 kilogram</option>
                                            <option value="1000"{!! selected(1000, @$last->trip_type) !!}>1.0 ton</option>
                                            <option value="1250"{!! selected(1250, @$last->trip_type) !!}>1.25 ton</option>
                                            <option value="1500"{!! selected(1500, @$last->trip_type) !!}>1.5 ton</option>
                                            <option value="2000"{!! selected(2000, @$last->trip_type) !!}>2.0 ton</option>
                                            <option value="2500"{!! selected(2500, @$last->trip_type) !!}>2.5 ton</option>
                                            <option value="3000"{!! selected(3000, @$last->trip_type) !!}>3.0 ton</option>
                                            <option value="5000"{!! selected(5000, @$last->trip_type) !!}>5.0 ton</option>
                                            <option value="6500"{!! selected(6500, @$last->trip_type) !!}>6.5 ton</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[last][trip_rate]" placeholder="0.00" value="{{ @$last->trip_rate }}" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[last][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[last][rate_type]" value="trip">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển nhà, văn phòng -->
    <!-- Giá theo chuyến -->
    <div id="shipping-rate-3-1">
        <?php
            $deliveryType = 4; //Vận chuyển trong vòng 1 - 2 ngày
        ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="row">
                    <div class="shipping-delivery-box col-md-5">
                        <div class="form-group">
                            <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                @if( $deliveries->count() > 0)
                                    @foreach($deliveries as $delivery)
                                        <option {{$delivery->id==$deliveryType?'selected':''}} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <select name="shippingMath" id="shipping-math" class="form-control">
                                <option value="3000" selected>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                <option value="5000">Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                <option value="6000">Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="col-md-1"></div>
                    <div class="shipping-guidance col-md-5">
                        <h3>{{ trans('front.guidance') }}</h3>
                        <p>{{ trans('front.guidance-text-1') }}</p>
                        <p>{{ trans('front.guidance-text-2') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.load-capacity') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.rate-per-trip-fee') }}</th>
                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.loading-per-trip') }}</th>
                                <th style="width: 25%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $last = $shipping->getRateItems('last'); ?>
                            <tr class="shipping-rate-item-last">
                                <td>
                                    <div class="form-group">
                                        <select name="shippingRate[last][trip_type]" class="form-control last-unit">
                                            <option value="500"{!! selected(500, @$last->trip_type) !!}>500 kilogram</option>
                                            <option value="750"{!! selected(750, @$last->trip_type) !!}>750 kilogram</option>
                                            <option value="1000"{!! selected(1000, @$last->trip_type) !!}>1.0 ton</option>
                                            <option value="1250"{!! selected(1250, @$last->trip_type) !!}>1.25 ton</option>
                                            <option value="1500"{!! selected(1500, @$last->trip_type) !!}>1.5 ton</option>
                                            <option value="2000"{!! selected(2000, @$last->trip_type) !!}>2.0 ton</option>
                                            <option value="2500"{!! selected(2500, @$last->trip_type) !!}>2.5 ton</option>
                                            <option value="3000"{!! selected(3000, @$last->trip_type) !!}>3.0 ton</option>
                                            <option value="5000"{!! selected(5000, @$last->trip_type) !!}>5.0 ton</option>
                                            <option value="6500"{!! selected(6500, @$last->trip_type) !!}>6.5 ton</option>
                                        </select>
                                        <input type="hidden" name="shippingRate[last][type]" value="last">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[last][trip_rate]" placeholder="0.00" value="{{ @$last->trip_rate }}" class="form-control shipping-rate-input last-rate">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="shippingRate[last][trip_loading]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                            <input type="hidden" name="shippingRate[last][rate_type]" value="trip">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="actions">
                                    <button class="btn btn-default add-more add-shipping-trip">{{ trans('front.add') }}+</button>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển hàng không -->
    <!-- Trong nước -->
    <div id="shipping-rate-7-1">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-sm-12">
                                <div class="row">
                                    <div class="shipping-delivery-box col-md-5">
                                        <div class="form-group">
                                            <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                                @if( $deliveries->count() > 0)
                                                    @foreach($deliveries as $delivery)
                                                        <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select name="shippingMath" id="shipping-math" class="form-control">
                                                <option value="3000"{!! selected($shipping->math, 3000) !!}>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                                <option value="5000"{!! selected($shipping->math, 5000) !!}>Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                                <option value="6000"{!! selected($shipping->math, 6000) !!}>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="shipping-guidance col-md-5">
                                        <h3>{{ trans('front.guidance') }}</h3>
                                        <p>{{ trans('front.guidance-text-1') }}</p>
                                        <p>{{ trans('front.guidance-text-2') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                                <div class="row">
                                    <div class="col-md-10">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                                <th style="width: 25%;"></th>
                                            </tr>
                                            <?php $min = $shipping->getRateItems('min')->first(); ?>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{ $min->weight }}">
                                                            <div class="input-group-addon unit-weight">kg</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[first][rate]" placeholder="0.00" value="{{ $min->rate }}" class="form-control shipping-rate-input">
                                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                                        <span class="require">(*)</span>
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if( $shipping->rates->count() )
                                                <?php $i=1; ?>
                                                @foreach($shipping->getRateItems() as $rate )
                                                    <tr id="shipping-rate-item-{{$i}}">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{ $rate->weight }}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" value="{{ $rate->rate }}" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                                <option value="0"{!! selected(0, $rate->unit) !!}>(*)</option>
                                                                <option value="0.5"{!! selected(0.5, $rate->unit) !!}>Per 0.5kg(*)</option>
                                                                <option value="1"{!! selected(1, $rate->unit) !!}>Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[{{$i}}][type]" value="{!! $rate->type !!}">
                                                            <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="{!! $rate->rate_type !!}">
                                                        </div>
                                                    </td>
                                                    <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                                </tr>
                                                @endforeach
                                                <?php $last = $shipping->getRateItems('last')->first(); ?>
                                                <tr class="shipping-rate-item-last">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{ $last->weight }}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][rate]" placeholder="0.00" value="{{ $last->rate }}" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                                <option value="0"{!! selected(0, $last->unit) !!}>(*)</option>
                                                                <option value="0.5"{!! selected(0.5, $last->unit) !!}>Per 0.5kg(*)</option>
                                                                <option value="1"{!! selected(1, $last->unit) !!}>Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[last][type]" value="last">
                                                            <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions">
                                                        <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                                    </td>
                                                </tr>
                                            @else
                                                @for($i=1;$i<(count($weightList)-1);$i++)
                                                <tr id="shipping-rate-item-{{$i}}">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                                <option value="0" selected>(*)</option>
                                                                <option value="0.5">Per 0.5kg(*)</option>
                                                                <option value="1">Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                                            <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                                </tr>
                                                @endfor
                                                <tr class="shipping-rate-item-last">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                                <option value="0" selected>(*)</option>
                                                                <option value="0.5">Per 0.5kg(*)</option>
                                                                <option value="1">Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[last][type]" value="last">
                                                            <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions">
                                                        <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            <?php $over = $shipping->getRateItems('over')->first(); ?>
                                            <tr>
                                                <td>
                                                    <div class="form-group text-right" id="over-last-item-0">
                                                        ><span class="over-text">{{ $over->weight }}</span>
                                                        <span class="weight-unit unit-weight">kg</span>
                                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[over][rate]" value="{{ $over->rate }}" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select">
                                                            <option value="0"{!! selected(0, $over->unit) !!}>(*)</option>
                                                            <option value="0.5"{!! selected(0.5, $over->unit) !!}>Per 0.5kg(*)</option>
                                                            <option value="1"{!! selected(1, $over->unit) !!}>Per kg(*)</option>
                                                        </select>
                                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

    <!-- Vận chuyển hàng không -->
    <!-- Ngoài nước -->
    <div id="shipping-rate-1-2">
        <?php
                            $deliveryType = 1; //Vận chuyển trong vòng 6 giờ
                            $weightList = array('0.05','0.1','0.25','0.5','1.0','1.5','2.0');
                        ?>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-sm-12">
                                <div class="row">
                                    <div class="shipping-delivery-box col-md-5">
                                        <div class="form-group">
                                            <select id="shipping-delivery" class="shipping-delivery form-control" name="delivery" data-show-icon="true">
                                                @if( $deliveries->count() > 0)
                                                    @foreach($deliveries as $delivery)
                                                        <option{!! selected($shipping->delivery_id, $delivery->id) !!} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select name="shippingMath" id="shipping-math" class="form-control">
                                                <option value="3000"{!! selected($shipping->math, 3000) !!}>Khối lượng thể tích = L x W x H x Số lượng/3000</option>
                                                <option value="5000"{!! selected($shipping->math, 5000) !!}>Khối lượng thể tích = L x W x H x Số lượng/5000</option>
                                                <option value="6000"{!! selected($shipping->math, 6000) !!}>Khối lượng thể tích = L x W x H x Số lượng/6000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="shipping-guidance col-md-5">
                                        <h3>{{ trans('front.guidance') }}</h3>
                                        <p>{{ trans('front.guidance-text-1') }}</p>
                                        <p>{{ trans('front.guidance-text-2') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 shipping-rate-box">
                                <div class="row">
                                    <div class="col-md-10">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.weight-to') }}</th>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.shipping-rate-fee') }}</th>
                                                <th style="width: 25%;text-align: center;text-decoration: underline;">{{ trans('front.unit') }}</th>
                                                <th style="width: 25%;"></th>
                                            </tr>
                                            <?php $min = $shipping->getRateItems('min')->first(); ?>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[first][weight]" placeholder="0.0" class="form-control shipping-weight-input" value="{{ $min->weight }}">
                                                            <div class="input-group-addon unit-weight">kg</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[first][rate]" placeholder="0.00" value="{{ $min->rate }}" class="form-control shipping-rate-input">
                                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="hidden" name="shippingRate[first][unit]" class="form-control shipping-unit-select">
                                                        <input type="hidden" name="shippingRate[first][type]" value="min">
                                                        <input type="hidden" name="shippingRate[first][rate_type]" value="weight">
                                                        <span class="require">(*)</span>
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if( $shipping->rates->count() )
                                                <?php $i=1; ?>
                                                @foreach($shipping->getRateItems() as $rate )
                                                    <tr id="shipping-rate-item-{{$i}}">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{ $rate->weight }}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" value="{{ $rate->rate }}" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                                <option value="0"{!! selected(0, $rate->unit) !!}>(*)</option>
                                                                <option value="0.5"{!! selected(0.5, $rate->unit) !!}>Per 0.5kg(*)</option>
                                                                <option value="1"{!! selected(1, $rate->unit) !!}>Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[{{$i}}][type]" value="{!! $rate->type !!}">
                                                            <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="{!! $rate->rate_type !!}">
                                                        </div>
                                                    </td>
                                                    <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                                </tr>
                                                @endforeach
                                                <?php $last = $shipping->getRateItems('last')->first(); ?>
                                                <tr class="shipping-rate-item-last">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{ $last->weight }}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][rate]" placeholder="0.00" value="{{ $last->rate }}" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                                <option value="0"{!! selected(0, $last->unit) !!}>(*)</option>
                                                                <option value="0.5"{!! selected(0.5, $last->unit) !!}>Per 0.5kg(*)</option>
                                                                <option value="1"{!! selected(1, $last->unit) !!}>Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[last][type]" value="last">
                                                            <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions">
                                                        <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                                    </td>
                                                </tr>
                                            @else
                                                @for($i=1;$i<(count($weightList)-1);$i++)
                                                <tr id="shipping-rate-item-{{$i}}">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][weight]" data-id="over-last-item-{{$i}}" placeholder="0.0" value="{{$weightList[$i]}}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[{{$i}}][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[{{$i}}][unit]" class="form-control last-unit">
                                                                <option value="0" selected>(*)</option>
                                                                <option value="0.5">Per 0.5kg(*)</option>
                                                                <option value="1">Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[{{$i}}][type]" value="item">
                                                            <input type="hidden" name="shippingRate[{{$i}}][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions"><button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-{{$i}}">Xóa</button></td>
                                                </tr>
                                                @endfor
                                                <tr class="shipping-rate-item-last">
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][weight]" data-id="over-last-item-0" placeholder="0.0" value="{{end($weightList)}}" class="form-control shipping-weight-input last-weight">
                                                                <div class="input-group-addon unit-weight">kg</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="shippingRate[last][rate]" placeholder="0.00" class="form-control shipping-rate-input last-rate">
                                                                <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="shippingRate[last][unit]" class="form-control last-unit">
                                                                <option value="0" selected>(*)</option>
                                                                <option value="0.5">Per 0.5kg(*)</option>
                                                                <option value="1">Per kg(*)</option>
                                                            </select>
                                                            <input type="hidden" name="shippingRate[last][type]" value="last">
                                                            <input type="hidden" name="shippingRate[last][rate_type]" value="weight">
                                                        </div>
                                                    </td>
                                                    <td class="actions">
                                                        <button class="btn btn-default add-more btn-add-more-shipping-rate">{{ trans('front.add') }}+</button>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            <?php $over = $shipping->getRateItems('over')->first(); ?>
                                            <tr>
                                                <td>
                                                    <div class="form-group text-right" id="over-last-item-0">
                                                        ><span class="over-text">{{ $over->weight }}</span>
                                                        <span class="weight-unit unit-weight">kg</span>
                                                        <input type="hidden" name="shippingRate[over][weight]" class="over-input">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="shippingRate[over][rate]" value="{{ $over->rate }}" placeholder="0.00" class="form-control shipping-rate-input over-rate">
                                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="shippingRate[over][unit]" class="form-control over-unit shipping-unit-select">
                                                            <option value="0"{!! selected(0, $over->unit) !!}>(*)</option>
                                                            <option value="0.5"{!! selected(0.5, $over->unit) !!}>Per 0.5kg(*)</option>
                                                            <option value="1"{!! selected(1, $over->unit) !!}>Per kg(*)</option>
                                                        </select>
                                                        <input type="hidden" name="shippingRate[over][type]" value="over">
                                                        <input type="hidden" name="shippingRate[over][rate_type]" value="weight">
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
    </div>
    <!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
    
</div>
<div class="container-fluid container-create">
    {!! Form::open(array('url' => 'shipping/edit/'.$shipping->id,  'method' => 'POST', 'id' => 'post-shipping', 'role'=>'form','files' => true, 'class'=> 'form')) !!}
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="title">{{ trans('front.upload-shipping') }}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2>1. {{ trans('front.shipping-services') }}</h2>
            </div>
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="">
                    <div class="shipping-service-box">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>{{ trans('front.delivery-service') }} <span class="red-color">(*)</span></h4>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group shipping-services{{ $errors->has('service') ? ' has-error' : '' }}">
                                            <select id="shipping-service" class="shipping-service" name="service" data-show-icon="true" required>
                                                <!-- <option value="" data-content="{{ trans('front.all-services') }} <img src='{!! Theme::asset()->url('img/icon-service-1.png') !!}' class='pull-right' />">{{ trans('front.all-services') }}</option> -->
                                                @if( $services )
                                                    @foreach( $services as $service )
                                                        <option value="{{ $service->id }}" {!! selected($service->id, $shipping->service_id) !!} data-servicetype="{{ $service->listType() }}" data-content="{{ $service->translation($currentLanguage->id, 'name') }} <img src='{{ image_url($service->image) }}' class='pull-right' />">{{ $service->translation($currentLanguage->id, 'name') }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('service'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('service') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <!-- <div class="form-group text-center">
                                            <img src="{!! Theme::asset()->url('img/form-arrow-right.png') !!}" class="img-responsive" alt="" />
                                        </div> -->
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group shipping-service-type{{ $errors->has('serviceType') ? ' has-error' : '' }}">
                                            <select name="serviceType" id="select-service-type" class="form-control select-service-type">
                                                @if( $serviceTypes )
                                                    @foreach( $serviceTypes as $serviceType )
                                                        <option value="{{ $serviceType->id }}" id="service-id-{{ $serviceType->id }}" data-type="{{ $serviceType->type }}">{{ $serviceType->translation($currentLanguage->id, 'name') }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('serviceType'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('serviceType') }}</strong>
                                                </span>
                                            @endif
                                            <div class="after-service-type">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>    
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="from-to-box">
                @if( $shipping->ways->count())
                    <?php $i=0; ?>
                    @foreach($shipping->ways as $way)
                    <div class="from-to-box-item" id="from-to-box-item-{{ $i }}" data-id="{{ $i }}">
                        <div class="col-md-10 col-md-offset-1 col-sm-12">
                            <div class="row">
                                <div class="shipping-service-box col-md-5">
                                    <h4>{{ trans('front.from') }}</h4>
                                    <div class="form-group">
                                        <input type="hidden" name="address[{{ $i }}][from_long]" id="address-from-long-0" value="{{ $way->from_long }}">
                                        <input type="hidden" name="address[{{ $i }}][from_lat]" id="address-from-lat-0" value="{{ $way->from_lat }}">
                                        <input type="hidden" name="address[{{ $i }}][from_location]" id="address-from-location-0" value="{{ $way->from_location }}">
                                        <input type="text" name="address[{{ $i }}][from]" id="address-from-{{ $i }}" required value="{{ $way->from }}" placeholder="Ex: Hồ Chí Minh city, Vietnam…" class="form-control google-maps-input">
                                    </div>
                                    <label id="address-from-{{ $i }}-error" class="error" for="address-from-{{ $i }}"></label>
                                </div>
                                <div class="col-md-1">
                                    <h4>&nbsp;</h4>
                                    <div class="form-group text-center">
                                        <img src="{!! Theme::asset()->url('img/form-arrow-left-right.png') !!}" class="img-responsive" alt="" />
                                    </div>
                                </div>
                                <div class="shipping-service-types col-md-5">
                                    <div class="row">
                                        <div class="col-md-8"><h4>{{ trans('front.to') }}</h4></div>
                                        <div class="col-md-4 text-right"><h4>{{ trans('front.two-direct') }}</h4></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group from-and-to">
                                            <input type="hidden" name="address[{{ $i }}][to_long]" id="address-to-long-{{ $i }}" value="{{ $way->to_long }}">
                                            <input type="hidden" name="address[{{ $i }}][to_lat]" id="address-to-lat-{{ $i }}" value="{{ $way->to_lat }}">
                                            <input type="hidden" name="address[{{ $i }}][to_location]" id="address-to-location-{{ $i }}" value="{{ $way->to_location }}">
                                            <input type="text" class="form-control google-maps-input" required id="address-to-{{ $i }}" value="{{ $way->to }}" name="address[{{ $i }}][to]" placeholder="Ex: Ha Noi city, Vietnam…" />
                                            <div class="input-group-addon">
                                                <label>
                                                    <input type="checkbox" class="form-control"{!! checked(1, $way->two_way)!!} name="address[{{ $i }}][two_way]" value="1" checked />
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <label id="address-to-{{ $i }}-error" class="error" for="address-to-{{ $i }}"></label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <h4 class="remove-btn">&nbsp;</h4>
                                    <a href="" class="btn btn-default add-more add-more-shipping-address">{{ trans('front.add') }}+</a>
                                </div>
                            </div>

                            <!---More file for specail-->
                            <div class="row more-field hidden">
                                <div class="shipping-service-box col-md-5">
                                    <h4>{{ trans('front.number-trip') }}</h4>
                                    <div class="form-group">
                                        <input type="text" name="address[0][trip]" placeholder="" value="Many" class="form-control bg-yellow">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <!-- <h4>&nbsp;</h4>
                                    <div class="form-group text-center">
                                        <img src="{!! Theme::asset()->url('img/form-arrow-left-right.png') !!}" class="img-responsive" alt="" />
                                    </div> -->
                                </div>
                                <div class="shipping-service-types col-md-5">
                                    <div class="row">
                                        <div class="col-md-12"><h4>{{ trans('front.date-go') }}</h4></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="from-and-to">
                                            <select name="address[0][dateGo]" class="form-control bg-yellow" placeholder="">
                                                <option value="">Mổi ngày</option>
                                                <option value="">2 Ngày/Tuần</option>
                                                <option value="">3 Ngày/Tuần</option>
                                                <option value="" class="" id=""></option>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                </div>
                            </div>
                        </div>
                    </div><!-- END #from-to-box-item-0 -->
                    @endforeach
                @else
                <div class="from-to-box-item" id="from-to-box-item-0" data-id="0">
                    <div class="col-md-10 col-md-offset-1 col-sm-12">
                        <div class="row">
                            <div class="shipping-service-box col-md-5">
                                <h4>{{ trans('front.from') }}</h4>
                                <div class="form-group">
                                    <input type="hidden" name="address[0][from_long]" id="address-from-long-0" value="0">
                                    <input type="hidden" name="address[0][from_lat]" id="address-from-lat-0" value="0">
                                    <input type="hidden" name="address[0][from_location]" id="address-from-location-0" value="0">
                                    <input type="text" name="address[0][from]" id="address-from-0" required placeholder="Ex: Hồ Chí Minh city, Vietnam…" class="form-control google-maps-input">
                                </div>
                                <label id="address-from-0-error" class="error" for="address-from-0"></label>
                            </div>
                            <div class="col-md-1">
                                <h4>&nbsp;</h4>
                                <div class="form-group text-center">
                                    <img src="{!! Theme::asset()->url('img/form-arrow-left-right.png') !!}" class="img-responsive" alt="" />
                                </div>
                            </div>
                            <div class="shipping-service-types col-md-5">
                                <div class="row">
                                    <div class="col-md-8"><h4>{{ trans('front.to') }}</h4></div>
                                    <div class="col-md-4 text-right"><h4>{{ trans('front.two-direct') }}</h4></div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group from-and-to">
                                        <input type="hidden" name="address[0][to_long]" id="address-to-long-0" value="0">
                                        <input type="hidden" name="address[0][to_lat]" id="address-to-lat-0" value="0">
                                        <input type="hidden" name="address[0][to_location]" id="address-to-location-0" value="0">
                                        <input type="text" class="form-control google-maps-input" required id="address-to-0" name="address[0][to]" placeholder="Ex: Ha Noi city, Vietnam…" />
                                        <div class="input-group-addon">
                                            <label>
                                                <input type="checkbox" class="form-control" name="address[0][two_way]" value="1" checked />
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <label id="address-to-0-error" class="error" for="address-to-0"></label>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <h4 class="remove-btn">&nbsp;</h4>
                                <a href="" class="btn btn-default add-more add-more-shipping-address">{{ trans('front.add') }}+</a>
                            </div>
                        </div>

                        <!---More file for specail-->
                        <div class="row more-field hidden">
                            <div class="shipping-service-box col-md-5">
                                <h4>{{ trans('front.number-trip') }}</h4>
                                <div class="form-group">
                                    <input type="text" name="address[0][trip]" placeholder="" value="Many" class="form-control bg-yellow">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <!-- <h4>&nbsp;</h4>
                                <div class="form-group text-center">
                                    <img src="{!! Theme::asset()->url('img/form-arrow-left-right.png') !!}" class="img-responsive" alt="" />
                                </div> -->
                            </div>
                            <div class="shipping-service-types col-md-5">
                                <div class="row">
                                    <div class="col-md-12"><h4>{{ trans('front.date-go') }}</h4></div>
                                </div>
                                <div class="form-group">
                                    <div class="from-and-to">
                                        <select name="address[0][dateGo]" class="form-control bg-yellow" placeholder="">
                                            <option value="">Mổi ngày</option>
                                            <option value="">2 Ngày/Tuần</option>
                                            <option value="">3 Ngày/Tuần</option>
                                            <option value="" class="" id=""></option>

                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </div>
                </div><!-- END #from-to-box-item-0 -->
                @endif

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2>2. {{ trans('front.shipping-rate') }}</h2>
            </div>
            <!-- <div class="col-md-5 col-sm-12">
                <h4 class="text-center">{{ trans('front.shipping-delivery') }}</h4>
            </div> -->
        </div>
        <div id="shipping-rate-full-content">
            <!-- FULL CONTENT GET FROM HIDDEN -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <h2>3. {{ trans('front.surcharge') }}</h2>
            </div>
            <div class="col-md-10 col-md-offset-1 surcharge">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table col-4">
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="vatSurcharge">VAT <span class="require">(*)</span></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="vatSurcharge" id="vatSurcharge" class="form-control" placeholder="0.00" />
                                            <div class="input-group-addon">%</div>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                                <td rowspan="3">
                                    {!!  trans('front.total-shipping-cost') !!}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="fuelSurcharge">{{ trans('front.fuelSurcharge') }}</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="fuelSurcharge" id="fuelSurcharge" value="{{ $shipping->fuel_surcharge }}" class="form-control" placeholder="0.00" />
                                            <div class="input-group-addon">%</div>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="fragileSurcharge">{{ trans('front.fragileSurcharge') }}</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="fragileSurcharge" id="fragileSurcharge" value="{{ $shipping->fragilesurcharge }}" class="form-control" placeholder="0.00">
                                            <div class="input-group-addon">%</div>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="loadingUnloadingSurcharge">{{ trans('front.loadingUnloadingSurcharge') }}</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="loadingUnloadingSurcharge" value="{{ $shipping->loading_unloading_surcharge }}" id="loadingUnloadingSurcharge" class="form-control" placeholder="0.00">
                                            <div class="input-group-addon unit-currency">{!! $currentCurrency->symbol !!}</div>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <div class="col-md-3 col-md-offset-1 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="fuelSurcharge">{{ trans('front.fuelSurcharge') }} <span class="require">(*)</span></label>
                </div>
                <div class="form-group">
                    <label for="fragileSurcharge">{{ trans('front.fragileSurcharge') }} <span class="require">(*)</span></label>
                </div>
                <div class="form-group">
                    <label for="loadingUnloadingSurcharge">{{ trans('front.loadingUnloadingSurcharge') }} <span class="require">(*)</span></label>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="fuelSurcharge" id="fuelSurcharge" class="form-control" placeholder="0.00" />
                        <div class="input-group-addon">%</div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="fragileSurcharge" id="fragileSurcharge" class="form-control" placeholder="0.00">
                        <div class="input-group-addon">%</div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="loadingUnloadingSurcharge" id="loadingUnloadingSurcharge" class="form-control" placeholder="0.00">
                        <div class="input-group-addon unit-currency">US$</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                {!!  trans('front.total-shipping-cost') !!}
            </div> -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2>4. {!! trans('front.package-requirement') !!}</h2>
            </div>
            <div class="col-md-10 col-md-offset-1 surcharge">
                <div class="row">
                    <div class="col-md-10">
                        <table class="table col-4">
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="maxLength">Chiều dài lớn nhất</label>
                                        <input type="text" name="maxLength" id="maxLength" value="{{ $shipping->max_length }}" class="form-control" placeholder="Không yêu cầu" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="maxWidth">Chiều rộng lớn nhất</label>
                                        <input type="text" name="maxWidth" id="maxWidth" value="{{ $shipping->max_width }}" class="form-control" placeholder="Không yêu cầu" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="maxHeight">Chiều cao lớn nhất</label>
                                        <input type="text" name="maxHeight" id="maxHeight" value="{{ $shipping->max_height }}" class="form-control" placeholder="Không yêu cầu" />
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="minWeight">Khối lượng tối thiểu</label>
                                        <input type="text" name="minWeight" id="minWeight" value="{{ $shipping->min_weight }}" class="form-control" placeholder="Không yêu cầu" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="maxWeight">Khối lượng tối đa</label>
                                        <input type="text" name="maxWeight" id="maxWeight" value="{{ $shipping->max_weight }}" class="form-control" placeholder="Không yêu cầu" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="maxValue">Giá trị gói hàng không quá</label>
                                        <input type="text" name="maxValue" id="maxValue" value="{{ $shipping->max_value }}" class="form-control" placeholder="Không yêu cầu" />
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="packageDeliveryDate">Thời gian chuyển hàng</label>
                                        <select name="packageDeliveryDate" id="packageDeliveryDate" class="form-control">
                                            <option value="24/24">24/24 giờ</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="packageDeliveryDate">Thời gian giao hàng</label>
                                        <select name="packageDeliveryDate" id="packageDeliveryDate" class="form-control">
                                            <option value="24/24">24/24 giờ</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="package-type">
                                            <img src="{!! Theme::asset()->url('img/door-to-door.png') !!}" alt="">
                                            <p>{{ trans('front.door-to-door') }}</p>
                                            <label>
                                                <input type="checkbox"{!! checked(1, $shipping->min_weight) !!} name="doorToDoor" value="1">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-md-3 col-md-offset-1 col-sm-5 col-xs-12">
                <div class="form-group">
                    <label for="maxLength">Chiều dài lớn nhất</label>
                    <input type="text" name="maxLength" id="maxLength" class="form-control" placeholder="Không yêu cầu" />
                </div>
                <div class="form-group">
                    <label for="minWeight">Khối lượng tối thiểu</label>
                    <input type="text" name="minWeight" id="minWeight" class="form-control" placeholder="Không yêu cầu" />
                </div>
                <div class="form-group">
                    <label for="packageDeliveryDate">Thời gian chuyển hàng</label>
                    <select name="packageDeliveryDate" id="packageDeliveryDate" class="form-control">
                        <option value="24/24">24/24 giờ</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3 col-sm-5 col-xs-12">
                <div class="form-group">
                    <label for="maxWidth">Chiều rộng lớn nhất</label>
                    <input type="text" name="maxWidth" id="maxWidth" class="form-control" placeholder="Không yêu cầu" />
                </div>
                <div class="form-group">
                    <label for="maxWeight">Khối lượng tối đa</label>
                    <input type="text" name="maxWeight" id="maxWeight" class="form-control" placeholder="Không yêu cầu" />
                </div>
                <div class="form-group">
                    <label for="packageDeliveryDate">Thời gian giao hàng</label>
                    <select name="packageDeliveryDate" id="packageDeliveryDate" class="form-control">
                        <option value="24/24">24/24 giờ</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3 col-sm-5 col-xs-12">
                <div class="form-group">
                    <label for="maxHeight">Chiều cao lớn nhất</label>
                    <input type="text" name="maxHeight" id="maxHeight" class="form-control" placeholder="Không yêu cầu" />
                </div>
                <div class="form-group">
                    <label for="maxValue">Giá trị gói hàng không quá</label>
                    <input type="text" name="maxValue" id="maxValue" class="form-control" placeholder="Không yêu cầu" />
                </div>
                <div class="form-group">
                    <div class="checkbox-box">
                        <div class="checkbox-img"><img src="{!! Theme::asset()->url('img/door-to-door.png') !!}" /></div>
                        <div class="checkbox-label">{{ trans('front.door-to-door') }}</div>
                        <div class="checkbox-input">
                            <label>
                                <input type="checkbox" name="doorToDoor" value="1">
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- <div class="row">
            <div class="col-lg-12">
                <h2>6. {{ trans('front.guarantee-policy') }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="form-group">
                    <textarea name="guarantee_policy" id="shipping-content" class="form-control">{{ Auth()->user()->guarantee_policy }}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2>  {{ trans('front.requirement') }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="form-group">
                    <textarea name="requirement" id="shipping-requirement" class="form-control"></textarea>
                </div>
            </div>
            <div class="col-lg-10 col-lg-offset-1">
                <div class="form-group">
                    <label for="shipping-image">Image</label>
                    <input type="file" name="shipping_image" id="shipping-image" class="form-control" />
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12 text-center btn-groups">
                <input type="submit" class="btn btn-cancel" value="{{ trans('front.cancel') }}">
                <input type="submit" class="btn btn-preview" value="{{ trans('front.preview') }}">
                <input type="submit" class="btn btn-upload" value="{{ trans('front.upload') }}">
            </div>
        </div>
    {!! Form::close() !!}
</div>
<div id="map" style="display: none;"></div>
<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#shipping-service').selectpicker();
    $("#post-shipping").validate();
    $('.form-group.att-icon .glyphicon').click(function(){
        var id = $(this).attr('data-id');
        $("#"+id).click();
    });
    $('.form-group.att-icon input[type="file"]').change(function(e){
        var id = $(this).attr('id');

        $.each(event.target.files, function(key, value){
            console.log(value.name);
            $("#"+id+"-text").val(value.name);
        });
    });
});
</script>