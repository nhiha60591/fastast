<script type="text/javascript">
    $(function () {
        $('#datepicker').datetimepicker();
    });
</script>
{!! Form::open(array('url' => 'find-shipping',  'method' => 'GET', 'id' => 'search-shipping', 'role'=>'form', 'class'=> 'form')) !!}
<div class="find-package find-form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="form-inline find-package-form">
                    @if( $services->count() > 0)
                        <?php $serviceChecked = isset($_REQUEST['service']) && !empty($_REQUEST['service']) && $_REQUEST['service'] != '0' ? $_REQUEST['service'] : 1; ?>
                        @foreach($services as $service)
                            <div class="form-group">
                                <label>
                                    <input type="radio" value="{{ $service->id }}" name="service" {{ checked($service->id, $serviceChecked) }} data-type="{{ $service->listType('type') }}" />
                                    <span class="icon">
                                        <img src="{!! image_url($service->image) !!}" alt="{{$service->name}}" class="service-item">
                                    </span>
                                </label>
                            </div>
                        @endforeach
                            <div class="form-group all-package">
                                <label>All packages
                                    <input type="radio" value=""  name="all-package" placeholder=""  data-type="" />
                                </label>
                            </div>
                    @endif

                    <div class="form-group price">
                        <?php $shipperTime = isset($_REQUEST['delivery']) && !empty($_REQUEST['delivery']) && $_REQUEST['delivery'] != '0' ? $_REQUEST['delivery'] : 1; ?>
                        <select name="delivery" class="form-control select-arrow">
                            <option value="0"> All Sheeping Time</option>

                            @if( $deliveries->count() > 0)
                                <?php
                                    $array=array('delivery','Delivery','Vận Chuyển', 'vận chuyển', 'Vận chuyển', trans('front.delivery'));

                                ?>
                                @foreach($deliveries as $delivery)
                                    <?php var_dump($array);?>
                                    <option value="{{ $delivery->id }}"{{ selected($delivery->id, $shipperTime) }}>{{ str_replace($array, '', $delivery->translation($currentLanguage->id, 'name')) }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <div class='input-group date' id=''>
                            <input type="date" class="form-control" id="datepicker"  style="width: 165px;" value="{{ isset($_REQUEST['shipperDate']) && !empty($_REQUEST['shipperDate']) ? date('Y-m-d', strtotime( $_REQUEST['shipperDate'] ) ) : '' }}" name="shipperDate" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <select name="shipperRelevance" class="form-control" style="width: 165px;">
                            <option value="0">Price: Relevance</option>
                            <option value="1">Price low to  high</option>
                            <option value="2">Price high to low</option>
                        </select>
                    </div>
                    <div class="form-group pull-right btn-transfer">
                        <a class="btn btn-default find-package-btn" href="{{ url('find-package') }}">Find Package</a>
                        <img src="{!! Theme::asset()->url('img/find-package-button.png') !!}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="list-shipping-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 col-sm-12 left">
                <div class="form-horizontal">
                    <div class="search-type-service"></div>
                    <div class="tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="search-type-item search-per-weight active"><a href="#per-weight" aria-controls="per-weight" role="tab" data-toggle="tab">Rate per weight</a></li>
                            <li role="presentation" class="search-type-item search-per-trip"><a href="#per-trip" aria-controls="per-trip" role="tab" data-toggle="tab">Rate per trip</a></li>
                            <li role="presentation" class="search-type-item search-per-cbmlcl"><a href="#per-cbmlcl" aria-controls="per-cbmlcl" role="tab" data-toggle="tab">Rate per CBM(LCL)</a></li>
                            <li role="presentation" class="search-type-item search-per-cbmfcl"><a href="#per-cbmfcl" aria-controls="per-cbmfcl" role="tab" data-toggle="tab">Rate per Container(FLC)</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="per-weight">
                                @if(isset($packageSize['KLTT']) && sizeof($packageSize['KLTT']) > 0)
                                    @foreach($packageSize['KLTT'] as $num => $data)
                                        <div class="package-size-per-weight" id="package-size-per-weigt-{{ $num }}">
                                            @if( $num <= 0)
                                            <div class="form-group top-title">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    Width(cm)
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    Length(cm)
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    Height(cm)
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            Quantity
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    
                                                </div>
                                            </div>
                                            @endif
                                            <div class="form-group">
                                            @if( $num <= 0)
                                                <div class="col-sm-2 package-title">
                                                    <label class="label-control">{{ trans('front.package-size') }}</label>
                                                </div>
                                            @else
                                                <div class="col-sm-2 package-title"></div>
                                            @endif
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" value="{{ $data['width'] or '' }}" name="packageSize[KLTT][{{ $num }}][width]" >
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" value="{{ $data['length'] or '' }}" name="packageSize[KLTT][{{ $num }}][length]" >
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" value="{{ $data['height'] or '' }}" name="packageSize[KLTT][{{ $num }}][height]" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="text" class="form-control" value="{{ $data['quantity']>0?$data['quantity']:1 }}" name="packageSize[KLTT][{{ $num }}][quantity]" >
                                                        </div>
                                                    </div>
                                                </div>
                                                @if( $num <= 0)
                                                <div class="col-sm-2">
                                                    <span class="removebtn"></span>
                                                    <button class="btn btn-default add-more add-more-package-size"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                                </div>
                                                @else
                                                <div class="col-sm-2">
                                                    <a href="" class="btn remove-btn add-more btn-default remove-package-size" data-target="#package-size-per-weigt-{{ $num }}"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="package-size-per-weight" id="package-size-per-weigt-0">
                                        <div class="form-group top-title">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-8">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                Width(cm)
                                                            </div>
                                                            <div class="col-sm-4">
                                                                Length(cm)
                                                            </div>
                                                            <div class="col-sm-4">
                                                                Height(cm)
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        Quantity
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 package-title">
                                                <label class="label-control">{{ trans('front.package-size') }}</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control width" value="{{ $packageSize['KLTT'][0]['width'] or '' }}" name="packageSize[KLTT][0][width]">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control length" value="{{ $packageSize['KLTT'][0]['length'] or '' }}" name="packageSize[KLTT][0][length]" >
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control height" value="{{ $packageSize['KLTT'][0]['height'] or '' }}" name="packageSize[KLTT][0][height]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control quantity" value="{{ $packageSize['KLTT'][0]['quantity'] or 1 }}" name="packageSize[KLTT][0][quantity]">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <span class="removebtn"></span>
                                                <button class="btn btn-default add-more add-more-package-size"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <input type="hidden" id="currentNumber" value="{{ sizeof(@$packageSize['KLTT']) }}">
                                <div class="package-size-per-weight-box"></div>
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label for="">
                                            {{ trans('front.total-gros-weight') }}
                                        </label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ isset($packageSize['totalCost']) && !empty($packageSize['totalCost']) ? $packageSize['totalCost'] : '' }}" id="inputCost" name="packageSize[totalCost]" />
                                    </div>
                                    <div class="col-sm-2" style="margin-top:7px;">
                                        kg
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane per-trip" id="per-trip">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label for="">{{ trans('front.load-capacity') }}</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="hidden">
                                                    <select class="form-control packageSize" id="packageSize-3">
                                                        <option value="500"{{ selected(500, @$packageSize['trip']['loadCapacity']) }}>500 kilogram</option>
                                                        <option value="750"{{ selected(750, @$packageSize['trip']['loadCapacity']) }}>750 kilogram</option>
                                                        <option value="1000"{{ selected(1000, @$packageSize['trip']['loadCapacity']) }}>1.0 ton</option>
                                                        <option value="1250"{{ selected(1250, @$packageSize['trip']['loadCapacity']) }}>1.25 ton</option>
                                                        <option value="1500"{{ selected(1500, @$packageSize['trip']['loadCapacity']) }}>1.5 ton</option>
                                                        <option value="2000"{{ selected(2000, @$packageSize['trip']['loadCapacity']) }}>2.0 ton</option>
                                                        <option value="2500"{{ selected(2500, @$packageSize['trip']['loadCapacity']) }}>2.5 ton</option>
                                                        <option value="3000"{{ selected(3000, @$packageSize['trip']['loadCapacity']) }}>3.0 ton</option>
                                                        <option value="5000"{{ selected(5000, @$packageSize['trip']['loadCapacity']) }}>5.0 ton</option>
                                                        <option value="6500"{{ selected(6500, @$packageSize['trip']['loadCapacity']) }}>6.5 ton</option>
                                                    </select>
                                                    <select class="form-control packageSize" id="packageSize-4">
                                                        <option value="500"{{ selected(500, @$packageSize['trip']['loadCapacity']) }}>500 kilogram</option>
                                                        <option value="750"{{ selected(750, @$packageSize['trip']['loadCapacity']) }}>750 kilogram</option>
                                                        <option value="1000"{{ selected(1000, @$packageSize['trip']['loadCapacity']) }}>1.0 ton</option>
                                                        <option value="1250"{{ selected(1250, @$packageSize['trip']['loadCapacity']) }}>1.25 ton</option>
                                                        <option value="1500"{{ selected(1500, @$packageSize['trip']['loadCapacity']) }}>1.5 ton</option>
                                                    </select>
                                                    <select class="form-control packageSize" id="packageSize-5">
                                                        <option value="500"{{ selected(500, @$packageSize['trip']['loadCapacity']) }}>500 kilogram</option>
                                                        <option value="750"{{ selected(750, @$packageSize['trip']['loadCapacity']) }}>750 kilogram</option>
                                                        <option value="1000"{{ selected(1000, @$packageSize['trip']['loadCapacity']) }}>1.0 ton</option>
                                                        <option value="1250"{{ selected(1250, @$packageSize['trip']['loadCapacity']) }}>1.25 ton</option>
                                                        <option value="1500"{{ selected(1500, @$packageSize['trip']['loadCapacity']) }}>1.5 ton</option>
                                                        <option value="2000"{{ selected(2000, @$packageSize['trip']['loadCapacity']) }}>2.0 ton</option>
                                                        <option value="2500"{{ selected(2500, @$packageSize['trip']['loadCapacity']) }}>2.5 ton</option>
                                                        <option value="3000"{{ selected(3000, @$packageSize['trip']['loadCapacity']) }}>3.0 ton</option>
                                                        <option value="5000"{{ selected(5000, @$packageSize['trip']['loadCapacity']) }}>5.0 ton</option>
                                                        <option value="6500"{{ selected(6500, @$packageSize['trip']['loadCapacity']) }}>6.5 ton</option>
                                                    </select>
                                                    <select class="form-control packageSize" id="packageSize-6">
                                                        <option value="20"{{ selected(20, @$packageSize['trip']['loadCapacity']) }}>20DC</option>
                                                        <option value="40"{{ selected(40, @$packageSize['trip']['loadCapacity']) }}>40DC</option>
                                                        <option value="40"{{ selected(40, @$packageSize['trip']['loadCapacity']) }}>40HC</option>
                                                        <option value="8"{{ selected(8, @$packageSize['trip']['loadCapacity']) }}>8 ton</option>
                                                        <option value="9-10"{{ selected(9-10, @$packageSize['trip']['loadCapacity']) }}>9-10 ton</option>
                                                        <option value="12-15"{{ selected(12-15, @$packageSize['trip']['loadCapacity']) }}>12-15 ton</option>
                                                        <option value="15-17"{{ selected(15-17, @$packageSize['trip']['loadCapacity']) }}>15-17 ton</option>
                                                        <option value="17-20"{{ selected(17-20, @$packageSize['trip']['loadCapacity']) }}>17-20 ton</option>
                                                        <option value="20-25"{{ selected(20-25, @$packageSize['trip']['loadCapacity']) }}>20-25 ton</option>
                                                        <option value="25-30"{{ selected(25-30, @$packageSize['trip']['loadCapacity']) }}>25-30 ton</option>
                                                        <option value="30"{{ selected(30, @$packageSize['trip']['loadCapacity']) }}>> 30 ton</option>
                                                    </select>
                                                </div>
                                                <select name="packageSize[trip][loadCapacity]" class="form-control packageSize" id="packageSize"></select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="">{{ trans('front.number-of-trip') }}</label>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" value="{{ isset($packageSize['trip']['numberTrip']) && !empty($packageSize['trip']['numberTrip']) ? $packageSize['trip']['numberTrip'] : 1 }}" name="packageSize[trip][numberTrip]" placeholder="1">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2"></div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane active" id="per-cbmlcl">
                                @if(isset($packageSize['KLTT']) && sizeof($packageSize['KLTT']) > 0)
                                    @foreach($packageSize['KLTT'] as $num => $data)
                                        <div class="package-size-per-weight" id="package-size-per-weigt-{{ $num }}">
                                            @if( $num <= 0)
                                            <div class="form-group top-title">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    Width(cm)
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    Length(cm)
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    Height(cm)
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            Quantity
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    
                                                </div>
                                            </div>
                                            @endif
                                            <div class="form-group">
                                            @if( $num <= 0)
                                                <div class="col-sm-2 package-title">
                                                    <label class="label-control">{{ trans('front.package-size') }}</label>
                                                </div>
                                            @else
                                                <div class="col-sm-2 package-title"></div>
                                            @endif
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" value="{{ $data['width'] or '' }}" name="packageSize[KLTT][{{ $num }}][width]" >
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" value="{{ $data['length'] or '' }}" name="packageSize[KLTT][{{ $num }}][length]" >
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="text" class="form-control" value="{{ $data['height'] or '' }}" name="packageSize[KLTT][{{ $num }}][height]" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="text" class="form-control" value="{{ $data['quantity']>0?$data['quantity']:1 }}" name="packageSize[KLTT][{{ $num }}][quantity]" >
                                                        </div>
                                                    </div>
                                                </div>
                                                @if( $num <= 0)
                                                <div class="col-sm-2">
                                                    <span class="removebtn"></span>
                                                    <button class="btn btn-default add-more add-more-package-size"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                                </div>
                                                @else
                                                <div class="col-sm-2">
                                                    <a href="" class="btn remove-btn add-more btn-default remove-package-size" data-target="#package-size-per-weigt-{{ $num }}"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="package-size-per-weight" id="package-size-per-weigt-0">
                                        <div class="form-group top-title">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-8">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                Width(cm)
                                                            </div>
                                                            <div class="col-sm-4">
                                                                Length(cm)
                                                            </div>
                                                            <div class="col-sm-4">
                                                                Height(cm)
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        Quantity
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 package-title">
                                                <label class="label-control">{{ trans('front.package-size') }}</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control width" value="{{ $packageSize['KLTT'][0]['width'] or '' }}" name="packageSize[KLTT][0][width]">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control length" value="{{ $packageSize['KLTT'][0]['length'] or '' }}" name="packageSize[KLTT][0][length]" >
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control height" value="{{ $packageSize['KLTT'][0]['height'] or '' }}" name="packageSize[KLTT][0][height]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control quantity" value="{{ $packageSize['KLTT'][0]['quantity'] or 1 }}" name="packageSize[KLTT][0][quantity]">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <span class="removebtn"></span>
                                                <button class="btn btn-default add-more add-more-package-size"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <input type="hidden" id="currentNumber" value="{{ sizeof(@$packageSize['KLTT']) }}">
                                <div class="package-size-per-weight-box"></div>
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label for="">
                                            {{ trans('front.total-gros-weight') }}
                                        </label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" value="{{ isset($packageSize['totalCost']) && !empty($packageSize['totalCost']) ? $packageSize['totalCost'] : '' }}" id="inputCost" name="packageSize[totalCost]" />
                                    </div>
                                    <div class="col-sm-2" style="margin-top:7px;">
                                        kg
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane per-cbmfcl" id="per-cbmfcl">
                                <div class="row">
                                    <div class="col-sm-2 col-sm-offset-9 text-center">
                                        Quantity
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="">{{ trans('front.container-size') }}</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="container-size label-control">
                                            <input type="radio"{{ checked('20DC', @$packageSize['container']['loadCapacity']) }} name="packageSize[container][loadCapacity]" value="20DC">
                                            <span>20'DC</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="container-size label-control">
                                            <input type="radio"{{ checked('40DC', @$packageSize['container']['loadCapacity']) }} name="packageSize[container][loadCapacity]" value="40DC">
                                            <span>40'DC</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="container-size label-control">
                                            <input type="radio"{{ checked('40HC', @$packageSize['container']['loadCapacity']) }} name="packageSize[container][loadCapacity]" value="40HC">
                                            <span>40'HC</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" style="text-align: center;" class="form-control" value="{{ isset($packageSize['container']['numberTrip']) && !empty($packageSize['container']['numberTrip']) ? $packageSize['trip']['numberTrip'] : 1 }}" name="packageSize[container][numberTrip]" placeholder="1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="searchType" value="{{ isset($request['searchType']) && !empty($request['searchType']) ? $request['searchType'] : 'per-weight' }}">
                    <div class="form-group">
                        <?php
                            $packageType = isset($request['packageType']) && is_array( $request['packageType'] ) ? $request['packageType'] : array();
                        ?>
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <div class="col-sm-4" style="margin-left: -9px;">
                                    <div class="package-type">
                                        <img src="{!! Theme::asset()->url('img/door-to-door.png') !!}" alt="">
                                        <p>{!! trans('front.door-to-door') !!}</p>
                                        <label>
                                            <input type="checkbox"{{ checked(in_array('door-to-door', $packageType), true) }} checked name="packageType[]" value="door-to-door">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="package-type">
                                        <img src="{!! Theme::asset()->url('img/fragile-package.png') !!}" alt="">
                                        <p>{!! trans('front.fragile-package') !!}</p>
                                        <label>
                                            <input type="checkbox"{{ checked(in_array('fragile-package', $packageType), true) }} name="packageType[]" value="fragile-package">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="package-type">
                                        <img src="{!! Theme::asset()->url('img/loading-unloading.png') !!}" alt="">
                                        <p>{!! trans('front.loading-unloading') !!}</p>
                                        <label>
                                            <input type="checkbox"{{ checked(in_array('loading-unloading', $packageType), true) }} name="packageType[]" value="loading-unloading">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="collapse" id="collapseAdvancedSearch">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="form-group">
                                    <select name="shipperType" class="form-control">
                                        <option value="0">All Shipper</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" class="btn btn-default find-shipper" value="Find Shipper">
                        <!-- <a role="button" data-toggle="collapse" href="#collapseAdvancedSearch" aria-expanded="false" class="btn pull-right" aria-controls="collapseAdvancedSearch">More options >></a> -->
                    </div>
                    <div class="way-inputs">
                        <div class="more-request">
                            <div class="hidden">
                                <input type="text" class="from form-control" id="maps-from" value="{{ isset($_REQUEST['from']) && !empty( $_REQUEST['from'] ) ? $_REQUEST['from'] : '' }}">
                                <input type="text" class="to form-control" id="maps-to" value="{{ isset($_REQUEST['to']) && !empty( $_REQUEST['to'] ) ? $_REQUEST['to'] : '' }}">
                            </div>

                            <input type="hidden" name="shipperFromLat" id="shipperFromLat" class="form-control" value="{{ isset($_REQUEST['shipperFromLat']) && !empty( $_REQUEST['shipperFromLat'] ) ? $_REQUEST['shipperFromLat'] : '0' }}">
                            <input type="hidden" name="shipperFromLng" id="shipperFromLng" class="form-control" value="{{ isset($_REQUEST['shipperFromLng']) && !empty( $_REQUEST['shipperFromLng'] ) ? $_REQUEST['shipperFromLng'] : '0' }}">
                            
                            <!-- <input type="hidden" name="service" class="form-control" id="more-input-service" value="{{ isset($_REQUEST['service']) && !empty( $_REQUEST['service'] ) ? $_REQUEST['service'] : '' }}"> -->
                            <!-- <input type="hidden" name="delivery" class="form-control" id="more-input-delivery" value="{{ isset($_REQUEST['delivery']) && !empty( $_REQUEST['delivery'] ) ? $_REQUEST['delivery'] : '' }}"> -->
                        </div>
                    </div>
                </div>
                <div class="list-shipping row">
                    <div class="row">
                        @if( $shippings->count() )
                            @foreach($shippings as $shipping)
                                <div class="col-lg-4">
                                    <div class="shipping-item" data-lang="" data-lat="">
                                        <a href="{{ url('shipping/show', $shipping->id) }}">
                                            <div class="shipping-image">
                                                @if( !empty( $shipping->shipping_image ) )
                                                    <img src="{!! image_url( $shipping->shipping_image ) !!}" class="img-responsive" alt="{{ $shipping->user->full_name }}">
                                                @else
                                                    <img src="{!! image_url( $shipping->service->image ) !!}" class="img-responsive" alt="{{ $shipping->user->full_name }}">
                                                @endif
                                                <span class="shipping-image-detail">{{ $shipping->costOnMaps($packageSize, $type) }}</span>
                                                    <div class="shipping-user-avatar">
                                                        <a href="{{ url('shipping/show', array('id'=>$shipping->id, 'kltg'=>100)) }}">
                                                            @if( !empty( $shipping->user->avatar ))
                                                                <img src="{!! image_url($shipping->user->avatar) !!}" class="img-responsive" alt="{{ $shipping->user->full_name }}">
                                                            @else
                                                                <img src="http://caselaw.vn/img/no-avt.jpg" class="img-responsive" alt="{{ $shipping->user->full_name }}">
                                                            @endif
                                                        </a>
                                                    </div>
                                            </div>
                                        </a>
                                        <div class="row">
                                            <div class="shipping-user-info ">
                                                <div class="col-lg-12">
                                                    <div class="shipping-user-name">
                                                        <div class="row">
                                                            <h4 class="col-md-6 col-xs-12">{{ $shipping->user->full_name }}</h4>
                                                            <div class="col-md-6 col-xs-12 shipping-user-rates">
                                                                <div class="shipping-user-rate" style="width: 50%;"></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>s
                                            </div>
                                            <div class="shipping-info col-lg-12">
                                                <div class="col-lg-12">
                                                    <p>{{ trans('front.max') }}: <strong>{{ $shipping->max_weight ? $shipping->max_weight." kg" : trans('front.unlimited') }}</strong></p>
                                                    <p>{{ trans('front.min') }}: <strong>{{ $shipping->min_weight ? $shipping->min_weight." kg" : trans('front.unlimited') }}</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- END .shipping-item -->
                                </div><!-- END .col-lg-4 -->
                            @endforeach
                        @else
                            <div class="col-lg-12 text-center">
                                <h2>{{ trans('front.no-shipping') }}</h2>
                            </div>
                        @endif
                    </div><!-- END .row -->
                </div><!-- END .list-shipping -->
            </div>
            <div class="row text-center paging">
                {{ $shippings->links() }}
            </div>
            <div class="col-lg-5 col-sm-12 map">
                <div id="map"></div>
                <div class="search-box">
                    <input type="text" name="from" required class="form-control from" id="more-input-from" value="{{ isset($_REQUEST['from']) && !empty( $_REQUEST['from'] ) ? $_REQUEST['from'] : '' }}">
                    <input type="text" name="to" required class="form-control to" id="more-input-to" value="{{ isset($_REQUEST['to']) && !empty( $_REQUEST['to'] ) ? $_REQUEST['to'] : '' }}">
                </div>
                <script type="text/javascript">
                    var locations = [@if( $shippings->count() )
                        @foreach($shippings as $shipping)
                            @if(count($shipping->shipper_locations)>0)
                                @foreach($shipping->shipper_locations as $shipper_location)
                                    ['{{ $shipping->costOnMaps($packageSize, $type) }}', '{{ $shipping->user->full_name }}', {{ $shipper_location['lat'] }},{{ $shipper_location['lng'] }} , '#location-item-{{ $shipping->id }}'],
                                @endforeach
                            @endif
                        @endforeach
                        @endif
                    ];
                </script>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
<div class="hidden">
    @if( $shippings->count() )
        @foreach( $shippings as $shipping )
            <div id="location-item-{{ $shipping->id }}">
                <div class="box_title">
                    <div class="map-shipping-image">
                        @if( !empty( $shipping->user->shipping_image ) )
                            <img src="{!! image_url( $shipping->shipping_image ) !!}" class="img-responsive" alt="{{ $shipping->user->full_name }}">
                        @else
                            <img src="{!! image_url( $shipping->service->image ) !!}" class="img-responsive" alt="{{ $shipping->user->full_name }}">
                        @endif
                        <div class="map-guaranteed">
                            <img src="{!! Theme::asset()->url('img/guaranteed.png') !!}" class="img-responsive" alt="Kehner">
                        </div>
                        <a class="map-shipping-image-detail" href="{{ url('shipping/show', $shipping->id) }}">{{ trans('front.detail') }}</a>
                    </div>
                    <div class="map-shipping-info">
                        <div class="row">
                            <div class="col-lg-12">
                                <p>{{ trans('front.max') }}: <strong>{{ $shipping->max_weight ? $shipping->max_weight." kg" : trans('front.unlimited') }}</strong></p>
                                <p>{{ trans('front.min') }}: <strong>{{ $shipping->min_weight ? $shipping->min_weight." kg" : trans('front.unlimited') }}</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
                <span class='trig_arrow'></span>
            </div>
        @endforeach
    @endif
</div>
<script type="application/javascript">
    jQuery(document).ready(function ($) {

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("aria-controls"); // activated tab
            $('input[name="searchType"]').val(target);
        });
        
        $.each($('.find-package-form input[name="service"]'), function () {
            if($(this).is(':checked')){
                var type = JSON.parse( $(this).attr( 'data-type' ) );
                var serviceId = $(this).val();
                if( type.length > 0 ){
                    $('.search-type-item').removeClass('active').hide();
                    $('.tab-pane').removeClass('active');
                    $.each(type, function (i, val) {
                        if( val == 'trip'){
                            $('.search-type-item.search-per-trip').show();
                        }
                        if( val == 'weight'){
                            $('.search-type-item.search-per-weight').show();
                        }
                        if( val == 'cbmlcl'){
                            $('.search-type-item.search-per-cbmlcl').show();
                        }
                        if( val == 'cbmfcl'){
                            $('.search-type-item.search-per-cbmfcl').show();
                        }
                    });
                    $('.search-type-item:visible:first a').trigger('click');
                }
                if($(this).val()==3 || $(this).val()==4 || $(this).val()==5 || $(this).val()==6) {
                    $('#packageSize').html($('#packageSize-'+$(this).val()).html());
                }
                if($(this).val()==3)
                {
                    var numberTrip = getUrlParameter('packageSize[trip][numberTrip]');
                    if(typeof numberTrip === "undefined")
                    {
                        $('input.find-shipper').trigger('click');  
                    }
                }
            }
        });

        var searchType = getUrlParameter('searchType');
        $('.search-'+searchType+' a').trigger('click');

        $('.find-package-form input[name="service"]').change(function () {
            if($(this).is(':checked')){
                var type = JSON.parse( $(this).attr( 'data-type' ) );
                if( type.length > 0 ) {
                    $('.search-type-item').removeClass('active').hide();
                    $('.tab-pane').removeClass('active');
                    $.each(type, function (i, val) {
                        $('.search-type-item.search-per-'+val).show();
                    });
                    $('.search-type-item:visible:first a').trigger('click');
                }
                $('#more-input-service').val($(this).val());
                if($(this).val()==3 || $(this).val()==4 || $(this).val()==5 || $(this).val()==6) {
                    $('#packageSize').html($('#packageSize-'+$(this).val()).html());
                }
                /*if($(this).val()==3)
                {
                    var numberTrip = getUrlParameter('packageSize[trip][numberTrip]');
                    if(typeof numberTrip === "undefined")
                    {
                        $('input.find-shipper').trigger('click');  
                    }
                }*/
            }
        });
        /*$('body').on('click', '.search-per-trip', function(event) {
            event.preventDefault();
            $('input.find-shipper').trigger('click');
        });*/
        var number = $('#currentNumber').val();
        $(document).on('click', '.add-more-package-size', function () {
            number ++;
            var html = $('#package-size-per-weigt-0').html();
            html = html.replace(new RegExp('\\[0\\]', 'g'),'['+number+']');
            html = html.replace('package-size-per-weigt-0', 'package-size-per-weigt-'+number);
            html = html.replace('<span class="removebtn"></span>', '<a href="" class="btn remove-btn add-more btn-default remove-package-size" data-target="#package-size-per-weigt-'+number+'"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>');
            html = '<div class="package-size-per-weight" id="package-size-per-weigt-'+number+'">'+html+'</div>';
            $('.package-size-per-weight-box').append(html);
            $('#package-size-per-weigt-'+number+' .package-title').html('');
            $('#package-size-per-weigt-'+number).find('.add-more-package-size').remove();
            $('#package-size-per-weigt-'+number).find('.top-title').remove();
            return false;
        });
        $(document).on('click', '.remove-package-size', function () {
            var target = $(this).attr('data-target');
            $(target).remove();
            return false;
        });
    });
</script>