<div class="container auth register">

    <div class="col-lg-12">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="box">
                <div class="row">
                    <div class="col-lg-12 tab">
                        <div class="row">
                            <div class="col-lg-6 active">
                                <div class="row">
                                    <a href="{{ url('register') }}">
                                        <span>{{ trans('auth.register') }}</span>
                                        <span>{{ trans('auth.register_caption') }}</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <a href="{{ url('login') }}">
                                        <span>{{ trans('auth.login') }}</span>
                                        <span>{{ trans('auth.login_caption') }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12 social">
                    <div class="row">
                        <div class="col-lg-4"><div class="row item"><a href="{{ url('social-redirect/facebook') }}"><img src="{!! Theme::asset()->url('img/facebook-icon.png') !!}" alt="" class="img-responsive"></a></div></div>
                        <div class="col-lg-4"><div class="row item"><a href="{{ url('social-redirect/google') }}"><img src="{!! Theme::asset()->url('img/google-icon.png') !!}" alt="" class="img-responsive"></a></div></div>
                        <div class="col-lg-4"><div class="row item"><a href="{{ url('social-redirect/twitter') }}"><img src="{!! Theme::asset()->url('img/twitter-icon.png') !!}" alt="" class="img-responsive"></a></div></div>
                    </div>
                </div>
                

                <div class="col-lg-12 title">
                    <span class="text">{{ trans('auth.sign_up_with_your_email') }}</span>
                </div>

                <form class="form-horizontal" role="form" method="POST" action="{{ url('register') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <div class="col-lg-12">
                            <input id="name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required placeholder="{{ trans('auth.first_name') }}">

                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <div class="col-lg-12">
                            <input id="name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required placeholder="{{ trans('auth.last_name') }}">

                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-lg-12">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="{{ trans('auth.email') }}">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-lg-12">
                            <input id="password" type="password" class="form-control" name="password" required placeholder="{{ trans('auth.password') }}">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <div class="col-lg-12">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="{{ trans('auth.confirm_password') }}">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn">
                                {{ trans('auth.create_my_account') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
</div>