<div class="container auth login">

    <div class="col-lg-12">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="box">
                <div class="row">
                    <div class="col-lg-12 tab">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <a href="{{ url('register') }}">
                                        <span>{{ trans('auth.register') }}</span>
                                        <span>{{ trans('auth.register_caption') }}</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-6 active">
                                <div class="row">
                                    <a href="{{ url('login') }}">
                                        <span>{{ trans('auth.login') }}</span>
                                        <span>{{ trans('auth.login_caption') }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12 social">
                    <div class="row">
                        <div class="col-lg-4"><div class="row item"><a href="{{ url('social-redirect/facebook') }}"><img src="{!! Theme::asset()->url('img/facebook-icon.png') !!}" alt="" class="img-responsive"></a></div></div>
                        <div class="col-lg-4"><div class="row item"><a href="{{ url('social-redirect/google') }}"><img src="{!! Theme::asset()->url('img/google-icon.png') !!}" alt="" class="img-responsive"></a></div></div>
                        <div class="col-lg-4"><div class="row item"><a href="{{ url('social-redirect/twitter') }}"><img src="{!! Theme::asset()->url('img/twitter-icon.png') !!}" alt="" class="img-responsive"></a></div></div>
                    </div>
                </div>
                

                <div class="col-lg-12 title">
                    <span class="text">{{ trans('auth.log_in_with_your_email') }}</span>
                </div>

                <form class="form-horizontal" role="form" method="POST" action="{{ url('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-lg-12">
                            <input id="email" type="email" required class="form-control" placeholder="{{ trans('auth.email') }}" name="email" value="{{ old('email') }}">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-lg-12">
                            <input id="password" type="password" required class="form-control" placeholder="{{ trans('auth.password') }}" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <a class="btn btn-link" href="{{ url('password/reset') }}">{{ trans('auth.forgot_password') }}</a>
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> {{ trans('auth.remember') }}
                                </label>
                            </div>
                        </div>
                    </div> -->

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn">
                                {{ trans('auth.log_in_my_account') }}
                            </button>

                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
</div>