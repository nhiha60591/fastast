<header>
    <div class="home-banner-search-box" id="home-banner-search-box">
        <div class="container-fluid">
            <div class="search-tabs">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="find-shipper active">
                        <a href="#find-shipper" aria-controls="home" role="tab" data-toggle="tab">
                            <span class="icon"></span>
                            {{ trans('front.find-shipping-rate') }}
                        </a>
                    </li>
                    <li role="presentation" class="find-package">
                        <a href="#find-package" aria-controls="profile" role="tab" data-toggle="tab">
                            <span class="icon"></span>
                            {{ trans('front.find-package') }}
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="find-shipper">
                        <form class="form-inline" action="{{ url('find-shipping') }}" method="get">
                            <div class="form-group col-md-2">
                                <div class="row location">
                                    <input type="text" class="col-md-10" name="from" id="shipper-from" required placeholder="{{ trans('front.from') }}">
                                    <input type="hidden" name="shipperFromLat" id="shipperFromLat">
                                    <input type="hidden" name="shipperFromLng" id="shipperFromLng">
                                    <span class="icon icon-icon-fastast-04 col-md-2"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="row location">
                                    <input type="text" class="col-md-10" id="shipper-to" name="to" required placeholder="{{ trans('front.to') }}">
                                    <span class="icon icon-icon-fastast-04 col-md-2"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="row">
                                    <select id="shipping-service" class="shipping-service" name="service" data-show-icon="true">
                                        @if( $services->count()> 0 )
                                            @foreach($services as $service)
                                                <option value="{{ $service->id }}" data-content="{{ $service->translation($currentLanguage->id, 'name') }} <img src='{!! image_url($service->image) !!}' class='pull-right' />">-</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="row">
                                    <select id="shipping-delivery" class="shipping-delivery" name="delivery" data-show-icon="true">
                                        @if( $deliveries->count()> 0 )
                                            @foreach($deliveries as $delivery)
                                                <option value="{{ $delivery->id }}" data-content="{{ $delivery->translation($currentLanguage->id, 'name') }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-1">
                                <div class="row">
                                    <button type="submit" class="btn btn-default search-button">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in" id="find-package">
                        <form class="form-inline" action="{{ url('find-package') }}" method="get">
                            <div class="form-group col-md-2">
                                <div class="row location">
                                    <input type="text" class="col-md-10" id="package-from" required name="from" placeholder="{{ trans('front.from') }}">
                                    <input type="hidden" name="packageFromLat" id="packageFromLat">
                                    <input type="hidden" name="packageFromLng" id="packageFromLng">
                                    <span class="icon icon-icon-fastast-04 col-md-2"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <div class="row location">
                                    <input type="text" class="col-md-10" id="package-to" name="to" required placeholder="{{ trans('front.to') }}">
                                    <span class="icon icon-icon-fastast-04 col-md-2"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="row">
                                    <select id="package-service" class="package-service" name="service" data-show-icon="true">
                                        @if( $services->count()> 0 )
                                            @foreach($services as $service)
                                                <option value="{{ $service->id }}" data-content="{{ $service->translation($currentLanguage->id, 'name') }} <img src='{!! image_url($service->image) !!}' class='pull-right' />">-</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="row">
                                    <select id="package-delivery" class="package-delivery" name="delivery" data-show-icon="true">
                                        @if( $deliveries->count()> 0 )
                                            @foreach($deliveries as $delivery)
                                                <option value="{{ $delivery->id }}" data-content="{{ $delivery->translation($currentLanguage->id, 'name') }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-1">
                                <div class="row">
                                    <button type="submit" class="btn btn-default search-button">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="how-does-work" id="how-does-work">
    <div class="container-fluid">
        <h2 class="text-center text-uppercase">FASTAST hoạt động như thế nào</h2>
        <div class="row box-items">
            <div class="col-md-4 col-sm-12 col-lg-4 text-center">
                <a href="#">
                    <img src="{!! Theme::asset()->url('img/home-find-and-book.png') !!}" class="img-circle" alt="...">
                </a>
                <h4 class="text-center text-uppercase">Tìm và đặt hàng</h4>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4 text-center">
                <a href="#">
                    <img src="{!! Theme::asset()->url('img/home-delivery-process.png') !!}" class="img-circle" alt="...">
                </a>
                <h4 class="text-center text-uppercase">Thực hiện vận chuyển</h4>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4 text-center">
                <a href="#">
                    <img src="{!! Theme::asset()->url('img/home-shipping-completion.png') !!}" class="img-circle" alt="...">
                </a>
                <h4 class="text-center text-uppercase">Kết thúc</h4>
            </div>
        </div>
        <div class="row steps">
            <div class="col-md-4 col-sm-12 col-lg-4 step-item step-first">
                <p class="text-center"><span class="step-number">1</span></p>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4 step-item">
                <p class="text-center"><span class="step-number">2</span></p>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4 step-item step-last">
                <p class="text-center"><span class="step-number">3</span></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12 col-lg-4">
                <p class="text-center">
                    Tìm đơn vị vận chuyển hoặc hàng cần vận<br />
                    chuyển ở vị trí gần nhất, giá vận chuyển hợp lý<br />
                    nhất và đặt vận chuyển trực tuyến.
                </p>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4">
                <p class="text-center">
                    Đơn vị vận chuyển sẽ đến nhận hàng<br />
                    và thực hiện vận chuyển
                </p>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4">
                <p class="text-center">
                    Quá trình vận chuyển kết thúc, hàng đã đến<br />
                    địa chỉ bạn cần gửi.
                </p>
            </div>
        </div>
    </div>
</section>
<section class="they-talk-about-us" id="they-talk-about-us">
    <div class="container-fluid">
        <h2 class="text-center text-uppercase they-talk-title"><span>Họ nói gì về chúng tôi</span></h2>
        <div class="row">
            <div class="col-lg-3 col-lg-offset-1 col-sm-12">
                <img src="{!! Theme::asset()->url('img/khanh-tran-picture.png') !!}" align="" class="img-circle">
            </div>
            <div class="col-lg-7 col-sm-12">
                <div class="they-talk-content">
                    Fastast là một website về vận tải, chuyển phát nhanh mang đến nhiều lợi ích cho
                    các công ty vận tải vừa và nhỏ như Vietpost. Chúng tôi có trên 10 năm kinh nghiệm
                    trong lĩnh vực chuyển phát nhanh và phục vụ trên 2000 đơn đặt hàng mỗi ngày. Mặc
                    dù là một công ty trẻ trong lĩnh vực chuyển phát nhanh, chúng tôi luôn cố gắng nâng
                    cao chất lượng dịch vụ và sự hài lòng của khách hàng luôn được đặt lên hàng đầu.
                    Fastast sẽ là một đối tác quan trọng không chỉ giúp đỡ chúng tôi mà còn nhiều công
                    ty, cá nhân khác trong lĩnh vực vận tải và chuyển phát nhanh phát triển nhanh hơn
                    và phục vụ tốt hơn. Đối với người gửi hàng là các cá nhân, công ty sản xuất và bán
                    lẻ... đây sẽ là một kênh vận chuyển lý tưởng giúp họ chọn được đơn vị vận chuyển
                    phù hợp để giúp họ giảm chi phí vận chuyển nhiều hơn và tối ưu hóa lợi nhuận,
                    chiến lược kinh doanh của họ.
                    <p class="they-about-customer"><b>Khanh Tran - CEO of Vietpost</b></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="clients" id="clients">
    <div class="container-fluid">
        <h2 class="text-center text-uppercase">Khách hàng chia sẽ</h2>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="our-client-items">
                    <div class="our-client-item">
                        …I had paid too much shipping fees for airlines shipping via express service for years when I bought some items from Amazon, USA. But now, everything is changed when some travelers accept to pick and deliver my items with triple time scheaper than before. Thank you h2hshipper…
                    </div>
                    <div class="our-client-item">
                        …I had paid too much shipping fees for airlines shipping via express service for years when I bought some items from Amazon, USA. But now, everything is changed when some travelers accept to pick and deliver my items with triple time scheaper than before. Thank you h2hshipper…
                    </div>
                    <div class="our-client-item">
                        …I had paid too much shipping fees for airlines shipping via express service for years when I bought some items from Amazon, USA. But now, everything is changed when some travelers accept to pick and deliver my items with triple time scheaper than before. Thank you h2hshipper…
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <div class="our-client-items-nav text-center">
                    <div class="our-client-item-nav">
                        <img alt="Client 1" src="{!! Theme::asset()->url('img/client-1.png') !!}" class="img-circle" />
                        <p>
                            <strong>Jenny Nguyen</strong>
                            <span class="client-position">American traveller</span>
                        </p>
                    </div>
                    <div class="our-client-item-nav">
                        <img alt="Client 2" src="{!! Theme::asset()->url('img/client-2.png') !!}" class="img-circle" />
                        <p>
                            <strong>Jenny Nguyen 2</strong>
                            <span class="client-position">American traveller</span>
                        </p>
                    </div>
                    <div class="our-client-item-nav">
                        <img alt="Client 3" src="{!! Theme::asset()->url('img/client-3.png') !!}" class="img-circle" />
                        <p>
                            <strong>Jenny Nguyen 3</strong>
                            <span class="client-position">American traveller</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="partner" id="partner">
    <div class="container-fluid">
        <h2 class="section-title text-center text-uppercase">Đối tác của FASTAST</h2>
        <div class="row">
            <div class="col-lg-4 col-sm-12 text-center">
                <div class="partner-image">
                    <img src="{!! Theme::asset()->url('img/partner-1.png') !!}" alt="" class="img-responsive" />
                </div>
                <div class="partner-info">
                    <h4>SD Trading & Technique Ltd Co</h4>
                    <p>Specialize in providing whole sales and retails products of Igus, Tsubaki, Setup automation system, Waste water technology.</p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-12 text-center">
                <div class="partner-image">
                    <img src="{!! Theme::asset()->url('img/partner-2.png') !!}" alt="" class="img-responsive" />
                </div>
                <div class="partner-info">
                    <h4>Vietpost Investment Corporation</h4>
                    <p>Specialize in leading courier in Vietnam</p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-12 text-center">
                <div class="partner-image">
                    <img src="{!! Theme::asset()->url('img/partner-3.png') !!}" alt="" class="img-responsive" />
                </div>
                <div class="partner-info">
                    <h4>Rao.vn Ltd Co,</h4>
                    <p>Specialize in online retails for various types of products and tour management in domestic and Cambodia, China.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $('#shipping-service').selectpicker();
    $('#shipping-delivery').selectpicker();
    $('#package-service').selectpicker();
    $('#package-delivery').selectpicker();
    $('.our-client-items').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.our-client-items-nav'
    });
    $('.our-client-items-nav').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.our-client-items',
        focusOnSelect: true
    });
</script>