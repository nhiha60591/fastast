<header>
    <div class="home-banner-search-box" id="home-banner-search-box">
        <div class="container-fluid">
            <div class="search-tabs">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#find-shipper" aria-controls="home" role="tab" data-toggle="tab">
                            <span class="icon icon-icon-fastast-01"></span>
                            {{ trans('front.find-shipping-rate') }}
                            <span class="icon icon-icon-fastast-02"></span>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#find-package" aria-controls="profile" role="tab" data-toggle="tab">
                            <span class="icon icon-icon-fastast-21"></span>
                            {{ trans('front.find-package') }}
                            <span class="icon icon-icon-fastast-05"></span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="find-shipper">
                        <form class="form-inline" action="{{ url('find-shipping') }}" method="get">
                            <div class="form-group">
                                <input type="text" class="form-control" name="from" id="shipper-from" placeholder="{{ trans('front.from') }}">
                                <span class="icon icon-icon-fastast-04"></span>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="shipper-to" name="to" placeholder="{{ trans('front.to') }}">
                                <span class="icon icon-icon-fastast-04"></span>
                            </div>
                            <div class="form-group">
                                <select id="shipping-service" class="shipping-service" name="service" data-show-icon="true">
                                    <option value="" data-content="{{ trans('front.all-services') }} <img src='{!! Theme::asset()->url('img/icon-service-1.png') !!}' class='pull-right' />">-</option>
                                    <option value="" data-content="Express Envelope <img src='{!! Theme::asset()->url('img/icon-service-1.png') !!}' class='pull-right' />">-</option>
                                    <option value="" data-content="Road freight <img src='{!! Theme::asset()->url('img/icon-service-2.png') !!}' class='pull-right' />">-</option>
                                    <option value="" data-content="Home/office moving <img src='{!! Theme::asset()->url('img/icon-service-3.png') !!}' class='pull-right' />">-</option>
                                    <option value="" data-content="Car freight <img src='{!! Theme::asset()->url('img/icon-service-4.png') !!}' class='pull-right' />">-</option>
                                    <option value="" data-content="Light truck freight <img src='{!! Theme::asset()->url('img/icon-service-5.png') !!}' class='pull-right' />">-</option>
                                    <option value="" data-content="Container/heavy truck freight <img src='{!! Theme::asset()->url('img/icon-service-6.png') !!}' class='pull-right' />">-</option>
                                    <option value="" data-content="Air freight <img src='{!! Theme::asset()->url('img/icon-service-7.png') !!}' class='pull-right' />">-</option>
                                    <option value="" data-content="Sea freight <img src='{!! Theme::asset()->url('img/icon-service-8.png') !!}' class='pull-right' />">-</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="shipping-delivery" class="shipping-delivery" name="delivery" data-show-icon="true">
                                    <option value="">Delivery within 1-2 days</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default search-button">Search</button>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in" id="find-package">
                        <form class="form-inline" action="{{ url('find-package') }}" method="get">
                            <div class="form-group">
                                <input type="text" class="form-control" id="shipper-from" name="from" placeholder="{{ trans('front.from') }}">
                                <span class="icon icon-icon-fastast-04"></span>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="shipper-to" name="to" placeholder="{{ trans('front.to') }}">
                                <span class="icon icon-icon-fastast-04"></span>
                            </div>
                            <div class="form-group">
                                <select id="package-service" class="package-service" name="service" data-show-icon="true">
                                    <option data-content="{{ trans('front.all-services') }} <img src='{!! Theme::asset()->url('img/icon-service-1.png') !!}' class='pull-right' />">-</option>
                                    <option data-content="Express Envelope <img src='{!! Theme::asset()->url('img/icon-service-1.png') !!}' class='pull-right' />">-</option>
                                    <option data-content="Road freight <img src='{!! Theme::asset()->url('img/icon-service-2.png') !!}' class='pull-right' />">-</option>
                                    <option data-content="Home/office moving <img src='{!! Theme::asset()->url('img/icon-service-3.png') !!}' class='pull-right' />">-</option>
                                    <option data-content="Car freight <img src='{!! Theme::asset()->url('img/icon-service-4.png') !!}' class='pull-right' />">-</option>
                                    <option data-content="Light truck freight <img src='{!! Theme::asset()->url('img/icon-service-5.png') !!}' class='pull-right' />">-</option>
                                    <option data-content="Container/heavy truck freight <img src='{!! Theme::asset()->url('img/icon-service-6.png') !!}' class='pull-right' />">-</option>
                                    <option data-content="Air freight <img src='{!! Theme::asset()->url('img/icon-service-7.png') !!}' class='pull-right' />">-</option>
                                    <option data-content="Sea freight <img src='{!! Theme::asset()->url('img/icon-service-8.png') !!}' class='pull-right' />">-</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="package-delivery" class="package-delivery" name="delivery" data-show-icon="true">
                                    <option value="">Delivery within 1-2 days</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default search-button">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="how-does-work" id="how-does-work">
    <div class="container-fluid">
        <h2 class="text-center text-uppercase">How does fastast work</h2>
        <div class="row box-items">
            <div class="col-md-4 col-sm-12 col-lg-4 text-center">
                <a href="#">
                    <img src="{!! Theme::asset()->url('img/home-find-and-book.png') !!}" class="img-circle" alt="...">
                </a>
                <h4 class="text-center text-uppercase">Find a book</h4>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4 text-center">
                <a href="#">
                    <img src="{!! Theme::asset()->url('img/home-delivery-process.png') !!}" class="img-circle" alt="...">
                </a>
                <h4 class="text-center text-uppercase">DELIVERY PROCESS</h4>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4 text-center">
                <a href="#">
                    <img src="{!! Theme::asset()->url('img/home-shipping-completion.png') !!}" class="img-circle" alt="...">
                </a>
                <h4 class="text-center text-uppercase">Find a book</h4>
            </div>
        </div>
        <div class="row steps">
            <div class="col-md-4 col-sm-12 col-lg-4 step-item step-first">
                <p class="text-center"><span class="step-number">1</span></p>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4 step-item">
                <p class="text-center"><span class="step-number">2</span></p>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4 step-item step-last">
                <p class="text-center"><span class="step-number">3</span></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12 col-lg-4">
                <p class="text-center">
                    Find your shippers or package on ourplatform <br />
                    then click to Book a shipping.
                </p>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4">
                <p class="text-center">
                    Shipper will pickup your package and deliver it on<br />
                    schedule.
                </p>
            </div>
            <div class="col-md-4 col-sm-12 col-lg-4">
                <p class="text-center">
                    Delivery completed!
                </p>
            </div>
        </div>
    </div>
</section>
<section class="they-talk-about-us" id="they-talk-about-us">
    <div class="container-fluid">
        <h2 class="text-center text-uppercase they-talk-title"><span>They talk about us</span></h2>
        <div class="row">
            <div class="col-lg-3 col-lg-offset-1 col-sm-12">
                <img src="{!! Theme::asset()->url('img/khanh-tran-picture.png') !!}" align="" class="img-circle">
            </div>
            <div class="col-lg-7 col-sm-12">
                <div class="they-talk-content">
                    <p>Fastast is a logistics and transportation platform that sharing many benefits for small logistics and transportation companies like Vietpost.</p>

                    <p>We’re having over 10 years experience in logistics that are serving around 2000 customers everyday in Vietnam. Although we are young company in delivery service, we always endeavor to improve service quality and support clients better. Fastast will be a great partner to help not only our company but also another logistic and transportation companies grow faster and serve better. For senders, manufacturers or retailers, this is a market place to allow them saving more shipping cost and optimizing their profit.</p>
                    <p class="they-about-customer"><b>Khanh Tran - CEO of Vietpost</b></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="clients" id="clients">
    <div class="container-fluid">
        <h2 class="text-center text-uppercase">OUR CLIENTS SHARING</h2>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="our-client-items">
                    <div class="our-client-item">
                        …I had paid too much shipping fees for airlines shipping via express service for years when I bought some items from Amazon, USA. But now, everything is changed when some travelers accept to pick and deliver my items with triple time scheaper than before. Thank you h2hshipper…
                    </div>
                    <div class="our-client-item">
                        …I had paid too much shipping fees for airlines shipping via express service for years when I bought some items from Amazon, USA. But now, everything is changed when some travelers accept to pick and deliver my items with triple time scheaper than before. Thank you h2hshipper…
                    </div>
                    <div class="our-client-item">
                        …I had paid too much shipping fees for airlines shipping via express service for years when I bought some items from Amazon, USA. But now, everything is changed when some travelers accept to pick and deliver my items with triple time scheaper than before. Thank you h2hshipper…
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <div class="our-client-items-nav text-center">
                    <div class="our-client-item-nav">
                        <img alt="Client 1" src="{!! Theme::asset()->url('img/client-1.png') !!}" class="img-circle" />
                        <p>
                            <strong>Jenny Nguyen</strong>
                            <span class="client-position">American traveller</span>
                        </p>
                    </div>
                    <div class="our-client-item-nav">
                        <img alt="Client 2" src="{!! Theme::asset()->url('img/client-2.png') !!}" class="img-circle" />
                        <p>
                            <strong>Jenny Nguyen 2</strong>
                            <span class="client-position">American traveller</span>
                        </p>
                    </div>
                    <div class="our-client-item-nav">
                        <img alt="Client 3" src="{!! Theme::asset()->url('img/client-3.png') !!}" class="img-circle" />
                        <p>
                            <strong>Jenny Nguyen 3</strong>
                            <span class="client-position">American traveller</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="partner" id="partner">
    <div class="container-fluid">
        <h2 class="section-title text-center text-uppercase">Our Partner</h2>
        <div class="row">
            <div class="col-lg-4 col-sm-12 text-center">
                <div class="partner-image">
                    <img src="{!! Theme::asset()->url('img/partner-1.png') !!}" alt="" class="img-responsive" />
                </div>
                <div class="partner-info">
                    <h4>SD Trading & Technique Ltd Co</h4>
                    <p>Specialize in providing whole sales and retails products of Igus, Tsubaki, Setup automation system, Waste water technology.</p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-12 text-center">
                <div class="partner-image">
                    <img src="{!! Theme::asset()->url('img/partner-2.png') !!}" alt="" class="img-responsive" />
                </div>
                <div class="partner-info">
                    <h4>Vietpost Investment Corporation</h4>
                    <p>Specialize in leading courier in Vietnam</p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-12 text-center">
                <div class="partner-image">
                    <img src="{!! Theme::asset()->url('img/partner-3.png') !!}" alt="" class="img-responsive" />
                </div>
                <div class="partner-info">
                    <h4>Rao.vn Ltd Co,</h4>
                    <p>Specialize in online retails for various types of products and tour management in domestic and Cambodia, China.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $('#shipping-service').selectpicker();
    $('#shipping-delivery').selectpicker();
    $('#package-service').selectpicker();
    $('#package-delivery').selectpicker();
    $('.our-client-items').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.our-client-items-nav'
    });
    $('.our-client-items-nav').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.our-client-items',
        focusOnSelect: true
    });
</script>