{!! Form::open(array('url' => 'package/send-package/'.$id,  'method' => 'POST', 'id' => 'post-package', 'files'=>true, 'role'=>'form', 'class'=> 'form')) !!}
<div class="container-fluid container-create">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-title text-center">{{ trans('front.package-sender-information') }}</h1>
        </div>
        <div class="col-md-6 col-sm-12">
            <h3>{{ trans('front.package-information') }} <span class="require">(*)</span></h3>
            <div class="form-group{{ $errors->has('packageInformation') ? ' has-error' : '' }}">
                <textarea name="packageInformation" id="package-information" style="width: 100%;min-height: 363px;" rows="10" class="package-information form-control tinymce"></textarea>
                @if ($errors->has('packageInformation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('packageInformation') }}</strong>
                    </span>
                @endif
            </div>
        </div><!-- END .col-md-6 -->
        <div class="col-md-6 col-sm-12">
            <h3>{{ trans('front.shipping-requirements') }}</h3>
            <div class="package-requirement">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="right-input">
                            <input type="checkbox" id="packageRequirement-1" name="packageRequirement[]" value="1">
                            <label for="packageRequirement-1"></label>
                        </span>
                        <label for="packageRequirement-1">
                            I. {{ trans('front.issue-invoice') }}
                        </label>
                    </li>
                    <li class="list-group-item">
                        <span class="right-input">
                            <input type="checkbox" id="packageRequirement-2" name="packageRequirement[]" value="2">
                            <label for="packageRequirement-2"></label>
                        </span>
                        <label for="packageRequirement-2">
                            II. {{ trans('front.delivery-guaranteed') }}
                        </label>
                    </li>
                    <li class="list-group-item">
                        <span class="right-input">
                            <input type="checkbox" id="packageRequirement-3" name="packageRequirement[]" value="3">
                            <label for="packageRequirement-3"></label>
                        </span>
                        <label for="packageRequirement-3">
                            III. {{ trans('front.cash-on-delivery') }}
                        </label>
                    </li>
                    <li class="list-group-item">
                        <span class="right-input">
                            <input type="checkbox" id="packageRequirement-4" name="packageRequirement[]" value="4">
                            <label for="packageRequirement-4"></label>
                        </span>
                        <label for="packageRequirement-4">
                            IV. {{ trans('front.shipping-insurance') }}
                        </label>
                    </li>
                </ul>
                <div class="package-images">
                    <h4>{{ trans('front.package-image') }}</h4>
                    <div class="hidden">
                        <input type="file" name="files[]" id="filer_input2" multiple="multiple" data-action="{{ url('package/upload') }}">
                    </div>
                    <div class="row">
                        <div class="col-md-9 col-sm-12 list-package-image"></div>
                        <div class="col-md-3 col-sm-12">
                            <a href="" class="btn add-package-image"><img src="{!! Theme::asset()->url('img/camera-img.png') !!}" class="img-bordered img-responsive"></a>
                        </div>
                    </div>
                </div>
            </div><!-- END .package-requirement -->
        </div><!-- END .col-md-6 -->
    </div><!-- END .row -->
    <div class="row send-package-contact">
        <div class="col-md-6 col-sm-12">
            <h3>{{ trans('front.Origination') }}</h3>
            <div class="form-horizontal">
                <div class="form-group{{ $errors->has('origination-contact-name') ? ' has-error' : '' }}">
                    <label for="origination-contact-name" class="col-sm-3 control-label">{{ trans('front.contact-name') }} <span class="require">(*)</span></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" required id="origination-contact-name" name="origination[name]">
                    </div>
                </div>
                <div class="form-group{{ $errors->has('origination-company-name') ? ' has-error' : '' }}">
                    <label for="origination-company-name" class="col-sm-3 control-label">{{ trans('front.company-name') }}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="origination-company-name" name="origination[company]">
                    </div>
                </div>
                <div class="form-group{{ $errors->has('origination-phone-number') ? ' has-error' : '' }}">
                    <label for="origination-phone-number" class="col-sm-3 control-label">{{ trans('front.phone-number') }} <span class="require">(*)</span></label>
                    <div class="col-sm-9">
                        <input type="tel" class="form-control" required id="origination-phone-number" name="origination[phone]">
                    </div>
                </div>
                <div class="form-group{{ $errors->has('origination-email') ? ' has-error' : '' }}">
                    <label for="origination-email" class="col-sm-3 control-label">{{ trans('front.email') }} <span class="require">(*)</span></label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" required id="origination-email" name="origination[email]">
                    </div>
                </div>
                <div class="form-group{{ $errors->has('origination-address') ? ' has-error' : '' }}">
                    <label for="origination-address" class="col-sm-12 control-label">{{ trans('front.address') }} <span class="require">(*)</span></label>
                    <div class="col-sm-12">
                        <textarea name="origination[address]" required id="origination-address" class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <h3>{{ trans('front.Destination') }}</h3>
            <div class="form-horizontal">
                <div class="form-group{{ $errors->has('destination-contact-name') ? ' has-error' : '' }}">
                    <label for="destination-contact-name" class="col-sm-3 control-label">{{ trans('front.contact-name') }} <span class="require">(*)</span></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" required id="destination-contact-name" name="destination[name]">
                    </div>
                </div>
                <div class="form-group{{ $errors->has('destination-company-name') ? ' has-error' : '' }}">
                    <label for="destination-company-name" class="col-sm-3 control-label">{{ trans('front.company-name') }}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="destination-company-name" name="destination[company]">
                    </div>
                </div>
                <div class="form-group{{ $errors->has('destination-phone-number') ? ' has-error' : '' }}">
                    <label for="destination-phone-number" class="col-sm-3 control-label">{{ trans('front.phone-number') }} <span class="require">(*)</span></label>
                    <div class="col-sm-9">
                        <input type="tel" class="form-control" required id="destination-phone-number" name="destination[phone]">
                    </div>
                </div>
                <div class="form-group{{ $errors->has('destination-email') ? ' has-error' : '' }}">
                    <label for="destination-email" class="col-sm-3 control-label">{{ trans('front.email') }} <span class="require">(*)</span></label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="destination-email" required name="destination[email]">
                    </div>
                </div>
                <div class="form-group{{ $errors->has('destination-address') ? ' has-error' : '' }}">
                    <label for="destination-address" class="col-sm-12 control-label">{{ trans('front.address') }} <span class="require">(*)</span></label>
                    <div class="col-sm-12">
                        <textarea name="destination[address]" required id="destination-address" class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END .send-package-contact -->
    <div class="row">
        <div class="col-md-12 text-center btn-groups">
            <input type="submit" class="btn btn-cancel" name="cancel" value="{{ trans('front.cancel') }}">
            <input type="submit" class="btn btn-cancel" name="preview" value="{{ trans('front.preview') }}">
            <input type="submit" class="btn btn-upload" name="confirm" value="{{ trans('front.confirm') }}">
        </div>
    </div>
</div><!-- END .container -->
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(document).on('click','.add-package-image',function(){
            $('#filer_input2').click();
            return false;
        });
        $(document).on('change','#filer_input2',function(event){
            $('.list-package-images').html('');
            var i = 1;
            var html = '';
            $.each(event.target.files, function(key, value){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.list-package-image').append('<div class="col-md-3 col-sm-6 col-xs-12"><img src="'+e.target.result+'" class="img-responsive"></div>');
                };
                reader.readAsDataURL(value);
                i++;
            });
            return false;
        });
    });
</script>
{!! Form::close() !!}