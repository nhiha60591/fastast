<div class="container">
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <h1 class="text-center">Track package</h1>
            <div class="list-tracking-codes">
                @if($bookings->count() > 0)
                    @foreach($bookings as $booking)
                        <h3>Tracking Code 1 <span class="color-orange">{{ $booking->track_number }}</span></h3>
                    @endforeach
                @endif
            </div>
            <div class="tracking-status">
                <h3 class="text-center color-green">Shipping is progressing...</h3>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div id="map"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function initMap() {
        var map, latLng, homeLatLng;
        latLng = new google.maps.LatLng( 10.754919, 106.643336);
        homeLatLng = new google.maps.LatLng(10.753238, 106.639430);
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: homeLatLng
        });
        var travel_mode = 'DRIVING';
        var origin_place_id = null;
        var destination_place_id = null;

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        directionsDisplay.setMap(map);
        @if($bookings->count() > 0)
            @foreach($bookings as $booking)
                route(new google.maps.LatLng( {{ $booking->shipping->location() }} ), new google.maps.LatLng( {{ $booking->shipping->toLocation() }}), travel_mode,
                directionsService, directionsDisplay);
            @endforeach
        @endif
        function expandViewportToFitPlace(map, place) {
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
        }
        function route(origin_place_id, destination_place_id, travel_mode,
                       directionsService, directionsDisplay) {
            if (!origin_place_id || !destination_place_id) {
                return;
            }
            directionsService.route({
                origin: origin_place_id,
                destination: destination_place_id,
                travelMode: travel_mode
            }, function(response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
        }
    }
</script>