<div class="page-header find-package">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                {!! Form::open(array('url' => 'find-package',  'method' => 'GET', 'id' => 'search-package', 'role'=>'form', 'class'=> 'form form-inline find-package-form')) !!}
                    @if( $services->count() > 0)
                        <?php $serviceChecked = isset($_REQUEST['service']) && !empty($_REQUEST['service']) && $_REQUEST['service'] != '0' ? $_REQUEST['service'] : 1; ?>
                        @foreach($services as $service)
                            <div class="form-group">
                                <label>
                                    <input type="radio" value="{{ $service->id }}" name="service" {{ checked($service->id, $serviceChecked) }} data-type="{{ $service->listType('type') }}" />
                                    <img src="{!! image_url($service->image) !!}" alt="" class="service-item">
                                </label>
                            </div>
                        @endforeach
                    @endif
                    <div class="form-group">
                        <?php $shipperTime = isset($_REQUEST['shipperTime']) && !empty($_REQUEST['shipperTime']) && $_REQUEST['shipperTime'] != '0' ? $_REQUEST['shipperTime'] : 1; ?>
                        <select name="shipperTime" class="form-control">
                            @if( $deliveries->count() > 0)
                                @foreach($deliveries as $delivery)
                                    <option value="{{ $delivery->id }}"{{ selected($delivery->id, $shipperTime) }}>{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="date" class="form-control" value="{{ isset($_REQUEST['shipperDate']) && !empty($_REQUEST['shipperDate']) ? date('Y-m-d', strtotime( $_REQUEST['shipperDate'] ) ) : '' }}" name="shipperDate" />
                    </div>
                    <div class="form-group">
                        <select name="shipperRelevance" class="form-control">
                            <option value="0">Relevance</option>
                        </select>
                    </div>
                    <div class="form-group pull-right">
                        <a href="{{ url('find-shipping') }}" class="btn btn-default find-package-btn"><div class="icon icon-icon-fastast-01"></div> {{ trans('front.find-shipper') }}</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="list-shipping-box">
            <div class="col-lg-7 col-sm-12">
                <div class="list-shipping row">
                    <div class="row">
                        @if( $packages->count() )
                            @foreach($packages as $package)
                                <div class="col-lg-4">
                                    <div class="shipping-item" data-lang="" data-lat="">
                                        <a href="{{ url('package/show', $package->id) }}">
                                            <div class="shipping-image">
                                                @if( !empty( $package->images->count() ) )
                                                    <img src="{!! image_url( $package->images->first()->file_url ) !!}" class="img-responsive" alt="{{ $package->user->full_name }}">
                                                @else
                                                    <img src="{!! image_url( $package->services->first()->image ) !!}" class="img-responsive" alt="{{ $package->user->full_name }}">
                                                @endif
                                                <span class="shipping-image-detail">{{ $package->getCost() }}</span>
                                            </div>
                                        </a>
                                        <div class="row">
                                            <div class="shipping-user-info col-lg-12">
                                                <div class="shipping-user-avatar">
                                                    <a href="{{ url('package/show', array('id'=>$package->id, 'kltg'=>100)) }}">
                                                        @if( !empty( $package->user->avatar ))
                                                            <img src="{!! image_url($package->user->avatar) !!}" class="img-responsive" alt="{{ $package->user->full_name }}">
                                                        @else
                                                            <img src="http://caselaw.vn/img/no-avt.jpg" class="img-responsive" alt="{{ $package->user->full_name }}">
                                                        @endif
                                                    </a>
                                                </div>
                                                <div class="shipping-user-name">
                                                    <h4>{{ $package->user->full_name }}</h4>
                                                    <div class="list-services">
                                                        @if($package->services->count())
                                                            @foreach($package->services as $packageService)
                                                                <img src="{{ image_url($packageService->image) }}" >
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="shipping-info col-lg-12">
                                                <div class="col-lg-12">
                                                    <p>Shipping {!! $package->getShippingTime() !!} </p>
                                                    <p>Departure : {!! date('d/m/Y', strtotime($package->departure_date)) !!}</p>
                                                    <p>Arrival :  {!! date('d/m/Y', strtotime($package->arrival_date)) !!}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- END .shipping-item -->
                                </div><!-- END .col-lg-4 -->
                            @endforeach
                        @else
                            <div class="col-lg-12 text-center">
                                <h2>{{ trans('front.no-package') }}</h2>
                            </div>
                        @endif
                    </div><!-- END .row -->
                    <div class="row text-center">
                        {{ $packages->links() }}
                    </div>
                </div><!-- END .list-shipping -->
            </div>
            <div class="col-lg-5 col-sm-12">
                <div id="map"></div>
                <script type="text/javascript">
                    var map, latLng, homeLatLng;
                    function initMap() {
                        latLng = new google.maps.LatLng( 10.754919, 106.643336);
                        homeLatLng = new google.maps.LatLng(10.753238, 106.639430);
                        var locations = [@if( $packages->count() )
                            @foreach($packages as $package)
                        ['{{ $package->getCost() }}', '{{ $package->user->full_name }}', {{ $package->getLocations() }}, '#location-item-{{ $package->id }}'],
                                @endforeach
                                @endif
                        ];

                        map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 12,
                            center: new google.maps.LatLng(10.8056949,106.6321204),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                    }
                </script>
            </div>
        </div>
    </div>
</div>

<div class="hidden">
    @if( $packages->count() )
        @foreach($packages as $package)
            <div class="location-item-{{ $package->id }}">
                <div class="shipping-item" data-lang="" data-lat="">
                    <a href="{{ url('package/show', $package->id) }}">
                        <div class="shipping-image">
                            @if( !empty( $package->images->count() ) )
                                <img src="{!! image_url( $package->images->first()->file_url ) !!}" class="img-responsive" alt="{{ $package->user->full_name }}">
                            @else
                                <img src="{!! image_url( $package->services->first()->image ) !!}" class="img-responsive" alt="{{ $package->user->full_name }}">
                            @endif
                            <span class="shipping-image-detail">{{ $package->getCost() }}</span>
                        </div>
                    </a>
                    <div class="row">
                        <div class="shipping-user-info col-lg-12">
                            <div class="shipping-user-avatar">
                                <a href="{{ url('package/show', array('id'=>$package->id, 'kltg'=>100)) }}">
                                    @if( !empty( $package->user->avatar ))
                                        <img src="{!! image_url($package->user->avatar) !!}" class="img-responsive" alt="{{ $package->user->full_name }}">
                                    @else
                                        <img src="http://caselaw.vn/img/no-avt.jpg" class="img-responsive" alt="{{ $package->user->full_name }}">
                                    @endif
                                </a>
                            </div>
                            <div class="shipping-user-name">
                                <h4>{{ $package->user->full_name }}</h4>
                                <div class="list-services">
                                    @if($package->services->count())
                                        @foreach($package->services as $packageService)
                                            <img src="{{ image_url($packageService->image) }}" >
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="shipping-info col-lg-12">
                            <div class="col-lg-12">
                                <p>Shipping {!! $package->getShippingTime() !!} </p>
                                <p>Departure : {!! date('d/m/Y', strtotime($package->departure_date)) !!}</p>
                                <p>Arrival :  {!! date('d/m/Y', strtotime($package->arrival_date)) !!}</p>
                            </div>
                        </div>
                    </div>
                </div><!-- END .shipping-item -->
            </div><!-- END .col-lg-4 -->
        @endforeach
    @endif
</div>