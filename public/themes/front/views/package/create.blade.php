{!! Form::open(array('url' => 'package/post-package',  'method' => 'POST', 'id' => 'post-package', 'files'=>true, 'role'=>'form', 'class'=> 'form')) !!}
    <style type="text/css">
        .container-create .form-group {
            margin-bottom: 15px;
        }
        .package-option .form-group {
            margin-bottom: 0px;
            padding-top: 7px;
        }
        .package-option .form-group label {
            text-align: center;
        }
    </style>
    <div class="container-fluid container-create">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="text-center">Packages need quote</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2>Shipping Information</h2>
            </div>
        </div>
        <div class="row">
            <div style="position: absolute;left:50px;">
                <img alt="Brand" src="{!! Theme::asset()->url('img/map-icon.png') !!}">
            </div>
            <div class="col-md-4 col-md-offset-2 col-sm-12">
                <div class="form-group">
                    <label for="origination">Origination <span class="require">(*)</span></label>
                    <input type="hidden" name="originationLocation" value="{{ $package->origination_location }}" id="origination-location">
                    <input type="text" name="origination" value="{{ $package->origination }}" required id="origination" class="origination form-control" placeholder="Ex: HồChíMinh city, Vietnam…" />
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="form-group">
                    <label for="destination">Destination <span class="require">(*)</span></label>
                    <input type="hidden" name="destinationLocation" value="{{ $package->destination_location }}" id="destination-location">
                    <input type="text" name="destination" id="destination" value="{{ $package->destination }}" required class="destination form-control" placeholder="Ex: HồChíMinh city, Vietnam…" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-2 col-sm-12">
                <p><label>Delivery Service <span class="require">(*)</span></label></p>
            </div>
        </div>
        @if( $package->services->count() > 0 )
            <?php $i=0; ?>
            @foreach( $package->services as $serviceItem )
                <div class="delivery-service-item" id="delivery-service-item-{{ $i }}">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group">
                                <select name="deliveryService[{{ $i }}]" class="form-control delivery-service" required>
                                    @if($services->count() > 0)
                                        @foreach($services as $service)
                                            <option {{ selected($serviceItem->id, $service->id) }} value="{{ $service->id }}" data-content="{{ $service->translation($currentLanguage->id, 'name') }} <img src='{{ image_url($service->image) }}' class='pull-right' />">{{ $service->translation($currentLanguage->id, 'name') }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-default add-more btn-add-delivery-service"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                        </div>
                    </div>
                </div>
                <?php $i++; ?>
            @endforeach
            <input type="hidden" id="delivery-service-count" value="{{ $package->services->count() }}" />
        @else
            <div class="delivery-service-item" id="delivery-service-item-0">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            <select name="deliveryService[]" class="form-control delivery-service" required>
                                @if($services->count() > 0)
                                    @foreach($services as $service)
                                        <option value="{{ $service->id }}" data-content="{{ $service->translation($currentLanguage->id, 'name') }} <img src='{{ image_url($service->image) }}' class='pull-right' />">{{ $service->translation($currentLanguage->id, 'name') }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-default add-more btn-add-delivery-service"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                    </div>
                </div>
            </div>
            <input type="hidden" id="delivery-service-count" value="1" />
        @endif
        <div class="delivery-service-box">

        </div>
        <div class="row">
            <div class="col-md-2 col-sm-12"></div>
            <div class="col-md-4 col-sm-12">
                <div class="form-group">
                    <label for="shippingCost">Total shipping cost (including VAT)</label>
                    <input type="text" value="{{ $package->shipping_cost }}" name="shippingCost" id="shippingCost" class="shippingCost form-control" placeholder="Negotiation" />
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="form-group">
                    <label for="shippingTime">Shipping time</label>
                    <select name="shippingTime" id="shippingTime" class="shippingTime form-control">
                        <option value="0">All shipping time</option>
                        @if( $deliveries->count() > 0)
                            @foreach($deliveries as $delivery)
                                <option {{ selected($delivery->id, $package->shipping_time) }} value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-sm-12"></div>
            <div class="col-md-4 col-sm-12">
                <div class="form-group">
                    <label for="departureDate">Departure date</label>
                    <div class="input-group date" id="datetimepicker1">
                        <input type="text" name="departureDate" value="{{ $package->departure_date }}" id="departureDate" class="departureDate form-control" placeholder="Not required" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="form-group">
                    <label for="arrivalDate">Arrival date</label>
                    <div class="input-group date" id="datetimepicker2">
                        <input type="text" name="arrivalDate" value="{{ $package->arrival_date }}" id="arrivalDate" class="arrivalDate form-control" placeholder="Not required" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php $packageOptions = explode(',', $package->options ); ?>
            <div class="col-md-2 col-sm-12"></div>
            <div class="col-md-2 col-sm-6">
                <div class="package-option">
                    <div class="form-group">
                        
                        <div class="package-option-image"><div class="icon icon-icon-fastast-16"></div></div>
                        <label for="packageOption-1" style="font-weight: normal;">Giao hàng tận nơi</label>
                        <input type="checkbox" {{ checked( in_array('1', $packageOptions), true) }} name="packageOption[]" value="1" id="packageOption-1" class="packageOption form-control" />
                        <label for="packageOption-1"></label>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="package-option">
                    <div class="form-group">
                        
                        <div class="package-option-image"><div class="icon icon-icon-fastast-17"></div></div>
                        <label for="packageOption-4" style="font-weight: normal;">Hàng dễ vỡ</label>
                        <input type="checkbox" {{ checked( in_array('4', $packageOptions), true) }} name="packageOption[]" value="4" id="packageOption-4" class="packageOption form-control" />
                        <label for="packageOption-4"></label>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="package-option">
                    <div class="form-group">
                        
                        <div class="package-option-image"><div class="icon icon-icon-fastast-18"></div></div>
                        <label for="packageOption-3" style="font-weight: normal;">Bốc xếp</label>
                        <input type="checkbox" {{ checked( in_array('3', $packageOptions), true) }} name="packageOption[]" value="3" id="packageOption-3" class="packageOption form-control" />
                        <label for="packageOption-3"></label>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="package-option">
                    <div class="form-group">
                        
                        <div class="package-option-image"><div class="icon icon-icon-fastast-24"></div></div>
                        <label for="packageOption-2" style="font-weight: normal;">Vùng sâu</label>
                        <input type="checkbox" {{ checked( in_array('2', $packageOptions), true) }} name="packageOption[]" value="2" id="packageOption-2" class="packageOption form-control" />
                        <label for="packageOption-2"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2>Description</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12">
                <div class="form-group">
                    <textarea class="form-control" rows="5" name="packageContent">{{ $package->content }}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <input type="hidden" id="default-camera-image" value="{!! Theme::asset()->url('img/camera-img.png') !!}" />
            <div class="col-md-8 col-md-offset-2 col-sm-12">
                <div class="package-image-item" id="package-image-item-1">
                    <a href="#" class="btn-upload-image" data-type="add" data-id="1"><img src="{!! Theme::asset()->url('img/camera-img.png') !!}" alt="" class="img-responsive"></a>
                    <input type="hidden" name="images[]" />
                    <div class="remove" data-type="remove" data-id="1"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                </div>
                <div class="package-image-item" id="package-image-item-2">
                    <a href="#" class="btn-upload-image" data-type="add" data-id="2"><img src="{!! Theme::asset()->url('img/camera-img.png') !!}" alt="" class="img-responsive"></a>
                    <input type="hidden" name="images[]" />
                    <div class="remove" data-type="remove" data-id="2"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                </div>
                <div class="package-image-item" id="package-image-item-3">
                    <a href="#" class="btn-upload-image" data-type="add" data-id="3"><img src="{!! Theme::asset()->url('img/camera-img.png') !!}" alt="" class="img-responsive"></a>
                    <input type="hidden" name="images[]" />
                    <div class="remove" data-type="remove" data-id="3"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                </div>
                <div class="package-image-item" id="package-image-item-4">
                    <a href="#" class="btn-upload-image" data-type="add" data-id="4"><img src="{!! Theme::asset()->url('img/camera-img.png') !!}" alt="" class="img-responsive"></a>
                    <input type="hidden" name="images[]" />
                    <div class="remove" data-type="remove" data-id="4"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                </div>
                <div class="package-image-item" id="package-image-item-5">
                    <a href="#" class="btn-upload-image" data-type="add" data-id="5"><img src="{!! Theme::asset()->url('img/camera-img.png') !!}" alt="" class="img-responsive"></a>
                    <input type="hidden" name="images[]" />
                    <div class="remove" data-type="remove" data-id="5"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-md-offset-2">
                <div class="row" style="margin-bottom: 20px;">
                    <div class="list-package-images">
                        @if( $package->images->count() > 0)
                            @foreach( $package->images as $image)
                                <div class="col-lg-3 col-md-3 col-sm-6-col-xs-12">
                                    <img src="{{ image_url($image->file_url) }}" alt="" class="img-responsive">
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div id="map"></div>
                <div class="list-locations" id="list-locations"></div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-2 col-sm-12">
                <h3>Contact</h3>
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-4">First - Last name <span class="require">(*)</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="fullName" value="{{ $package->contact_name }}" id="inputName" required placeholder="First - Last name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-sm-4">Email <span class="require">(*)</span></label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="inputEmail" value="{{ $package->contact_email }}" name="email" required placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPhone" class="col-sm-4">Phone Number <span class="require">(*)</span></label>
                        <div class="col-sm-8">
                            <input type="tel" class="form-control" id="inputPhone" value="{{ $package->contact_phone }}" name="phone" required placeholder="Phone">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center btn-groups">
                <input type="submit" class="btn btn-cancel" name="cancel" value="{{ trans('front.cancel') }}">
                <input type="submit" class="btn btn-cancel" name="preview" value="{{ trans('front.preview') }}">
                <input type="submit" class="btn btn-upload" name="confirm" value="{{ trans('front.upload') }}">
            </div>
        </div>
        <div class="row">
            <div class="hidden">
                <input type="file" name="files[]" id="filer-input-1" class="package-image-file">
                <input type="file" name="files[]" id="filer-input-2" class="package-image-file">
                <input type="file" name="files[]" id="filer-input-3" class="package-image-file">
                <input type="file" name="files[]" id="filer-input-4" class="package-image-file">
                <input type="file" name="files[]" id="filer-input-5" class="package-image-file">
                <span class="create-marker-icon"><img src="{!! Theme::asset()->url('img/map-marker.png') !!}"></span>
                <span class="marker-pin-icon"><img src="{!! Theme::asset()->url('img/map-pins.png') !!}"></span>
            </div>
        </div>
    </div><!-- END .container -->
    <style type="text/css">
        .bootstrap-select.form-control.delivery-service{
            width: 96%;
        }
    </style>
{!! Form::close() !!}