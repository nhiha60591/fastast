{!! Form::open(array('url' => 'find-package',  'method' => 'GET', 'id' => 'search-package', 'role'=>'form', 'class'=> 'form form-inline find-package-form')) !!}
<div class="page-header find-package">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="form-inline find-package-form">
                    @if( $services->count() > 0)
                        <?php $i=1; ?>
                        @foreach($services as $service)
                            <div class="form-group">
                                <label>
                                    <input type="radio" value="{{ $service->id }}" name="service" {{ checked($i, 1) }} data-type="{{ $service->listType('type') }}" />
                                    <img src="{!! image_url($service->image) !!}" alt="" class="service-item">
                                </label>
                            </div>
                            <?php $i++; ?>
                        @endforeach
                    @endif
                    <div class="form-group">
                        <select name="delivery" class="form-control">
                            @if( $deliveries->count() > 0)
                                @foreach($deliveries as $delivery)
                                    <option value="{{ $delivery->id }}">{{ $delivery->translation($currentLanguage->id, 'name') }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="date" class="form-control" value="2015-06-15" name="shipperDate" />
                    </div>
                    <div class="form-group">
                        <select name="shipperRelevance" class="form-control">
                            <option value="0">Relevance</option>
                        </select>
                    </div>
                    <div class="form-group pull-right">
                        <a href="{{ url('find-shipping') }}" class="btn btn-default find-package-btn"><div class="icon icon-icon-fastast-01"></div> {{ trans('front.find-shipper') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="list-shipping-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 col-sm-12">
                <div class="list-shipping">
                    <div class="row">
                        @if( $packages->count() )
                            @foreach($packages as $package)
                                <div class="col-lg-4">
                                    <div class="shipping-item" data-lang="" data-lat="">
                                        <a href="{{ url('package/show', $package->id) }}">
                                            <div class="shipping-image">
                                                @if( !empty( $package->images->first()->count() ))
                                                    <img src="{!! image_url($package->images->first()->file_url) !!}" class="img-responsive" alt="{{ $package->image }}">
                                                @else
                                                    <img src="{!! Theme::asset()->url('img/logo.png') !!}" class="img-responsive" alt="{{ $package->user->full_name }}">
                                                @endif
                                                <a class="map-shipping-image-detail" href="{{ url('package/show', $package->id) }}">{{ $package->getCost() }}</a>
                                            </div>
                                        </a>
                                        <div class="row">
                                            <div class="shipping-user-info col-lg-12">
                                                <div class="shipping-user-avatar">
                                                    <a href="{{ url('shipping/show', array('id'=>$package->id)) }}">
                                                        @if( !empty( $package->user->avatar ))
                                                            <img src="{!! image_url($package->user->avatar) !!}" class="img-responsive" alt="{{ $package->user->full_name }}">
                                                        @else
                                                            <img src="{!! Theme::asset()->url('img/shipping-avatar.jpg') !!}" class="img-responsive" alt="{{ $package->user->full_name }}">
                                                        @endif
                                                    </a>
                                                </div>
                                                <div class="shipping-user-name">
                                                    <h4>{{ $package->user->full_name }}</h4>
                                                    {{--<div class="shipping-user-rates">
                                                        <div class="shipping-user-rate" style="width: 50%;"></div>
                                                    </div>--}}
                                                    <div class="package-services">
                                                        @if( $package->services->count() > 0)
                                                            @foreach($package->services as $service)
                                                                <img src="{!! image_url($service->image) !!}" alt="" class="image-service">
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="shipping-info col-lg-12">
                                                <div class="col-lg-12">
                                                    <p>Shipping: {!! $package->getShippingTime() !!}</p>
                                                    <p>Departure: {{ $package->departure_date ? date('d/m/Y', strtotime($package->departure_date) ) : 'Not Require' }}</p>
                                                    <p>Arrival: {{ $package->arrival_date ? date('d/m/Y', strtotime($package->arrival_date) ) : 'Not Require' }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- END .shipping-item -->
                                </div><!-- END .col-lg-4 -->
                            @endforeach
                        @else
                            <div class="col-lg-12 text-center">
                                <h2>{{ trans('front.no-shipping') }}</h2>
                            </div>
                        @endif
                    </div><!-- END .row -->
                    <div class="row text-center">
                        {{ $packages->links() }}
                    </div>
                </div><!-- END .list-shipping -->
            </div>
            <div class="col-lg-5 col-sm-12">
                <div id="map"></div>
                <script type="text/javascript">
                    var locations = [@if( $packages->count() )
                        @foreach($packages as $package)
                            ['{{ $package->getCost() }}', '{{ $package->user->full_name }}', {{ $package->getLocations() }}, '#location-item-{{ $package->id }}'],
                        @endforeach
                        @endif
                    ];
                </script>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
<div class="hidden">
    <input type="text" class="from form-control" id="maps-from" value="{{ $_REQUEST['from'] or '' }}">
    <input type="text" class="to form-control" id="maps-to" value="{{ $_REQUEST['to'] or '' }}">
</div>
<div class="hidden">
    @if( $packages->count() )
        @foreach( $packages as $package )
            <div id="location-item-{{ $package->id }}">
                <div class="box_title">
                    <div class="map-shipping-image">
                        @if( !empty( $package->images->first()->count() ))
                            <img src="{!! image_url($package->images->first()->file_url) !!}" class="img-responsive" alt="{{ $package->image }}">
                        @else
                            <img src="{!! Theme::asset()->url('img/logo.png') !!}" class="img-responsive" alt="{{ $package->user->full_name }}">
                        @endif
                            <a class="map-shipping-image-detail" href="{{ url('package/show', $package->id) }}">{{ $package->getCost() }}</a>
                    </div>
                    <div class="map-shipping-info">
                        <div class="row">
                            <div class="col-lg-8">
                                <p>{!! $package->getShippingTime() !!}</p>
                            </div>
                            <div class="col-lg-4">
                                <p>
                                    @if( $package->services->count() > 0)
                                        @foreach($package->services as $service)
                                            <img src="{!! image_url($service->image) !!}" alt="" class="image-service">
                                        @endforeach
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <span class='trig_arrow'></span>
            </div>
        @endforeach
    @endif
</div>