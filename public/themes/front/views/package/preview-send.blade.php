<div class="container-fluid container-create container-package">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12"><h1>{{ trans('front.shipping-information') }}</h1></div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><h2><span class="color-orange">{{ $booking->track_number }}</span></h2></div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><h2>{{ date('d F, Y', strtotime( $booking->created_at ) ) }}</h2></div>
    </div>
    <?php $searchParams = unserialize( $booking->service_info ); ?>
    <div class="row">
        <div class="col-md-8 col-sm-12">
            <h2>{!! $shipping->service->translation($currentLanguage->id, 'name') !!}</h2>
            <p>{{ trans('front.shipping-time') }}: {{ $shipping->delivery->translation($currentLanguage->id, 'name') }}</p>
            <p>{{ trans('front.departure-date') }}: July 1, 2016</p>
            <p>{{ trans('front.arrival-date') }}: July 30, 2016</p>
            <div class="ways">
                <div class="ways-item way-{{ $shipping->service->id }}">
                    <p class="from">{{ trans('front.from') }}: <br>
                        <strong>{{ $searchParams['from'] or $shipping->ways->first()->from }}</strong>
                    </p>

                    <img class="img-responsive" src="{!! Theme::asset()->url('img/'.$shipping->getWayImage().'.png') !!}" alt="">
                    <p class="to">{{ trans('front.to') }}: <br>
                        <strong>{{ $searchParams['to'] or $shipping->ways->first()->to }}</strong>
                    </p>
                </div>
            </div>
            <div class="row row2 package-detail">
                <div class="row two">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="package-requirement-item">
                                <div class="col-xs-12"><p class="title">{{ trans('front.door-toDoor') }}</p></div>
                                <div class="col-xs-6">
                                    <p class="text-uppercase package-option-title">
                                        {!! isset($searchParams['packageType']) && is_array($searchParams['packageType']) && in_array('door-to-door', $searchParams['packageType']) ? trans('front.yes') : trans('front.no') !!}
                                    </p>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <div class="icon icon-icon-fastast-16"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="package-requirement-item">
                                <div class="col-xs-12"><p class="title">{{ trans('front.fragilePackage') }}</p></div>
                                <div class="col-xs-6">
                                    <p class="text-uppercase package-option-title">
                                        {!! isset($searchParams['packageType']) && is_array($searchParams['packageType']) && in_array('fragile-package', $searchParams['packageType']) ? trans('front.yes') : trans('front.no') !!}
                                    </p>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <div class="icon icon-icon-fastast-17"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="package-requirement-item">
                                <div class="col-xs-12"><p class="title">{{ trans('front.loading-unloading') }}</p></div>
                                <div class="col-xs-6">
                                    <p class="text-uppercase package-option-title">
                                        {!! isset($searchParams['packageType']) && is_array($searchParams['packageType']) && in_array('loading-unloading', $searchParams['packageType']) ? trans('front.yes') : trans('front.no') !!}
                                    </p>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <div class="icon icon-icon-fastast-18"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 pull-right">
            <div class="top row" style="border-bottom: 1px solid #cdcdcd;margin-bottom: 20px;">
                <p class="text-center">{{ trans('front.totalShippingCost') }}</p>
                <h3 class="text-center"><span class="unit-currency">VND</span> {{ $shipping->costOnMaps(session('packageSize'), session('type')) }}</h3>
            </div>
            <div class="detail">
                <p>{{ trans('front.rate-includes') }}</p>
                <ul class="include">
                    <li><p>{{ trans('front.freight-rate-include-vat') }}</p></li>
                    <li><p>{{ trans('front.fragile-package-surcharge-if-any') }}</p></li>
                    <li><p>{{ trans('front.loading-unloading-surcharge-if-any') }}</p></li>
                    <li><p>{{ trans('front.fuel-surcharge') }}</p></li>
                    <li><p>{{ trans('front.service-fees') }}</p></li>
                </ul>
                <p>{!! trans('front.shipping-show-other') !!}</p>
                <p class="text-right"><a href="" class="btn btn-primary text-uppercase print-btn">Print (form A4)</a></p>
            </div>
        </div>
    </div>
    <hr width="100%" height="1" color="#cdcdcd" />
    <div class="row">
        <div class="col-md-7 col-sm-12">
            <div class="row">
                @if( $booking->images->count() > 0)
                    @foreach( $booking->images as $image)
                        <div class="col-md-6 col-xs-12">
                            <img src="{!! image_url($image->file_url) !!}" alt="" class="img-responsive">
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="col-md-5 col-sm-12">
            <h2 class="shipping-show-title text-capitalize">{{ trans('front.package-measurement') }}</h2>
            <div class="package-box-detail">
                @if(isset($searchParams['packageSize']['KLTT']) && is_array($searchParams['packageSize']['KLTT']) && sizeof($searchParams['packageSize']['KLTT']) > 0 )
                    <?php $i=1; ?>
                    @foreach($searchParams['packageSize']['KLTT'] as $package)
                        <p>{{ trans('front.package', ['number'=> $i]) }}: {{ $package['width'] }}x{{ $package['length'] }}x{{ $package['height'] }}cm {{ trans('front.quantity', ['number'=>$package['quantity']]) }}</p>
                        <?php $i++; ?>
                    @endforeach
                @endif
                <p>{{ trans('front.total-gross-weight') }}: {{ @$searchParams['packageSize']['totalCost'] }}kg</p>
            </div>
            <h2 class="shipping-show-title text-capitalize">{{ trans('front.package-containing') }}</h2>
            <div class="package-box-detail">{!! @$booking->containing !!}</div>
            <h2 class="shipping-show-title text-capitalize">{{ trans('front.shipping-requirements') }}</h2>
            <div class="package-box-detail">
                <?php
                    $requirements = isset($booking->requirements) ? unserialize($booking->requirements) : array();
                    $requirements = is_array($requirements) ? $requirements : array();
                ?>
                <div class="package-requirement">
                    <ul class="list-group">
                        <li class="list-group-item">
                        <span class="right-input">
                            <input type="checkbox" {{ checked(in_array(1, $requirements), 1) }} id="packageRequirement-1" name="packageRequirement[]" value="1">
                            <label for="packageRequirement-1"></label>
                        </span>
                            <label for="packageRequirement-1">
                                I. {{ trans('front.issue-invoice') }}
                            </label>
                        </li>
                        <li class="list-group-item">
                        <span class="right-input">
                            <input type="checkbox" {{ checked(in_array(2, $requirements), 1) }} id="packageRequirement-2" name="packageRequirement[]" value="2">
                            <label for="packageRequirement-2"></label>
                        </span>
                            <label for="packageRequirement-2">
                                II. {{ trans('front.delivery-guaranteed') }}
                            </label>
                        </li>
                        <li class="list-group-item">
                        <span class="right-input">
                            <input type="checkbox" {{ checked(in_array(3, $requirements), 1) }} id="packageRequirement-3" name="packageRequirement[]" value="3">
                            <label for="packageRequirement-3"></label>
                        </span>
                            <label for="packageRequirement-3">
                                III. {{ trans('front.cash-on-delivery') }}
                            </label>
                        </li>
                        <li class="list-group-item">
                        <span class="right-input">
                            <input type="checkbox" {{ checked(in_array(4, $requirements), 1) }} id="packageRequirement-4" name="packageRequirement[]" value="4">
                            <label for="packageRequirement-4"></label>
                        </span>
                            <label for="packageRequirement-4">
                                IV. {{ trans('front.shipping-insurance') }}
                            </label>
                        </li>
                    </ul>
                </div><!-- END .package-requirement -->
            </div>
        </div>
    </div>
    <hr width="100%" height="1" color="#cdcdcd" />
    <div class="row">
        <div class="col-lg-12">
            <h2 class="shipping-show-title text-capitalize">{{ trans('front.contact') }}</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <h3>{{ trans('front.Origination') }}</h3>
            <div class="contact-detail-box">
                <?php $origination = unserialize($booking->origination) ?>
                <p>{{ trans('front.contact-name') }}: {!! $origination['name'] !!}</p>
                <p>{{ trans('front.company-name') }}: {!! $origination['company'] !!}</p>
                <p>{{ trans('front.phone-number') }}: {!! $origination['phone'] !!}</p>
                <p>{{ trans('front.email') }}: {!! $origination['email'] !!}</p>
                <p>{{ trans('front.address') }}: {!! $origination['address'] !!}</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <h3>{{ trans('front.Destination') }}</h3>
            <div class="contact-detail-box">
                <?php $destination = unserialize($booking->destination) ?>
                <p>{{ trans('front.contact-name') }}: {!! $destination['name'] !!}</p>
                <p>{{ trans('front.company-name') }}: {!! $destination['company'] !!}</p>
                <p>{{ trans('front.phone-number') }}: {!! $destination['phone'] !!}</p>
                <p>{{ trans('front.email') }}: {!! $destination['email'] !!}</p>
                <p>{{ trans('front.address') }}: {!! $destination['address'] !!}</p>
            </div>
        </div>
    </div><!-- END CONTACT SECTION -->
    <div class="row">
        <div class="col-lg-12">
            <h2 class="shipping-show-title text-capitalize">{{ trans('front.shipper') }}</h2>
            <p>{{ trans('front.contact-name') }}: {!! $shipping->user->full_name !!}</p>
            <p>{{ trans('front.company-name') }}: {!! $shipping->user->company !!}</p>
            <p>{{ trans('front.phone-number') }}: {!! $shipping->user->phone !!}</p>
            <p>{{ trans('front.email') }}: {!! $shipping->user->email !!}</p>
            <p>{{ trans('front.address') }}: {!! $shipping->user->address !!}</p>
        </div>
    </div>
    <div class="row">
        {!! Form::open(array('url' => 'package/confirm-booking/'.$booking->id.'/'.$shipping->id,  'method' => 'POST', 'id' => 'post-package', 'files'=>true, 'role'=>'form', 'class'=> 'form')) !!}
        <div class="col-md-12 text-center btn-groups">
            <input type="submit" class="btn btn-cancel" name="cancel" value="{{ trans('front.cancel') }}">
            <input type="submit" class="btn btn-cancel" name="back" value="{{ trans('front.back') }}">
            <input type="submit" class="btn btn-upload" name="confirm" value="{{ trans('front.confirm') }}">
        </div>
        {!! Form::close() !!}
    </div>
</div>