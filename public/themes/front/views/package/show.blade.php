<figure class="container-fluid banner">
    <div class="row">
        <img class="img-responsive" src="{{ image_url(!empty($package->user->banner) ? $package->user->banner : 'images/banner-default.jpg') }}" alt="">
    </div>
    <div class="packer container">
        <div class="row">
            <div class="col-md-3">
                <div class="package-avatar">
                    <img  class="person img-responsive" src="{{ image_url(!empty($package->user->avatar) ? $package->user->avatar : 'images/avatar-default.jpg') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="package-banner-info">
                    <h2>{{ $package->user->full_name }}</h2>
                    <p>{!! trans('front.record') !!}: 10 {!! trans('front.listings') !!}</p>
                </div>
            </div>
        </div>
    </div>
</figure>
<main class="container-fluid">
    <div class="row row1">
        <div class="col-md-6 col-md-offset-1">
            @if( $package->services->count() > 0)
                <?php $i=1; ?>
                @foreach($package->services as $service)
                    <div class="row">
                        <div class="col-md-4">
                            @if( $i>1 )
                                <h4 class="ways-or">{!! trans('front.or') !!}</h4>
                            @endif
                            <h3>{!! $service->translation($currentLanguage->id, 'name') !!}</h3>
                        </div>
                        <div class="col-md-8 maping">
                            <p class="ways-item way-{{ $service->id }}"><img src="{!! Theme::asset()->url('img/'.$service->serviceWayImage().'.png') !!}" class="img-responsive" alt=""></p>
                            <p class="from">{{ trans('front.from') }}: <br>
                                <strong>{{ $package->origination }}</strong>
                            </p>
                            <p class="to">{{ trans('front.to') }}: <br>
                                <strong>{{ $package->destination }}</strong>
                            </p>
                        </div>
                    </div>
                    <?php $i++; ?>
                @endforeach
            @endif

        </div>
        <div class="col-md-4 total ">
            <div class="top row">
                <div class="price col-md-12 text-center">
                    <p>{!! trans('front.total-shipping-cost-detail') !!}</p>
                    <p><strong>{{ $package->getCost() }}</strong></p>
                </div>
                <div class="packet col-md-10 col-md-offset-1">
                    <p>{!! trans('front.shipping-time') !!}: {!! $package->getShippingTime() !!}</p>
                    <p>{!! trans('front.departure-date') !!}: {{ $package->departure_date ? date('M d, Y', strtotime($package->departure_date) ) : 'Not Require' }}</p>
                    <p>{!! trans('front.arrival-date') !!}: {{ $package->arrival_date ? date('M d, Y', strtotime($package->arrival_date) ) : 'Not Require' }}</p>
                </div>
            </div>

        </div>
    </div><!---End .packet-row1-->
    <div class="row row2 packet-detail">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-md-3">
                    <div class="package-requirement-item">
                        <div class="col-xs-12"><p class="title">{!! trans('front.door-toDoor') !!}</p></div>
                        <div class="col-xs-6">
                            <p class="text-uppercase package-option-title">
                                {!! in_array(1, $package->getOptions()) ? trans('front.yes') : trans('front.no') !!}
                            </p>
                        </div>
                        <div class="col-xs-6 text-right">
                            <div class="icon icon-icon-fastast-16"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="package-requirement-item">
                        <div class="col-xs-12"><p class="title">{!! trans('front.fuel-surcharge') !!}</p></div>
                        <div class="col-xs-6">
                            <p class="text-uppercase package-option-title">
                                {!! in_array(2, $package->getOptions()) ? trans('front.yes') : trans('front.no') !!}
                            </p>
                        </div>
                        <div class="col-xs-6 text-right">
                            <div class="icon icon-icon-fastast-24"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="package-requirement-item">
                        <div class="col-xs-12"><p class="title">{!! trans('front.fragilePackage') !!}</p></div>
                        <div class="col-xs-6">
                            <p class="text-uppercase package-option-title">
                                {!! in_array(3, $package->getOptions()) ? trans('front.yes') : trans('front.no') !!}
                            </p>
                        </div>
                        <div class="col-xs-6 text-right">
                            <div class="icon icon-icon-fastast-18"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="package-requirement-item">
                        <div class="col-xs-12"><p class="title">{!! trans('front.loading-unloading') !!}</p></div>
                        <div class="col-xs-6">
                            <p class="text-uppercase package-option-title">
                                {!! in_array(4, $package->getOptions()) ? trans('front.yes') : trans('front.no') !!}
                            </p>
                        </div>
                        <div class="col-xs-6 text-right">
                            <div class="icon icon-icon-fastast-17"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!---End .row2-->
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div id="map"></div>
            <script type="text/javascript">
                var locations = [];
                @if( $package->locations->count() )
                    @foreach( $package->locations  as $location)
                        locations.push([{{ $location->lng }}, {{ $location->lat }}]);
                    @endforeach
                @endif
            </script>
        </div>
        <div class="hidden">
            <span class="marker-pin-icon"><img src="{!! Theme::asset()->url('img/map-pins.png') !!}"></span>
        </div>
    </div>
    <div class="row row3">
        <div class="col-md-6 col-md-offset-1">
            <div class="Slider">
                <div class="slider-for row text-center">
                    @if( !empty( $package->images->count() ))
                        @foreach($package->images as $image)
                            <div class="col-xs-12"><img src="{!! image_url($image->file_url) !!}" class="img-responsive" alt="{{ $image->caption }}"></div>
                        @endforeach
                    @endif
                </div>
                <div class="slider-nav">
                    @if( !empty( $package->images->count() ))
                        @foreach($package->images as $image)
                            <div class="package-image-item"><img src="{!! image_url($image->file_url) !!}" class="img-responsive" alt="{{ $image->caption }}"></div>
                        @endforeach
                    @endif
                </div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="package-contains-requirement">
                <h3>{!! trans('front.package-contains-requirement') !!}</h3>
                <div class="package-contains-requirement-text">
                    {!! $package->content !!}
                </div>
            </div>
            <div class="">
                <h3>{!! trans('front.contact') !!}</h3>
                <div class="row">
                    <div class="col-md-4 text-left">
                        <p>{!! trans('front.sender-name') !!}</p>
                        <p>{!! trans('front.phone-number') !!}</p>
                        <p>{!! trans('front.email') !!}</p>
                        <p>{!! trans('front.address') !!}</p>
                    </div>

                    <div class="col-xs-8 text-left">
                        <p><strong>{{ $package->contact_name }}</strong></p>
                        @if( Auth::check() )
                            <p>{{ $package->contact_phone }}</p>
                            <p>{{ $package->contact_email }}</p>
                            <p>{{ $package->origination }}</p>
                        @else
                            <div class="contact-alert">
                                <p>{!! trans('front.see-contact-if-login', ['link' => url('register')]) !!}</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
//        dots: true,
//        centerMode: true,
        focusOnSelect: true
    });
</script>