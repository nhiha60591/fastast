<div class="row">
    <div class="banner-user">
        <form action="demo_form.asp">
            <input type="file" name="pic" accept="image/*">
        </form>
        <img src="{!! Theme::asset()->url('img/user-banner.png') !!}" class="img-responsive" alt="...">
        <div class="user-rate col-md-9 col-lg-offset-3">
            <img src="" alt="">
            <div class="">
                <h3>Your Name</h3>
                <p class="rate"></p>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-2">
        <img src="{!! Theme::asset()->url('img/user.png') !!}" class="img-responsive" alt="...">
    </div>
    <div class="col-md-5">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-5" for="who_you_are">You're<span class="required">(*)</span></label>
                <div class="col-sm-7">
                    <select name="who_you_are" id="who_you_are">
                        <option value="">Company</option>
                        <option value="">Individual</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-5" for="first_last_name">First-Last Name <span class="required">(*)</span></label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" id="first_last_name" placeholder="Enter password">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-5" for="nationality">Nationality</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" id="nationality" placeholder="Enter password">
                </div>
            </div>


        </div>
    </div>
    <div class="col-md-3 col-lg-offset-2">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-3" for="viber"><span class="glyphicon glyphicon-bed"></span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="viber" placeholder="Phone Number">
                </div>
            </div>
        </div>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-3" for="what_app"><span class="glyphicon glyphicon-bed"></span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="what_app" placeholder="Phone Number">
                </div>
            </div>
        </div>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-3" for="line"><span class="glyphicon glyphicon-bed"></span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="line" placeholder="Phone Number">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <form id="edit-user" class="form-horizontal"
              action="{{ url('user/update-profile') }}" method="post"
              data-toggle="validator">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-sm-2 control-label">{{ trans('admin.email') }}</label>

                    <div class="col-sm-10">
                        <input type="email" value="{{ $user->email }}" readonly="readonly" name="email"
                               class="form-control" id="email" placeholder="{{ trans('admin.email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                    <label for="first-name" class="col-sm-2 control-label">{{ trans('admin.first_name') }}</label>

                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->first_name }}" name="first_name" class="form-control"
                               id="first-name" placeholder="{{ trans('admin.first_name') }}">
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label for="last-name" class="col-sm-2 control-label">{{ trans('admin.last_name') }}</label>

                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->last_name }}" name="last_name" class="form-control"
                               id="last-name" placeholder="{{ trans('admin.last_name') }}">
                        @if ($errors->has('last_name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                    <label for="full-name" class="col-sm-2 control-label">{{ trans('admin.full_name') }}</label>

                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->full_name }}" name="full_name" class="form-control"
                               id="full-name" placeholder="{{ trans('admin.full_name') }}">
                        @if ($errors->has('full_name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('full_name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">{{ trans('admin.address') }}</label>

                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->address }}" name="address" class="form-control"
                               id="address" placeholder="{{ trans('admin.address') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">{{ trans('admin.phone') }}</label>

                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->phone }}" name="phone" class="form-control" id="phone"
                               placeholder="{{ trans('admin.phone') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="gender" class="col-sm-2 control-label">{{ trans('admin.gender') }}</label>

                    <div class="col-sm-10">
                        <select name="gender" class="form-control" id="gender">
                            <option value="0">{{ trans('admin.male') }}</option>
                            <option value="1">{{ trans('admin.female') }}</option>
                            <option value="2">{{ trans('admin.other') }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="guarantee_policy" class="col-sm-2 control-label">{{ trans('front.guarantee-policy') }}</label>

                    <div class="col-sm-10">
                            <textarea type="text" name="guarantee_policy" class="form-control" id="guarantee_policy"
                                      placeholder="{{ trans('front.guarantee-policy') }}">{{ $user->guarantee_policy }}</textarea>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-sm-2 control-label">{{ trans('admin.password') }}</label>

                    <div class="col-sm-10">
                        <input type="password" name="password" class="form-control" id="phone"
                               placeholder="{{ trans('admin.password') }}">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password_confirmation"
                           class="col-sm-2 control-label">{{ trans('admin.confirm_password') }}</label>

                    <div class="col-sm-10">
                        <input type="password" name="password_confirmation" class="form-control"
                               id="password_confirmation" placeholder="{{ trans('admin.confirm_password') }}">
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">{{ trans('admin.update') }}</button>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
</div>