<div class="container-fluid">
    <nav class="navbar">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-left" action="" method="get">
                <div class="form-group">

                </div>
                <div class="form-group">
                    {!! Form::label('from_date', trans('front.from_date')) !!}
                    {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
                </div>
                <div class="form-group">
                    {!! Form::label('to_date', trans('front.to_date')) !!}
                    {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
                </div>
                <div class="form-group">
                    <input class="btn btn-primary" type="submit" value="{{ trans('front.search') }}"/>
                </div>
            </form>
        </div><!-- /.navbar-collapse -->
    </nav>
    <div class="row">
        <h2 class="col-md-8" style="margin-left: 30px">All Statement View</h2>
    </div>
    <div class="container payment-wrap">
        <div class="row">
            <div class="col-md-10 col-lg-offset-1">
                <div class="row border-bottom">
                    <div class="col-md-10"><strong>Delivered package</strong></div>
                    <div class="col-md-2"><strong>{{ $deliveredPackage }}</strong></div>
                </div>
                <div class="row">
                    <div class="col-md-10"><strong>Sender's fee</strong></div>
                    <div class="col-md-2"><span>{{ $senderFee }}</span></div>
                </div>
                <div class="row">
                    <div class="col-md-10"><strong>Shipper's fee</strong></div>
                    <div class="col-md-2"><span>{{ $shipperFee }}</span></div>
                </div>
                <div class="row border-bottom">
                    <div class="col-md-10"><span>Tax (VAT)</span></div>
                    <div class="col-md-2"><span>{{ $taxFee }}</span></div>
                </div>
                <div class="row">
                    <div class="col-md-10"><strong>Close balance</strong></div>
                    <div class="col-md-2"><strong>{{ $closeBalance }}</strong></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p></p>
            </div>
        </div>
    </div>
</div>