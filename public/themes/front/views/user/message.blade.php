<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
            {!! Theme::partial('chat-message-sidebar', ['inboxCount'=>$inboxCount]) !!}
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="main-mail">
                <ul class="media-list">
                @foreach($inbox as $ib)
                    @if( $ib->to == Auth::user()->id && $ib->read > 0)
                    <li class="media">
                        <div class="media-left">
                            <a href="{{ url('user/chat-message', $ib->conversation_id) }}">
                            @if( !empty( $ib->toUser->avatar ))
                                <img src="{!! image_url($ib->toUser->avatar) !!}" class="img-circle img-responsive" alt="{{ $ib->toUser->getFullName() }}">
                            @else
                                <img src="http://caselaw.vn/img/no-avt.jpg" class="img-circle img-responsive" alt="{{ $ib->toUser->getFullName() }}">
                            @endif
                            </a>
                        </div>
                        <div class="media-body">
                            <a href="{{ url('user/chat-message', $ib->conversation_id) }}">
                                <h4 class="media-heading">{{ $ib->toUser->getFullName() }}</h4>
                                <p>{{ $ib->body }}</p>
                            </a>
                        </div>
                    </li>
                    @else
                    <li class="media no-read">
                        <div class="media-left">
                            <a href="{{ url('user/chat-message', $ib->conversation_id) }}">
                            @if( !empty( $ib->toUser->avatar ))
                                <img src="{!! image_url($ib->toUser->avatar) !!}" class="img-circle img-responsive" alt="{{ $ib->toUser->getFullName() }}">
                            @else
                                <img src="http://caselaw.vn/img/no-avt.jpg" class="img-circle img-responsive" alt="{{ $ib->toUser->getFullName() }}">
                            @endif
                            </a>
                        </div>
                        <div class="media-body">
                            <a href="{{ url('user/chat-message', $ib->conversation_id) }}">
                                <h4 class="media-heading">{{ $ib->toUser->getFullName() }}</h4>
                                <p>{{ $ib->body }}</p>
                            </a>
                        </div>
                    </li>
                    @endif
                @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

        </div>
    </div>
    {!! Theme::partial('chat-message-footer', ['users'=>$users]) !!}
</div>
