<nav class="navbar">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <form class="navbar-form navbar-left" action="" method="get">
            <div class="form-group">
                <div class="inner-addon right-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <input type="text" name="search" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('from_date', trans('front.from_date')) !!}
                {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                {!! Form::label('to_date', trans('front.to_date')) !!}
                {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                <input class="btn btn-primary" type="submit" value="{{ trans('front.search') }}"/>
            </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('user/manage-export', array( 'type'=>'listing', 'ext'=>'pdf' ) ) }}"><img src="{!! Theme::asset()->url('img/pdf-flat.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="{{ url('user/manage-export', array( 'type'=>'listing', 'ext'=>'xls' ) ) }}"><img src="{!! Theme::asset()->url('img/excel-icon.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="javascript:window.print()"><img src="{!! Theme::asset()->url('img/print-icon.png') !!}" width="30" class="img-responsive"></a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>

<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>{{ trans('front.date') }}</th>
                <th>{{ trans('front.groupt_listing') }}</th>
                <th>{{ trans('front.from') }}</th>
                <th>{{ trans('front.to') }}</th>
                <th>{{ trans('front.service') }}</th>
                <th>{{ trans('front.shipping_time') }}</th>
                <th>{!! trans('front.minimun_rate') !!}</th>
                <th>{!! trans('front.status_rate') !!}</th>
                <th>{{ trans('front.manager') }}</th>
            </tr>
            </thead>
            <tbody>

                @if($shippings->count())
                    @foreach($shippings as $shipping)
                    <tr class="text-center">
                    <td>{{ date('M, d Y', strtotime($shipping->created_at)) }}</td>
                    <td class="shipping-group-item">
                        <div class="shipping-title">{{ $shipping->title }}</div>
                        <div class="hidden change-group-name">
                            {!! Form::open(array('url' => 'user/manage-listing-update/'.$shipping->id,  'method' => 'POST', 'role'=>'form', 'class'=> 'form update-shipping-title')) !!}
                            <input type="text" class="form-control" name="title" value="{{ $shipping->title }}">
                            <input type="hidden" class="form-control" name="shipping_id" value="{{ $shipping->id }}">
                            {!! Form::close() !!}
                        </div>
                    </td>
                    <td>{!! @$shipping->ways()->first()->from !!}</td>
                    <td>{!! @$shipping->ways()->first()->to !!}</td>
                    <td><img src="{!! image_url(@$shipping->service->image)  !!}" /></td>
                    <td>{!! @$shipping->delivery()->first()->translation($currentLanguage->id, 'name') !!}</td>
                    <td>Enverlope</td>
                    <td>Negotiation</td>
                    <td>
                        <div class="dropdown">
                            <a class="btn btn-secondary dropdown-toggle" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                View Detail
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{!! url('shipping/show', $shipping->id) !!}">View Detail</a>
                                <a class="dropdown-item" href="{!! url('shipping/edit', $shipping->id) !!}">Edit</a>
                                <a class="dropdown-item" href="{!! url('shipping/make-copy', $shipping->id) !!}">Make a copy</a>
                                <a class="dropdown-item" href="{!! url('shipping/unlist-relist', $shipping->id) !!}">{!! $shipping->status == 'yes' ? 'Unlist' : 'Relist' !!}</a>
                                <a class="dropdown-item" href="{!! url('shipping/delete', $shipping->id) !!}">Deleted</a>
                            </div>
                        </div>
                    </td>
                </tr>
                    @endforeach
                @else
                <tr class="text-center">
                    <td colspan="9">List of shipping rate not found!</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            {!! $shippings->links() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.update-shipping-title').submit(function(){
            var dis = $(this);
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: {title: $(this).find('input[name="title"]').val(), id: $(this).find('input[name="title"]').val()},
                success: function( msg ) {
                    dis.parent().parent().find('.shipping-title').removeClass('hidden').html(dis.find('input[name="title"]').val());
                    dis.parent('.change-group-name').addClass('hidden');
                }
            });
            return false;
        });
        $('.shipping-group-item').click(function(){
            $(this).find('.shipping-title').addClass('hidden');
            $(this).find('.change-group-name').removeClass('hidden');
            return false;
        });
    });
</script>