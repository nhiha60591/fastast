<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
            {!! Theme::partial('chat-message-sidebar', ['inboxCount'=>$inboxCount]) !!}
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="main-mail">
                <ul class="media-list">
                    <li class="media">
                        <div class="media-left">
                            @if( !empty( Auth::user()->avatar ))
                                <img src="{!! image_url(Auth::user()->avatar) !!}" class="img-circle img-responsive" alt="{{ Auth::user()->getFullName() }}">
                            @else
                                <img src="http://caselaw.vn/img/no-avt.jpg" class="img-circle img-responsive" alt="{{ Auth::user()->getFullName() }}">
                            @endif
                        </div>
                        <div class="media-body">
                            {!! Form::open(array('url' => 'user/chat-message/'.$id,  'method' => 'POST', 'id' => 'post-message', 'files'=>true, 'role'=>'form', 'class'=> 'form')) !!}
                            <div class="form-group">
                                <textarea name="message" id="message" class="form-control" rows="4"></textarea>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </li>
                    @foreach($messages as $message)
                        @if( $message->from == Auth::user()->id)
                            <li class="media me">
                                <div class="media-body text-right">
                                    <p>{{ $message->body }}</p>
                                </div>
                                <div class="media-right">
                                    @if( !empty( $message->fromUser->avatar ))
                                        <img src="{!! image_url($message->fromUser->avatar) !!}" class="img-circle img-responsive" alt="{{ $message->fromUser->getFullName() }}">
                                    @else
                                        <img src="http://caselaw.vn/img/no-avt.jpg" class="img-circle img-responsive" alt="{{ $message->fromUser->getFullName() }}">
                                    @endif
                                </div>
                            </li>
                        @else
                            <li class="media">
                                <div class="media-left">
                                    @if( !empty( $message->fromUser->avatar ))
                                        <img src="{!! image_url($message->fromUser->avatar) !!}" class="img-circle img-responsive" alt="{{ $message->fromUser->getFullName() }}">
                                    @else
                                        <img src="http://caselaw.vn/img/no-avt.jpg" class="img-circle img-responsive" alt="{{ $message->fromUser->getFullName() }}">
                                    @endif
                                </div>
                                <div class="media-body">
                                    <p>{{ $message->body }}</p>
                                </div>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {!! $messages->links() !!}
        </div>
    </div>
    {!! Theme::partial('chat-message-footer', ['users'=>$users]) !!}
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $("#message").keyup(function(e){
            var code = e.which; // recommended to use e.which, it's normalized across browsers
            if(code==13) $("#post-message").submit();
        });
    });
</script>