<div class="row search-wrap">
    <div class="col-lg-12">
        <div class="row ">
            <div class="col-md-3">
                <div class="search">
                    <div class="inner-addon right-addon">
                        <i class="glyphicon glyphicon-search"></i>
                        <input type="text" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row form-inline">
                    <div class="col-md-4 form-inline">
                        {!! Form::label('from_date', trans('front.from_date')) !!}
                        {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="col-md-4 form-group">
                        {!! Form::label('to_date', trans('front.to_date')) !!}
                        {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
        {!! Form::open(array('url' => 'user/manage-shipping',  'method' => 'POST', 'id' => 'manage-shipping', 'class'=> 'form-inline', 'role'=>'form')) !!}
        <div class="form-group">


        </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="row">
    <h2 class="col-md-8" style="margin-left: 30px">All Statement View</h2>
</div>


<div class="container payment-wrap">
    <div class="row">
        <div class="col-md-10 col-lg-offset-1">
            <div class="col-1">
                <div class="row col">
                    <div class="col-md-4">
                        <strong>Begining Balance</strong>
                    </div>
                    <div class="col-md-5">
                        <strong>Previous statement balance</strong>
                    </div>
                    <div class="col-md-3 text-right">
                        <p>1,000,000,000</p>
                        <p>VND</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Transaction Payment</strong>
                    </div>
                    <div class="col-md-5">
                        <strong>Jan 12, 2016</strong>
                    </div>
                    <div class="col-md-3 text-right">
                        <p>1.508,000,000</p>
                    </div>
                </div>
            </div>
            <div class="col-2 col">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-5">
                        <strong>Total:</strong>
                    </div>
                    <div class="col-md-3 text-right">
                        <p><strong>2,508,000,000</strong></p>
                        <p><strong>VND</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Service fees</strong>
                    </div>
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-3 text-right">
                        <p><strong>7,340,000 VND</strong></p>
                    </div>
                </div>
            </div>

            <div class="col-3 col">
                <div class="row">
                    <div class="col-md-4">
                        <p>Refunds of cancellation</p>
                    </div>
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-3 text-right">
                        <p>0.00</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Close Balance</strong>
                    </div>
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-3 text-right">
                        <p><strong>2,500,660,000</strong></p>
                        <p><strong>VND</strong></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>