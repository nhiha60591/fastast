<div class="row search-wrap">
    <div class="col-lg-12">
        <div class="row ">
            <div class="col-md-3">
                <div class="search">
                    <div class="inner-addon right-addon">
                        <i class="glyphicon glyphicon-search"></i>
                        <input type="text" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row form-inline">
                    <div class="col-md-4 form-inline">
                        {!! Form::label('from_date', trans('front.from_date')) !!}
                        {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="col-md-4 form-group">
                        {!! Form::label('to_date', trans('front.to_date')) !!}
                        {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
        {!! Form::open(array('url' => 'user/manage-shipping',  'method' => 'POST', 'id' => 'manage-shipping', 'class'=> 'form-inline', 'role'=>'form')) !!}
        <div class="form-group">


        </div>
        {!! Form::close() !!}
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>{!! trans('front.shipper') !!}</th>
                    <th>{!! trans('front.shipper_id') !!}</th>
                    <th>{!! trans('front.type') !!}</th>
                    <th>{!! trans('front.sender_fees_1') !!}</th>
                    <th>{!! trans('front.transaction_fees') !!}</th>
                    <th>{!! trans('front.card_value') !!}</th>
                    <th>{!! trans('front.card_value_remaining') !!}(4)=(3)-(1)-(2)</th>
                    <th>{!! trans('front.fastast_payment') !!} (5)=(1)+(2)</th>
                    <th>{!! trans('front.payment_status') !!}</th>
                    <th>{!! trans('front.manager') !!}</th>
                </tr>
                </thead>
                <tbody>
                    <tr class="text-center">
                        <td class="text-center">1</td>
                        <td>VietPost</td>
                        <td>XAZ1234BNnmb</td>
                        <td>Contracted</td>
                        <td>2,000,000 VND</td>
                        <td>0</td>
                        <td>-</td>
                        <td>-</td>
                        <td>2,000,000 VND</td>
                        <td>Not yet</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                    View Detail
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                    <a class="dropdown-item" href="#">View Detail</a>
                                    <a class="dropdown-item" href="#">Pending</a>
                                    <a class="dropdown-item" href="#">Deleted</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-center">1</td>
                        <td>VietPost</td>
                        <td>XAZ1234BNnmb</td>
                        <td>Contracted</td>
                        <td>2,000,000 VND</td>
                        <td>0</td>
                        <td>-</td>
                        <td>-</td>
                        <td>2,000,000 VND</td>
                        <td>Not yet</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                    View Detail
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                    <a class="dropdown-item" href="#">View Detail</a>
                                    <a class="dropdown-item" href="#">Pending</a>
                                    <a class="dropdown-item" href="#">Deleted</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-center">1</td>
                        <td>VietPost</td>
                        <td>XAZ1234BNnmb</td>
                        <td>Contracted</td>
                        <td>2,000,000 VND</td>
                        <td>0</td>
                        <td>-</td>
                        <td>-</td>
                        <td>2,000,000 VND</td>
                        <td>Success</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                    View Detail
                                    </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                    <a class="dropdown-item" href="#">View Detail</a>
                                    <a class="dropdown-item" href="#">Pending</a>
                                    <a class="dropdown-item" href="#">Deleted</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-center">1</td>
                        <td>VietPost</td>
                        <td>XAZ1234BNnmb</td>
                        <td>Contracted</td>
                        <td>2,000,000 VND</td>
                        <td>0</td>
                        <td>-</td>
                        <td>-</td>
                        <td>2,000,000 VND</td>
                        <td>Success</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                    View Detail
                                    </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                    <a class="dropdown-item" href="#">View Detail</a>
                                    <a class="dropdown-item" href="#">Pending</a>
                                    <a class="dropdown-item" href="#">Deleted</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td class="text-center">1</td>
                        <td>VietPost</td>
                        <td>XAZ1234BNnmb</td>
                        <td>Contracted</td>
                        <td>2,000,000 VND</td>
                        <td>0</td>
                        <td>-</td>
                        <td>-</td>
                        <td>2,000,000 VND</td>
                        <td>Success</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                    View Detail
                                    </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="#">Unlock</a>
                                    <a class="dropdown-item" href="#">Locked</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                </tbody>
                <tfoot>
                <tr>
                    <th colspan="1"></th>
                    <th colspan="3">Total</th>
                    <th>10,000,000</th>
                    <th>0</th>
                    <th>10</th>
                    <th>500</th>
                    <th>10,000,000</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="container">
    <h5>Shipper's type:</h5>
    <p>1, <span>Contracted:</span> Dành cho các công ty đã ký hợp đồng chấp thuận cung cấp dịch vụ trên Fastast nên không cần nạp card</p>
    <p>2, Registered: Dành cho các công ty, cá nhân đã đăng ký tài khoản trên Fastast và cấp dịch vụ</p>
    <p>3, Membership: Dành cho các công ty, cá nhân đã đăng ký tài khoản trên Fastast và cấp dịch vụ, đồng thời đã thanh toán phí thành viên hàng năm trên Fastast</p>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <ul class="pagination">
                <li class="previous"><a href="#">First</a></li>
                <li><a href="#">&laquo;</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">6</a></li>
                <li><a href="#">7</a></li>
                <li><a href="#">8</a></li>
                <li><a href="#">9</a></li>
                <li><a href="#">&raquo;</a></li>
                <li class="next"><a href="#">Last</a></li>
            </ul>
        </div>
    </div>
</div>
