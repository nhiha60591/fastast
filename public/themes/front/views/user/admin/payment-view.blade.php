<main class="row main">
    <aside class="left-sidebar col-md-3">
        <div class="wrap-sidebar">
            <ul>
                <li><a href="">All payment</a></li>
                <li><a href="">Statement view</a></li>
                <li><a href="">Deposit acount</a></li>
                <li><a href="">Ads Payment</a></li>
            </ul>
        </div>
    </aside>
    <section class="col-md-9">
        <div class="content col-md-7">
            <form class="">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="email">Deposit Account</label>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="email">Bank Account Number</label>
                        <div class="col-sm-7">
                            <input type="email" class="form-control" id="email" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="email">Re-type Bank Account Number</label>
                        <div class="col-sm-7">
                            <input type="email" class="form-control" id="email" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="email">Account Holder Name</label>
                        <div class="col-sm-7">
                            <input type="email" class="form-control" id="email" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="email">Country</label>
                        <div class="col-sm-7">
                            <input type="email" class="form-control" id="email" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="email">Bank Name</label>
                        <div class="col-sm-7">
                            <input type="email" class="form-control" id="email" placeholder="Enter email">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-lg-offset-2">
                        <div class="row">
                            <button  class="col-md-6">Cancel</button>
                            <button type="submit" class="col-md-6">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="right-sidebar col-md-5">

        </div>
    </section>
</main>
<div class="col-md-12 description">
    <div class="">
        <p>1. Thời hạn thanh toán cho shipper hoặc hoàn lại tiền hoặc bồi thường cho sender sẻ được thực hiện vào ngày 1 và ngày 15 hàng tháng.</p>
        <p>2. Shipper sẽ chỉ nhận được tiền nếu sender confirm đã nhận được hàng qua email được gửi đến sender ngay sau khi nhận được hàng theo tracking. Hoặc nếu sender chọn gửi tiền ngay sau khi nhận hàng thì shipper sẽ được nhận tiền theo yêu cầu này.</p>
    </div>
</div>