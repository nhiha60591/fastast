<nav class="navbar">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <form class="navbar-form navbar-left" action="" method="get">
            <div class="form-group">
                <div class="inner-addon right-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <input type="text" name="search" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('from_date', trans('front.from_date')) !!}
                {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                {!! Form::label('to_date', trans('front.to_date')) !!}
                {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                <input class="btn btn-primary" type="submit" value="{{ trans('front.search') }}"/>
            </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('user-admin/manage-export', array( 'type'=>'package', 'ext'=>'pdf' ) ) }}"><img src="{!! Theme::asset()->url('img/pdf-flat.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="{{ url('user-admin/manage-export', array( 'type'=>'package', 'ext'=>'xls' ) ) }}"><img src="{!! Theme::asset()->url('img/excel-icon.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="javascript:window.print()"><img src="{!! Theme::asset()->url('img/print-icon.png') !!}" width="30" class="img-responsive"></a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
<div class="row">
    <h1 class="title">{{ trans('front.list_package') }} </h1>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>{{ trans('front.sender') }}</th>
                    <th>{{ trans('front.sender_id') }}</th>
                    <th>{{ trans('front.address') }}</th>
                    <th>{{ trans('front.service') }}</th>
                    <th>{{ trans('front.number_of_listing') }}</th>
                    <th>{{ trans('front.cancelled_listing') }}</th>
                    <th>{!! trans('front.number_of_listing_weight') !!}</th>
                    <th>{!! trans('front.number_of_listing_trip') !!}</th>
                    <th>{{ trans('front.manager') }}</th>
                </tr>
                </thead>
                <tbody>
                @if( $packages->count() )
                    <?php $i = 1; ?>
                    @foreach($packages as $package)
                        <tr class="text-center">
                            <td class="text-center">{{ $i }}</td>
                            <td>{{ $package->getFullName() }}</td>
                            <td>{{ $package->id }}</td>
                            <td>{{ $package->address }}</td>
                            <td class="text-center">{!! $package->getPackageServiceImage() !!}</td>
                            <td class="text-center">{{ $package->getPackageNumber() }}</td>
                            <td class="text-center">{{ $package->getCancelPackage() }}</td>
                            <td class="text-center">{{ $package->getPackageWeightNumber() }}</td>
                            <td class="text-center">{{ $package->getPackageTripNumber() }}</td>
                            <td class="text-center">
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu{{ $package->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        View Detail
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu{{ $package->id }}">
                                        <a class="dropdown-item" href="{{ url('user/manage-listing-package', $package->id) }}">View Detail</a>
                                        <a class="dropdown-item" href="{{ url('user-admin/package-status/pending', $package->id) }}">Pending</a>
                                        <a class="dropdown-item" href="{{ url('user-admin/package-status/relist', $package->id) }}">Relist</a>
                                        <a class="dropdown-item" href="{{ url('user-admin/package-status/delete', $package->id) }}">Deleted</a>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                @else
                    <tr class="text-center">
                        <td class="text-center" colspan="9">Package not found!</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            {!! $packages->links() !!}
        </div>
    </div>
</div>