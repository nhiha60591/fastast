<div class="row search-wrap">
    <div class="col-lg-12">
        <div class="row ">
            <div class="col-md-3">
                <div class="search">
                    <div class="inner-addon right-addon">
                        <i class="glyphicon glyphicon-search"></i>
                        <input type="text" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row form-inline">
                    <div class="col-md-4 form-inline">
                        {!! Form::label('from_date', trans('front.from_date')) !!}
                        {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="col-md-4 form-group">
                        {!! Form::label('to_date', trans('front.to_date')) !!}
                        {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
        {!! Form::open(array('url' => 'user/manage-shipping',  'method' => 'POST', 'id' => 'manage-shipping', 'class'=> 'form-inline', 'role'=>'form')) !!}
        <div class="form-group">


        </div>
        {!! Form::close() !!}
    </div>
</div>
<main class="row main">
    <aside class="left-sidebar col-md-2">
        <div class="wrap-sidebar">
            <ul>
                <li><a href="">All payment</a></li>
                <li><a href="">Statement view</a></li>
                <li><a href="">Deposit acount</a></li>
                <li><a href="">Ads Payment</a></li>
            </ul>
        </div>
    </aside>
    <section class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>{!! trans('front.date') !!}  </th>
                            <th>{!! trans('front.position_on_website') !!}  </th>
                            <th>{!! trans('front.page') !!} </th>
                            <th>{!! trans('front.location') !!} </th>
                            <th>{!! trans('front.number_of_day') !!} </th>
                            <th>{!! trans('front.unit_rate') !!} </th>
                            <th>{!! trans('front.count') !!}</th>
                            <th>{!! trans('front.total_payment') !!}</th>
                            <th>{!! trans('front.manager') !!} </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="text-center">
                            <td>Jan 12,2016</td>
                            <td>
                                <select name="position_on_website" id="">
                                    <option value="">First row</option>
                                    <option value="">Second row</option>
                                </select>
                            </td>
                            <td><select name="page" id="">
                                    <option value="">Page 1</option>
                                    <option value="">Page 2</option>
                                </select></td>
                            <td>Ho Chi Minh</td>
                            <td>Not limit</td>
                            <td>
                                <select name="" id="">
                                    <option value="">$0.1 per click</option>
                                    <option value="">$1.0 per day</option>
                                    <option value="">$10 per week</option>
                                    <option value="">$100 per month</option>
                                </select>
                            </td>
                            <td>300</td>
                            <td>$500</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                        Advertising
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                        <a class="dropdown-item" href="#">Advertising</a>
                                        <a class="dropdown-item" href="#">Add new Ads</a>
                                        <a class="dropdown-item" href="#">Stopped</a>
                                        <a class="dropdown-item" href="#">Deleted</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>Jan 12,2016</td>
                            <td>
                                <select name="position_on_website" id="">
                                    <option value="">First row</option>
                                    <option value="">Second row</option>
                                </select>
                            </td>
                            <td><select name="page" id="">
                                    <option value="">Page 1</option>
                                    <option value="">Page 2</option>
                                </select></td>
                            <td>Ho Chi Minh</td>
                            <td>Not limit</td>
                            <td>
                                <select name="" id="">
                                    <option value="">$0.1 per click</option>
                                    <option value="">$1.0 per day</option>
                                    <option value="">$10 per week</option>
                                    <option value="">$100 per month</option>
                                </select>
                            </td>
                            <td>300</td>
                            <td>$500</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                        Advertising
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                        <a class="dropdown-item" href="#">Advertising</a>
                                        <a class="dropdown-item" href="#">Add new Ads</a>
                                        <a class="dropdown-item" href="#">Stopped</a>
                                        <a class="dropdown-item" href="#">Deleted</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>Jan 12,2016</td>
                            <td>
                                <select name="position_on_website" id="">
                                    <option value="">First row</option>
                                    <option value="">Second row</option>
                                </select>
                            </td>
                            <td><select name="page" id="">
                                    <option value="">Page 1</option>
                                    <option value="">Page 2</option>
                                </select></td>
                            <td>Ho Chi Minh</td>
                            <td>Not limit</td>
                            <td>
                                <select name="" id="">
                                    <option value="">$0.1 per click</option>
                                    <option value="">$1.0 per day</option>
                                    <option value="">$10 per week</option>
                                    <option value="">$100 per month</option>
                                </select>
                            </td>
                            <td>300</td>
                            <td>$500</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                        Advertising
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                        <a class="dropdown-item" href="#">Advertising</a>
                                        <a class="dropdown-item" href="#">Add new Ads</a>
                                        <a class="dropdown-item" href="#">Stopped</a>
                                        <a class="dropdown-item" href="#">Deleted</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>Jan 12,2016</td>
                            <td>
                                <select name="position_on_website" id="">
                                    <option value="">First row</option>
                                    <option value="">Second row</option>
                                </select>
                            </td>
                            <td><select name="page" id="">
                                    <option value="">Page 1</option>
                                    <option value="">Page 2</option>
                                </select></td>
                            <td>Ho Chi Minh</td>
                            <td>Not limit</td>
                            <td>
                                <select name="" id="">
                                    <option value="">$0.1 per click</option>
                                    <option value="">$1.0 per day</option>
                                    <option value="">$10 per week</option>
                                    <option value="">$100 per month</option>
                                </select>
                            </td>
                            <td>300</td>
                            <td>$500</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                        Advertising
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                        <a class="dropdown-item" href="#">Advertising</a>
                                        <a class="dropdown-item" href="#">Add new Ads</a>
                                        <a class="dropdown-item" href="#">Stopped</a>
                                        <a class="dropdown-item" href="#">Deleted</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>Jan 12,2016</td>
                            <td>
                                <select name="position_on_website" id="">
                                    <option value="">First row</option>
                                    <option value="">Second row</option>
                                </select>
                            </td>
                            <td><select name="page" id="">
                                    <option value="">Page 1</option>
                                    <option value="">Page 2</option>
                                </select></td>
                            <td>Ho Chi Minh</td>
                            <td>5 ngày</td>
                            <td>
                                <select name="" id="">
                                    <option value="">$0.1 per click</option>
                                    <option value="">$1.0 per day</option>
                                    <option value="">$10 per week</option>
                                    <option value="">$100 per month</option>
                                </select>
                            </td>
                            <td>300</td>
                            <td>$500</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                        Advertising
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                        <a class="dropdown-item" href="#">Advertising</a>
                                        <a class="dropdown-item" href="#">Add new Ads</a>
                                        <a class="dropdown-item" href="#">Stopped</a>
                                        <a class="dropdown-item" href="#">Deleted</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>Jan 12,2016</td>
                            <td>
                                <select name="position_on_website" id="">
                                    <option value="">First row</option>
                                    <option value="">Second row</option>
                                </select>
                            </td>
                            <td><select name="page" id="">
                                    <option value="">Page 1</option>
                                    <option value="">Page 2</option>
                                </select></td>
                            <td>Ho Chi Minh</td>
                            <td>10 ngày</td>
                            <td>
                                <select name="" id="">
                                    <option value="">$0.1 per click</option>
                                    <option value="">$1.0 per day</option>
                                    <option value="">$10 per week</option>
                                    <option value="">$100 per month</option>
                                </select>
                            </td>
                            <td>300</td>
                            <td>$500</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                        Advertising
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                        <a class="dropdown-item" href="#">Advertising</a>
                                        <a class="dropdown-item" href="#">Add new Ads</a>
                                        <a class="dropdown-item" href="#">Stopped</a>
                                        <a class="dropdown-item" href="#">Deleted</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>Jan 12,2016</td>
                            <td>
                                <select name="position_on_website" id="">
                                    <option value="">First row</option>
                                    <option value="">Second row</option>
                                </select>
                            </td>
                            <td><select name="page" id="">
                                    <option value="">Page 1</option>
                                    <option value="">Page 2</option>
                                </select></td>
                            <td>Ho Chi Minh</td>
                            <td>20 ngày</td>
                            <td>
                                <select name="" id="">
                                    <option value="">$0.1 per click</option>
                                    <option value="">$1.0 per day</option>
                                    <option value="">$10 per week</option>
                                    <option value="">$100 per month</option>
                                </select>
                            </td>
                            <td>300</td>
                            <td>$500</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                        Advertising
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                        <a class="dropdown-item" href="#">Advertising</a>
                                        <a class="dropdown-item" href="#">Add new Ads</a>
                                        <a class="dropdown-item" href="#">Stopped</a>
                                        <a class="dropdown-item" href="#">Deleted</a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</main>
