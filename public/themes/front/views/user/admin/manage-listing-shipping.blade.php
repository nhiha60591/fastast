<nav class="navbar">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <form class="navbar-form navbar-left" action="" method="get">
            <div class="form-group">
                <div class="inner-addon right-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <input type="text" name="search" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('from_date', trans('front.from_date')) !!}
                {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                {!! Form::label('to_date', trans('front.to_date')) !!}
                {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                <input class="btn btn-primary" type="submit" value="{{ trans('front.search') }}"/>
            </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('user-admin/manage-export', array( 'type'=>'listing', 'ext'=>'pdf' ) ) }}"><img src="{!! Theme::asset()->url('img/pdf-flat.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="{{ url('user-admin/manage-export', array( 'type'=>'listing', 'ext'=>'xls' ) ) }}"><img src="{!! Theme::asset()->url('img/excel-icon.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="javascript:window.print()"><img src="{!! Theme::asset()->url('img/print-icon.png') !!}" width="30" class="img-responsive"></a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
<div class="row">
    <h1 class="title">{{ trans('front.list_shipping') }} </h1>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>{{ trans('front.sender') }}</th>
                    <th>{{ trans('front.sender_id') }}</th>
                    <th>{{ trans('front.address') }}</th>
                    <th>{{ trans('front.number_of_listing') }}</th>
                    <th>{{ trans('front.cancelled_listing') }}</th>
                    <th>{!! trans('front.number_of_listing_weight') !!}</th>
                    <th>{!! trans('front.number_of_listing_trip') !!}</th>
                    <th>{{ trans('front.manager') }}</th>
                </tr>
                </thead>
                <tbody>
                @if( $shippings->count() )
                    <?php $i = 1; $listingNumber = $cancelListing = $listingWeight = $listingTrip = 0; ?>
                    @foreach( $shippings as $shipping)
                        <tr class="text-center">
                            <td class="text-center">{{ $i }}</td>
                            <td>{{ $shipping->getFullName() }}</td>
                            <td>{{ $shipping->id }}</td>
                            <td>{{ $shipping->address }}</td>
                            <td>{{ $shipping->getListingNumber() }}<?php $listingNumber = $listingNumber + (int)$shipping->getListingNumber(); ?></td>
                            <td>{{ $shipping->getCancelListing() }}<?php $cancelListing = $cancelListing + (int)$shipping->getCancelListing(); ?></td>
                            <td>{{ $shipping->getListingWeightNumber() }}<?php $listingWeight = $listingWeight + (int)$shipping->getListingWeightNumber(); ?></td>
                            <td>{{ $shipping->getListingTripNumber() }}<?php $listingTrip = $listingTrip + (int)$shipping->getListingTripNumber(); ?></td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-secondary dropdown-toggle" href="http://example.com" id="dropdownMenuLink" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                        View Detail
                                        </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                        <a class="dropdown-item" href="{{ url('user/manage-listing-shipping', $shipping->id) }}">View Detail</a>
                                        <a class="dropdown-item" href="{{ url('user-admin/listing-status/pending', $shipping->id) }}">Pending</a>
                                        <a class="dropdown-item" href="{{ url('user-admin/listing-status/relist', $shipping->id) }}">Relist</a>
                                        <a class="dropdown-item" href="{{ url('user-admin/listing-status/delete', $shipping->id) }}">Deleted</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                @else
                    <tr class="text-center">
                        <td class="text-center" colspan="9">Listing not found!</td>
                    </tr>
                @endif
                </tbody>
                <tfoot>
                    @if( $shippings->count() )
                    <tr>
                        <th colspan="1"></th>
                        <th colspan="3">Total</th>
                        <th>{{ $listingNumber }}</th>
                        <th>{{ $cancelListing }}</th>
                        <th>{{ $listingWeight }}</th>
                        <th>{{ $listingTrip }}</th>
                        <th></th>
                    </tr>
                    @endif
                </tfoot>
            </table>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            {!! $shippings->links() !!}
        </div>
    </div>
</div>