<div class="row search-wrap">
    <div class="col-lg-12">
        <div class="row ">
            <div class="col-md-3">
                <div class="search">
                    <div class="inner-addon right-addon">
                        <i class="glyphicon glyphicon-search"></i>
                        <input type="text" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row form-inline">
                    <div class="col-md-4 form-inline">
                        {!! Form::label('from_date', trans('front.from_date')) !!}
                        {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="col-md-4 form-group">
                        {!! Form::label('to_date', trans('front.to_date')) !!}
                        {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
        {!! Form::open(array('url' => 'user/manage-shipping',  'method' => 'POST', 'id' => 'manage-shipping', 'class'=> 'form-inline', 'role'=>'form')) !!}
        <div class="form-group">


        </div>
        {!! Form::close() !!}
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>{{ trans('front.sender') }}</th>
                    <th>{{ trans('front.sender_id') }}</th>
                    <th>{{ trans('front.address') }}</th>
                    <th>{{ trans('front.service') }}</th>
                    <th>{{ trans('front.number_of_listing') }}</th>
                    <th>{{ trans('front.cancelled_listing') }}</th>
                    <th>{!! trans('front.number_of_listing_weight') !!}</th>
                    <th>{!! trans('front.number_of_listing_trip') !!}</th>
                    <th>{{ trans('front.manager') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr class="text-center">
                    <td class="text-center">1</td>
                    <td>VietPost</td>
                    <td>XAZ1234BNnmb</td>
                    <td>Ho Chi Minh, Viet Nam</td>
                    <td>Hinh</td>
                    <td>50</td>
                    <td>2</td>
                    <td>20</td>
                    <td>10</td>
                    <td>
                        <select name="manage">
                            <option value="View Detail">View Detail</option>
                            <option value="edit">Pedding</option>
                            <option value="cancel">Relist</option>
                            <option value="delete">Delete</option>
                        </select>
                    </td>
                </tr>
                <tr class="text-center">
                    <td class="text-center">1</td>
                    <td>VietPost</td>
                    <td>XAZ1234BNnmb</td>
                    <td>Ho Chi Minh, Viet Nam</td>
                    <td>Hinh</td>
                    <td>50</td>
                    <td>2</td>
                    <td>20</td>
                    <td>10</td>
                    <td>
                        <select name="manage">
                            <option value="View Detail">View Detail</option>
                            <option value="edit">Pedding</option>
                            <option value="cancel">Relist</option>
                            <option value="delete">Delete</option>
                        </select>
                    </td>
                </tr>
                <tr class="text-center">
                    <td class="text-center">1</td>
                    <td>VietPost</td>
                    <td>XAZ1234BNnmb</td>
                    <td>Ho Chi Minh, Viet Nam</td>
                    <td>Hinh</td>
                    <td>50</td>
                    <td>2</td>
                    <td>20</td>
                    <td>10</td>
                    <td>
                        <select name="manage">
                            <option value="View Detail">View Detail</option>
                            <option value="edit">Pedding</option>
                            <option value="cancel">Relist</option>
                            <option value="delete">Delete</option>
                        </select>
                    </td>
                </tr>
                <tr class="text-center">
                    <td class="text-center">1</td>
                    <td>VietPost</td>
                    <td>XAZ1234BNnmb</td>
                    <td>Ho Chi Minh, Viet Nam</td>
                    <td>Hinh</td>
                    <td>50</td>
                    <td>2</td>
                    <td>20</td>
                    <td>10</td>
                    <td>
                        <select name="manage">
                            <option value="View Detail">View Detail</option>
                            <option value="edit">Pedding</option>
                            <option value="cancel">Relist</option>
                            <option value="delete">Delete</option>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <ul class="pagination">
                <li class="previous"><a href="#">First</a></li>
                <li><a href="#">&laquo;</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">6</a></li>
                <li><a href="#">7</a></li>
                <li><a href="#">8</a></li>
                <li><a href="#">9</a></li>
                <li><a href="#">&raquo;</a></li>
                <li class="next"><a href="#">Last</a></li>
            </ul>
        </div>
    </div>
</div>