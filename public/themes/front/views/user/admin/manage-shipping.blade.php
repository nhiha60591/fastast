<nav class="navbar">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <form class="navbar-form navbar-left" action="" method="get">
            <div class="form-group">
                <div class="inner-addon right-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <input type="text" name="search" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('from_date', trans('front.from_date')) !!}
                {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                {!! Form::label('to_date', trans('front.to_date')) !!}
                {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                <input class="btn btn-primary" type="submit" value="{{ trans('front.search') }}"/>
            </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('user-admin/manage-export', array( 'type'=>'shipping', 'ext'=>'pdf' ) ) }}"><img src="{!! Theme::asset()->url('img/pdf-flat.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="{{ url('user-admin/manage-export', array( 'type'=>'shipping', 'ext'=>'xls' ) ) }}"><img src="{!! Theme::asset()->url('img/excel-icon.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="javascript:window.print()"><img src="{!! Theme::asset()->url('img/print-icon.png') !!}" width="30" class="img-responsive"></a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>

<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th class="text-center">#</th>
                <th>{!! trans('front.shipper') !!}</th>
                <th>{!! trans('front.shipper_id') !!}</th>
                <th>{!! trans('front.transaction_allow') !!}</th>
                <th>{!! trans('front.package_click') !!}</th>
                <th>{!! trans('front.number_of_transaction') !!}</th>
                <th>{!! trans('front.fail_transaction') !!}</th>
                <th>{!! trans('front.success_transaction') !!}</th>
                <th>{!! trans('front.transaction_mount') !!}</th>
                <th>{!! trans('front.manager') !!}</th>
            </tr>
            </thead>
            <tbody>
            <?php 
                $totalTransaction = $totalFailTransaction = $totalSuccessTransaction = $totalSuccessTransaction = $totalTransactionAmount = 0;
            ?>
            @if($shippers->count())
                @foreach( $shippers as $shipper)
                    {!! Form::open(array('url' => 'user-admin/manage-shipping/'.$shipper->id,  'method' => 'POST', 'id' => 'manage-shipping-id-'.$shipper->id, 'class'=> 'form-inline', 'role'=>'form')) !!}
                    <tr class="text-center">
                        <td class="text-center">{!! $shipper->id !!}</td>
                        <td>{!! $shipper->getFullName() !!}</td>
                        <td><a href="{{ url('user/profile', $shipper->id) }}">FA{!! $shipper->id !!}</a></td>
                        <td>
                            <select class="select-count" name="transaction_count" data-formid="manage-shipping-id-{{ $shipper->id }}">
                                <option{!! selected($shipper->transaction_count, -1) !!} value="-1">Unlimited</option>
                                <option{!! selected($shipper->transaction_count, 10) !!} value="10">10</option>
                                <option{!! selected($shipper->transaction_count, 20) !!} value="20">20</option>
                                <option{!! selected($shipper->transaction_count, 30) !!} value="30">30</option>
                            </select>
                        </td>
                        <td>
                            <select class="select-count" name="package_click_count" data-formid="manage-shipping-id-{{ $shipper->id }}">
                                <option{!! selected($shipper->package_click_count, -1) !!} value="-1">Unlimited</option>
                                <option{!! selected($shipper->package_click_count, 10) !!} value="10">10</option>
                                <option{!! selected($shipper->package_click_count, 20) !!} value="20">20</option>
                                <option{!! selected($shipper->package_click_count, 30) !!} value="30">30</option>
                            </select>
                        </td>
                        <td>{{ $shipper->getNumberTransaction() }} <?php $totalTransaction =  $totalTransaction + (int)$shipper->getNumberTransaction(); ?></td>
                        <td>{{ $shipper->getFailTransaction() }} <?php $totalFailTransaction =  $totalFailTransaction + (int)$shipper->getFailTransaction(); ?></td>
                        <td>{{ $shipper->getSuccessTransaction() }} <?php $totalSuccessTransaction =  $totalSuccessTransaction + (int)$shipper->getSuccessTransaction(); ?></td>
                        <td>{{ $shipper->getTransactionAmount() }}<?php $totalTransactionAmount = (double)$totalTransactionAmount + (double)$shipper->getTransactionAmount(); ?> <span class="unit-currency">{!! $currentCurrency->symbol !!}</span></td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-secondary dropdown-toggle" id="dropdownMenuLink-{{ $shipper->id }}" data-target="#" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                    View Detail</a>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink-{{ $shipper->id }}">
                                    <a class="dropdown-item" href="{{ url('user/manage-shipping', $shipper->id) }}">View Detail</a>
                                    <a class="dropdown-item make-status" data-alert="{{ trans('user.make-pending') }}" href="{{ url('user-admin/make-status/pending', $shipper->id) }}">Pending</a>
                                    <a class="dropdown-item make-status" data-alert="{{ trans('user.make-delete') }}" href="{{ url('user-admin/make-status/delete', $shipper->id) }}">Deleted</a>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    {!! Form::close() !!}
                @endforeach
            @else
            <tr class="text-center">
                <td class="text-center" colspan="10">Shipping Not found</td>
            </tr>
            @endif
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="1"></th>
                    <th colspan="3">Total</th>
                    <th></th>
                    <th>{{ $totalTransaction }}</th>
                    <th>{{ $totalFailTransaction }}</th>
                    <th>{{ $totalSuccessTransaction }}</th>
                    <th class="text-right">{{ $totalTransactionAmount }} <span class="unit-currency">{!! $currentCurrency->symbol !!}</span></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            @include('pagination.pagination', ['paginator' => $shippers])
            {{--{{ $shippers->links() }}--}}
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.select-count').change(function(){
            var id = $(this).attr('data-formid');
            $('#'+id).submit();
            return false;
        });
        $('.make-status').click(function(){
            var alert = $(this).attr('data-alert');
            if( confirm(alert) ){
                return true;
            }
            return false;
        });
    });
</script>