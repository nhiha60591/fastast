{!! Form::open(array('url' => 'user/update-profile',  'method' => 'POST', 'id' => 'update-profile', 'files'=>true, 'role'=>'form', 'class'=> 'form')) !!}
<div class="row" style="position: relative;">
    <div class="banner-upload-btn">
        <a href="#" class="upload-banner"><img src="{!! Theme::asset()->url('img/camera-icon-upload.png') !!}" width="32" class="img-upload" alt="..."></a>
    </div>
    <div class="banner-user">
        <img src="{!! $user->banner ? image_url($user->banner) : Theme::asset()->url('img/user-banner.png') !!}" width="100%" class="img-responsive" alt="...">
    </div>
    <div class="container" id="user-info-box">
        <div class="row">
            <div class="col-md-2" style="position: relative">
                <div class="avatar-upload-btn">
                    <a href="#" class="upload-avatar"><img src="{!! Theme::asset()->url('img/camera-icon-upload.png') !!}" width="32" class="img-upload" alt="..."></a>
                </div>
                <div class="user-avatar">
                    <img src="{!! $user->avatar ? image_url($user->avatar) : Theme::asset()->url('img/user.png') !!}" class="img-responsive" alt="...">
                </div>
            </div>
            <div class="col-md-10">
                <div class="user-info">
                    <h3>{{ $user->full_name }}</h3>
                    <p class="rate"></p>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="user-info">
                <h3>{{ $user->full_name }}</h3>
                <p class="rate"></p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-md-11 col-lg-offset-1">
        <div class="row info-detail">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="user-type">{{ trans('user.you-are') }} <span class="required">(*)</span></label>
                                <div class="col-sm-7">
                                    <select name="user_type" id="user-type" class="form-control">
                                        <option value="company" {!! selected('company', $user->user_type) !!}>{{ trans('user.company') }}</option>
                                        <option value="individual" {!! selected('individual', $user->user_type) !!}>{{ trans('user.individual') }}</option>
                                    </select>
                                    <div class="hidden">
                                        <input type="checkbox" id="checked-company" checked>
                                        <input type="checkbox" id="checked-individual">
                                    </div>
                                </div>
                            </div>
                            <div class="individual">
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="full-name">{{ trans('user.user-first-last-name') }} <span class="required">(*)</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" value="{!! $user->full_name !!}" id="full-name" name="full_name" placeholder="{{ trans('user.enter') }} {{ trans('user.full-name') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="nationality">{{ trans('user.nationality') }}</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" value="{!! $user->nationality !!}" name="nationality" id="nationality" placeholder="{{ trans('user.enter') }} {{ trans('user.nationality') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="company">
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="company-name">{{ trans('user.company-name') }} <span class="required">(*)</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="company-name" value="{!! $user->company_name !!}" name="companyName" placeholder="{{ trans('user.enter') }} {{ trans('user.company-name') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="address">{{ trans('user.address') }}</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="address" value="{!! $user->address !!}" id="address" placeholder="{{ trans('user.enter') }} {{ trans('user.address') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <p>Choose one or more of the apps belows <span class="required">(*)</span></p>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="viber">
                            <img src="{!! Theme::asset()->url('img/viber.png') !!}" class="img-responsive social-icon">
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="viber" name="viber" value="{!! $user->viber !!}" placeholder="Phone Number">
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="whats_app">
                        <img src="{!! Theme::asset()->url('img/whats-app.png') !!}" class="img-responsive social-icon">
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="whats_app" name="whats_app" value="{!! $user->whats_app !!}" placeholder="Phone Number">
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="line">
                            <img src="{!! Theme::asset()->url('img/line.png') !!}" class="img-responsive social-icon">
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="line" name="line" value="{!! $user->line !!}" placeholder="Phone Number">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container license">
    <div class="col-md-11 col-lg-offset-1">
        <div class="row">
            <div class="col-md-8">
                <div class="company">
                    <div class="row">
                        <?php $hotLine = unserialize($user->hotline); $i=0; ?>
                        @if( isset($hotLine) && !empty($hotLine) && is_array($hotLine) && sizeof($hotLine)>0)
                            @foreach($hotLine as $hl)
                                <div id="hot-line-{{ $i }}" class="hot-line-item">
                                    <div class="col-md-9">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-sm-5" for="hotline">{{ trans('user.hotline') }}<span class="required">(*)</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="hotline[]" value="{!! $hl !!}" id="hotline" placeholder="Automatic filling">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-default add-more btn-add-more-hotline"><span class="glyphicon glyphicon-plus"></span></button>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            @endforeach
                        @else
                            <div id="hot-line-0" class="hot-line-item">
                                <div class="col-md-9">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="hotline">{{ trans('user.hotline') }}<span class="required">(*)</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="hotline[]" value="{!! $user->hotline !!}" id="hotline" placeholder="Automatic filling">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-default add-more btn-add-more-hotline"><span class="glyphicon glyphicon-plus"></span></button>
                                </div>
                            </div>
                        @endif
                        <div class="hot-line-items-append"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="fax_number">{{ trans('user.fax_number') }}<span class="required">(*)</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="fax_number" name="fax_number" value="{!! $user->fax_number !!}" placeholder="Automatic filling">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-horizontal">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-5" for="email">{{ trans('user.email') }}<span class="required">(*)</span></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="email" name="email" value="{!! $user->email !!}" placeholder="Automatic filling">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="individual">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="phone_number_license">{!! trans('user.phone-number-license') !!} <span class="required">(*)</span></label>
                                    <div class="col-sm-7">
                                        <input type="tel" class="form-control" name="phone_number_license" id="phone_number_license" value="{!! $user->phone_number_license !!}" placeholder="Phone number">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="individual_passport_license">{!! trans('user.individual-passport-license') !!} <span class="required">(*)</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="individual_passport_license" id="individual_passport_license" value="{!! $user->company_registered_license !!}" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="pic" accept="image/*">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="individual_driver_license">{!! trans('user.individual-driver-license') !!} <span class="required">(*)</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="individual_driver_license" id="individual_driver_license" value="{!! $user->company_registered_license !!}" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="pic" accept="image/*">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="individual_car_license">{!! trans('user.individual-car-license') !!} <span class="required">(*)</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="individual_car_license" id="individual_car_license" value="{!! $user->company_registered_license !!}" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="pic" accept="image/*">
                        </div>
                    </div>
                </div>
                <div class="company">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="website">{!! trans('user.website') !!}</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="website" value="{!! $user->website !!}" id="website" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-5" for="company_registered_license">{!! trans('user.company-registered-license') !!} <span class="required">(*)</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="company_registered_license" id="company_registered_license" value="{!! $user->company_registered_license !!}" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="pic" accept="image/*">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-horizontal">
                    <div class="form-group">
                       <label class="control-label col-sm-3" for="facebook">
                            <img src="{!! Theme::asset()->url('img/facebook.png') !!}" class="img-responsive social-icon">
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="facebook" name="facebook" value="{!! $user->facebook !!}" placeholder="Phone Number">
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="zalo">
                        <img src="{!! Theme::asset()->url('img/zalo.png') !!}" class="img-responsive social-icon">
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="zalo" name="zalo" value="{!! $user->zalo !!}" placeholder="Phone Number">
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="skype">
                        <img src="{!! Theme::asset()->url('img/skype.png') !!}" class="img-responsive social-icon">
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="skype" name="skype" value="{!! $user->skype !!}" placeholder="Phone Number">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>{!! trans('user.Let-locate-your-network-on-Map') !!}</p>
                <div id="map">

                </div>
                <div id="list-locations"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="wrap-textarea">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="textarea-box">
                                <label for="introduction">{!! trans('user.introduction') !!}</label>
                                <textarea class="form-control" rows="3" id="introduction" name="introduction">{!! $user->introduction !!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="textarea-box">
                                <label for="service">{!! trans('user.service') !!}</label>
                                <textarea class="form-control" rows="3" id="service" name="service">{!! $user->service !!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="textarea-box">
                                <label for="other_surcharges">{!! trans('user.other-surcharges') !!}</label>
                                <textarea class="form-control" rows="3" id="other_surcharges" name="other_surcharges">{!! $user->other_surcharges !!}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="textarea-box">
                                <label for="requirements">{!! trans('user.requirements') !!}</label>
                                <textarea class="form-control" rows="3" id="requirements" name="requirements">{!! $user->requirements !!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="textarea-box">
                                <label for="guarantee_policy">{!! trans('user.guarantee-policy') !!}</label>
                                <textarea class="form-control" rows="3" id="guarantee_policy" name="guarantee_policy">{!! $user->guarantee_policy !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center btns">
                <input type="submit" class="btn btn-cancel" name="cancel" value="{!! trans('user.cancel') !!}">
                <input type="submit" class="btn btn-save" name="save" value="{!! trans('user.save') !!}">
            </div>
        </div>
    </div>
</div>
<div class="hidden">
    <input type="file" name="banner" id="user-banner-file" />
    <input type="file" name="avatar" id="user-avatar-file" />
</div>
{!! Form::close() !!}
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var nb = 1;
        $(document).on('click', '.btn-add-more-hotline', function () {
            var HTML = $('#hot-line-0').html();
            HTML = '<div id="hot-line-'+nb+'" class="hot-line-item">'+HTML+'</div>';
            $('.hot-line-items-append').append(HTML);
            var btnHTML = '<button class="btn btn-default add-more btn-add-more-hotline"><span class="glyphicon glyphicon-plus"></span></button>';
                btnHTML += '<button class="btn btn-default remove btn-remove-more-hotline" data-target="#hot-line-'+nb+'"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>';
            $('#hot-line-'+nb).find('.col-md-3').html(btnHTML);
            nb++;
            return false;
        });
        $(document).on('click', '.btn-remove-more-hotline', function () {
            var id = $(this).attr('data-target');
            $(id).remove();
            return false;
        });
        $("#user-type").change(function () {
            updateUserType();
        });
        updateUserType();
        function updateUserType(){
            if( $("#user-type").val() == 'company' ){
                $('.company').show();
                $('.individual').hide();
                $('#checked-company').prop( "checked", true );
                $('#checked-individual').prop( "checked", false );
            }else{
                $('.company').hide();
                $('.individual').show();
                $('#checked-company').prop( "checked", false );
                $('#checked-individual').prop( "checked", true );
            }
        }
        // validate signup form on keyup and submit
        $("#update-profile").validate({
            rules: {
                companyName: {
                    required: "#checked-company:checked",
                    minlength: 2
                },
                'hotline[]': {
                    required: "#checked-company:checked",
                    minlength: 5
                },
                fax_number: {
                    required: "#checked-company:checked",
                    minlength: 9
                },
                company_registered_license: {
                    required: "#checked-company:checked"
                },
                full_name: {
                    required: "#checked-individual:checked",
                    minlength: 5
                },
                phone_number_license: {
                    required: "#checked-individual:checked",
                    minlength: 9
                },
                email: {
                    required: true,
                    email: true
                }
            }
        });
        $('.banner-upload-btn a').click(function(){
            $("#user-banner-file").click();
            return false;
        });
        $('.avatar-upload-btn a').click(function(){
            $("#user-avatar-file").click();
            return false;
        });
        $(document).on('change','#user-banner-file',function(event){
            $.each(event.target.files, function(key, value){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.banner-user img').attr('src', e.target.result);
                };
                reader.readAsDataURL(value);
            });
            return false;
        });
        $(document).on('change','#user-avatar-file',function(event){
            $.each(event.target.files, function(key, value){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.user-avatar img').attr('src',e.target.result);
                };
                reader.readAsDataURL(value);
            });
            return false;
        });
    });
</script>