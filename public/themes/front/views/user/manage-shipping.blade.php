<nav class="navbar">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <form class="navbar-form navbar-left" action="" method="get">
            <div class="form-group">
                <div class="inner-addon right-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <input type="text" name="search" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('from_date', trans('front.from_date')) !!}
                {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                {!! Form::label('to_date', trans('front.to_date')) !!}
                {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                <input class="btn btn-primary" type="submit" value="{{ trans('front.search') }}"/>
            </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('user/manage-export', array( 'type'=>'shipping', 'ext'=>'pdf' ) ) }}"><img src="{!! Theme::asset()->url('img/pdf-flat.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="{{ url('user/manage-export', array( 'type'=>'shipping', 'ext'=>'xls' ) ) }}"><img src="{!! Theme::asset()->url('img/excel-icon.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="javascript:window.print()"><img src="{!! Theme::asset()->url('img/print-icon.png') !!}" width="30" class="img-responsive"></a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>

<div class="row">
    <div class="col-lg-12">
            <table id="user-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{!! trans('front.date') !!}</th>
                    <th>{!! trans('front.tracking_number') !!}</th>
                    <th>{!! trans('front.from') !!}</th>
                    <th>{!! trans('front.to') !!}</th>
                    <th>{!! trans('front.shipping_time') !!}</th>
                    <th>{!! trans('front.sender') !!}</th>
                    <th>{!! trans('front.shipping_package_information') !!}</th>
                    <th>{!! trans('front.status') !!}</th>
                    <th>{!! trans('front.manager') !!}</th>
                </tr>
                </thead>
                <tbody>
                @if( $books->count() )
                    @foreach( $books as $book)
                        <?php
                            $origination = unserialize($book->origination);
                            $destination = unserialize($book->destination);
                            $serviceData = unserialize( $book->service_info)
                        ?>
                        <tr class="text-center">
                            <td>{!! date('M, d Y', strtotime($book->created_at)) !!}</td>
                            <td>{{ $book->track_number }}</td>
                            <td>{!! @$serviceData['from'] !!}</td>
                            <td>{!! @$serviceData['to'] !!}</td>
                            <td>{!! date('M, d Y', strtotime($book->departure_date)) !!}</td>
                            <td>{{ $origination['name'] }}</td>
                            <td>Clothes</td>
                            <td>{!! $book->status(true) !!}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu{{ $book->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        View Detail
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu{{ $book->id }}">
                                        <li><a href="{{ url('package/show-booking', array('id' => $book->id, 'shippingid'=>$book->shipping_id)) }}">View Detail</a></li>
                                        <li><a href="{{ url('package/make-shipping', array('id' => $book->id, 'shippingid'=>$book->shipping_id)) }}"> Make Shipping</a></li>
                                        <li><a href="{{ url('package/make-cancel', array('id' => $book->id, 'shippingid'=>$book->shipping_id)) }}">Make Cancel</a></li>
                                        <li><a href="{{ url('package/make-success', array('id' => $book->id, 'shippingid'=>$book->shipping_id)) }}">Make Success</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr class="text-center">
                        <td colspan="9">Shipping Not Found!</td>
                    </tr>
                @endif
                </tbody>
            </table>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            {!! $books->links() !!}
        </div>
    </div>
</div>