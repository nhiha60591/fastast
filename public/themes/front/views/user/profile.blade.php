{!! Form::open(array('url' => 'user/update-profile',  'method' => 'POST', 'id' => 'update-profile', 'files'=>true, 'role'=>'form', 'class'=> 'form')) !!}
<div class="row">
    <div class="banner-user">
        <img src="{!! Theme::asset()->url('img/user-banner.png') !!}" width="100%" class="img-responsive" alt="...">
    </div>
</div>
<div class="container" id="user-info-box">
    <div class="row">
        <div class="col-md-2">
            <div class="user-avatar">
                <img src="{!! Theme::asset()->url('img/user.png') !!}" class="img-responsive" alt="...">
            </div>
        </div>
        <div class="col-md-10">
            <div class="user-info">
                <h3>{{ $user->full_name }}</h3>
                <p class="rate"></p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-lg-9">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-5" for="user-type">{{ trans('user.user-type') }}</label>
                            <div class="col-sm-7">
                                {{ $user->user_type ? $user->user_type : trans('user.individual') }}
                                <input type="hidden" id="user-type" value="{{ $user->user_type ? $user->user_type : 'individual' }}">
                            </div>
                        </div>
                        <div class="individual">
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="full-name">{{ trans('user.full-name') }}</label>
                                <div class="col-sm-7">
                                    {!! $user->getFullName() !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="nationality">{{ trans('user.nationality') }}</label>
                                <div class="col-sm-7">
                                    {!! $user->nationality !!}
                                </div>
                            </div>
                        </div>
                        <div class="company">
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="company-name">{{ trans('user.company-name') }}</label>
                                <div class="col-sm-7">
                                    {!! $user->company_name !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="address">{{ trans('user.address') }}</label>
                                <div class="col-sm-7">
                                    {!! $user->address !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="viber">
                    <img src="{!! Theme::asset()->url('img/viber.png') !!}" class="img-responsive social-icon">
                    </label>
                    <div class="col-sm-9">
                        {!! $user->viber !!}
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="whats_app">
                    <img src="{!! Theme::asset()->url('img/whats-app.png') !!}" class="img-responsive social-icon">
                    </label>
                    <div class="col-sm-9">
                        {!! $user->whats_app !!}
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="line">
                        <img src="{!! Theme::asset()->url('img/line.png') !!}" class="img-responsive social-icon">
                    </label>
                    <div class="col-sm-9">
                        {!! $user->line !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="company">
                <div class="row">
                    <?php $hotLine = @unserialize($user->hotline); $i=0; ?>
                    @if( isset($hotLine) && !empty($hotLine) && is_array($hotLine) && sizeof($hotLine)>0)
                        @foreach($hotLine as $hl)
                            <div id="hot-line-{{ $i }}" class="hot-line-item">
                                <div class="col-md-9">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="hotline">{{ trans('user.hotline') }}</label>
                                            <div class="col-sm-7">
                                                {!! $hl !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>
                        @endforeach
                    @endif
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="fax_number">{{ trans('user.fax_number') }}</label>
                                <div class="col-sm-7">
                                    {!! $user->fax_number !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="form-horizontal">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="control-label col-sm-5" for="email">{{ trans('user.email') }}</label>
                            <div class="col-sm-7">
                                {!! $user->email !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="individual">
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="phone_number_license">{!! trans('user.phone-number-license') !!}</label>
                                <div class="col-sm-7">
                                    {!! $user->phone_number_license !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="company">
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="website">{!! trans('user.website') !!}</label>
                                <div class="col-sm-7">
                                    {!! $user->website !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-5" for="company_registered_license">{!! trans('user.company-registered-license') !!}</label>
                                <div class="col-sm-7">
                                    {!! $user->company_registered_license !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="facebook">
                        <img src="{!! Theme::asset()->url('img/facebook.png') !!}" class="img-responsive social-icon">
                    </label>
                    <div class="col-sm-9">
                        {!! $user->facebook !!}
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="zalo">
                    <img src="{!! Theme::asset()->url('img/zalo.png') !!}" class="img-responsive social-icon">
                    </label>
                    <div class="col-sm-9">
                        {!! $user->zalo !!}
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="skype">
                    <img src="{!! Theme::asset()->url('img/skype.png') !!}" class="img-responsive social-icon">
                    </label>
                    <div class="col-sm-9">
                        {!! $user->skype !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <p>{!! trans('user.Let-locate-your-network-on-Map') !!}</p>
        <div id="map"></div>
        @if( $user->locations()->count() )
            <?php $i=0; ?>
            <script type="text/javascript">
                var locations = [];
                @foreach($user->locations as $location)
                locations[{{ $i }}] = [{{ $location->lat }}, {{ $location->lng }}];
                <?php $i++; ?>
                @endforeach
            </script>
        @endif
    </div>

    <div class="row">
        <div class="wrap-textarea">
            <div class="row">
                <div class="col-md-12">
                    <h3>{!! trans('user.introduction') !!}</h3>
                    {!! $user->introduction !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>{!! trans('user.service') !!}</h3>
                    {!! $user->service !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>{!! trans('user.other-surcharges') !!}</h3>
                    {!! $user->other_surcharges !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h3>{!! trans('user.requirements') !!}</h3>
                    {!! $user->requirements !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-11">
                    <h3>{!! trans('user.guarantee-policy') !!}</h3>
                    {!! $user->guarantee_policy !!}
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        updateUserType();
        function updateUserType(){
            if( $("#user-type").val() == 'company' ){
                $('.company').show();
                $('.individual').hide();
            }else{
                $('.company').hide();
                $('.individual').show();
            }
        }
    });
</script>