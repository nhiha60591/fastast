<nav class="navbar">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <form class="navbar-form navbar-left" action="" method="get">
            <div class="form-group">
                <div class="inner-addon right-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <input type="text" name="search" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('from_date', trans('front.from_date')) !!}
                {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                {!! Form::label('to_date', trans('front.to_date')) !!}
                {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
            </div>
            <div class="form-group">
                <input class="btn btn-primary" type="submit" value="{{ trans('front.search') }}"/>
            </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('user/manage-export', array( 'type'=>'package', 'ext'=>'pdf' ) ) }}"><img src="{!! Theme::asset()->url('img/pdf-flat.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="{{ url('user/manage-export', array( 'type'=>'package', 'ext'=>'xls' ) ) }}"><img src="{!! Theme::asset()->url('img/excel-icon.png') !!}" width="30" class="img-responsive"></a></li>
            <li><a href="javascript:window.print()"><img src="{!! Theme::asset()->url('img/print-icon.png') !!}" width="30" class="img-responsive"></a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
<div class="container-fluid">
    <div class="row">
        <h1 class="title">{{ trans('front.list_package') }} </h1>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-striped" style="margin-bottom: 120px;">
                <thead>
                <tr>
                    <th>{{ trans('front.date') }}</th>
                    <th>{{ trans('front.package-listing') }}</th>
                    <th>{{ trans('front.from') }}</th>
                    <th>{{ trans('front.to') }}</th>
                    <th>{{ trans('front.service') }}</th>
                    <th>{{ trans('front.shipping_time') }}</th>
                    <th>{!! trans('front.package_contain') !!}</th>
                    <th>{!! trans('front.rate') !!}</th>
                    <th>{{ trans('front.manager') }}</th>
                </tr>
                </thead>
                <tbody>
                @if($packages->count())
                    @foreach($packages as $package)
                        <tr class="text-center">
                            <td>{{ date('M, d Y', strtotime($package->created_at)) }}</td>
                            <td class="package-group-item">
                                <div class="package-title">{{ $package->title }}</div>
                                <div class="hidden change-group-name">
                                    {!! Form::open(array('url' => 'user/manage-package-update/'.$package->id,  'method' => 'POST', 'role'=>'form', 'class'=> 'form update-package-title')) !!}
                                    <input type="text" class="form-control" name="title" value="{{ $package->title }}">
                                    {!! Form::close() !!}
                                </div>
                            </td>
                            <td>{{ $package->origination }}</td>
                            <td>{{ $package->destination }}</td>
                            <td>
                            @if($package->services->count())
                                @foreach($package->services as $packageService)
                                    <img src="{{ image_url($packageService->image) }}" >
                                @endforeach
                            @endif
                            </td>
                            <td>{!! $package->getShippingTime() !!}</td>
                            <td>Enverlope</td>
                            <td>{!! $package->getCost() !!}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu{{ $package->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Edit
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu{{ $package->id }}">
                                        <li><a href="{{ url('package/edit', $package->id) }}">Edit</a></li>
                                        <li><a href="{{ url('package/make-copy', array('id'=>$package->id) ) }}">Make a copy</a></li>
                                        <li><a href="{{ url('package/update-status', array('id'=>$package->id, 'status' => 'relist') ) }}">Relist</a></li>
                                        <li><a class="remove-item" href="{{ url('package/delete', array('id'=>$package->id) ) }}" data-alert="{{ trans('front.remove-confirm') }}">Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @else
                    <tr class="text-center">
                        <td colspan="9">Package need quote not found!</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 text-center">
            {!! $packages->links() !!}
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.update-package-title').submit(function(){
            var dis = $(this);
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: {title: $(this).find('input[name="title"]').val(), id: $(this).find('input[name="title"]').val()},
                success: function( msg ) {
                    dis.parent().parent().find('.package-title').removeClass('hidden').html(dis.find('input[name="title"]').val());
                    dis.parent('.change-group-name').addClass('hidden');
                }
            });
            return false;
        });
        $('.remove-item').click(function(){
            var dis = $(this);
            if(confirm($(this).attr('data-alert'))){
                $.ajax({
                    type: "DELETE",
                    url: $(this).attr('href'),
                    success: function( msg ) {
                        var response = JSON.parse(msg);
                        if( response.code == 1){
                            alert(response.message);
                            dis.closest('tr').remove();
                        }else{
                            alert(response.message);
                        }
                    }
                });
            }
            return false;
        });
        $('.package-group-item').click(function(){
            $(this).find('.package-title').addClass('hidden');
            $(this).find('.change-group-name').removeClass('hidden');
            return false;
        });
    });
</script>