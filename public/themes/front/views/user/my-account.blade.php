<section id="user-profile" class="user-profile">
    <div class="cover-image"><img src="{{ !empty($banner) ? $banner : image_url('images/banner-default.jpg') }}" alt="User Cover" class="avatar img-responsive"></div>
    <div class="user-header container">
        <div class="row">
            <div class="col-lg-4">
                <img src="{{ !empty($avatar) ? $avatar : image_url('images/avatar-default.jpg') }}" alt="Avatar" class="avatar img-responsive" />
            </div>
        </div>
    </div>
</section>