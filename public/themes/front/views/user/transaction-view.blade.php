<div class="row search-wrap">
    <div class="col-lg-12">
        <div class="row ">
            <div class="col-md-3">
                <div class="search">
                    <div class="inner-addon right-addon">
                        <i class="glyphicon glyphicon-search"></i>
                        <input type="text" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row form-inline">
                    <div class="col-md-4 form-inline">
                        {!! Form::label('from_date', trans('front.from_date')) !!}
                        {!! Form::text('from_date', null, array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="col-md-4 form-group">
                        {!! Form::label('to_date', trans('front.to_date')) !!}
                        {!! Form::text('to_date', null, array( 'class' => 'form-control' ) )  !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
        {!! Form::open(array('url' => 'user/manage-shipping',  'method' => 'POST', 'id' => 'manage-shipping', 'class'=> 'form-inline', 'role'=>'form')) !!}
        <div class="form-group">


        </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{!! trans('front.date') !!}</th>
                    <th>{!! trans('front.tracking_number') !!}</th>
                    <th>{!! trans('front.from') !!}</th>
                    <th>{!! trans('front.to') !!}</th>
                    <th>{!! trans('front.service') !!}</th>
                    <th>{!! trans('front.status') !!}</th>
                    <th>{!! trans('front.total_rate') !!}<br> (1)</th>
                    <th>{!! trans('front.sender_fees') !!}<br>(2)</th>
                    <th>{!! trans('front.transaction') !!}<br> (3)</th>
                    <th>{!! trans('front.fastast_fees') !!}<br>(4)=(2) + (3)</th>
                    <th>{!! trans('front.shipper_payment') !!}<br> (5)=(1)-(4)</th>
                </tr>
                </thead>
                <tbody>
                <tr class="text-center">
                    <td class="text-center">Jan 12, 2016</td>
                    <td>VietPost</td>
                    <td>XAZ1234BNnmb</td>
                    <td>Contracted</td>
                    <td>2,000,000 VND</td>
                    <td>0</td>
                    <td>-</td>
                    <td>-</td>
                    <td>0.00</td>
                    <td>$0.01</td>
                    <td>$24.99</td>
                </tr>
                <tr class="text-center">
                    <td class="text-center">Jan 12, 2016</td>
                    <td>VietPost</td>
                    <td>XAZ1234BNnmb</td>
                    <td>Contracted</td>
                    <td>2,000,000 VND</td>
                    <td>0</td>
                    <td>-</td>
                    <td>-</td>
                    <td>0.00</td>
                    <td>$0.01</td>
                    <td>$24.99</td>
                </tr>
                <tr class="text-center">
                    <td class="text-center">Jan 12, 2016</td>
                    <td>VietPost</td>
                    <td>XAZ1234BNnmb</td>
                    <td>Contracted</td>
                    <td>2,000,000 VND</td>
                    <td>0</td>
                    <td>-</td>
                    <td>-</td>
                    <td>0.00</td>
                    <td>$0.01</td>
                    <td>$24.99</td>
                </tr>
                <tr class="text-center">
                    <td class="text-center">Jan 12, 2016</td>
                    <td>VietPost</td>
                    <td>XAZ1234BNnmb</td>
                    <td>Contracted</td>
                    <td>2,000,000 VND</td>
                    <td>0</td>
                    <td>-</td>
                    <td>-</td>
                    <td>0.00</td>
                    <td>$0.01</td>
                    <td>$24.99</td>
                </tr>
                <tr class="text-center">
                    <td class="text-center">Jan 12, 2016</td>
                    <td>VietPost</td>
                    <td>XAZ1234BNnmb</td>
                    <td>Contracted</td>
                    <td>2,000,000 VND</td>
                    <td>0</td>
                    <td>-</td>
                    <td>-</td>
                    <td>0.00</td>
                    <td>$0.01</td>
                    <td>$24.99</td>
                </tr>

                </tbody>
                <tfoot>
                <tr>

                    <th colspan="6"> Total</th>
                    <th>$410</th>
                    <th>0.91</th>
                    <th>0.00</th>
                    <th>$0.91</th>
                    <th>$409.99</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="container">
    <h5>Shipper's type:</h5>
    <p>1, <span>Contracted:</span> Dành cho các công ty đã ký hợp đồng chấp thuận cung cấp dịch vụ trên Fastast nên không cần nạp card</p>
    <p>2, Registered: Dành cho các công ty, cá nhân đã đăng ký tài khoản trên Fastast và cấp dịch vụ</p>
    <p>3, Membership: Dành cho các công ty, cá nhân đã đăng ký tài khoản trên Fastast và cấp dịch vụ, đồng thời đã thanh toán phí thành viên hàng năm trên Fastast</p>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <ul class="pagination">
                <li class="previous"><a href="#">First</a></li>
                <li><a href="#">&laquo;</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">6</a></li>
                <li><a href="#">7</a></li>
                <li><a href="#">8</a></li>
                <li><a href="#">9</a></li>
                <li><a href="#">&raquo;</a></li>
                <li class="next"><a href="#">Last</a></li>
            </ul>
        </div>
    </div>
</div>
