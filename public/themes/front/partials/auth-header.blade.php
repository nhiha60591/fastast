<nav class="navbar">
    <div class="container col-md-12">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header col-md-2">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#home-nav-bar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{!! url('/') !!}">
                <img alt="Brand" src="{!! Theme::asset()->url('img/home-logo.png') !!}">
            </a>
        </div>
    </div><!-- /.container-fluid -->
</nav>