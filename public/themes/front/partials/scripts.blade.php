<script type="text/javascript">
    var message = [];
    message['add'] = '{{ trans('front.add') }}';
    message['remove'] = '{{ trans('front.remove') }}';
    message['package-size'] = '{{ trans('front.package-size') }}';
    message['base_url'] = '{{ url('/') }}';
    message['mapsArrow'] = '{!! Theme::asset()->url('img/maps-arrow-path.png') !!}';
    var base_url = '{{ url('/') }}';
    var theme_url = '{!! Theme::asset()->url('') !!}';
    
    var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;
	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};

</script>