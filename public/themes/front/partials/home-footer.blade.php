<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-sm-12 pull-left">
                <ul class="nav navbar-nav footer-select">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{!! get_cache('currentLanguage')->name !!}<span class="caret"></span></a>
                        <ul class="dropdown-menu" id="footer-language-menu">
                            @if(Theme::get('languages'))
                                @foreach(Theme::get('languages') as $language)
                                    <li><a href="{{ url('update/settings', array('type'=>'language', 'id' => $language->id)) }}">{{ $language->name }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{!! get_cache('currentCurrency')->name !!}<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @if(Theme::get('currencies'))
                                @foreach(Theme::get('currencies') as $currency)
                                    <li><a href="{{ url('update/settings', array('type'=>'currency', 'id' => $currency->id)) }}">{{ $currency->name }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{!! get_cache('currentWeightUnit')->name !!}<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @if(Theme::get('weightUnit'))
                                @foreach(Theme::get('weightUnit') as $unit)
                                    <li><a href="{{ url('update/settings', array('type'=>'weight-unit', 'id' => $unit->id)) }}">{{ $unit->name }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{!! get_cache('currentLongUnit')->name !!}<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @if(Theme::get('longUnit'))
                                @foreach(Theme::get('longUnit') as $unit)
                                    <li><a href="{{ url('update/settings', array('type'=>'long-unit', 'id' => $unit->id)) }}">{{ $unit->name }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-lg-6 col-sm-12 pull-right">
                <ul class="nav nav-pills navbar-right">
                    <li><a href="{{ url('about') }}">About</a></li>
                    <li><a href="{{ url('careers') }}">Careers</a></li>
                    <li><a href="{{ url('press') }}">Press</a></li>
                    <li><a href="{{ url('insurance-policies') }}">Insurance Policies</a></li>
                    <li><a href="{{ url('terms-privacy') }}">Terms & Privacy</a></li>
                    <li><a href="{{ url('contact') }}">Contact</a></li>
                </ul>
            </div>
        </div><!-- END .row -->

        <div class="social">
            <div class="col-lg-6 col-sm-12 pull-left">
                <p>
                    {{ trans('front.keep_in_touch') }}:
                    <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </p>
            </div>
            <div class="col-lg-6 col-sm-12 pull-right">
                <p class="text-right"><strong>{{ trans('front.copyright') }}</strong></p>
            </div>
        </div><!-- END .row -->
    </div>
</footer>