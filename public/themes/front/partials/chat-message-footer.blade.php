<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    {!! Form::open(array('url' => 'user/make-message',  'method' => 'POST', 'id' => 'post-message', 'files'=>true, 'role'=>'form', 'class'=> 'form')) !!}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create new message</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="to">To user</label>
                    <select name="to" id="to" class="form-control">
                        @foreach( $users as $user)
                        <option value="{{ $user->id }}">{!! $user->getFullName() !!}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="body">Message</label>
                    <textarea name="body" id="body" class="form-control" rows="4"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Create">
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>