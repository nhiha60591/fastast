<nav class="navbar navbar-fixed-top top-menu">
    <div class="container col-md-12">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header col-md-2">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#home-nav-bar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{!! url('/') !!}">
                <img alt="Brand" src="{!! Theme::asset()->url('img/home-logo.png') !!}">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="home-nav-bar">
            <div class="col-md-4">
                <form role="search" action="{!! url('search') !!}" method="get">
                    <div class="form-group">
                        <input type="text" name="trackingNumber" class="form-control top-header-search-box" placeholder="{{ trans('front.track_your_package_code_search') }}">
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6 form-group"><a href="{{ url('shipping/post-shipping') }}" class="btn">{{ trans('front.list-a-shipping-rate') }}</a></div>
                    <div class="col-md-6 form-group"><a href="{{ url('package/post-package') }}" class="btn">{{ trans('front.packages-need-quote') }}</a></div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    @if (Auth::guest())
                        <div class="col-md-6 form-group"><a href="{{ url('login') }}" class="btn border-none">{{ trans('front.login') }}</a></div>
                        <div class="col-md-6 form-group"><a href="{{ url('register') }}" class="btn border-none">{{ trans('front.register') }}</a></div>
                    @else
                        <div class="col-md-6 account-message"><a href="{{ url('user/chat-message') }}"><span class="header-message">4</span></a></div>
                        <div class="col-md-6 account-name">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        {{ trans('front.hello') }}<br />
                                        <strong>{{ Auth::user()->last_name }}</strong>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('user/update-profile') }}">{{ trans('front.my_account') }}</a></li>
                                        <li><a href="{{ url('logout') }}">{{ trans('front.logout') }}</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>