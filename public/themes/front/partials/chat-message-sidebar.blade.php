<div class="list-group">
    <a href="{{ url('user/chat-message') }}" class="list-group-item">
        <span class="badge">{!! $inboxCount or 0 !!}</span>
        Inbox
    </a>
    <a href="#" class="list-group-item" data-toggle="modal" data-target="#myModal">
        Create new message
    </a>
</div>