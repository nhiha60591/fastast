var markerNumber = 0;
var markerLocation = [];
function CenterControl(controlDiv, map, location) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.title = 'Click to create new place';
    controlUI.style.marginTop = '15px';
    controlUI.style.marginRight = '15px';
    controlUI.style.cursor = 'pointer';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.innerHTML = jQuery('.create-marker-icon').html();
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function(e) {
        map.setCenter(map.getCenter());
        placeMarker(location, map);
    });

}
function initMap() {
    var location = {lat: 10.8056949, lng: 106.6321204};
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: location,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    if( typeof locations == 'undefined') {
        // Create the DIV to hold the control and call the CenterControl()
        // constructor passing in this DIV.
        var centerControlDiv = document.createElement('div');
        var centerControl = new CenterControl(centerControlDiv, map, location);

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);
    }
}
function placeMarker(position, map) {
    var image = {
        url: jQuery('.marker-pin-icon').find('img').attr('src'),
        // This marker is 20 pixels wide by 32 pixels high.
        size: new google.maps.Size(38, 53),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(19, 55)
    };
    var marker = new google.maps.Marker({
        position: position,
        map: map,
        icon: image,
        draggable:true,
        title:"Drag me!"
    });
    var currentPosition = markerNumber;
    markerLocation[currentPosition] = [position.lng, position.lat];
    appendInputLocation();
    console.log(markerLocation);
    marker.addListener("dblclick", function(event) {
        marker.setMap(null);
        markerLocation.splice(currentPosition, 1);
        console.log(markerLocation);
        appendInputLocation();
    });
    google.maps.event.addListener(marker, 'dragend', function(event){
        markerLocation[currentPosition] = [event.latLng.lng(), event.latLng.lat()];
        console.log(markerLocation);
        appendInputLocation();
    });
    markerNumber++;
}
function appendInputLocation() {
    var html = '';
    jQuery.each(markerLocation, function( index, value ) {
        html += '<input type="hidden" id="location-item-long-'+index+'" name="locations['+index+'][long]" value="'+value[0]+'">\
            <input type="hidden" id="location-item-lat-'+index+'" name="locations['+index+'][lat]" value="'+value[1]+'">'
    });
    jQuery('#list-locations').html(html);
}
jQuery(document).ready(function ($) {
    var bounds = new google.maps.LatLngBounds();
    if( typeof locations != 'undefined') {
        for (var i = 0; i < locations.length; i++) {
            var image = {
                url: jQuery('.marker-pin-icon').find('img').attr('src'),
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(38, 53),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(19, 55)
            };
            var marker = new google.maps.Marker({
                position: {lat: locations[i][0], lng: locations[i][1]},
                map: map,
                icon: image,
            });
            bounds.extend(marker.getPosition());
        }
        map.fitBounds(bounds);
    }
});