var map;
jQuery(document).ready(function ($) {
    //code add more for info trip
    var number = 1;
    $(document).on('click', '.add-more-shipping-address', function () {
        var shippingServiceId = $('#shipping-service').val();

        var html = $('#from-to-box-item-default').html();
        html = html.replace(new RegExp('address\\[default\\]', 'g'),'address['+number+']');
        html = html.replace('address-from-default', 'address-from-'+number).replace('address-to-default', 'address-to-'+number);
        html = html.replace('address-from-default-error', 'address-from-'+number+'-error').replace('address-to-default-error', 'address-to-'+number+'-error');
        html = html.replace('for="address-from-default"', 'for="address-from-'+number+'"').replace('for="address-to-default"', 'for="address-to-'+number+'"');
        html = html.replace('address-from-long-default', 'address-from-long-'+number).replace('address-from-lat-default', 'address-from-lat-'+number).replace('address-to-long-default', 'address-to-long-'+number).replace('address-to-lat-default', 'address-to-lat-'+number);
        html = html.replace('address-from-location-default', 'address-from-location-'+number).replace('address-to-location-default', 'address-to-location-'+number);
        html = html.replace('<i class="remove-btn"></i>', '<a href="" class="btn remove-address btn-default remove margin-bottom-10" data-target="#from-to-box-item-'+number+'">'+message['remove']+'</a> <br />');
        
        html = '<div class="from-to-box-item" id="from-to-box-item-'+number+'" data-id="'+number+'">'+html+'</div>';
        $('.from-to-box').append(html);
        setInputMapsAddress('address-from-'+number, 'address-to-'+number, 'address-from-long-'+number, 'address-from-lat-'+number, 'address-to-long-'+number, 'address-to-lat-'+number, 'address-from-location-'+number, 'address-to-location-'+number);
        
        $('#from-to-box-item-'+number).find('.actions').children('.remove-address').remove();
        $('#from-to-box-item-'+number).find('.actions').children('br').remove();

        var prevElement = $('#from-to-box-item-'+number).prev('.from-to-box-item');
        if(prevElement.attr('data-id')==0)
            prevElement.find('.actions').html('<h4 class="remove-btn">&nbsp;</h4><a href="" class="btn remove-address btn-default remove margin-bottom-10" data-target="#from-to-box-item-'+prevElement.attr('data-id')+'">'+message['remove']+'</a>');
        else
            prevElement.find('.actions').html('<h4 class="remove-btn hidden">&nbsp;</h4><a href="" class="btn remove-address btn-default remove margin-bottom-10" data-target="#from-to-box-item-'+prevElement.attr('data-id')+'">'+message['remove']+'</a>');
        
        showHideOtherField(shippingServiceId);
        number ++;
        return false;
    });
    $(document).on('click', '.remove-address', function () {
        var target = $(this).attr('data-target');
        $(target).remove();
        var shippingServiceId = $('#shipping-service').val();
        showHideOtherField(shippingServiceId);
        return false;
    });

    //code add more for price trip
    var number2 = 0;
    $(document).on('click', '.btn-add-more-shipping-rate', function () {
        if(number2==0)
            number2 = $(this).parents('tbody').children('tr').length; 
        var html = $(this).parent().parent('.shipping-rate-item-last').html();
            html = html.replace(new RegExp('shippingRate\\[last\\]', 'g'),'shippingRate['+number2+']');
            html = html.replace('value="last"', 'value="item"');
            html = html.replace('over-last-item-0', 'over-last-item-'+number2);
            html = '<tr id="shipping-rate-item-'+number2+'">'+html+'</tr>';
        $(html).insertBefore($(this).parent().parent('.shipping-rate-item-last'));
        $(this).parents('tbody').children('tr#shipping-rate-item-'+number2).find('.actions').html('<button class="btn btn-default remove remove-shipping-rate" data-target="#shipping-rate-item-'+number2 + '">'+ message['remove'] +'</button>');
        
        var lastItem = $(this).parent().parent('.shipping-rate-item-last');
        var preItem = lastItem.prev('tr');
        preItem.find('.last-weight').val(lastItem.find('.last-weight').val());
        preItem.find('.last-rate').val(lastItem.find('.last-rate').val());
        preItem.find('.last-unit').val(lastItem.find('.last-unit').val());

        lastItem.find('.last-weight').val('');
        lastItem.find('.last-rate').val('');
        lastItem.find('.last-unit').val(0);
        number2++;
        
        return false;
    });

    $(document).on('click', '.remove-shipping-rate', function () {
        var target = $(this).parents('tr');
        $(target).remove();
        return false;
    });

    $(document).on('change', 'select.last-unit', function () {
        var current = $(this);
        var unitValue = current.val();
        current.parents('tr').nextAll('tr').find('select.last-unit').val(unitValue);
        return false;
    });

    //code add more for price trip
    var number3 = 1;
    $(document).on('click', '.add-shipping-trip', function () {
        var html = $(this).parent().parent('.shipping-rate-item-last').html();
        html = html.replace(new RegExp('shippingRate\\[last\\]', 'g'),'shippingRate['+number3+']');
        html = html.replace('value="last"', 'value="item"');
        html = html.replace('over-last-item-0', 'over-last-item-'+number3);
        html = '<tr id="shipping-rate-trip-item-'+number3+'">'+html+'</tr>';
        $(html).insertBefore($(this).parent().parent('.shipping-rate-item-last'));
        
        $(this).parents('.table').find('#shipping-rate-trip-item-'+number3).find('.actions').html('<button class="btn btn-default remove remove-shipping-trip-rate" data-target="#shipping-rate-trip-item-'+number3 + '">'+ message['remove'] +'</button>');
        
        //Copy data from last row
        $(this).parents('.table').find('#shipping-rate-trip-item-'+number3).find('select').val($(this).parents('tr').find('select').val());
        $(this).parents('.table').find('#shipping-rate-trip-item-'+number3).find('input[name*="trip_rate"]').val($(this).parents('tr').find('input[name*="trip_rate"]').val());
        $(this).parents('.table').find('#shipping-rate-trip-item-'+number3).find('input[name*="trip_loading"]').val($(this).parents('tr').find('input[name*="trip_loading"]').val());

        //Reset data of last row
        $(this).parents('tr').find('select').val($(this).parents('tr').find('select option:first').val());
        $(this).parents('tr').find('input[name*="trip_rate"]').val('');
        $(this).parents('tr').find('input[name*="trip_loading"]').val('');

        number3 ++;
        return false;
    });

    $(document).on('click', '.remove-shipping-trip-rate', function () {
        var target = $(this).attr('data-target');
        $(target).remove();
        return false;
    });

    $(document).on('keyup', '.shipping-rate-item-last .shipping-weight-input', function (e) {
        var dis = $(this).parent().parent().parent().parent().parent().parent();
        dis.find('.over-text').html($(this).val());
        dis.find('input.over-input').val($(this).val());
    });
    
    //Default Load
    var arr = JSON.parse($('#shipping-service option:selected').attr('data-servicetype'));
    setDefault(arr);

    $('#shipping-service').change(function (dis) {
        var arr = JSON.parse($(this).find('option:selected').attr('data-servicetype'));
        setDefault(arr);

        //Hiển thị số chuyến mỗi ngày và tiêu đề của From - To
        showHideOtherField($(this).val());
    });

    function showHideOtherField(shippingServiceId)
    {
        if(shippingServiceId==1 || shippingServiceId==7 || shippingServiceId==8)
        {
            $('.from-to-box-item').find('.location-title').hide();
            $('.row.more-field').addClass('hidden');
            $('.from-to-box-item').find('h4').addClass('hidden');
        }
        else
        {
            $('.from-to-box-item').find('.location-title').show();
            $('.row.more-field').removeClass('hidden');
            $('.from-to-box-item').find('h4').removeClass('hidden');
        }
        //Áp dụng riêng cho dòng đầu tiên
        $('.from-to-box .from-to-box-item:first').find('.location-title').show();
        $('.from-to-box .from-to-box-item:first').find('h4').removeClass('hidden');
    }

    function setDefault(arr)
    {
        var typeSelect = $('#select-service-type');
        var afterTypeSelect = $('.after-service-type');
        if( arr.length > 1){
            typeSelect.show();
            typeSelect.find('option').hide();
            var i=0;
            $.each(arr, function (i, val) {
                var currentOption = typeSelect.find('option#service-id-'+val);
                currentOption.show();
                i++;
                if( i == 1){
                    currentOption.prop('selected', true);
                }
            });
        }else if(arr.length == 1){
            typeSelect.hide();
            typeSelect.find('option').hide();
            typeSelect.val(1);
            afterTypeSelect.html('<input type="hidden" name="serviceType" value="'+arr[0]+'" />');
        }else{
            typeSelect.hide();
            typeSelect.find('option').hide();
            typeSelect.val(1);
        }
        if($('#shipping-service').val() == 3 || $('#shipping-service').val() == 4 ){
            $('.row.more-field').removeClass('hidden');
        }
        else
        {
            $('.row.more-field').addClass('hidden');
        }

        if($('#shipping-service').val() == 2 || $('#shipping-service').val() == 3 ){
            $('.shipping-service-box .arrow-right').addClass('hidden');
        }
        else
        {
            $('.shipping-service-box .arrow-right').removeClass('hidden');
        }

        showDefaultValue();
    }

    /*Nhan.Mai - 13-09-2016*/
    function showDefaultValue()
    {
        var serviceId = $('#shipping-service').val();
        var serviceType = $('#select-service-type').val();
        $('#shipping-rate-full-content').html($('#shipping-rate-'+serviceId+'-'+serviceType).html());
        var service = serviceId+'-'+serviceType;
        if(service=='3-1' || service=='4-4' || service=='5-4' || service=='6-4' || service=='8-6')
            $('#loadingUnloadingSurcharge').parents('tr').find('.form-group').hide();
        else
            $('#loadingUnloadingSurcharge').parents('tr').find('.form-group').show();
    }
    /*Nhan.Mai - 13-09-2016*/

    $('#select-service-type').change(function () {
        showDefaultValue();
    });

    $('body').on('focusin', '.shipping-rate-box tr', function(event) {
        event.preventDefault();
        $(this).addClass('focus');
    });

    $('body').on('focusout', '.shipping-rate-box tr', function(event) {
        event.preventDefault();
        $(this).removeClass('focus');
    });

});
/*tinymce.init({
    selector: 'textarea#shipping-content',
    height: 400,
    plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code'
    ],
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    content_css: [
        '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
        '//www.tinymce.com/css/codepen.min.css'
    ]
});
tinymce.init({
    selector: 'textarea#shipping-requirement',
    height: 400,
    plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code'
    ],
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    content_css: [
        '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
        '//www.tinymce.com/css/codepen.min.css'
    ]
});*/

function setInputMapsAddress(fromID, toID, fromLong, fromLat, toLong, toLat, fromLocation, toLocation) {
    var from = document.getElementById(fromID);
    var to = document.getElementById(toID);

    var fromAutoComplete = new google.maps.places.Autocomplete(from);
    var toAutoComplete = new google.maps.places.Autocomplete(to);

    fromAutoComplete.addListener('place_changed', function() {
        var place = fromAutoComplete.getPlace();
        document.getElementById(fromLat).value = place.geometry.location.lat();
        document.getElementById(fromLong).value = place.geometry.location.lng();
        document.getElementById(fromLocation).value = place.geometry.location;
        if (!place.geometry) {
            return;
        }
    });
    toAutoComplete.addListener('place_changed', function() {
        var place = toAutoComplete.getPlace();
        document.getElementById(toLat).value = place.geometry.location.lat();
        document.getElementById(toLong).value = place.geometry.location.lng();
        document.getElementById(toLocation).value = place.geometry.location;
        console.log(place);
        if (!place.geometry) {
            return;
        }
    });
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: new google.maps.LatLng(10.8056949,106.6321204),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var from = document.getElementById('address-from-0');
    var to = document.getElementById('address-to-0');

    var fromAutoComplete = new google.maps.places.Autocomplete(from);
    var toAutoComplete = new google.maps.places.Autocomplete(to);

    fromAutoComplete.addListener('place_changed', function() {

        var place = fromAutoComplete.getPlace();
        //console.log(place);
        if (!place.geometry) {
            return;
        }
        document.getElementById('address-from-lat-0').value = place.geometry.location.lat();
        document.getElementById('address-from-long-0').value = place.geometry.location.lng();
        document.getElementById('address-from-location-0').value = place.geometry.location;
    });
    toAutoComplete.addListener('place_changed', function() {
        var place = toAutoComplete.getPlace();
        if (!place.geometry) {
            return;
        }
        document.getElementById('address-to-lat-0').value = place.geometry.location.lat();
        document.getElementById('address-to-long-0').value = place.geometry.location.lng();
        document.getElementById('address-to-location-0').value = place.geometry.location;
    });
}