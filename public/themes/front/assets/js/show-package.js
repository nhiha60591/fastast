function initMap() {
    var homeLngLat = {lat: 10.8056949, lng: 106.6321204};
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: homeLngLat,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var image = jQuery('.marker-pin-icon').find('img').attr('src');
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < locations.length; i++) {
        var location = locations[i];
        var marker = new google.maps.Marker({
            position: {lat: location[1], lng: location[0]},
            map: map,
            icon: image
        });
        bounds.extend(marker.getPosition());
    }
    map.fitBounds(bounds);
}