var map, latLng, homeLatLng;
function initMap() {
    latLng = new google.maps.LatLng( 10.754919, 106.643336);
    homeLatLng = new google.maps.LatLng(10.753238, 106.639430);
    centerLatLng = new google.maps.LatLng(10.753238, 106.639430);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 2,
        center: centerLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var travel_mode = 'DRIVING';
    var origin_place_id = null;
    var destination_place_id = null;

    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(map);

    var inputFrom = document.getElementById('more-input-from');
    var inputTo = document.getElementById('more-input-to');
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(inputFrom);
    //map.controls[google.maps.ControlPosition.TOP_RIGHT].push(inputTo);
    var autocompleteFrom = new google.maps.places.Autocomplete(inputFrom);
    var autocompleteTo = new google.maps.places.Autocomplete(inputTo);
    //autocompleteFrom.bindTo('bounds', map);
    //autocompleteTo.bindTo('bounds', map);

    function expandViewportToFitPlace(map, place) {
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(2);
        }
    }

    autocompleteFrom.addListener('place_changed', function() {
        var place = autocompleteFrom.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
        document.getElementById('shipperFromLat').value = place.geometry.location.lat();
        document.getElementById('shipperFromLng').value = place.geometry.location.lng();
        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || ''),
                (place.address_components[3] && place.address_components[3].short_name || '')
            ].join(', ');
        }
        //document.getElementById('more-input-from').value = address;
        expandViewportToFitPlace(map, place);

        // If the place has a geometry, store its place ID and route if we have
        // the other place ID
        //origin_place_id = place.place_id;
        //route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay);
    });

    autocompleteTo.addListener('place_changed', function() {
        var place = autocompleteTo.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(', ');
        }
        //document.getElementById('more-input-to').value = address;
        //expandViewportToFitPlace(map, place);

        // If the place has a geometry, store its place ID and route if we have
        // the other place ID
        //destination_place_id = place.place_id;
        //route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay);
    });
    function route(origin_place_id, destination_place_id, travel_mode,
                   directionsService, directionsDisplay) {
        if (!origin_place_id || !destination_place_id) {
            return;
        }
        directionsService.route({
            origin: {'placeId': origin_place_id},
            destination: {'placeId': destination_place_id},
            travelMode: travel_mode
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
}
jQuery(document).ready(function ($) {
    var base_url = '';
    var bounds = new google.maps.LatLngBounds();
    //From
    var fromLat = getUrlParameter('shipperFromLat');
    var fromLng = getUrlParameter('shipperFromLng');
    var from = new google.maps.LatLng(fromLat, fromLng);
    bounds.extend(from);
    for (var i = 0; i < locations.length; i++) {
        var location = locations[i];
        var positionA = new google.maps.LatLng(location[2], location[3])
        var marker = new MarkerWithLabel({
            position: positionA,
            draggable: false,
            raiseOnDrag: false,
            map: map,
            labelContent: location[0],
            labelAnchor: new google.maps.Point(52, 36),
            labelClass: "labels map-marker-label", // the CSS class for the label
            labelStyle: {opacity: 1},
            icon: theme_url+'img/maps-arrow-path.png',
            title: location[0] + '- ' + location[1]
        });

        var boxText = document.createElement("div");
        boxText.style.cssText = "font-weigth: 600; font-color: white;  background: #0DD2A1; padding: 5px; border-radius: 4px; margin-top: -5px;";
        var boxHTML = $(location[4]).html();
        boxText.innerHTML = boxHTML;
        //var foot = document.createElement("div");
        //foot.style.cssText = "margin-bottom: 20px; 		width: 0; 		height: 0; 		border-style: solid; 		border-width: 10px 8px 0 8px; 		border-color: #03eccd transparent transparent transparent; ";
        var myOptions = {
            content: boxText
            ,disableAutoPan: false
            ,maxWidth: 0
            ,pixelOffset: new google.maps.Size(-100, -240)
            ,zIndex: null
            ,boxStyle: {
                background: ""
                ,opacity: 1
                ,width: "200px"
            }
            ,closeBoxMargin: "10px 2px 2px 2px"
            ,closeBoxURL: ""
            ,infoBoxClearance: new google.maps.Size(1, 1)
            ,isHidden: false
            ,pane: "floatPane"
            ,enableEventPropagation: false
        };
        var ib = new InfoBox(myOptions);
        //iw1.open(map, marker1);
        google.maps.event.addListener(marker, "click", function (e) {
            ib.close();
            ib.open(map, this);
        });
        $("#map").click(function(){
            ib.close();
        });
        bounds.extend(marker.getPosition());
        google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
            if (this.getZoom()){
                this.setZoom(14);
            }
        });
    }
    map.fitBounds(bounds);
    if(locations.length===0)
        map.setOptions({ maxZoom: 14 });
});