$('.delivery-service').selectpicker();
var map;
var markerNumber = 0;
var markerLocation = [];
jQuery(document).ready(function ($) {
    var cw = $('.package-image-item').width();
    $('.package-image-item').css({'height':cw+'px', 'line-height': cw+'px'});

    $('#datetimepicker1').datetimepicker();
    $('#datetimepicker2').datetimepicker({
        useCurrent: false //Important! See issue #1075
    });
    $("#datetimepicker1").on("dp.change", function (e) {
        $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker2").on("dp.change", function (e) {
        $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
    });
    var number = $('#delivery-service-count').val();
    $(document).on('click', '.btn-add-delivery-service', function () {
        var html = $('#delivery-service-item-0').html();
        html = '<div class="delivery-service-item" id="delivery-service-item-'+number+'">'+html+'</div>';
        $('.delivery-service-box').append(html);
        $('#delivery-service-item-'+number).find('.col-md-2').append('<button class="btn btn-default btn-remove-delivery-service" data-target="#delivery-service-item-'+number+'"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>');
        $('#delivery-service-item-'+number).find('.btn-add-delivery-service').remove();
        number++;
        return false;
    });
    $(document).on('click', '.btn-remove-delivery-service', function () {
        var target = $(this).attr('data-target');
        $(target).remove();
        return false;
    });
    $(document).on('click','.btn-upload-image',function(){
        var fileInput = $("#filer-input-"+$(this).attr('data-id'));
        fileInput.attr('data-id', $(this).attr('data-id') );
        fileInput.click();
        return false;
    });
    $(document).on('change','.package-image-file',function(event){
        var html = '';
        var id = $(this).attr('data-id');
        $.each(event.target.files, function(key, value){
            var reader = new FileReader();
            reader.onload = function (e) {
                var packaItem = $('#package-image-item-'+id);
                packaItem.find('img').attr('src', e.target.result);
                packaItem.find('.remove').show();
            };
            reader.readAsDataURL(value);
        });
        return false;
    });
    $(document).on('click', '.remove', function(event){
        var id = $(this).attr('data-id');
        var fileInput = $("#filer-input-"+id);
        fileInput.val('');
        var packaItem = $('#package-image-item-'+id);
        packaItem.find('img').attr('src', $('#default-camera-image').val());
        packaItem.find('.remove').hide();
        return false;
    });
});
function CenterControl(controlDiv, map, location) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.title = 'Click to create new place';
    controlUI.style.marginTop = '15px';
    controlUI.style.marginRight = '15px';
    controlUI.style.cursor = 'pointer';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.innerHTML = jQuery('.create-marker-icon').html();
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function(e) {
        map.setCenter(map.getCenter());
        placeMarker(map.getCenter(), map);
    });

}
function initMap() {
    var location = {lat: 10.8056949, lng: 106.6321204};
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: location,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    // Create the DIV to hold the control and call the CenterControl()
    // constructor passing in this DIV.
    var centerControlDiv = document.createElement('div');
    var centerControl = new CenterControl(centerControlDiv, map, location);

    centerControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);

    var from = document.getElementById('origination');
    var to = document.getElementById('destination');

    var fromAutoComplete = new google.maps.places.Autocomplete(from);
    var toAutoComplete = new google.maps.places.Autocomplete(to);

    fromAutoComplete.addListener('place_changed', function() {

        var place = fromAutoComplete.getPlace();
        //console.log(place);
        if (!place.geometry) {
            return;
        }
        document.getElementById('origination-location').value = place.geometry.location;
    });
    toAutoComplete.addListener('place_changed', function() {
        var place = toAutoComplete.getPlace();
        if (!place.geometry) {
            return;
        }
        document.getElementById('destination-location').value = place.geometry.location;
    });
}
function placeMarker(position, map) {
    var image = {
        url: jQuery('.marker-pin-icon').find('img').attr('src'),
        // This marker is 20 pixels wide by 32 pixels high.
        size: new google.maps.Size(38, 53),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(19, 55)
    };
    var marker = new google.maps.Marker({
        position: position,
        map: map,
        icon: image,
        draggable:true,
        title:"Drag me!"
    });
    var currentPosition = markerNumber;
    markerLocation[currentPosition] = [position.lng, position.lat];
    appendInputLocation();
    console.log(markerLocation);
    marker.addListener("dblclick", function(event) {
        marker.setMap(null);
        markerLocation.splice(currentPosition, 1);
        console.log(markerLocation);
        appendInputLocation();
    });
    google.maps.event.addListener(marker, 'dragend', function(event){
        markerLocation[currentPosition] = [event.latLng.lng(), event.latLng.lat()];
        console.log(markerLocation);
        appendInputLocation();
    });
    markerNumber++;
}
function appendInputLocation() {
    var html = '';
    jQuery.each(markerLocation, function( index, value ) {
        html += '<input type="hidden" id="location-item-long-'+index+'" name="locations['+index+'][long]" value="'+value[0]+'">\
            <input type="hidden" id="location-item-lat-'+index+'" name="locations['+index+'][lat]" value="'+value[1]+'">'
    });
    jQuery('#list-locations').html(html);
}