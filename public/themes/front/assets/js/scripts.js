tinymce.init({
    selector: 'textarea.tinymce',
    height: 200,
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
});

$(document).ready(function() {
	$('.selectpicker').selectpicker();
});