jQuery(document).ready(function ($) {
    var stickyNavTop = $('.navbar.navbar-fixed-top').offset().top;
    var stickyNav = function(){
        var scrollTop = $(window).scrollTop();
        if (scrollTop > stickyNavTop) {
            $('.navbar.navbar-fixed-top').addClass('sticky');
        } else {
            $('.navbar.navbar-fixed-top').removeClass('sticky');
        }
    };

    stickyNav();

    $(window).scroll(function() {
        stickyNav();
    });

});
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: new google.maps.LatLng(10.8056949,106.6321204),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var shipperFrom = document.getElementById('shipper-from');
    var shipperTo = document.getElementById('shipper-to');
    var packageFrom = document.getElementById('package-from');
    var packageTo = document.getElementById('package-to');

    var shipperFromAutoComplete = new google.maps.places.Autocomplete(shipperFrom);
    var shipperToAutoComplete = new google.maps.places.Autocomplete(shipperTo);
    var packageFromAutoComplete = new google.maps.places.Autocomplete(packageFrom);
    var packageToAutoComplete = new google.maps.places.Autocomplete(packageTo);

    shipperFromAutoComplete.addListener('place_changed', function() {

        var place = shipperFromAutoComplete.getPlace();
        //console.log(place);
        if (!place.geometry) {
            return;
        }
        document.getElementById('shipperFromLat').value = place.geometry.location.lat();
        document.getElementById('shipperFromLng').value = place.geometry.location.lng();
    });
    shipperToAutoComplete.addListener('place_changed', function() {
        var place = shipperToAutoComplete.getPlace();
        if (!place.geometry) {
            return;
        }
    });

    packageFromAutoComplete.addListener('place_changed', function() {
        var place = packageFromAutoComplete.getPlace();
        if (!place.geometry) {
            return;
        }
        document.getElementById('packageFromLat').value = place.geometry.location.lat();
        document.getElementById('packageFromLng').value = place.geometry.location.lng();
    });

    packageToAutoComplete.addListener('place_changed', function() {
        var place = packageToAutoComplete.getPlace();
        if (!place.geometry) {
            return;
        }
    });
}