<script type="text/javascript">
    var oLanguage = {
        "oAria": {
            "sSortAscending": ": {{ trans('pagination.sSortAscending') }}",
            "sSortDescending": ": {{ trans('pagination.sSortDescending') }}"
        },
        "oPaginate": {
            "sFirst": "{{ trans('pagination.sFirst') }}",
            "sLast": "{{ trans('pagination.sLast') }}",
            "sNext": "{{ trans('pagination.sNext') }}",
            "sPrevious": "{{ trans('pagination.sPrevious') }}"
        },
        "sEmptyTable": "{{ trans('pagination.sEmptyTable') }}",
        "sInfo": "{{ trans('pagination.sInfo') }}",
        "sInfoEmpty": "{{ trans('pagination.sInfoEmpty') }}",
        "sInfoFiltered": "{{ trans('pagination.sInfoFiltered') }}",
        "sInfoPostFix": "",
        "sDecimal": "",
        "sThousands": ",",
        "sLengthMenu": "{{ trans('pagination.sLengthMenu') }}",
        "sLoadingRecords": "{{ trans('pagination.sLoadingRecords') }}",
        "sProcessing": "{{ trans('pagination.sProcessing') }}",
        "sSearch": "{{ trans('pagination.sSearch') }}",
        "sSearchPlaceholder": "",
        "sUrl": "",
        "sZeroRecords": "{{ trans('pagination.sZeroRecords') }}"
    };
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
                <div class="pull-right box-tools">
                    <a href="{{ route('admin.unit.create') }}" class="btn btn-info btn-sm" data-toggle="tooltip" title="" data-original-title="Create"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>{{ trans('admin.no') }}</th>
                        <th>{{ trans('admin.name') }}</th>
                        <th>{{ trans('admin.slug') }}</th>
                        <th>{{ trans('admin.type') }}</th>
                        <th class="text-center">{{ trans('admin.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach( $allUnits as $u )
                        <tr>
                            <td>{{ $u->id}}</td>
                            <td>{{ $u->name}}</td>
                            <td>{{ $u->slug}}</td>
                            <td>{{ $u->type}}</td>
                            <td class="text-center">
                                <a href="{{ route('admin.unit.edit',array($u->id)) }}" data-method="post" rel="nofollow"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="{{ route('admin.unit.destroy',array($u->id)) }}" class="jquery-postback" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to delete this?"><i class="fa fa-times" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>{{ trans('admin.no') }}</th>
                        <th>{{ trans('admin.name') }}</th>
                        <th>{{ trans('admin.slug') }}</th>
                        <th>{{ trans('admin.type') }}</th>
                        <th class="text-center">{{ trans('admin.actions') }}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->