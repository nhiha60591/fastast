<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="edit-user" class="form-horizontal" action="{{ route('admin.unit.update',array($unit->id)) }}" method="post" data-toggle="validator">
                <input type="hidden" name="_method" value="PUT">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="email" class="col-sm-2 control-label">{{ trans('admin.name') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ $unit->name }}" name="name" class="form-control" id="name" placeholder="{{ trans('admin.name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                        <label for="slug" class="col-sm-2 control-label">{{ trans('admin.slug') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ $unit->slug }}" name="slug" class="form-control" id="slug" placeholder="{{ trans('admin.slug') }}">
                            @if ($errors->has('slug'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('slug') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="slug" class="col-sm-2 control-label">{{ trans('admin.type') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ $unit->type }}" name="type" class="form-control" id="type" placeholder="{{ trans('admin.type') }}">
                            @if ($errors->has('type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{{ trans('admin.update') }}</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>