<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                {!! Form::open(array('route' => array( 'admin.shipping-service.store'),  'method' => 'POST', 'id' => 'settings', 'role'=>'form','files' => true)) !!}
                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    {!! Form::label( 'status', trans( 'admin.status' ) ) !!}
                    {!! Form::select( 'status', array( '1' => 'Yes', '0' => 'No' ), old('status'), array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('status'))
                        <span class="help-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('types') ? ' has-error' : '' }}">
                    {!! Form::label( 'types', trans( 'admin.service_types' ) ) !!}
                    {!! Form::select( 'types[]', array( '1' => 'Giá theo khối lượng', '2' => 'Giá theo chuyến', '3' => 'Giá theo thể tích', '4' => 'Trong nước', '5' => 'Ngoài nước' ), old('services'), array( 'class' => 'form-control select2', 'multiple'=>'multiple', 'id' => 'types' ) ) !!}
                    @if ($errors->has('types'))
                        <span class="help-block">
                            <strong>{{ $errors->first('types') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    {!! Form::label( 'image', trans( 'admin.image' ) ) !!}
                    {!! Form::file( 'image', old('image'), array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
                </div>
                @if( !empty( $languages ) )
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <?php $i=1; ?>
                            @foreach($languages as $language)
                                <li{!! $i==1 ? ' class="active" ' : '' !!}><a href="#tab-{{ $language->id }}" data-toggle="tab">{{ $language->name }}</a></li>
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            <?php $i=1; ?>
                            @foreach($languages as $language)
                                <div class="tab-pane{!! $i==1 ? ' active' : '' !!}" id="tab-{{ $language->id }}">
                                    <div class="form-group{{ $errors->has('trans') ? ' has-error' : '' }}">
                                        {!! Form::label( 'trans-name-'.$language->id, trans( 'admin.name' ) ) !!}
                                        {!! Form::text( 'trans['.$language->id.'][name]', old('trans['.$language->id.'][name]'), array( 'class' => 'form-control', 'id' => 'trans-name-'.$language->id ) ) !!}
                                        @if ($errors->has('trans'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('trans') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('trans') ? ' has-error' : '' }}">
                                        {!! Form::label( 'trans-description-'.$language->id, trans( 'admin.description' ) ) !!}
                                        {!! Form::text( 'trans['.$language->id.'][description]', old('trans['.$language->id.'][description]'), array( 'class' => 'form-control', 'id' => 'trans-description-'.$language->id ) ) !!}
                                        @if ($errors->has('trans'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('trans') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <?php $i++; ?>
                            @endforeach
                        </div>
                        <!-- /.tab-content -->
                    </div>
                @endif
                <div class="box-footer text-right">
                    <input type="submit" class="btn btn-primary" value="{{ trans('admin.save') }}">
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".select2").select2();
    });
</script>