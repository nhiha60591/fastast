<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="edit-user" class="form-horizontal" action="{{ route('admin.user.store') }}" method="post" data-toggle="validator">
                <input type="hidden" name="_method" value="POST">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-sm-2 control-label">{{ trans('admin.email') }}</label>

                        <div class="col-sm-10">
                            <input type="email" value="{{ old('email') }}" name="email" class="form-control" id="email" placeholder="{{ trans('admin.email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <label for="first-name" class="col-sm-2 control-label">{{ trans('admin.first_name') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ old('first_name') }}" name="first_name" class="form-control" id="first-name" placeholder="{{ trans('admin.first_name') }}">
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <label for="last-name" class="col-sm-2 control-label">{{ trans('admin.last_name') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ old('last_name') }}" name="last_name" class="form-control" id="last-name" placeholder="{{ trans('admin.last_name') }}">
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                        <label for="full-name" class="col-sm-2 control-label">{{ trans('admin.full_name') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ old('full_name') }}" name="full_name" class="form-control" id="full-name" placeholder="{{ trans('admin.full_name') }}">
                            @if ($errors->has('full_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('full_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">{{ trans('admin.address') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ old('address') }}" name="address" class="form-control" id="address" placeholder="{{ trans('admin.address') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-sm-2 control-label">{{ trans('admin.phone') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ old('phone') }}" name="phone" class="form-control" id="phone" placeholder="{{ trans('admin.phone') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gender" class="col-sm-2 control-label">{{ trans('admin.gender') }}</label>

                        <div class="col-sm-10">
                            <select name="gender" class="form-control" id="gender">
                                <option value="0">{{ trans('admin.male') }}</option>
                                <option value="1">{{ trans('admin.female') }}</option>
                                <option value="2">{{ trans('admin.other') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">{{ trans('admin.status') }}</label>

                        <div class="col-sm-10">
                            <select name="status" class="form-control" id="status">
                                <option value="1">{{ trans('admin.active') }}</option>
                                <option value="0">{{ trans('admin.deactivate') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="role" class="col-sm-2 control-label">{{ trans('admin.role') }}</label>

                        <div class="col-sm-10">
                            <select name="role[]" multiple="multiple" data-placeholder="{{ trans('admin.role') }}" style="width: 100%;" class="form-control select2" id="role">
                                @foreach($roles as $role)
                                    <option {!! in_array($role->id, old('role') ? old('role') : array() ) ? ' selected="selected"' : '' !!} value="{{ $role->id }}">{{ $role->translation(1, 'name') }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-sm-2 control-label">{{ trans('admin.password') }}</label>

                        <div class="col-sm-10">
                            <input type="password" name="password" class="form-control" id="phone" placeholder="{{ trans('admin.password') }}">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password_confirmation" class="col-sm-2 control-label">{{ trans('admin.confirm_password') }}</label>

                        <div class="col-sm-10">
                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="{{ trans('admin.confirm_password') }}">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{{ trans('admin.update') }}</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>