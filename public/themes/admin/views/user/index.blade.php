<script type="text/javascript">
    var oLanguage = {
        "oAria": {
            "sSortAscending": ": {{ trans('pagination.sSortAscending') }}",
            "sSortDescending": ": {{ trans('pagination.sSortDescending') }}"
        },
        "oPaginate": {
            "sFirst": "{{ trans('pagination.sFirst') }}",
            "sLast": "{{ trans('pagination.sLast') }}",
            "sNext": "{{ trans('pagination.sNext') }}",
            "sPrevious": "{{ trans('pagination.sPrevious') }}"
        },
        "sEmptyTable": "{{ trans('pagination.sEmptyTable') }}",
        "sInfo": "{{ trans('pagination.sInfo') }}",
        "sInfoEmpty": "{{ trans('pagination.sInfoEmpty') }}",
        "sInfoFiltered": "{{ trans('pagination.sInfoFiltered') }}",
        "sInfoPostFix": "",
        "sDecimal": "",
        "sThousands": ",",
        "sLengthMenu": "{{ trans('pagination.sLengthMenu') }}",
        "sLoadingRecords": "{{ trans('pagination.sLoadingRecords') }}",
        "sProcessing": "{{ trans('pagination.sProcessing') }}",
        "sSearch": "{{ trans('pagination.sSearch') }}",
        "sSearchPlaceholder": "",
        "sUrl": "",
        "sZeroRecords": "{{ trans('pagination.sZeroRecords') }}"
    };
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ trans('user.list') }}</h3>
                <div class="pull-right box-tools">
                    <a href="{{ route('admin.user.create') }}" class="btn btn-info btn-sm" data-toggle="tooltip" title="" data-original-title="Create"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>{{ trans('admin.no') }}</th>
                        <th>{{ trans('admin.full_name') }}</th>
                        <th>{{ trans('admin.email') }}</th>
                        <th>{{ trans('admin.status') }}</th>
                        <th>{{ trans('admin.role') }}</th>
                        <th>{{ trans('admin.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->full_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{!! isset($user->status) && $user->status ? '<span class="label label-success">'.trans('admin.active').'</span>' : '<span class="label label-danger">'.trans('admin.deactivate').'</span>' !!}</td>
                            <td>
                                @if( $user->roles )
                                    @foreach($user->roles as $role)
                                        {{ $role->translation() }}
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.user.edit',array($user->id)) }}" data-method="post" rel="nofollow"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="{{ route('admin.user.destroy',array($user->id)) }}" class="jquery-postback" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to delete this?"><i class="fa fa-times" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>{{ trans('admin.no') }}</th>
                        <th>{{ trans('admin.full_name') }}</th>
                        <th>{{ trans('admin.email') }}</th>
                        <th>{{ trans('admin.status') }}</th>
                        <th>{{ trans('admin.role') }}</th>
                        <th>{{ trans('admin.actions') }}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->