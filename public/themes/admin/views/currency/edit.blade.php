{!! Form::open(array('route' => array( 'admin.localization.currency.update', $currency->id ), 'method' => 'POST', 'class' => 'form-horizontal single-dropzone', 'files' => true)) !!}
<input type="hidden" name="_method" value="PUT">
<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <div class="box-body">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {{ Form::label('name', trans('admin.name'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::text('name', $currency->name, array('class'=>'form-control', 'id'=>'name')) !!}
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                    {{ Form::label('code', trans('admin.code'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::text('code', $currency->code, array('class'=>'form-control', 'id'=>'code')) !!}
                        @if ($errors->has('code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('code') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('default') ? ' has-error' : '' }}">
                    {{ Form::label('default', trans('admin.default'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::select('default', array('1'=>'Yes', '0'=>'No'), $currency->default, array('class'=>'form-control', 'id'=>'default')) !!}
                        @if ($errors->has('default'))
                            <span class="help-block">
                                <strong>{{ $errors->first('default') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    {{ Form::label('status', trans('admin.status'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::select('status', array('yes'=>'Yes', 'no'=>'No'), $currency->status, array('class'=>'form-control', 'id'=>'status')) !!}
                        @if ($errors->has('status'))
                            <span class="help-block">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                    {{ Form::label('position', trans('admin.position'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::select('position', array('left'=>trans('currency.left'), 'left_space'=>trans('currency.left_space'), 'right'=>trans('currency.right'), 'right_space'=>trans('currency.right_space')), $currency->position, array('class'=>'form-control', 'id'=>'position')) !!}
                        @if ($errors->has('position'))
                            <span class="help-block">
                                <strong>{{ $errors->first('position') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('decimal_place') ? ' has-error' : '' }}">
                    {{ Form::label('decimal_place', trans('admin.decimal_place'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::text('decimal_place', $currency->decimal_place, array('class'=>'form-control', 'id'=>'decimal_place')) !!}
                        @if ($errors->has('decimal_place'))
                            <span class="help-block">
                                <strong>{{ $errors->first('decimal_place') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                    {{ Form::label('value', trans('admin.value'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::text('value', $currency->value, array('class'=>'form-control', 'id'=>'value')) !!}
                        @if ($errors->has('value'))
                            <span class="help-block">
                                <strong>{{ $errors->first('value') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('symbol') ? ' has-error' : '' }}">
                    {{ Form::label('symbol', trans('admin.symbol'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::text('symbol', $currency->symbol, array('class'=>'form-control', 'id'=>'symbol')) !!}
                        @if ($errors->has('symbol'))
                            <span class="help-block">
                                <strong>{{ $errors->first('symbol') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">{{ trans('admin.update') }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}