<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
            {!! Form::open(array('route' => array( 'admin.country.update', $country->id),  'method' => 'POST', 'id' => 'settings', 'role'=>'form','files' => true)) !!}
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    {!! Form::label( 'status', trans( 'admin.status' ) ) !!}
                    {!! Form::select( 'status', array( '1' => 'Yes', '0' => 'No' ), $country->status, array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('status'))
                        <span class="help-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('lon') ? ' has-error' : '' }}">
                    {!! Form::label( 'lon', trans( 'admin.lon' ) ) !!}
                    {!! Form::text( 'lon', $country->lon, array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('lon'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lon') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('lat') ? ' has-error' : '' }}">
                    {!! Form::label( 'lat', trans( 'admin.lat' ) ) !!}
                    {!! Form::text( 'lat', $country->lat, array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('lat'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lat') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('time_zone') ? ' has-error' : '' }}">
                    {!! Form::label( 'time_zone', trans( 'admin.time_zone' ) ) !!}
                    {!! Form::text( 'time_zone', $country->time_zone, array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('time_zone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('time_zone') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('post_code') ? ' has-error' : '' }}">
                    {!! Form::label( 'post_code', trans( 'admin.post_code' ) ) !!}
                    {!! Form::text( 'post_code', $country->post_code, array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('post_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('post_code') }}</strong>
                        </span>
                    @endif
                </div>
                @if( !empty( $languages ) )
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <?php $i=1; ?>
                        @foreach($languages as $language)
                            <li{!! $i==1 ? ' class="active" ' : '' !!}><a href="#tab-{{ $language->id }}" data-toggle="tab">{{ $language->name }}</a></li>
                            <?php $i++; ?>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        <?php $i=1; ?>
                        @foreach($languages as $language)
                            <div class="tab-pane{!! $i==1 ? ' active' : '' !!}" id="tab-{{ $language->id }}">
                                <div class="form-group{{ $errors->has('trans') ? ' has-error' : '' }}">
                                    {!! Form::label( 'trans-name-'.$language->id, trans( 'admin.name' ) ) !!}
                                    {!! Form::text( 'trans['.$language->id.'][name]', $country->translation($language->id, 'name'), array( 'class' => 'form-control', 'id' => 'trans-name-'.$language->id ) ) !!}
                                    @if ($errors->has('trans'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('trans') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('trans') ? ' has-error' : '' }}">
                                    {!! Form::label( 'trans-description-'.$language->id, trans( 'admin.description' ) ) !!}
                                    {!! Form::text( 'trans['.$language->id.'][description]', $country->translation($language->id, 'description'), array( 'class' => 'form-control', 'id' => 'trans-description-'.$language->id ) ) !!}
                                    @if ($errors->has('trans'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('trans') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <?php $i++; ?>
                        @endforeach
                    </div>
                    <!-- /.tab-content -->
                </div>
                @endif
                <div class="box-footer text-right">
                    <input type="submit" class="btn btn-primary" value="{{ trans('admin.save') }}">
                </div>
            {!! Form::close() !!}
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>
