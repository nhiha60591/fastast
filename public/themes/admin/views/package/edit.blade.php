<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="edit-user" class="form-horizontal" action="{{ route('admin.package.update',array($user->id)) }}" method="post" data-toggle="validator">
                <input type="hidden" name="_method" value="PUT">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-sm-2 control-label">{{ trans('user.email') }}</label>

                        <div class="col-sm-10">
                            <input type="email" value="{{ $user->email }}" readonly="readonly" name="email" class="form-control" id="email" placeholder="{{ trans('user.email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <label for="first-name" class="col-sm-2 control-label">{{ trans('user.first_name') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ $user->first_name }}" name="first_name" class="form-control" id="first-name" placeholder="{{ trans('user.first_name') }}">
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <label for="last-name" class="col-sm-2 control-label">{{ trans('user.last_name') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ $user->last_name }}" name="last_name" class="form-control" id="last-name" placeholder="{{ trans('user.last_name') }}">
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                        <label for="full-name" class="col-sm-2 control-label">{{ trans('user.full_name') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ $user->full_name }}" name="full_name" class="form-control" id="full-name" placeholder="{{ trans('user.full_name') }}">
                            @if ($errors->has('full_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('full_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">{{ trans('user.address') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ $user->address }}" name="address" class="form-control" id="address" placeholder="{{ trans('user.address') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-sm-2 control-label">{{ trans('user.phone') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ $user->phone }}" name="phone" class="form-control" id="phone" placeholder="{{ trans('user.phone') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gender" class="col-sm-2 control-label">{{ trans('user.gender') }}</label>

                        <div class="col-sm-10">
                            <select name="gender" class="form-control" id="gender">
                                <option value="0">{{ trans('user.male') }}</option>
                                <option value="1">{{ trans('user.female') }}</option>
                                <option value="2">{{ trans('user.other') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">{{ trans('user.status') }}</label>

                        <div class="col-sm-10">
                            <select name="status" class="form-control" id="status">
                                <option value="1">{{ trans('user.active') }}</option>
                                <option value="0">{{ trans('user.deactivate') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="role" class="col-sm-2 control-label">{{ trans('user.role') }}</label>

                        <div class="col-sm-10">
                            <select name="role[]" multiple="multiple" data-placeholder="{{ trans('user.role') }}" style="width: 100%;" class="form-control select2" id="role">
                                @foreach($roles as $role)
                                    <option {!! in_array($role->id, $currentRoles ) ? ' selected="selected"' : '' !!} value="{{ $role->id }}">{{ $role->display_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-sm-2 control-label">{{ trans('user.password') }}</label>

                        <div class="col-sm-10">
                            <input type="password" name="password" class="form-control" id="phone" placeholder="{{ trans('user.password') }}">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password_confirmation" class="col-sm-2 control-label">{{ trans('user.confirm_password') }}</label>

                        <div class="col-sm-10">
                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="{{ trans('user.confirm_password') }}">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{{ trans('user.update') }}</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>