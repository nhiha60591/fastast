<script type="text/javascript">
    var oLanguage = {
        "oAria": {
            "sSortAscending": ": {{ trans('pagination.sSortAscending') }}",
            "sSortDescending": ": {{ trans('pagination.sSortDescending') }}"
        },
        "oPaginate": {
            "sFirst": "{{ trans('pagination.sFirst') }}",
            "sLast": "{{ trans('pagination.sLast') }}",
            "sNext": "{{ trans('pagination.sNext') }}",
            "sPrevious": "{{ trans('pagination.sPrevious') }}"
        },
        "sEmptyTable": "{{ trans('pagination.sEmptyTable') }}",
        "sInfo": "{{ trans('pagination.sInfo') }}",
        "sInfoEmpty": "{{ trans('pagination.sInfoEmpty') }}",
        "sInfoFiltered": "{{ trans('pagination.sInfoFiltered') }}",
        "sInfoPostFix": "",
        "sDecimal": "",
        "sThousands": ",",
        "sLengthMenu": "{{ trans('pagination.sLengthMenu') }}",
        "sLoadingRecords": "{{ trans('pagination.sLoadingRecords') }}",
        "sProcessing": "{{ trans('pagination.sProcessing') }}",
        "sSearch": "{{ trans('pagination.sSearch') }}",
        "sSearchPlaceholder": "",
        "sUrl": "",
        "sZeroRecords": "{{ trans('pagination.sZeroRecords') }}"
    };
</script>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
                <div class="pull-right box-tools">
                    <a href="{{ route('admin.package.create') }}" class="btn btn-info btn-sm" data-toggle="tooltip" title="" data-original-title="Create"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>{{ trans('admin.no') }}</th>
                        <th>{{ trans('admin.name') }}</th>
                        <th>{{ trans('admin.quality') }}</th>
                        <th>{{ trans('admin.price') }}</th>
                        <th>{{ trans('admin.locate_send') }}</th>
                        <th>{{ trans('admin.locate_to') }}</th>
                        <th>{{ trans('admin.status') }}</th>
                        <th>{{ trans('admin.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if( !empty( $packages ) )
                            @foreach( $packages as $package )
                                <tr>
                                    <td>{{ trans('admin.no') }}</td>
                                    <td>{{ trans('admin.name') }}</td>
                                    <td>{{ trans('admin.quality') }}</td>
                                    <td>{{ trans('admin.price') }}</td>
                                    <td>{{ trans('admin.locate_send') }}</td>
                                    <td>{{ trans('admin.locate_to') }}</td>
                                    <td>{{ trans('admin.status') }}</td>
                                    <td>{{ trans('admin.actions') }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>{{ trans('admin.no') }}</th>
                        <th>{{ trans('admin.name') }}</th>
                        <th>{{ trans('admin.quality') }}</th>
                        <th>{{ trans('admin.price') }}</th>
                        <th>{{ trans('admin.locate_send') }}</th>
                        <th>{{ trans('admin.locate_to') }}</th>
                        <th>{{ trans('admin.status') }}</th>
                        <th>{{ trans('admin.actions') }}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->