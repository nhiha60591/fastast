<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="edit-package" class="form-horizontal" action="{{ route('admin.package.store') }}" method="post" data-toggle="validator">
                <input type="hidden" name="_method" value="POST">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group{{ $errors->has('	name') ? ' has-error' : '' }}">
                        <label for="name" class="col-sm-2 control-label">{{ trans('package.name') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="{{ trans('package.name') }}">
                            @if ($errors->has('	name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first( 'name' ) }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="unit" class="col-sm-2 control-label">{{ trans('package.cat') }}</label>

                        <div class="col-sm-10">
                            <select name="cat" class="form-control" id="cat">
                                <option value="0">{{ trans('package.centi') }}</option>
                                <option value="1">{{ trans('package.met') }}</option>
                                <option value="2">{{ trans('package.met') }}</option>
                                <option value="3">{{ trans('package.met') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                        <label for="price" class="col-sm-2 control-label">{{ trans('package.price') }}</label>

                        <div class="col-sm-10">
                            <input type="number" value="{{ old('price') }}" name="price" class="form-control" id="price" placeholder="{{ trans('package.price') }}">
                            @if ($errors->has('price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('locate_send') ? ' has-error' : '' }}">
                        <label for="locate_send" class="col-sm-2 control-label">{{ trans('package.locate_send') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ old('locate_send') }}" name="locate_send" class="form-control" id="locate_send" placeholder="{{ trans('package.locate_send') }}">
                            @if ($errors->has('locate_send'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('locate_send') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="locate_to" class="col-sm-2 control-label">{{ trans('package.locate_to') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ old('locate_to') }}" name="locate_to" class="form-control" id="locate_to" placeholder="{{ trans('package.locate_to') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="time_send" class="col-sm-2 control-label">{{ trans('package.time_send') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ old('time_send') }}" name="locate_to" class="form-control" id="time_send" placeholder="{{ trans('package.time_send') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="locate_to" class="col-sm-2 control-label">{{ trans('package.locate_to') }}</label>

                        <div class="col-sm-10">
                            <input type="text" value="{{ old('locate_to') }}" name="locate_to" class="form-control" id="locate_to" placeholder="{{ trans('package.locate_to') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="height" class="col-sm-2 control-label">{{ trans('package.height') }}</label>
                        <div class="col-sm-10">
                            <input type="number" value="{{ old('height') }}" name="height" class="form-control" id="height" placeholder="{{ trans('package.height') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="width" class="col-sm-2 control-label">{{ trans('package.width') }}</label>
                        <div class="col-sm-10">
                            <input type="number" value="{{ old('width') }}" name="width" class="form-control" id="width" placeholder="{{ trans('package.width') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="depth" class="col-sm-2 control-label">{{ trans('package.depth') }}</label>
                        <div class="col-sm-10">
                            <input type="number" value="{{ old('depth') }}" name="depth" class="form-control" id="depth" placeholder="{{ trans('package.depth') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="unit" class="col-sm-2 control-label">{{ trans('package.unit') }}</label>

                        <div class="col-sm-10">
                            <select name="unit" class="form-control" id="unit">
                                <option value="0">{{ trans('package.centimeter') }}</option>
                                <option value="1">{{ trans('package.metre') }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="quality" class="col-sm-2 control-label">{{ trans('package.quality') }}</label>
                        <div class="col-sm-10">
                            <input type="number" value="{{ old('quality') }}" name="quality" class="form-control" id="quality" placeholder="{{ trans('package.quality') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="unit" class="col-sm-2 control-label">{{ trans('package.status') }}</label>

                        <div class="col-sm-10">
                            <select name="unit" class="form-control" id="unit">
                                <option value="0">{{ trans('package.have') }}</option>
                                <option value="1">{{ trans('package.Leave') }}</option>
                                <option value="2">{{ trans('package.delivered') }}</option>
                            </select>
                        </div>
                    </div>


                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{{ trans('package.update') }}</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>