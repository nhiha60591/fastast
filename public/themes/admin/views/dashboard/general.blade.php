{!! Form::open(array('url' => 'admin/general/update',  'method' => 'POST', 'id' => 'settings', 'role'=>'form', 'files' => true)) !!}
<div class="row">
    <div class="col-md-12">
        <!-- Custom Tabs (Pulled to the right) -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-general" data-toggle="tab">{{ trans('admin.general') }}</a></li>
                <li><a href="#tab-seo" data-toggle="tab">{{ trans('admin.seo') }}</a></li>
                <li><a href="#tab-localization" data-toggle="tab">{{ trans('admin.localization') }}</a></li>
                <li><a href="#tab-email" data-toggle="tab">{{ trans('admin.email') }}</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab-general">
                    <div class="form-group">
                        {!! Form::label('site_name', trans('admin.site_name')) !!}
                        {!! Form::text('site_name', !empty($ops['site_name']) ? $ops['site_name'] : 'Fastast', array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('site_owner', trans('admin.site_owner')) !!}
                        {!! Form::text('site_owner', !empty($ops['site_owner']) ? $ops['site_owner'] : 'Fastast', array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('site_address', trans('admin.site_address')) !!}
                        {!! Form::text('site_address', !empty($ops['site_address']) ? $ops['site_address'] : 'Địa chỉ website', array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('site_email', trans('admin.email')) !!}
                        {!! Form::text('site_email', !empty($ops['site_email']) ? $ops['site_email'] : 'nhiha60591@gmail.com', array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('site_phone', trans('admin.site_phone')) !!}
                        {!! Form::text('site_phone', !empty($ops['site_phone']) ? $ops['site_phone'] : '0123 456 789', array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('site_fax', trans('admin.site_fax')) !!}
                        {!! Form::text('site_fax', !empty($ops['site_fax']) ? $ops['site_fax'] : '0123 456 789', array( 'class' => 'form-control' ) )  !!}
                    </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab-seo">
                    <div class="form-group">
                        {!! Form::label('site_title', trans('admin.site_title')) !!}
                        {!! Form::text('site_title', !empty($ops['site_title']) ? $ops['site_title'] : 'Fastast', array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('site_keyword', trans('admin.site_keyword')) !!}
                        {!! Form::text('site_keyword', !empty($ops['site_keyword']) ? $ops['site_keyword'] : 'Fastast', array( 'class' => 'form-control' ) )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('site_description', trans('admin.site_description')) !!}
                        {!! Form::text('site_description', !empty($ops['site_description']) ? $ops['site_description'] : 'Mô tả website', array( 'class' => 'form-control' ) )  !!}
                    </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab-localization">
                    <div class="form-group">
                        {!! Form::label('site_language', trans('admin.site_language')) !!}
                        {!! Form::select('site_language', $languages, !empty($ops['site_language']) ? $ops['site_language'] : Config::get('app.locale'), array('class'=>'form-control') )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('site_currency', trans('admin.site_currency')) !!}
                        {!! Form::select('site_currency', $currencies, !empty($ops['site_currency']) ? $ops['site_currency'] : 'vnd', array('class'=>'form-control') )  !!}
                    </div>
                    <div class="form-group">
                          {!! Form::label('long_unit', trans('admin.long_unit')) !!}
                          {!! Form::select('long_unit', $longUnit, !empty($ops['long_unit']) ? $ops['long_unit'] : 1, array('class'=>'form-control') )  !!}
                    </div>
                    <div class="form-group">
                          {!! Form::label('weight_unit', trans('admin.weight_unit')) !!}
                          {!! Form::select('weight_unit', $weightUnit, !empty($ops['weight_unit']) ? $ops['weight_unit'] : 1, array('class'=>'form-control') )  !!}
                    </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab-email">
                    <div class="form-group">
                        {!! Form::label('email_host', trans('admin.email_host')) !!}
                        {!! Form::text('email_host', !empty($ops['email_host']) ? $ops['email_host'] : 'localhost', array('class'=>'form-control') )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('smtp_secure', trans('admin.smtp_secure')) !!}
                        {!! Form::select('smtp_secure', array('none'=>trans('setting.none'), 'ssl'=>trans('setting.ssl'), 'tsl'=>trans('setting.tsl')), !empty($ops['smtp_secure']) ? $ops['smtp_secure'] : 'none', array('class'=>'form-control') )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('email_port', trans('admin.email_port')) !!}
                        {!! Form::text('email_port', !empty($ops['email_port']) ? $ops['email_port'] : '25', array('class'=>'form-control') )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('smtp_authentication', trans('admin.smtp_authentication')) !!}
                        {!! Form::select('smtp_authentication', array('no'=>'No', 'yes'=>'Yes'), !empty($ops['smtp_authentication']) ? $ops['smtp_authentication'] : 'yes', array('class'=>'form-control') )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('smtp_username', trans('admin.smtp_username')) !!}
                        {!! Form::text('smtp_username', !empty($ops['smtp_username']) ? $ops['smtp_username'] : 'nhiha60591@gmail.com', array('class'=>'form-control') )  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('smtp_password', trans('admin.smtp_password')) !!}
                        {!! Form::password('smtp_password', array('class'=>'form-control') )  !!}
                    </div>
                </div>
            </div>
            <!-- /.tab-content -->
            <div class="box-footer text-right">
                <input type="submit" class="btn btn-primary" value="{{ trans('admin.save') }}">
            </div>

        </div>

        <!-- nav-tabs-custom -->
    </div>
</div>
{!! Form::close() !!}