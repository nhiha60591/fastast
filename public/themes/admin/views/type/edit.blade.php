<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                {!! Form::open(array('route' => array( 'admin.type.update', $type->id),  'method' => 'POST', 'id' => 'settings', 'role'=>'form','files' => true)) !!}
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    {!! Form::label( 'image', trans( 'admin.image' ) ) !!}
                    @if( !empty( $type->image ) )
                        <div class="thumbnail">
                            <img src="{{ url($type->image) }}">
                        </div>
                    @endif
                    {!! Form::file( 'image', array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
                </div>
                @if( !empty( $languages ) )
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <?php $i=1; ?>
                            @foreach($languages as $language)
                                <li{!! $i==1 ? ' class="active" ' : '' !!}><a href="#tab-{{ $language->id }}" data-toggle="tab">{{ $language->name }}</a></li>
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            <?php $i=1; ?>
                            @foreach($languages as $language)
                                <div class="tab-pane{!! $i==1 ? ' active' : '' !!}" id="tab-{{ $language->id }}">
                                    <div class="form-group{{ $errors->has('trans') ? ' has-error' : '' }}">
                                        {!! Form::label( 'trans-name-'.$language->id, trans( 'admin.name' ) ) !!}
                                        {!! Form::text( 'trans['.$language->id.'][name]', $type->translation($language->id, 'name'), array( 'class' => 'form-control', 'id' => 'trans-name-'.$language->id ) ) !!}
                                        @if ($errors->has('trans'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('trans') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('trans') ? ' has-error' : '' }}">
                                        {!! Form::label( 'trans-description-'.$language->id, trans( 'admin.description' ) ) !!}
                                        {!! Form::text( 'trans['.$language->id.'][description]', $type->translation($language->id, 'description'), array( 'class' => 'form-control', 'id' => 'trans-description-'.$language->id ) ) !!}
                                        @if ($errors->has('trans'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('trans') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <?php $i++; ?>
                            @endforeach
                        </div>
                        <!-- /.tab-content -->
                    </div>
                @endif
                <div class="box-footer text-right">
                    <input type="submit" class="btn btn-primary" value="{{ trans('admin.save') }}">
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>
