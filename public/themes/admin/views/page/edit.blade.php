<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                {!! Form::open(array('route' => array( 'admin.page.update', $page->id),  'method' => 'POST', 'id' => 'settings', 'role'=>'form','files' => true)) !!}
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    {!! Form::label( 'status', trans( 'admin.status' ) ) !!}
                    {!! Form::select( 'status', array( 'yes' => 'Yes', 'no' => 'No' ), $page->status, array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('status'))
                        <span class="help-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('layout') ? ' has-error' : '' }}">
                    {!! Form::label( 'layout', trans( 'admin.layout' ) ) !!}
                    {!! Form::text( 'layout', $page->layout, array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('layout'))
                        <span class="help-block">
                            <strong>{{ $errors->first('layout') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('view') ? ' has-error' : '' }}">
                    {!! Form::label( 'view', trans( 'admin.view' ) ) !!}
                    {!! Form::text( 'view', $page->view, array( 'class' => 'form-control' ) ) !!}
                    @if ($errors->has('view'))
                        <span class="help-block">
                            <strong>{{ $errors->first('view') }}</strong>
                        </span>
                    @endif
                </div>
                @if( !empty( $languages ) )
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <?php $i=1; ?>
                            @foreach($languages as $language)
                                <li{!! $i==1 ? ' class="active" ' : '' !!}><a href="#tab-{{ $language->id }}" data-toggle="tab">{{ $language->name }}</a></li>
                                <?php $i++; ?>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            <?php $i=1; ?>
                            @foreach($languages as $language)
                                <div class="tab-pane{!! $i==1 ? ' active' : '' !!}" id="tab-{{ $language->id }}">
                                    <div class="form-group{{ $errors->has('trans_'.$language->id.'_title') ? ' has-error' : '' }}">
                                        {!! Form::label( 'trans-title-'.$language->id, trans( 'admin.title' ) ) !!}
                                        {!! Form::text( 'trans['.$language->id.'][title]', $page->translation($language->id, 'title'), array( 'class' => 'form-control', 'id' => 'trans-title-'.$language->id ) ) !!}
                                        @if ($errors->has('trans_'.$language->id.'_title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('trans_'.$language->id.'_title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('trans_'.$language->id. '_slug') ? ' has-error' : '' }}">
                                        {!! Form::label( 'trans-slug-'.$language->id, trans( 'admin.slug' ) ) !!}
                                        {!! Form::text( 'trans['.$language->id.'][slug]', $page->translation($language->id, 'slug'), array( 'class' => 'form-control', 'id' => 'trans-slug-'.$language->id ) ) !!}
                                        @if ($errors->has('trans_'.$language->id. '_slug'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('trans_'.$language->id. '_slug') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('trans_'.$language->id. '_content') ? ' has-error' : '' }}">
                                        {!! Form::label( 'trans-content-'.$language->id, trans( 'admin.content' ) ) !!}
                                        {!! Form::textarea( 'trans['.$language->id.'][content]', $page->translation($language->id, 'content'), array( 'class' => 'form-control text-content', 'id' => 'trans-content-'.$language->id ) ) !!}
                                        @if ($errors->has('trans_'.$language->id. '_content'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('trans_'.$language->id. '_content') }}</strong>
                                            </span>
                                        @endif
                                        <script>
                                            $(function () {
                                                // Replace the <textarea id="editor1"> with a CKEditor
                                                // instance, using default configuration.
                                                CKEDITOR.replace('<?php echo 'trans-content-'.$language->id; ?>');
                                            });
                                        </script>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            @endforeach
                        </div>
                        <!-- /.tab-content -->
                    </div>
                @endif
                <div class="box-footer text-right">
                    <input type="submit" class="btn btn-primary" value="{{ trans('admin.save') }}">
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>
