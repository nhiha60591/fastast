{!! Form::open(array('route' => array( 'admin.localization.language.update', $language->id ), 'method' => 'POST', 'class' => 'form-horizontal single-dropzone', 'files' => true)) !!}
<input type="hidden" name="_method" value="PUT">
<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <div class="box-body">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {{ Form::label('name', trans('admin.name'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::text('name', $language->name, array('class'=>'form-control', 'id'=>'name')) !!}
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    {{ Form::label('image', trans('admin.flag'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::text('image', $language->image, array('class'=>'form-control', 'id'=>'image')) !!}
                        @if ($errors->has('image'))
                            <span class="help-block">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                    {{ Form::label('code', trans('admin.code'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::text('code', $language->code, array('class'=>'form-control', 'id'=>'code')) !!}
                        @if ($errors->has('code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('code') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('locale') ? ' has-error' : '' }}">
                    {{ Form::label('locale', trans('admin.locale'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::text('locale', $language->locale, array('class'=>'form-control', 'id'=>'code')) !!}
                        @if ($errors->has('locale'))
                            <span class="help-block">
                                <strong>{{ $errors->first('locale') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('default') ? ' has-error' : '' }}">
                    {{ Form::label('default', trans('admin.default'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::select('default', array('1'=>'Yes', '0'=>'No'), $language->default, array('class'=>'form-control', 'id'=>'default')) !!}
                        @if ($errors->has('default'))
                            <span class="help-block">
                                <strong>{{ $errors->first('default') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    {{ Form::label('status', trans('admin.status'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::select('status', array('yes'=>'Yes', 'no'=>'No'), $language->status, array('class'=>'form-control', 'id'=>'status')) !!}
                        @if ($errors->has('status'))
                            <span class="help-block">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                    {{ Form::label('position', trans('admin.position'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        {!! Form::text('position', $language->position, array('class'=>'form-control', 'id'=>'code')) !!}
                        @if ($errors->has('position'))
                            <span class="help-block">
                                <strong>{{ $errors->first('position') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">{{ trans('admin.update') }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}