{!! Form::open(array('route' => 'admin.media.store', 'method' => 'POST', 'id' => 'my-dropzone', 'class' => 'form single-dropzone', 'files' => true)) !!}
    <input type="file" name="files[]" id="filer_input" multiple="multiple">
{!! Form::close() !!}
<style type="text/css">
    .media-image img{
        width: 100%;
        min-height: 100%;
        height: 150px;
    }
    .media-image{
        overflow: hidden;
    }
</style>