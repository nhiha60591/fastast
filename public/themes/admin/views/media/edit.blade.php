{!! Form::open(array('route' => array('admin.media.update', $media->id),  'method' => 'POST', 'id' => 'my-dropzone', 'class' => 'form-horizontal single-dropzone', 'files' => true)) !!}
<input type="hidden" name="_method" value="PUT">
<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Theme::get('subtitle') !!}</h3>
            </div>
            <div class="box-body">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {{ Form::label('name', trans('admin.image'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        <div class="thumbnail">
                            <img src="{{ image_url($media->file_url) }}" alt="{{ $media->name }}">
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {{ Form::label('name', trans('admin.name'), ['class' => 'col-sm-2 control-label']) }}

                    <div class="col-sm-10">
                        <input type="text" value="{{ $media->name }}" name="name" class="form-control" id="name" placeholder="{{ trans('media.name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('caption') ? ' has-error' : '' }}">
                    {{ Form::label('caption', trans('admin.caption'), ['class' => 'col-sm-2 control-label']) }}
                    <div class="col-sm-10">
                        <input type="text" value="{{ $media->caption }}" name="caption" class="form-control" id="caption" placeholder="{{ trans('media.caption') }}">
                        @if ($errors->has('caption'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('caption') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="col-sm-2 control-label">{{ trans('admin.description') }}</label>

                    <div class="col-sm-10">
                        {!! Form::textarea('description', $media->description, array('class'=>'form-control', 'id'=>'description')) !!}
                        @if ($errors->has('description'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">{{ trans('admin.update') }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}