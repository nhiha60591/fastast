<div class="row media-items">
    @if(sizeof($medias))
        @foreach($medias as $media)
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 media-item">
            <div class="box media-item box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $media->name }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body media-image">
                    <img draggable="false" src="{{ image_url($media->file_url) }}" alt="{{ $media->name }}">
                </div>
                <!-- /.box-body -->
                <div class="box-footer media-action text-right">
                    <ul class="list-inline pull-left">
                        <li><span class="jFiler-item-others">{!! formatBytes($media->file_size) !!}</span></li>
                    </ul>
                    <ul class="list-inline pull-right">
                        <li><a class="icon-jfi-edit jFiler-item-edit-action" href="{{ route('admin.media.edit', array($media->id)) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                        <li><a class="icon-jfi-trash jFiler-item-trash-action remove-media" href="{{ route('admin.media.destroy', array($media->id)) }}"></a></li>
                    </ul>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
        @endforeach
    @else
        <div class="col-lg-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('admin.media_notFound') }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <p>{{ trans('admin.Please') }} <a href="{{ route('admin.media.create') }}">{{ trans('admin.media_create') }}</a></p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    @endif
    <div class="col-lg-12">
        {!! $medias->render() !!}
    </div>
</div>
<style type="text/css">
    .media-image img{
        width: 100%;
        min-height: 100%;
        height: 150px;
    }
    .media-image{
        overflow: hidden;
    }
</style>