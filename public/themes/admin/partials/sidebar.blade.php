<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Theme::asset()->url('img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Users</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">package</li>
            <li class="treeview{{ Theme::get('mainactive') == 'dashboard' ? ' active' : '' }}">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Theme::get('subactive') == 'general' ? ' active' : '' }}"><a href="{{ url('admin/general') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.general') }}</a></li>
                    <li class="{{ Theme::get('subactive') == 'localization' ? ' active' : '' }}">
                        <a href="#"><i class="fa fa-circle-o"></i> {{ trans('admin.localization') }}</a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('admin/localization/language') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.language') }}</a></li>
                            <li><a href="{{ url('admin/localization/currency') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.currency') }}</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview{{ Theme::get('mainactive') == 'page' ? ' active' : '' }}">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>{{ trans('admin.page') }}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Theme::get('subactive') == 'create' ? ' active' : '' }}"><a href="{{ route('admin.page.create') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.page_create') }}</a></li>
                    <li class="{{ Theme::get('subactive') == 'list' ? ' active' : '' }}"><a href="{{ route('admin.page.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.page') }}</a></li>
                </ul>
            </li>
            <li class="treeview{{ Theme::get('mainactive') == 'package' ? ' active' : '' }}">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>{{ trans('admin.package') }}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Theme::get('subactive') == 'shipping-service' ? ' active' : '' }}"><a href="{{ route('admin.shipping-service.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.shippingService') }}</a></li>
                    <li class="{{ Theme::get('subactive') == 'unit' ? ' active' : '' }}"><a href="{{ route('admin.unit.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.unit') }}</a></li>
                </ul>
            </li>
            {{--<li class="treeview{{ Theme::get('mainactive') == 'shipping' ? ' active' : '' }}">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>{{ trans('admin.shipping') }}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Theme::get('subactive') == 'create' ? ' active' : '' }}"><a href="{{ route('admin.shipping.create') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.shipping_create') }}</a></li>
                    <li class="{{ Theme::get('subactive') == 'list' ? ' active' : '' }}"><a href="{{ route('admin.shipping.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.shipping') }}</a></li>
                </ul>
            </li>--}}
            <li class="treeview{{ Theme::get('mainactive') == 'user' ? ' active' : '' }}">
                <a href="{{ route('admin.user.index') }}">
                    <i class="fa fa-user"></i>
                    <span>{{ trans('admin.user') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Theme::get('subactive') == 'list' ? ' active' : '' }}"><a href="{{ route('admin.user.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.user_list') }}</a></li>
                    <li class="{{ Theme::get('subactive') == 'create' ? ' active' : '' }}"><a href="{{ route('admin.user.create') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.user_create') }}</a></li>
                </ul>
            </li>
            <li class="treeview{{ Theme::get('mainactive') == 'media' ? ' active' : '' }}">
                <a href="{{ route('admin.media.index') }}">
                    <i class="fa fa-music" aria-hidden="true"></i>
                    <span>{{ trans('admin.media') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Theme::get('subactive') == 'list' ? ' active' : '' }}"><a href="{{ route('admin.media.index') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.media_list') }}</a></li>
                    <li class="{{ Theme::get('subactive') == 'create' ? ' active' : '' }}"><a href="{{ route('admin.media.create') }}"><i class="fa fa-circle-o"></i> {{ trans('admin.media_create') }}</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>