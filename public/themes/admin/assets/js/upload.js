$(document).ready(function() {
    var uploadUrl = $("#my-dropzone").attr('action');
    var item = '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 media-item">\
                    <div class="box media-item box-success">\
                        <div class="box-header with-border">\
                        <h3 class="box-title">{{fi-name}}</h3>\
                    </div>\
                    <div class="box-body media-image">\
                        {{fi-image}}\
                        </div>\
                    <div class="box-footer media-action text-right">\
                        <ul class="list-inline pull-left">\
                            <li>{{fi-progressBar}}</li>\
                        </ul>\
                        <ul class="list-inline pull-right">\
                            <li><a class="icon-jfi-edit jFiler-item-edit-action"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>\
                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                        </ul>\
                    </div>\
                </div>\
                </div>';
    var itemAppend = '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 media-item">\
                        <div class="box media-item box-success">\
                            <div class="box-header with-border">\
                            <h3 class="box-title">{{fi-name}}</h3>\
                        </div>\
                        <div class="box-body media-image">\
                            {{fi-image}}\
                        </div>\
                        <div class="box-footer media-action">\
                            <ul class="list-inline pull-left">\
                                <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                            </ul>\
                            <ul class="list-inline pull-right">\
                                <li><a class="icon-jfi-edit jFiler-item-edit-action"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>\
                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                            </ul>\
                        </div>\
                    </div>';
    $('#filer_input').filer({
        changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<div class="row media-items"></div>',
            item: item,
            itemAppend: itemAppend,
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: true,
            _selectors: {
                list: '.media-items',
                item: '.media-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
        },
        onRemove: function (vl) {
            $.post({
                type: 'DELETE',
                url: vl.find('.icon-jfi-trash.jFiler-item-trash-action').data('url')
            }).done(function (data) {
                alert("Remove successfully!");
            });
        },
        uploadFile: {
            url: uploadUrl,
            data: null,
            type: 'POST',
            enctype: 'multipart/form-data',
            beforeSend: function(){},
            success: function(data, el){
                var parent = el.find(".jFiler-jProgressBar").parent();
                var res = JSON.parse(data);
                el.find(".icon-jfi-edit.jFiler-item-edit-action").attr('href', res.editUrl).attr('data-url', res.editUrl).attr('data-id', res.id);
                el.find(".icon-jfi-trash.jFiler-item-trash-action").attr('data-url', res.removeUrl).attr('data-id', res.id);
                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                    $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
                });
            },
            error: function(el){
                var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                    $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                });
            },
            statusCode: null,
            onProgress: null,
            onComplete: null
        }
    });
    $('.remove-media').click(function () {
        if(confirm("Are you sure to remove this media")) {
            $.post({
                type: 'DELETE',
                url: $(this).attr('href')
            }).done(function (data) {
                alert("Remove successfully!");
                location.reload();
            });
        }
        return false;
    });
});