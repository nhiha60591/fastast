$(document).ready(function ($) {
    $("#example1").DataTable({
        "oLanguage": oLanguage
    });
    $(document).on('click', 'a.jquery-postback', function(e) {
        e.preventDefault(); // does not go through with the link.
        if(confirm("Are you to remove this item?")) {
            var $this = $(this);

            $.post({
                type: $this.data('method'),
                url: $this.attr('href')
            }).done(function (data) {
                var response = JSON.parse(data);
                alert(response.message);
                if(response.code == 200) {
                    $this.closest('tr').remove();
                }
            });
        }else{
            return false;
        }
    });    
});