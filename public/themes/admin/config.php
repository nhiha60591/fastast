<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            // You can remove this line anytime.
            $theme->setTitle('Dashboard');

            // Breadcrumb template.
            $theme->breadcrumb()->setTemplate('
                 <ul class="breadcrumb">
                    @foreach ($crumbs as $i => $crumb)
                     @if ($i != (count($crumbs) - 1))
                        <li><a href="{{ $crumb["url"] }}">{!! $crumb["label"] !!}</a><span class="divider">/</span></li>
                     @else
                        <li class="active">{!! $crumb["label"] !!}</li>
                     @endif
                    @endforeach
                </ul>
            ');
            $theme->breadcrumb()->add('<i class="fa fa-dashboard"></i> Home', url('/'));
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            // Style on header
            $theme->asset()->usePath()->add('bootstrap', 'bootstrap/css/bootstrap.min.css');
            $theme->asset()->add('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css');
            $theme->asset()->add('ionicons', 'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css');
            $theme->asset()->usePath()->add('AdminLTE', 'css/AdminLTE.min.css');
            $theme->asset()->usePath()->add('_all-skins', 'css/skins/_all-skins.min.css');
            $theme->asset()->usePath()->add('bootstrap3-wysihtml5', 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');


            // Scripts on footer
            $theme->asset()->usePath()->add('jquery', 'plugins/jQuery/jQuery-2.2.0.min.js');
            $theme->asset()->container('footer')->usePath()->add('settings', 'js/settings.js', array('jquery'));
            $theme->asset()->container('footer')->usePath()->add('bootstrap-js', 'bootstrap/js/bootstrap.min.js', array('jquery'));
            $theme->asset()->container('footer')->usePath()->add('jquery.slimscroll-js', 'plugins/slimScroll/jquery.slimscroll.min.js', array('jquery'));
            $theme->asset()->container('footer')->usePath()->add('fastclick-js', 'plugins/fastclick/fastclick.js', array('jquery'));
            $theme->asset()->container('footer')->usePath()->add('app-js', 'js/app.min.js', array('jquery'));
            $theme->asset()->container('footer')->usePath()->add('demo-js', 'js/demo.js', array('jquery'));
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => array(

            'default' => function($theme)
            {
                // $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            }

        )

    )

);