<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '1694651000811050',
        'client_secret' => '432ed7efbe57155fc9e7ea0be69bfed1',
        'redirect' => env('APP_URL', 'http://fastast.dev').'/vi/login/facebook',
    ],
    'google' => [
        'client_id' => '120525875709-4ile514fksoru2c9sic4pfnbkqbp9tap.apps.googleusercontent.com',
        'client_secret' => 'wg4ezU33yn3RZgKCjQbHHYGQ',
        'redirect' => env('APP_URL', 'http://fastast.dev'). '/vi/login/google',
    ],
    'twitter' => [
        'client_id' => 'uq1stZ91qwQXSTcBwHJc8aCic',
        'client_secret' => 'r2bnJQBcj3YH7QcYK54Su35ZfS8O4hsDwQwRHXIMRKZ3SlW1ii',
        'redirect' => env('APP_URL', 'http://fastast.dev').'/vi/login/twitter',
    ],

];
