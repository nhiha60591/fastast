<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rate';
    protected $fillable = ['1star', '2star', '3star', '4star', '5star', 'model', 'model_id'];
}
