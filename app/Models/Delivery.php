<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = 'delivery';
    protected $fillable = ['id', 'status'];
    public function shipping(){
        return $this->hasOne('App\Models\Shipping');
    }
    public function trans(){
        return $this->hasMany('App\Models\DeliveryTrans');
    }
    public function translation($language_id=1, $field='name'){
        if( $this->trans()->where('language_id', $language_id)->count() ) {
            $trans = $this->trans()->where('language_id', $language_id)->first()->$field;
            return $trans;
        }else{
            return null;
        }
    }
}
