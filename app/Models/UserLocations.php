<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLocations extends Model
{
    protected $table = 'user_locations';
    protected $fillable = ['id', 'user_id', 'lng', 'lat', 'name', 'type', 'description'];
    public function user(){
        return $this->belongsToMany('App\Models\User', 'user_id');
    }
}
