<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleTrans extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_trans';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['role_id', 'language_id', 'name', 'display_name', 'description'];
    public function role(){
        return $this->belongsToMany('App\Models\Role', 'role_id');
    }
    public function language(){
        return $this->belongsToMany('App\Models\Language', 'language_id');
    }
}
