<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingRate extends Model
{
    protected $table = 'shipping_rates';
    protected $fillable = ['id', 'shipping_id', 'weight', 'rate', 'unit', 'trip_type', 'trip_rate', 'trip_loading'];
    public function shippings(){
        return $this->belongsToMany('App\Models\Shipping', 'shipping_id');
    }
}
