<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    protected $table = 'service_type';
    protected $fillable = ['service_id', 'type_id'];
    public function service(){
        return $this->hasOne('App\Models\ShippingService');
    }
    public function type(){
        return $this->hasOne('App\Models\ServiceTypes');
    }
}
