<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Package extends Model
{
    protected $table = 'packages';
    protected $fillable = [
        'id',
        'status',
        'user_id',
        'origination',
        'origination_location',
        'destination',
        'destination_location',
        'shipping_cost',
        'shipping_time',
        'departure_date',
        'arrival_date',
        'options',
        'content',
        'contact_name',
        'contact_email',
        'contact_phone',
        'image'
    ];
    public function images() {
        return $this->belongsToMany('App\Models\Media', 'package_images', 'package_id', 'image_id');
    }
    public function services() {
        return $this->belongsToMany('App\Models\ShippingService', 'package_service', 'package_id', 'service_id');
    }
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function locations(){
        return $this->hasMany('App\Models\PackageLocations');
    }
    public function getShippingTime(){
        if(!$this->shipping_time){
            return 'All shipping time';
        }else{
            $currentLanguage = Cache::get( 'currentLanguage' );
            $delivery = Delivery::find($this->shipping_time);
            return $delivery->translation($currentLanguage->id, 'name');
        }
    }
    public function getLocations(){
        return str_replace(array('(', ')'), '', $this->origination_location);
    }
    public function getCost(){
        if( $this->shipping_cost ){
            $currentCurrency = Cache::get( 'currentCurrency' );
            $total = (double)$this->shipping_cost * (double)$currentCurrency->value;
            $total = number_format($total);
            return $total. " ". $currentCurrency->symbol;
        }else{
            return 'Negotiation';
        }
    }
    public function getOptions(){
        if( $this->options ){
            $array = explode(',', $this->options);
            return $array;
        }
        return array();
    }
}
