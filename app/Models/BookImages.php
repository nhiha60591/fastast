<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookImages extends Model
{
    protected $table = 'book_images';
    protected $fillable = ['book_id', 'media_id'];
    public function media(){
        return $this->hasOne('App\Models\Media');
    }
    public function book(){
        return $this->hasOne('App\Models\BookShipping');
    }
}
