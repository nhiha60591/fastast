<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'language';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'code', 'locale', 'image', 'default', 'status', 'position'];
    public function roles() {
        return $this->belongsToMany('App\Models\Role');
    }
    public function shipping_services() {
        return $this->belongsToMany('App\Models\ShippingService');
    }
    public function countries() {
        return $this->hasMany('App\Models\CountryTrans');
    }
    public function cities() {
        return $this->hasMany('App\Models\CityTrans');
    }
    public function districts() {
        return $this->hasMany('App\Models\DistrictTrans');
    }
    public function packages() {
        return $this->hasMany('App\Models\PackageTrans');
    }
    public function types() {
        return $this->hasMany('App\Models\TypeTrans');
    }
    public function pages() {
        return $this->hasMany('App\Models\PageTrans');
    }
    public function serviceTypes() {
        return $this->hasMany('App\Models\ServiceTypeTrans');
    }
}
