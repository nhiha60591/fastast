<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeTrans extends Model
{
    protected $table = 'type_trans';
    protected $fillable = ['type_id', 'language_id', 'name', 'description'];
    /**
     * Get Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type(){
        return $this->hasOne('App\Models\Type', 'type_id');
    }

    /**
     * Get Language
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language(){
        return $this->hasOne('App\Models\Language', 'language_id');
    }
}
