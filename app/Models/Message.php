<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'message';
    protected $fillable = [
        'id',
        'status',
        'conversation_id',
        'from',
        'to',
        'read',
        'subject',
        'body',
        'created_at',
        'updated_at'
    ];
    public function fromUser(){
        return $this->belongsTo('App\Models\User', 'from');
    }
    public function toUser(){
        return $this->belongsTo('App\Models\User', 'to');
    }
}
