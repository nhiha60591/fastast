<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingService extends Model
{
    protected $table = 'shipping_service';
    protected $fillable = ['id', 'image', 'status'];
    public function trans() {
        return $this->hasMany('App\Models\ShippingServiceTrans');
    }
    public function translation( $language = null, $filed='name'){
        if( $this->trans()->where('language_id', $language)->count() ) {
            $trans = $this->trans()->where('language_id', $language)->first()->$filed;
            return $trans;
        }else{
            return null;
        }
    }
    public function shipping(){
        return $this->hasOne('App\Models\Shipping');
    }
    public function types(){
        return $this->belongsToMany('App\Models\ServiceTypes', 'service_type', 'service_id', 'type_id');
    }
    public function listType($field='id'){
        $data = array();
        foreach ($this->types()->get() as $type){
            $data[] = $type->$field;
        }
        $data = array_unique($data);
        return json_encode($data);
    }
    public function package() {
        return $this->belongsToMany('App\Models\Package');
    }
    public function serviceWayImage(){
        switch ($this->id){
            case 1:
                return 'express-way';
                break;
            case 2:
                return 'road-way';
                break;
            case 3:
                return 'home-office-way';
                break;
            case 4:
                return 'car-way';
                break;
            case 5:
                return 'light-truct-way';
                break;
            case 6:
                return 'container-way';
                break;
            case 7:
                return 'airline-way';
                break;
            case 8:
                return 'sea-way';
                break;
            default:
                return 'express-way';
                break;
        }
    }
}
