<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    protected $table = 'option';
    protected $fillable = ['id', 'option_key', 'option_value', 'status'];
}
