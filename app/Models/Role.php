<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'status'];

    public function scopeActive($query) {
        return $query->whereStatus('1');
    }

    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
        return $this->belongsToMany('App\Models\User');
    }

    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trans() {
        return $this->hasMany('App\Models\RoleTrans');
    }
    public function translation($language_id=1, $field='name'){
        if( $this->trans()->where('language_id', $language_id)->count() ) {
            $trans = $this->trans()->where('language_id', $language_id)->first()->$field;
            return $trans;
        }else{
            return null;
        }
    }

}