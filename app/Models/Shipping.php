<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Shipping extends Model
{
    protected $table = 'shipping';
    protected $fillable = ['id', 'user_id', 'service_id', 'service_type_id', 'delivery_id', 'math', 'fuel_surcharge', 'fragile_surcharge', 'loading_unloading_surcharge', 'status', 'max_length', 'max_width', 'max_height', 'min_weight', 'max_weight', 'max_value', 'package_delivery_date', 'door_to_door', 'license_number', 'insurance_number', 'driver_license_number', 'title', 'description'];

    /**
     * Get user information
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * Get service information
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function service(){
        return $this->belongsTo('App\Models\ShippingService', 'service_id');
    }

    /**
     * Get type information
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function type(){
        return $this->belongsTo('App\Models\ServiceTypes', 'service_type_id');
    }

    /**
     * Get list ways
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ways(){
        return $this->hasMany('App\Models\ShippingWay');
    }

    /**
     * Get list rates
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rates(){
        return $this->hasMany('App\Models\ShippingRate');
    }
    public function delivery(){
        return $this->belongsTo('App\Models\Delivery', 'delivery_id');
    }
    /**
     * Get list booking senders
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books(){
        return $this->hasOne('App\Models\BookShipping');
    }
    public function weightTotal($kltg = array() ){
        $KLTG = $KLTTT =0;
        if($this->service_id==8)
        {
            if( isset($kltg['totalCost']) && !empty($kltg['totalCost']) ) {
                $kltg['totalCost'] = (double)$kltg['totalCost']/1000;
            }
        }
        if( sizeof($kltg) > 0 ){
            if( isset($kltg['totalCost']) && !empty($kltg['totalCost']) ){
                $KLT = $kltg['totalCost'];
            }else{
                $KLT = 0;
            }
            if( isset($kltg['KLTT']) && sizeof($kltg['KLTT']) > 0 && is_array($kltg['KLTT']) ) {
                foreach ($kltg['KLTT'] as $kltt) {
                    $KLTTT = $KLTTT + (((int)$kltt['width'] * (int)$kltt['length'] * (int)$kltt['height'] * (int)$kltt['quantity']))/(int)$this->math;
                }
            }

            $KLTG = max($KLT, $KLTTT);
        }
        return $KLTG;
    }
    public function tripWeightTotal($kltg=array()){
        $total = 0;
        $total = $kltg['trip']['loadCapacity'];
        return $total;
    }
    public function getRateItems($type='item'){
        return $this->rates()->where('type', $type)->get();
    }
    public function ratePerWeight($kltg=array()) {
        $KLTG = floatval($this->weightTotal($kltg));
        $minWeight = $minRate = $maxWeight = $maxRate = $overWeight = $overRate = $tempWeight = $tempRate = 0;
        $maxUnit = $overUnit  = $tempUnit = 0.0;
        $cost = 0;
        $min = $this->rates()->where('type', 'min')->first();
        $max = $this->rates()->where('type', 'last')->first();
        $over = $this->rates()->where('type', 'over')->first();
        $temp = $this->rates()->whereIn('type', ['item','last'])->where('weight','>=',$KLTG)->orderBy('weight','ASC')->first();

        if($min)
        {
            $minWeight = $min->weight;
            $cost = $minRate = $min->rate;
        }

        if($max)
        {
            $maxWeight = $max->weight;
            $maxRate = $max->rate;
            $maxUnit = floatval($max->unit);
        }

        if($over)
        {
            $overWeight = $over->weight;
            $overRate = $over->rate;
            $overUnit = floatval($over->unit);
        }

        if($temp)
        {
            $tempWeight = $temp->weight;
            $tempRate = $temp->rate;
            $tempUnit = floatval($temp->unit);
        }

        if($KLTG > $maxWeight) //Khối lượng vượt khung giá
        {
            if($maxUnit===0.0) //Đơn vị tính nguyên giá
            {
                $cost = $maxRate;
            }
            else //Đơn vị tính là 0.5 hoặc 1 kg
            {
                $cost += (($KLTG-$minWeight)/$maxUnit)*$maxRate;
            }

            if($overUnit===0.0) //Đơn vị tính nguyên giá
            {
                $cost = $overRate;
            }
            else //Đơn vị tính là 0.5 hoặc 1 kg
            {
                $cost += (($KLTG-$maxWeight)/$overUnit)*$overRate;
            }
        }
        else //Khối lượng nằm trong khung giá
        {
            if($KLTG > $minWeight)
            {
                if($tempUnit===0.0) //Đơn vị tính nguyên giá
                {
                    $cost = $tempRate;
                }
                else
                {
                    $cost += (($KLTG-$minWeight)/$tempUnit)*$tempRate;
                }
            }
        }        
        return $cost;
    }
    public function ratePerTrip($kltg=array()){
        $rate = $this->rates->where('rate_type', 'trip')->where('trip_type', $kltg['trip']['loadCapacity'])->first();
        if(!$rate)
        {
            $rate = $this->rates->where('rate_type', 'trip')->where('trip_type', '>=', $kltg['trip']['loadCapacity'])->first();
            if(!$rate)
            {
                $rate = $this->rates->where('rate_type', 'trip')->where('type','last')->first();
            }
        }
        
        $currentRate = 0;
        if($rate)
        {
            $currentRate = isset($rate->trip_rate) && !empty($rate->trip_rate) ? ((double)$rate->trip_rate + (double)$rate->trip_loading) : 0;
        }
        $cost = $currentRate * $kltg['trip']['numberTrip'];
        return $cost;
    }
    public function getCost($kltg=array(), $type='weight', $fragileSurcharge=0, $loadingUnloadingSurcharge=0){
        $total = 0;
        $val = null;
        switch ($type){
            case 'trip':
                $total = $this->ratePerTrip($kltg);
                break;
            case 'cbmfcl':
                if( $kltg['container']['loadCapacity'] ){
                    $tripType = $kltg['container']['loadCapacity'];
                    $val = $this->rates()->where('trip_type', $tripType)->first();
                    if( $val ){
                        $total = (double)$val->trip_rate * (int)$kltg['container']['numberTrip'];
                    }
                }
                break;
            case 'container':
                if( $kltg['container']['loadCapacity'] ){
                    $tripType = strtolower($kltg['container']['loadCapacity']);
                    $val = $this->rates()->where('trip_type', $tripType)->first();
                    if( $val ){
                        $total = (double)$val->trip_rate * (int)$kltg['container']['numberTrip'];
                    }
                }
                break;
            default:
                $total = $this->ratePerWeight($kltg);
                break;
        }
        $KLTG = $this->weightTotal($kltg);
        $dv = $bx = 0;
        $xd = $this->fuel_surcharge ? $this->fuel_surcharge : 0;
        if( $fragileSurcharge )
            $dv = $this->fragile_surcharge ? $this->fragile_surcharge : 0;
        if( $loadingUnloadingSurcharge )
            $bx = $this->loading_unloading_surcharge ? $this->loading_unloading_surcharge : 0;
        $currentCurrency = Cache::get( 'currentCurrency' );
        
        switch ($type) {
            case 'container':
                {
                    if( !isset($kltg['container']['loadCapacity'])){
                        return trans('front.detail');
                    }
                    if( $val ){
                        $total = $total + ($total*$xd)/100 + ($total*$dv)/100 + ((double)$val->trip_loading);
                    }
                }
                break;
            case 'trip':
                {
                    $vat = $this->vat;
                    $percentWithVat = (double)(100 + $vat)/100;

                    $total = ($total + ($total*$xd)/100 + ($total*$dv)/100)*$percentWithVat;
                }
                break;
            case 'weight':
                {
                    if( $KLTG == 0)
                    {
                        return trans('front.detail');
                    }
                    $vat = $this->vat;
                    $percentWithVat = (double)(100 + $vat)/100;

                    $total = ($total + ($total*$xd)/100 + ($total*$dv)/100 + $bx)*$percentWithVat;
                }
                break;
            case 'cbmfcl':
                {
                    $vat = $this->vat;
                    $percentWithVat = (double)(100 + $vat)/100;

                    $total = ($total + ($total*$xd)/100 + ($total*$dv)/100)*$percentWithVat;                    
                }
                break;    
            default:
                {
                    if( $KLTG == 0)
                    {
                        return trans('front.detail');
                    }
                    $vat = $this->vat;
                    $percentWithVat = (double)(100 + $vat)/100;

                    $total = ($total + ($total*$xd)/100 + ($total*$dv)/100 + $bx)*$percentWithVat;
                }
                break;
        }
        
        return $total;
    }
    public function costOnMaps($kltg=array(), $type='weight', $fragileSurcharge=0, $loadingUnloadingSurcharge=0){
        $total = $this->getCost($kltg, $type, $fragileSurcharge=0, $loadingUnloadingSurcharge=0);

        if( $total == 0 ){
            return trans('front.detail');
        }
        $currentCurrency = Cache::get( 'currentCurrency' );
        $total = $total * (double)$currentCurrency->value;
        $total = number_format($total, 2, ',', '.');
        return $total. " ". $currentCurrency->symbol;
    }
    public function costNotCurrencySumboy($kltg=array(), $type='weight', $fragileSurcharge=0, $loadingUnloadingSurcharge=0){
        $total = $this->getCost($kltg, $type='weight', $fragileSurcharge=0, $loadingUnloadingSurcharge=0);

        if( $total == 0 ){
            return 0;
        }
        $currentCurrency = Cache::get( 'currentCurrency' );
        $total = $total * (double)$currentCurrency->value;
        return $total;
    }
    public function fromLong(){
        return $this->ways()->first()->from_long;
    }
    public function fromLat(){
        return $this->ways()->first()->from_lat;
    }
    public function location($from){
        $location = $this->user->locations()->where('name', 'like', "%{$from}%")->first();
        if( $location ){
            return $location->lng.",".$location->lat;
        }else{
            $location = $this->user->locations()->first();
            if( $location ){
                return $location->lng.",".$location->lat;
            }else{
                if($this->ways()->where('from', 'like', "%{$from}%")->count() ){
                    return str_replace(array('(', ')'), '', $this->ways()->first()->from_location);
                }else{
                    return str_replace(array('(', ')'), '', $this->ways()->where('from', 'like', "%{$from}%")->first()->from_location);
                }

            }
        }

    }
    public function toLocation(){
        return str_replace(array('(', ')'), '', $this->ways()->first()->to_location);
    }
    public function getWayImage(){
        $image = 'ways-image';
        switch ($this->service->id){
            case 1:
                $image = 'express-way';
                break;
            case 2:
                $image = 'road-way';
                break;
            case 3:
                $image = 'home-office-way';
                break;
            case 4:
                $image = 'car-way';
                break;
            case 5:
                $image = 'light-truct-way';
                break;
            case 6:
                $image = 'container-way';
                break;
            case 7:
                $image = 'airline-way';
                break;
            case 8:
                $image = 'sea-way';
                break;
            default:
                break;
        }
        return $image;
    }
}
