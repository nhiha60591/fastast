<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLimitCount extends Model
{
    protected $table = 'user_limit_count';
    protected $fillable = ['id', 'user_id', 'item_id', 'type', 'month', 'year'];
    public function user(){
    	return $this->belongsTo('App\Models\User', 'user_id');
    }
}
