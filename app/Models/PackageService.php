<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageService extends Model
{
    protected $table = 'package_service';
    protected $fillable = [
        'package_id',
        'service_id'
    ];
    public function package()
    {
        return $this->hasOne('App\Models\Package');
    }

    public function service(){
        return $this->hasOne('App\Models\ShippingService');
    }
}
