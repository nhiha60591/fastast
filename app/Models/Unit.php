<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'unit';
    protected $fillable = ['name', 'slug', 'type', 'value', 'status', 'position'];
    public function weight_units(){
        return $this->belongsToMany('App\Models\Package');
    }
}
