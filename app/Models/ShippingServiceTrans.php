<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingServiceTrans extends Model
{
    protected $table = 'shipping_service_trans';
    protected $fillable = ['shipping_service_id', 'language_id', 'name', 'description'];
    public function role(){
        return $this->hasOne('App\Models\ShippingService');
    }
    public function language(){
        return $this->hasOne('App\Models\Language');
    }
}
