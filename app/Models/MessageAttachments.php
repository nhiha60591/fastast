<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageAttachments extends Model
{
    protected $table = 'message_attachments';
    protected $fillable = [
        'id',
        'status',
        'message_id',
        'media_id',
        'media_url',
        'created_at',
        'updated_at'
    ];
}
