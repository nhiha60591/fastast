<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceTypes extends Model
{
    protected $table = 'service_types';
    protected $fillable = ['id', 'status', 'type'];
    public function trans(){
        return $this->hasMany('App\Models\ServiceTypesTrans');
    }
    public function translation( $language = null, $filed='name'){
        if( $this->trans()->where('language_id', $language)->count() ) {
            $trans = $this->trans()->where('language_id', $language)->first()->$filed;
            return $trans;
        }else{
            return null;
        }
    }
    public function shipping(){
        return $this->hasOne('App\Models\Shipping');
    }
    public function services(){
        return $this->hasMany('App\Models\ShippingService');
    }
}
