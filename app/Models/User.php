<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;
    protected $table = 'user';
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_admin',
        'first_name',
        'last_name',
        'full_name',
        'avatar',
        'social',
        'social_id',
        'gender',
        'address',
        'phone',
        'status',
        'notify',
        'guarantee_policy',
        'user_type',
        'nationality',
        'company_name',
        'viber',
        'whats_app',
        'line',
        'hotline',
        'fax_number',
        'phone_number_license',
        'website',
        'company_registered_license',
        'facebook',
        'zalo',
        'skype',
        'introduction',
        'service',
        'other_surcharges',
        'requirements',
        'transaction_count',
        'package_click_count'
    ];
    protected $hidden = ['password', 'remember_token'];

    public function scopeActive($query) {
        return $query->whereStatus('1');
    }
    public function scopeAdmin($query) {
        return $query->whereIsAdmin('1');
    }
    public function scopeNotify($query) {
        return $query->whereNotify('1');
    }

    public function roles() {
        return $this->belongsToMany('App\Models\Role', 'role_user', 'user_id', 'role_id');
    }
    public function medias() {
        return $this->hasMany('App\Models\Media', 'user_id');
    }
    public function shipping(){
        return $this->hasOne('App\Models\Shipping');
    }
    public function package(){
        return $this->hasOne('App\Models\Package');
    }
    public function messageFrom(){
        return $this->hasOne('App\Models\Message');
    }
    public function messageTo(){
        return $this->hasOne('App\Models\Message');
    }
    /**
     * Get list booking senders
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books(){
        return $this->hasOne('App\Models\BookShipping');
    }
    public function locations(){
        return $this->hasMany('App\Models\UserLocations');
    }
    public function attachRole($role) {
        if (is_object($role)) {
            $role = $role->getKey();
        }
        if (is_array($role)) {
            $role = $role['id'];
        }
        $this->roles()->attach($role);
    }

    public function detachRole($role) {
        if (is_object($role)) {
            $role = $role->getKey();
        }
        if (is_array($role)) {
            $role = $role['id'];
        }
        $this->roles()->detach($role);
    }

    public function attachRoles($roles) {
        foreach ($roles as $role) {
            $this->attachRole($role);
        }
    }

    public function detachRoles($roles) {
        foreach ($roles as $role) {
            $this->detachRole($role);
        }
    }

    public function isSuperUser() {
        return (bool)$this->is_admin;
    }

    public function hasAccess($permissions, $all = true) {
        if ($this->isSuperUser()) {
            return true;
        }
        return $this->hasPermission($permissions);
    }

    public function hasPermission($permissions) {
        $mergedPermissions = $this->getMergedPermissions();
        dd($mergedPermissions);
        if (!is_array($permissions)) {
            $permissions = (array)$permissions;
        }

        foreach ($permissions as $permission) {
            $matched = false;
            // We will set a flag now for whether this permission was
            // matched at all.
            $founded_perms = find_in($mergedPermissions, "name", $permission);
            if (!empty($founded_perms)) {
                $matched = true;
            }

        }

        if ($matched === false) {
            return false;
        }

        return true;
    }

    public function getMergedPermissions() {
        $permissions = array();
        foreach ($this->getRoles() as $group) {
            $permissions = array_merge($permissions, $group->permissions()->get()->toArray());
        }
        return $permissions;
    }

    public function getRoles() {
        $roles = [];
        if ($this->roles()) {
            $roles = $this->roles()->get();
        }
        return $roles;
    }
    public function getFullName()
    {
        if( !empty( $this->full_name ) ) return $this->full_name;
        return $this->first_name ." ". $this->last_name;
    }
    public function getNumberTransaction(){
        $return = 0;
        $userID=$this->id;
        $return = BookShipping::whereHas('shipping', function($q) use($userID)
        {
            $q->where('user_id', $userID );

        })->count();
        return $return;
    }
    public function getFailTransaction(){
        $return = 0;
        $userID=$this->id;
        $return = BookShipping::where('status', 'cancel')->whereHas('shipping', function($q) use($userID)
        {
            $q->where('user_id', $userID );

        })->count();
        return $return;
    }
    public function getSuccessTransaction(){
        $return = 0;
        $userID=$this->id;
        $return = BookShipping::where('status', 'success')->whereHas('shipping', function($q) use($userID)
        {
            $q->where('user_id', $userID );

        })->count();
        return $return;
    }
    public function getTransactionAmount(){
        $return = 0;
        $userID=$this->id;
        $returnData = BookShipping::where('status', 'success')->whereHas('shipping', function($q) use($userID)
        {
            $q->where('user_id', $userID );

        })->get();
        $return = $returnData->sum('cost');
        return $return;
    }
    public function getListingNumber(){
        $returnData = $this->shipping()->get();
        return $returnData->count();
    }
    public function getCancelListing(){
        $returnData = $this->shipping()->where('status', 'no')->get();
        return $returnData->count();
    }
    public function getListingWeightNumber(){
        $returnData = $this->shipping()->whereHas('type', function($q){
            $q->where('type', 'weight' );
        })->get();
        return $returnData->count();
    }
    public function getListingTripNumber(){
        $returnData = $this->shipping()->whereHas('type', function($q){
            $q->where('type', 'trip' );
        })->get();
        return $returnData->count();
    }

    public function getPackageNumber(){
        $returnData = $this->package()->get();
        return $returnData->count();
    }
    public function getCancelPackage(){
        $returnData = $this->package()->where('status', 'cancel')->get();
        return $returnData->count();
    }
    public function getPackageWeightNumber(){
        return 0;
    }
    public function getPackageTripNumber(){
        return 0;
    }
    public function getPackageServiceImage(){
        $packages = Package::where('user_id', $this->id);
        if( $packages->count() ){
            $serviceImg = [];
            foreach( $packages->get() as $package ){
                if( $package->services->count() ){
                    foreach( $package->services as $service ){
                        $serviceImg[] = $service->image;
                    }
                }
            }
            $serviceImg = array_unique( $serviceImg );
            if( sizeof( $serviceImg ) ){
                $html = '';
                foreach( $serviceImg as $image){
                    $html .= '<img src="'.image_url($image).'">';
                }
                return $html;
            }
        }
        return '';
    }
}