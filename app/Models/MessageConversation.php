<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageConversation extends Model
{
    protected $table = 'message_conversation';
    protected $fillable = [
        'id',
        'status',
        'from',
        'read',
        'count',
        'subject',
    ];
}
