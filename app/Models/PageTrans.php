<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageTrans extends Model
{
    protected $table = 'pages_trans';
    protected $fillable = ['pages_id', 'language_id', 'slug', 'title', 'content'];

    /**
     * Get page
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function page(){
        return $this->belongsToMany('App\Models\Page', 'pages_id');
    }
    /**
     * Get Language
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language(){
        return $this->belongsToMany('App\Models\Language', 'language_id');
    }
}
