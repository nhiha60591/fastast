<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingWay extends Model
{
    protected $table = 'shipping_ways';
    protected $fillable = ['id', 'shipping_id', 'from', 'from_long', 'from_lat', 'to', 'to_long', 'to_lat', 'two_way', 'trip', 'date_go'];
    public function shippings(){
        return $this->belongsToMany('App\Models\Shipping', 'shipping_id');
    }
}
