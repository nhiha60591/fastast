<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageLocations extends Model
{
    protected $table = 'package_locations';
    protected $fillable = ['id', 'package_id', 'lng', 'lat', 'name', 'type', 'description'];
    public function package(){
        return $this->belongsToMany('App\Models\Package', 'package_id');
    }
}
