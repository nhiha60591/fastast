<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'type';
    protected $fillable = ['slug', 'image'];
    /**
     * Get the translation record associated with the package.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trans()
    {
        return $this->hasMany('App\Models\TypeTrans');
    }

    /**
     * Get translation by field name
     *
     * @param null $language
     * @param string $filed
     * @return $trans|null
     */
    public function translation( $language = null, $filed='name'){
        if( $this->trans()->where('language_id', $language)->count() ) {
            $trans = $this->trans()->where('language_id', $language)->first()->$filed;
            return $trans;
        }else{
            return null;
        }
    }
    /**
     * Get the package
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function packages(){
        return $this->belongsToMany('App\Models\Package', 'package_type', 'package_id', 'type_id');
    }
}
