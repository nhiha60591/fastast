<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookShipping extends Model
{
    protected $table = 'book_shipping';
    protected $fillable = ['id', 'shipping_id', 'cost', 'status', 'user_id', 'track_number', 'containing', 'requirements', 'origination', 'destination', 'departure_date', 'arrival_date', 'service_info'];

    /**
     * Get shipping information
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function shipping(){
        return $this->belongsTo('App\Models\Shipping', 'shipping_id');
    }

    /**
     * Get user information
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function images(){
        return $this->belongsToMany('App\Models\Media', 'book_images', 'book_id', 'media_id');
    }
    public function status($showHTML = false){
        $return = 'Waiting Approve';
        $startHTML = '<span class="normal status">';
        $endHTML = '</span>';
        switch ($this->status){
            case 'waiting':
                $return = 'Waiting Approve';
                $startHTML = '<span class="orange status">';
                break;
            case 'shipping':
                $return = 'Shipping ...';
                $startHTML = '<span class="shipping status">';
                break;
            case 'cancel':
                $return = 'Cancel';
                $startHTML = '<span class="cancel status">';
                break;
            case 'success':
                $return = 'Success';
                $startHTML = '<span class="success status">';
                break;
            default:
                $return = 'Waiting Approve';
                $startHTML = '<span class="waiting status">';
                break;
        }
        if( $showHTML ){
            return $startHTML.$return.$endHTML;
        }
        return $return;
    }
}
