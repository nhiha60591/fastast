<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageImages extends Model
{
    protected $table = 'package_images';
    protected $fillable = [
        'package_id',
        'image_id'
    ];

    public function package()
    {
        return $this->hasOne('App\Models\Package');
    }

    public function media(){
        return $this->hasOne('App\Models\Media');
    }
}
