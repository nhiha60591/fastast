<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryTrans extends Model
{
    protected $table = 'delivery_trans';
    protected $fillable = ['delivery_id', 'language_id', 'name', 'description'];
    public function delivery(){
        return $this->belongsToMany('App\Models\Delivery', 'delivery_id');
    }
}
