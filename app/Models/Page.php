<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $fillable = ['id', 'user_id', 'status'];
    public function trans()
    {
        return $this->hasMany('App\Models\PageTrans', 'pages_id');
    }
    /**
     * Get the service of package
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user(){
        return $this->hasOne('App\Models\User', 'user_id');
    }

    /**
     * Get translation by field name
     *
     * @param null $language
     * @param string $filed
     * @return $trans|null
     */
    public function translation( $language = null, $filed='name'){
        if( $this->trans()->where('language_id', $language)->count() ) {
            $trans = $this->trans()->where('language_id', $language)->first()->$filed;
            return $trans;
        }else{
            return null;
        }
    }
}
