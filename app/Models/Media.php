<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'media';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'caption', 'description', 'file_name', 'file_url', 'file_type', 'file_size', 'user_id'];
    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
        return $this->belongsToMany('App\Models\User', 'user_id');
    }

    /**
     * Get the package
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function packages(){
        return $this->belongsToMany('App\Models\Package', 'package_images', 'package_id', 'image_id');
    }

    /**
     * Get the book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function book(){
        return $this->belongsToMany('App\Models\BookShipping', 'book_images', 'media_id', 'book_id');
    }
}
