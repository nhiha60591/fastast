<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceTypesTrans extends Model
{
    protected $table = 'service_types_trans';
    protected $fillable = ['service_types_id', 'language_id', 'name', 'description'];
    public function services(){
        return $this->belongsToMany('App\Models\ServiceTypes', 'service_types_id');
    }
    public function languages(){
        return $this->belongsToMany('App\Models\Language', 'language_id');
    }
}
