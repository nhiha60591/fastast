<?php
/**
 * Upload image and insert it to database
 */
use App\Models\Media;
use Illuminate\Support\Facades\Cache;
function uploadFile($file){

    $tmpFileName = time()."-".$file->getClientOriginalName();

    $media = new Media();
    $media->name = $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->getClientOriginalName());
    $media->caption = $media->name;
    $media->description = $media->name;
    $media->file_name = $tmpFileName;
    $media->file_type = $file->getClientMimeType();
    $media->file_size = $file->getClientSize();
    $media->user_id = Auth()->user()->id;

    $uploadDir = upload_url();
    $file = $file->move($uploadDir['path'], $tmpFileName);
    $path = $uploadDir['url'] . $tmpFileName;

    $media->file_url = $path;
    $media->save();

    $res = array(
        'id' => $media->id,
        'fileUrl' => $media->file_url
    );
    return $res;
}
function upload_url(){
    $destinationPath = public_path() . '/uploads/';
    $year = date("Y");
    $month = date("m");
    $day = date("d");
    $uploadDir['url'] = "uploads/{$year}/{$month}/{$day}/";
    $uploadDir['path'] = $destinationPath."{$year}/{$month}/{$day}/";
    if(!is_dir($destinationPath.$year) ) @mkdir($destinationPath.$year);
    if(!is_dir($destinationPath.$year."/".$month) ) @mkdir($destinationPath.$year."/".$month);
    if(!is_dir($destinationPath.$year."/".$month."/".$day) ) @mkdir($destinationPath.$year."/".$month."/".$day);
    return $uploadDir;
}
function selected($val=0, $check=0){
    if($val == $check){
        return ' selected ';
    }else{
        return '';
    }
}
function checked($val=0, $check=0){
    if($val == $check){
        return ' checked ';
    }else{
        return '';
    }
}
function get_cache($key){
    return Cache::get( $key );
}
function showPricePosition($price, $position, $symbol){
    switch( $position ){
        case "left":
            return $symbol. number_format( $price, 2, '.', ',' );
            break;
        case "right":
            return number_format( $price, 2, '.', ',' ). $symbol;
            break;
        case "left_space":
            return $symbol. " " . number_format( $price, 2, '.', ',' );
            break;
        case "right_space":
            return number_format( $price, 2, '.', ',' ). " " . $symbol;
            break;
        default:
            return number_format( $price, 2, '.', ',' ). " " . $symbol;
            break;
    }
}