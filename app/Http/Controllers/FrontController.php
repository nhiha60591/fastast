<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Language;
use App\Models\Unit;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\User;
use Theme;
use App\Models\Options;
use Illuminate\Support\Facades\Cache;
use Excel;

class FrontController extends Controller
{
    /**
     * Theme instance.
     *
     * @var \Teepluss\Theme\Theme
     */
    protected $theme;
    protected $title = "Dash Board";
    protected $subtitle = "Dash Board";
    protected $layout = "default";
    protected $currentCurrency = null;
    protected $defaultCurrency = null;
    protected $currentLanguage = null;
    protected $currentWeightUnit = null;
    protected $currentLongUnit = null;

    /**
     * Construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->setTheme();
    }
    public function getIndex(){
        return $this->theme->scope('pages.index')->render();
    }
    public function setTheme(){
        if( Cache::has( 'allOptions' ) ){
            $options = Cache::get( 'allOptions' );
        }else{
            $options = Options::all();
            Cache::add( 'allOptions', $options, 3600);
        }
        if( Cache::has( 'allOptionData' ) ){
            $optionData = Cache::get( 'allOptionData' );
        }else{
            $optionData = array();
            foreach ($options as $option){
                $optionData[$option->option_key] = $option->option_value;
            }
            Cache::add( 'allOptionData', $optionData, 3600);
        }

        if( Cache::has( 'currentCurrency' ) ){
            $this->currentCurrency = Cache::get( 'currentCurrency' );
        }else{
            $this->currentCurrency = Currency::where('default', '1')->first();
            Cache::add( 'currentCurrency', $this->currentCurrency , 3600 );
        }

        if( Cache::has( 'defaultCurrency' ) ){
            $this->defaultCurrency = Cache::get( 'defaultCurrency' );
        }else{
            $this->defaultCurrency = Currency::find(2);
            Cache::add( 'defaultCurrency', $this->defaultCurrency , 3600 );
        }

        if( Cache::has( 'currentLanguage' ) ){
            $this->currentLanguage = Cache::get( 'currentLanguage' );
        }else{
            $this->currentLanguage = Language::where('default', '1')->first();
            Cache::add( 'currentLanguage', $this->currentLanguage , 3600 );
        }

        if( Cache::has( 'weightUnit' ) ){
            $weightUnit = Cache::get( 'weightUnit' );
        }else{
            $weightUnit = Unit::where('type', 'weight')->get();
            Cache::add( 'weightUnit', $weightUnit, 3600);
        }

        if( Cache::has( 'longUnit' ) ){
            $longUnit = Cache::get( 'longUnit' );
        }else{
            $longUnit = Unit::where('type', 'long')->get();
            Cache::add( 'longUnit', $longUnit, 3600);
        }

        if( Cache::has( 'currentWeightUnit' ) ){
            $this->currentWeightUnit = Cache::get( 'currentWeightUnit' );
        }else{
            $this->currentWeightUnit = $weightUnit->first();
            Cache::add( 'currentWeightUnit', $this->currentWeightUnit , 3600 );
        }

        if( Cache::has( 'currentLongUnit' ) ){
            $this->currentLongUnit = Cache::get( 'currentLongUnit' );
        }else{
            $this->currentLongUnit = $longUnit->first();
            Cache::add( 'currentLongUnit', $this->currentLongUnit , 3600 );
        }

        if( Cache::has( 'allCurrencies' ) ){
            $allCurrencies = Cache::get( 'allCurrencies' );
        }else{
            $allCurrencies = Currency::all();
            Cache::add( 'allCurrencies', $allCurrencies, 3600);
        }

        if( Cache::has( 'allLanguages' ) ){
            $allLanguages = Cache::get( 'allLanguages' );
        }else{
            $allLanguages = Language::all();
            Cache::add( 'allLanguages', $allLanguages, 3600);
        }
        $theme = Theme::uses('front')->layout($this->layout);
        $this->theme = $theme;
        // Using theme as a global.
        $this->theme->setTitle($optionData['site_title']);
        $this->theme->setSubtitle($optionData['site_title']);
        $this->theme->setLongUnit($longUnit);
        $this->theme->setWeightUnit($weightUnit);
        $this->theme->setCurrencies($allCurrencies);
        $this->theme->setLanguages($allLanguages);
    }
    public function updateSettings($type = '', $id= 0){
        switch ($type){
            case 'language' :
                $language = Language::find($id);
                $url = redirect()->getUrlGenerator()->previous();
                $url = str_replace("/{$this->currentLanguage->code}", "/{$language->code}", $url);
                $this->currentLanguage = $language;
                Cache::forget('currentLanguage');
                Cache::add( 'currentLanguage', $this->currentLanguage , 3600 );
                app()->setLocale($this->currentLanguage->code);
                return redirect($url);
                break;
            case 'currency' :
                $this->currentCurrency = Currency::find($id);
                Cache::forget('currentCurrency');
                Cache::add( 'currentCurrency', $this->currentCurrency , 3600 );
                break;
            case 'weight-unit' :
                $this->currentWeightUnit = Unit::find($id);
                Cache::forget('currentWeightUnit');
                Cache::add( 'currentWeightUnit', $this->currentWeightUnit , 3600 );
                break;
            case 'long-unit' :
                $this->currentLongUnit = Unit::find($id);
                Cache::forget('currentLongUnit');
                Cache::add( 'currentLongUnit', $this->currentLongUnit , 3600 );
                break;
        }
        return redirect( redirect()->getUrlGenerator()->previous() );
    }
    public function exportFile($filename, $data, $ext='xls'){
        Excel::create($filename, function($excel) use( $data) {
            // Set the title
            $excel->setTitle('Fastast Export Data');

            // Chain the setters
            $excel->setCreator('Fastast')
                ->setCompany('Fastast');

            // Call them separately
            $excel->setDescription('A demonstration to change the file properties');
            if( sizeof( $data )  ){
                foreach( $data as $key=>$val){
                    // Our first sheet
                    $excel->sheet($key, function($sheet) use ($val) {
                        $sheet->setOrientation('landscape');
                        $sheet->setPageMargin(0.5);
                        // Font family
                        $sheet->setFontFamily('Arial');

                        if( sizeof( $val )  ){
                            $i=1;
                            foreach( $val as $childVal){
                                $sheet->row($i, $childVal);
                                $i++;
                            }
                        }
                    });
                }
            }

        })->export($ext);
    }
}
