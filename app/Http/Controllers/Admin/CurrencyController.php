<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Models\Currency;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

use App\Http\Requests;

class CurrencyController extends AdminController
{
    protected $rules = array(
        'name' => 'required|max:255',
        'code' => 'required',
        'default' => 'required|max:255',
        'status' => 'required',
        'decimal_place' => 'required',
        'value' => 'required',
        'symbol' => 'required'
    );
    protected $rules_create = array(
        'name' => 'required|max:255',
        'code' => 'required|max:255|unique:currency',
        'default' => 'required|max:255',
        'status' => 'required',
        'decimal_place' => 'required',
        'value' => 'required',
        'symbol' => 'required'
    );
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->setSubactive('list');
        $this->theme->setTitle(trans('admin.currency'));
        $this->theme->setSubtitle(trans('admin.currency_list'));
        if( Cache::has( 'allCurrencies' ) ){
            $currencies = Cache::get( 'allCurrencies' );
        }else{
            $currencies = Currency::all();
            Cache::add( 'allCurrencies', $currencies, 3600);
        }
        $view = array(
            'currencies' => $currencies
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('currency.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->theme->setSubactive('create');
        $this->theme->setTitle(trans('admin.currency'));
        $this->theme->setSubtitle(trans('admin.currency_create'));
        $view = array();
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('currency.create', $view)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->input(), $this->rules_create);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.create-currency-failed'));
            return redirect(route('admin.localization.currency.create'))->withInput($request->input())->withErrors($validator);
        }else {
            $currency = new Currency();
            $currency->create($request->input());
            Cache::forget('allCurrencies');
            Session::flash('message', trans('admin.create-currency-success'));
            return redirect(route('admin.localization.currency.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setSubactive('edit');
        $this->theme->setTitle(trans('admin.currency'));
        $this->theme->setSubtitle(trans('admin.currency_edit'));
        $currency = Currency::find($id);
        $view = array(
            'currency' => $currency
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('currency.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->input(), $this->rules);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.update-currency-failed'));
            return redirect(route('admin.localization.currency.edit', array($id)))->withInput($request->input())->withErrors($validator);
        }else {
            $language = Currency::find($id);
            $language->update($request->input());
            Cache::forget('allCurrencies');
            Session::flash('message', trans('admin.update-currency-success'));
            return redirect(route('admin.localization.currency.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = array();
        Currency::destroy($id);
        $response['code'] = 200;
        $response['message'] = trans('currency.deleteSuccess');
        Cache::forget( 'allCurrencies' );

        echo json_encode($response);
        die();
    }
}
