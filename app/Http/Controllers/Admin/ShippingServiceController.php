<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Language;
use App\Models\ShippingService;
use App\Models\ShippingServiceTrans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Session;

class ShippingServiceController extends AdminController
{
    protected $mainActive = 'package';
    protected $subActive = 'shipping-service';

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'status' => 'required|max:255'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->setTitle(trans('admin.shippingService'));
        $this->theme->setSubtitle(trans('admin.shippingService_list'));
        if( Cache::has( 'allShippingServices' ) ){
            $shippingServices = Cache::get( 'allShippingServices' );
        }else{
            $shippingServices = ShippingService::all();
            Cache::add( 'allShippingServices', $shippingServices, 3600);
        }
        $currentLanguage = Language::where('locale', config('app.locale'))->first();
        $view = array(
            'shippingServices'=>$shippingServices,
            'currentLanguage' => $currentLanguage
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('shipping-service.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->theme->setTitle(trans('admin.shippingService'));
        $this->theme->setSubtitle(trans('admin.shippingService_create'));
        $languages = Language::all();
        $view = array(
            'languages' => $languages
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('shipping-service.create', $view)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array(
            'status' => $request->input('status'),
        );
        $validator = $this->validator($data);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.create-shipping-service-failed'));
            return redirect(route('admin.shipping-service.create'))->withInput($request->input())->withErrors($validator);
        }else{
            $shippingService = new ShippingService();
            if( $request->hasFile('image') ){
                $upload = uploadFile($request->file('image'));
                $shippingService->image = $upload['fileUrl'];
            }
            $shippingService->status = $request->input('status');
            $shippingService->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['name'] ) ){
                        $shippingServiceTrans = new ShippingServiceTrans();
                        $shippingServiceTrans->shipping_service_id = $shippingService->id;
                        $shippingServiceTrans->language_id = $language;
                        $shippingServiceTrans->name = $data['name'];
                        $shippingServiceTrans->description = $data['description'];
                        $shippingServiceTrans->save();
                    }
                }
            }
            Cache::forget('allShippingServices');
            Session::flash('message', trans('admin.create-shipping-service-success'));
            return redirect(route('admin.shipping-service.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setTitle(trans('admin.shippingService'));
        $this->theme->setSubtitle(trans('admin.shippingService_edit'));
        $shippingService = ShippingService::find($id);
        $languages = Language::all();
        $view = array(
            'shippingService' => $shippingService,
            'languages' => $languages
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('shipping-service.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'status' => $request->input('status')
        );
        $validator = $this->validator($data);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.update-shipping-service-failed'));
            return redirect( route( 'admin.shipping-service.update', array($id) ) )->withInput($request->input())->withErrors($validator);
        }else{
            $shippingService = ShippingService::find($id);
            if( $request->hasFile('image') ){
                $upload = uploadFile($request->file('image'));
                $shippingService->image = $upload['fileUrl'];
            }
            $shippingService->status = $request->input('status');
            $shippingService->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['name'] ) ){
                        $shippingServiceTrans = ShippingServiceTrans::where('language_id', $language)->where('shipping_service_id', $shippingService->id)->delete();
                        $shippingServiceTrans = new ShippingServiceTrans();
                        $shippingServiceTrans->shipping_service_id = $shippingService->id;
                        $shippingServiceTrans->language_id = $language;
                        $shippingServiceTrans->name = $data['name'];
                        $shippingServiceTrans->description = $data['description'];
                        $shippingServiceTrans->save();
                    }
                }
            }
            Cache::forget('allShippingServices');
            Session::flash('message', trans('admin.update-shipping-service-success'));
            return redirect(route('admin.shipping-service.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShippingServiceTrans::where('country_id', $id)->delete();
        ShippingService::destroy($id);
        $response['code'] = 200;
        $response['message'] = trans('admin.deleteSuccess');
        Cache::forget('allShippingServices');
        echo json_encode($response);
        die();
    }
}
