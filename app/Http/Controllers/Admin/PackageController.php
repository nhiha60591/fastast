<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Language;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Session;

class PackageController extends AdminController
{
    protected $mainActive = 'package';
    protected $subActive = 'package';

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'status' => 'required|max:255'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->setTitle(trans('admin.package'));
        $this->theme->setSubtitle(trans('admin.package_list'));
        if( Cache::has( 'allPackages' ) ){
            $packages = Cache::get( 'allPackages' );
        }else{
            $packages = Package::all();
            Cache::add( 'allPackages', $packages, 3600);
        }
        $currentLanguage = Language::where('locale', config('app.locale'))->first();
        $view = array(
            'packages'=>$packages,
            'currentLanguage' => $currentLanguage
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('package.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
