<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Cache;
use App\Models\Role;
use App\Models\User;
use App\Models\RoleUser;
use Validator;
use Session;

class AdminUserController extends AdminController
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected $mainActive = 'user';
    protected function validator_update(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'full_name' => 'required|max:255',
            'password' => 'min:6|confirmed',
        ]);
    }

    protected function validator_create(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:user',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'full_name' => 'required|max:255',
            'password' => 'required|min:6|confirmed',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->setSubactive('list');
        $this->theme->setTitle(trans('admin.user'));
        $this->theme->setSubtitle(trans('admin.user_list'));
        if( Cache::has( 'allUsers' ) ){
            $users = Cache::get( 'allUsers' );
        }else{
            $users = User::all();
            Cache::add( 'allUsers', $users, 3600);
        }
        if( Cache::has( 'allRoles' ) ){
            $roles = Cache::get( 'allRoles' );
        }else{
            $roles = Role::all();
            Cache::add( 'allRoles', $roles, 3600);
        }
        $view = array(
            'users' => $users,
            'roles' => $roles
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('user.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->theme->setSubactive('create');
        $this->theme->setTitle(trans('admin.user'));
        $this->theme->setSubtitle(trans('admin.user_create'));
        $users = User::paginate(15);
        $roles = Role::all();
        $view = array(
            'users' => $users,
            'roles' => $roles
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('user.create', $view)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator_create($request->input());
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.create-user-failed'));
            return redirect(route('admin.user.create'))->withInput($request->input())->withErrors($validator);
        }else{
            $user = User::create([
                'email' => $request->input('email'),
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'full_name' => $request->input('full_name'),
                'address' => $request->input('address'),
                'phone' => $request->input('phone'),
                'gender' => $request->input('gender'),
                'password' => bcrypt($request->input('password')),
            ]);

            RoleUser::where( 'user_id', $user->id)->delete();
            foreach ($request->input('role') as $role){
                RoleUser::create(array(
                    'user_id' => $user->id,
                    'role_id' => $role
                ));
            }
            Cache::forget( 'allUsers' );
            Session::flash('message', trans('admin.create-user-success'));
            return redirect(route('admin.user.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setSubactive('create');
        $this->theme->setTitle(trans('admin.user'));
        $this->theme->setSubtitle(trans('admin.user_edit'));
        $user = User::find($id);
        
        $roles = Role::all();

        $currentRoles = array();
        if( $user->roles ) {
            foreach ($user->roles as $role) {
                $currentRoles[] = $role->id;
            }
        }
        $view = array(
            'user' => $user,
            'roles' => $roles,
            'currentRoles' => $currentRoles
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('user.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator_update($request->input());
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.update-user-failed'));
            return redirect(route('admin.user.edit',array($id)))->withInput($request->input())->withErrors($validator);
        }else{
            $user = User::find($id);
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->full_name = $request->input('full_name');
            $user->address = $request->input('address');
            $user->phone = $request->input('phone');
            $user->gender = $request->input('gender');
            $user->status = $request->input('status');

            if( $request->hasFile('avatar') ){
                $upload = uploadFile($request->file('avatar'));
                $user->avatar = $upload['fileUrl'];
            }

            if( $request->hasFile('banner') ){
                $upload = uploadFile($request->file('banner'));
                $user->banner = $upload['fileUrl'];
            }
            $user->save();
            $roles = $request->input('role');
            if( isset( $roles ) && !empty( $roles ) ) {
                RoleUser::where( 'user_id', $user->id)->delete();
                foreach ($roles as $role) {
                    RoleUser::create(array(
                        'user_id' => $user->id,
                        'role_id' => $role
                    ));
                }
            }
            Cache::forever( 'editUser_'.$user->id, $user );
            Cache::forget( 'allUsers' );
            Session::flash('message', trans('admin.update-user-success'));
            return redirect(route('admin.user.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find( $id );
        $response = array();
        if( $user->isSuperUser() ){
            $response['code'] = 403;
            $response['message'] = trans('admin.notPermissionDeleteUser');
        }else{
            RoleUser::where( 'user_id', $id)->delete();
            User::destroy($id);
            $response['code'] = 200;
            $response['message'] = trans('admin.deleteSuccess');
            Cache::forget( 'allUsers' );
        }
        echo json_encode($response);
        die();
    }
}
