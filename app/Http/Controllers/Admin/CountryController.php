<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\CountryTrans;
use App\Models\Language;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Session;

class CountryController extends AdminController
{
    protected $mainActive = 'package';
    protected $subActive = 'country';

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'lon' => 'required|max:255',
            'lat' => 'required|max:255'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->setTitle(trans('admin.country'));
        $this->theme->setSubtitle(trans('admin.country_list'));
        if( Cache::has( 'allCountries' ) ){
            $countries = Cache::get( 'allCountries' );
        }else{
            $countries = Country::all();
            Cache::add( 'allCountries', $countries, 3600);
        }
        $currentLanguage = Language::where('locale', config('app.locale'))->first();
        $view = array(
            'countries'=>$countries,
            'currentLanguage' => $currentLanguage
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('country.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->theme->setTitle(trans('admin.country'));
        $this->theme->setSubtitle(trans('admin.country_create'));
        $languages = Language::all();
        $view = array(
            'languages' => $languages
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('country.create', $view)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array(
            'lon' => $request->input('lon'),
            'lat' => $request->input('lat')
        );
        $validator = $this->validator($data);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.create-country-failed'));
            return redirect(route('admin.country.create'))->withInput($request->input())->withErrors($validator);
        }else{
            $country = new Country();
            $country->status = $request->input('status');
            $country->lon = $request->input('lon');
            $country->lat = $request->input('lat');
            $country->time_zone = $request->input('time_zone');
            $country->post_code = $request->input('post_code');
            $country->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['name'] ) ){
                        $countryTrans = new CountryTrans();
                        $countryTrans->country_id = $country->id;
                        $countryTrans->language_id = $language;
                        $countryTrans->name = $data['name'];
                        $countryTrans->description = $data['description'];
                        $countryTrans->save();
                    }
                }
            }
            Cache::forget('allCountries');
            Session::flash('message', trans('admin.create-country-success'));
            return redirect(route('admin.country.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setTitle(trans('admin.country'));
        $this->theme->setSubtitle(trans('admin.country_edit'));
        $country = Country::find($id);
        $languages = Language::all();
        $view = array(
            'country' => $country,
            'languages' => $languages
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('country.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'lon' => $request->input('lon'),
            'lat' => $request->input('lat')
        );
        $validator = $this->validator($data);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.update-country-failed'));
            return redirect( route( 'admin.country.update', array($id) ) )->withInput($request->input())->withErrors($validator);
        }else{
            $country = Country::find($id);
            $country->status = $request->input('status');
            $country->lon = $request->input('lon');
            $country->lat = $request->input('lat');
            $country->time_zone = $request->input('time_zone');
            $country->post_code = $request->input('post_code');
            $country->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['name'] ) ){
                        $countryTrans = CountryTrans::where('language_id', $language)->where('country_id', $country->id)->delete();
                        $countryTrans = new CountryTrans();
                        $countryTrans->country_id = $country->id;
                        $countryTrans->language_id = $language;
                        $countryTrans->name = $data['name'];
                        $countryTrans->description = $data['description'];
                        $countryTrans->save();
                    }
                }
            }
            Cache::forget('allCountries');
            Session::flash('message', trans('admin.update-country-success'));
            return redirect(route('admin.country.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CountryTrans::where('country_id', $id)->delete();
        Country::destroy($id);
        $response['code'] = 200;
        $response['message'] = trans('admin.deleteSuccess');
        Cache::forget('allCountries');
        echo json_encode($response);
        die();
    }
}
