<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Language;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Session;

class UnitController extends AdminController
{
    protected $mainActive = 'package';
    protected $subActive = 'unit';
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'slug' => 'required|max:255',
            'type' => 'required|max:255'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->setTitle(trans('admin.unit'));
        $this->theme->setSubtitle(trans('admin.unit_list'));
        if( Cache::has( 'allUnits' ) ){
            $allUnits = Cache::get( 'allUnits' );
        }else{
            $allUnits = Unit::all();
            Cache::add( 'allUnits', $allUnits, 3600);
        }
        $currentLanguage = Language::where('locale', config('app.locale'))->first();
        $view = array(
            'allUnits'=>$allUnits,
            'currentLanguage' => $currentLanguage
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('unit.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->theme->setTitle(trans('admin.unit'));
        $this->theme->setSubtitle(trans('admin.unit_create'));
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('unit.create')->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->input());
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.create-unit-failed'));
            return redirect(route('admin.unit.create'))->withInput($request->input())->withErrors($validator);
        }else{
            $unit = new Unit();
            $unit->name = $request->input('name');
            $unit->slug = $request->input('slug');
            $unit->type = $request->input('type');
            $unit->save();
            Cache::forget('allUnits');
            Session::flash('message', trans('admin.update-unit-success'));
            return redirect(route('admin.unit.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setTitle(trans('admin.unit'));
        $this->theme->setSubtitle(trans('admin.unit_edit'));
        $shippingService = Unit::find($id);
        $view = array(
            'unit' => $shippingService
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('unit.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->input());
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.update-unit-failed'));
            return redirect( route( 'admin.unit.update', array($id) ) )->withInput($request->input())->withErrors($validator);
        }else{
            $unit = Unit::find($id);
            $unit->name = $request->input('name');
            $unit->slug = $request->input('slug');
            $unit->type = $request->input('type');
            $unit->save();
            Cache::forget('allUnits');
            Session::flash('message', trans('admin.update-unit-success'));
            return redirect(route('admin.unit.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Unit::destroy($id);
        $response['code'] = 200;
        $response['message'] = trans('admin.deleteSuccess');
        Cache::forget('allUnits');
        echo json_encode($response);
        die();
    }
}
