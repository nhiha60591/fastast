<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Language;
use App\Models\Page;
use App\Models\PageTrans;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Session;

class PageController extends AdminController
{
    protected $mainActive = 'page';
    protected $subActive = 'list';
    public function index(){
        $this->theme->setTitle(trans('admin.page'));
        $this->theme->setSubtitle(trans('admin.page_list'));
        $currentLanguage = Language::where('code', config('app.locale'))->first();
        if( empty( $currentLanguage ) ){
            $currentLanguage = Language::find(1)->first();
        }
        $pages = Page::all();
        $view = array(
            'pages'=>$pages,
            'currentLanguage' => $currentLanguage
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('page.index', $view)->render();
    }
    public function create()
    {
        $this->theme->setTitle(trans('admin.page'));
        $this->theme->setSubtitle(trans('admin.page_create'));
        $languages = Language::all();
        $view = array(
            'languages' => $languages
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        $this->theme->asset()->container('footer')->add('ckeditor', 'https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js', $dependencies );
        return $this->theme->scope('page.create', $view)->render();
    }
    public function validator($data){
        $languages = Language::all();
        $validator = [];
        foreach($languages as $language){
            $validator['trans_'.$language->id.'_title'] = 'required|max:255';
            $validator['trans_'.$language->id.'_slug'] = 'required|max:255';
            $validator['trans_'.$language->id.'_content'] = 'required';
        }
        return Validator::make($data, $validator);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = Language::all();
        $validators = [];
        $trans = $request->input('trans');
        foreach($languages as $language){
            $validators['trans_'.$language->id.'_title'] = $trans[$language->id]['title'];
            $validators['trans_'.$language->id.'_slug'] = $trans[$language->id]['slug'];
            $validators['trans_'.$language->id.'_content'] = $trans[$language->id]['content'];
        }
        $validator = $this->validator($validators);
        if ($validator->fails()) {
            Session::flash('message', trans('admin.create-page-failed'));
            return redirect(route('admin.page.create'))->withInput($request->input())->withErrors($validator);
        } else {
            $page = new Page();
            $page->status = $request->input('status');
            $page->user_id = Auth::user()->id;
            $page->layout = $request->input('layout');
            $page->view = $request->input('view');
            $page->save();
            if (!empty($request->input('trans'))) {
                foreach ($request->input('trans') as $language => $data) {
                    if (!empty($data['title'])) {
                        $pageTrans = new PageTrans();
                        $pageTrans->pages_id = $page->id;
                        $pageTrans->language_id = $language;
                        $pageTrans->slug = $data['slug'];
                        $pageTrans->title = $data['title'];
                        $pageTrans->content = $data['content'];
                        $pageTrans->save();
                    }
                }
            }
            Cache::forget('allPages');
            Session::flash('message', trans('admin.create-page-success'));
            return redirect(route('admin.page.index'));
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setTitle(trans('admin.page'));
        $this->theme->setSubtitle(trans('admin.page_edit'));

        $page = Page::find($id);
        $languages = Language::all();
        $view = array(
            'page' => $page,
            'languages' => $languages
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        $this->theme->asset()->container('footer')->add('ckeditor', 'https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js', $dependencies );
        return $this->theme->scope('page.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $languages = Language::all();
        $validators = [];
        $trans = $request->input('trans');
        foreach($languages as $language){
            $validators['trans_'.$language->id.'_title'] = $trans[$language->id]['title'];
            $validators['trans_'.$language->id.'_slug'] = $trans[$language->id]['slug'];
            $validators['trans_'.$language->id.'_content'] = $trans[$language->id]['content'];
        }
        $validator = $this->validator($validators);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.update-page-failed'));
            return redirect( route( 'admin.page.update', array($id) ) )->withInput($request->input())->withErrors($validator);
        }else{
            $page = Page::find($id);
            $page->status = $request->input('status');
            $page->layout = $request->input('layout');
            $page->view = $request->input('view');
            $page->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['title'] ) ){
                        PageTrans::where('language_id', $language)->where('pages_id', $page->id)->delete();
                        $pageTrans = new PageTrans();
                        $pageTrans->pages_id = $page->id;
                        $pageTrans->language_id = $language;
                        $pageTrans->slug = $data['slug'];
                        $pageTrans->title = $data['title'];
                        $pageTrans->content = $_REQUEST['trans'][$language]['content'];
                        $pageTrans->save();
                    }
                }
            }
            Cache::forget('allPages');
            Session::flash('message', trans('admin.update-page-success'));
            return redirect(route('admin.page.index'));
        }
    }
}
