<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Validator;
use App\Models\Media;
use Illuminate\Support\Facades\Input;
use Session;

class AdminMediaController extends AdminController
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected $mainActive = 'media';

    protected function validator_update(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'full_name' => 'required|max:255',
            'password' => 'min:6|confirmed',
        ]);
    }

    protected function validator_create(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:user',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'full_name' => 'required|max:255',
            'password' => 'required|min:6|confirmed',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->setSubactive('list');
        $this->theme->setTitle(trans('admin.media'));
        $this->theme->setSubtitle(trans('admin.media_list'));
        $view = array();
        $medias = Media::paginate(16);
        $view['medias'] = $medias;
        $this->theme->asset()->usePath()->add('jquery.filer', 'plugins/jQuery.filer/css/jquery.filer.css' );
        $this->theme->asset()->usePath()->add('jquery.filer-theme', 'plugins/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css' );
        $this->theme->asset()->container('footer')->usePath()->add('jquery.filer-js', 'plugins/jQuery.filer/js/jquery.filer.min.js', array('jquery') );
        $this->theme->asset()->container('footer')->usePath()->add('upload', 'js/upload.js', array('jquery') );
        return $this->theme->scope('media.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->theme->setSubactive('create');
        $this->theme->setTitle(trans('admin.media'));
        $this->theme->setSubtitle(trans('admin.media_create'));
        $view = array();
        $this->theme->asset()->usePath()->add('jquery.filer', 'plugins/jQuery.filer/css/jquery.filer.css' );
        $this->theme->asset()->usePath()->add('jquery.filer-theme', 'plugins/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css' );
        $this->theme->asset()->container('footer')->usePath()->add('jquery.filer-js', 'plugins/jQuery.filer/js/jquery.filer.min.js', array('jquery') );
        $this->theme->asset()->container('footer')->usePath()->add('upload', 'js/upload.js', array('jquery') );
        return $this->theme->scope('media.create', $view)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Input::hasFile('files')) {
            $files = Input::file('files');
            //upload an image to the /img/tmp directory and return the filepath.
            foreach ($files as $file){
                $tmpFileName = time()."-".$file->getClientOriginalName();

                $media = new Media();
                $media->name = $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->getClientOriginalName());
                $media->caption = $media->name;
                $media->description = $media->name;
                $media->file_name = $tmpFileName;
                $media->file_type = $file->getClientMimeType();
                $media->file_size = $file->getClientSize();
                $media->user_id = Auth()->user()->id;

                $uploadDir = $this->upload_url();
                $file = $file->move($uploadDir['path'], $tmpFileName);
                $path = $uploadDir['url'] . $tmpFileName;

                $media->file_url = $path;
                $media->save();

                $res = array(
                    'id' => 1,
                    'editUrl' => route('admin.media.edit', array($media->id)),
                    'removeUrl' => route('admin.media.destroy', array($media->id)),
                );
                echo json_encode($res);
            }
            die();
            //return redirect(route('admin.media.edit', array($id)));
        } else {
            return redirect(route('admin.media.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setTitle(trans('admin.media'));
        $this->theme->setSubtitle(trans('admin.media_edit'));
        $this->theme->setSubactive('create');
        $view = array();
        $media = Media::find($id);
        $view['media'] = $media;
        $this->theme->asset()->usePath()->add('jquery.filer', 'plugins/jQuery.filer/css/jquery.filer.css' );
        $this->theme->asset()->usePath()->add('jquery.filer-theme', 'plugins/jQuery.filer/css/themes/jquery.filer-dragdropbox-theme.css' );
        $this->theme->asset()->container('footer')->usePath()->add('jquery.filer-js', 'plugins/jQuery.filer/js/jquery.filer.min.js', array('jquery') );
        $this->theme->asset()->container('footer')->usePath()->add('upload', 'js/upload.js', array('jquery') );
        return $this->theme->scope('media.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $media = Media::find($id);
        $media->update($request->all());
        Session::flash('message', trans('admin.update-media-success'));
        return redirect(route('admin.media.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = Media::find($id);
        $file = public_path().'/uploads/'.$media->file_url;
        unset($file);
        $media->delete();
        Session::flash('message', trans('admin.delete-media-success'));
        die();
    }

    public function upload_url(){
        $destinationPath = public_path() . '/uploads/';
        $year = date("Y");
        $month = date("m");
        $day = date("d");
        $uploadDir['url'] = "uploads/{$year}/{$month}/{$day}/";
        $uploadDir['path'] = $destinationPath."{$year}/{$month}/{$day}/";
        if(!is_dir($destinationPath.$year) ) @mkdir($destinationPath.$year);
        if(!is_dir($destinationPath.$year."/".$month) ) @mkdir($destinationPath.$year."/".$month);
        if(!is_dir($destinationPath.$year."/".$month."/".$day) ) @mkdir($destinationPath.$year."/".$month."/".$day);
        return $uploadDir;
    }
}
