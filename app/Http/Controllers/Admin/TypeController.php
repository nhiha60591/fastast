<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Type;
use App\Models\Language;
use App\Models\TypeTrans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Session;

class TypeController extends AdminController
{
    protected $mainActive = 'package';
    protected $subActive = 'type';
    protected function validator(array $data)
    {
        $validateFields = [];
        $messsages = array();
        $languages = Language::all();
        if( $languages ){
            foreach ($languages as $language){
                $validateFields['trans_'.$language->id.'_name'] = 'required|max:255';
                $messsages['trans_'.$language->id.'_name.required'] = 'Vui lòng nhập tên của kiểu gói';
            }
        }

        return Validator::make($data, $validateFields, $messsages);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->setTitle(trans('admin.type'));
        $this->theme->setSubtitle(trans('admin.type_list'));
        if( Cache::has( 'allTypes' ) ){
            $types = Cache::get( 'allTypes' );
        }else{
            $types = Type::all();
            Cache::add( 'allTypes', $types, 3600);
        }
        $currentLanguage = Language::where('locale', config('app.locale'))->first();
        $view = array(
            'types'=>$types,
            'currentLanguage' => $currentLanguage
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('type.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::all();
        $view = array(
            'languages' => $languages
        );

        $this->theme->setTitle(trans('admin.type'));
        $this->theme->setSubtitle(trans('admin.type_create'));
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('type.create', $view)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        if($request->input('trans') ){
            foreach ($request->input('trans') as $k=>$v){
                $data['trans_'.$k.'_name'] = $v['name'];
            }
        }
        $validator = $this->validator($data);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.create-type-failed'));
            return redirect(route('admin.type.create'))->withInput($request->input())->withErrors($validator);
        }else{
            $type = new Type();
            $upload = uploadFile($request->file('image'));
            $type->image = $upload['fileUrl'];
            $type->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['name'] ) ){
                        $typeTrans = new TypeTrans();
                        $typeTrans->type_id = $type->id;
                        $typeTrans->language_id = $language;
                        $typeTrans->name = $data['name'];
                        $typeTrans->description = $data['description'];
                        $typeTrans->save();
                    }
                }
            }
            Cache::forget('allTypes');
            Session::flash('message', trans('admin.create-type-success'));
            return redirect(route('admin.type.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setTitle(trans('admin.type'));
        $this->theme->setSubtitle(trans('admin.type_edit'));
        $type = Type::find($id);
        $languages = Language::all();
        $view = array(
            'type' => $type,
            'languages' => $languages
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('type.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->input());
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.update-type-failed'));
            return redirect( route( 'admin.type.update', array($id) ) )->withInput($request->input())->withErrors($validator);
        }else{
            $type = Type::find($id);
            $upload = uploadFile($request->file('image'));
            $type->image = $upload['fileUrl'];
            $type->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['name'] ) ){
                        TypeTrans::where('language_id', $language)->where('type_id', $type->id)->delete();
                        $shippingServiceTrans = new TypeTrans();
                        $shippingServiceTrans->type_id = $type->id;
                        $shippingServiceTrans->language_id = $language;
                        $shippingServiceTrans->name = $data['name'];
                        $shippingServiceTrans->description = $data['description'];
                        $shippingServiceTrans->save();
                    }
                }
            }
            Cache::forget('allTypes');
            Session::flash('message', trans('admin.update-type-success'));
            return redirect(route('admin.type.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TypeTrans::where('type_id', $id)->delete();
        Type::destroy($id);
        $response['code'] = 200;
        $response['message'] = trans('admin.deleteSuccess');
        Cache::forget('allTypes');
        echo json_encode($response);
        die();
    }
}
