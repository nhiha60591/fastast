<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\City;
use App\Models\CityTrans;
use App\Models\Language;
use App\Models\Country;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;

class CityController extends AdminController
{
    protected $mainActive = 'package';
    protected $subActive = 'city';

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'lon' => 'required|max:255',
            'lat' => 'required|max:255'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->setTitle(trans('admin.city'));
        $this->theme->setSubtitle(trans('admin.city_list'));
        if( Cache::has( 'allCities' ) ){
            $cities = Cache::get( 'allCities' );
        }else{
            $cities = City::all();
            Cache::add( 'allCities', $cities, 3600);
        }
        $currentLanguage = Language::where('locale', config('app.locale'))->first();
        $view = array(
            'cities'=>$cities,
            'currentLanguage' => $currentLanguage
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('city.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->theme->setTitle(trans('admin.city'));
        $this->theme->setSubtitle(trans('admin.city_create'));
        $languages = Language::all();
        $countries = Country::all();
        $countriesList = array();
        foreach ($countries as $country){
            $countriesList[$country->id] = $country->translation(1);
        }
        $view = array(
            'languages' => $languages,
            'countries' => $countriesList
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('city.create', $view)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array(
            'lon' => $request->input('lon'),
            'lat' => $request->input('lat')
        );
        $validator = $this->validator($data);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.create-city-failed'));
            return redirect(route('admin.city.create'))->withInput($request->input())->withErrors($validator);
        }else{
            $city = new City();
            $city->status = $request->input('status');
            $city->lon = $request->input('lon');
            $city->lat = $request->input('lat');
            $city->country_id = $request->input('country_id');
            $city->time_zone = $request->input('time_zone');
            $city->post_code = $request->input('post_code');
            $city->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['name'] ) ){
                        $countryTrans = new CityTrans();
                        $countryTrans->city_id = $city->id;
                        $countryTrans->language_id = $language;
                        $countryTrans->name = $data['name'];
                        $countryTrans->description = $data['description'];
                        $countryTrans->save();
                    }
                }
            }
            Cache::forget('allCities');
            Session::flash('message', trans('admin.create-city-success'));
            return redirect(route('admin.city.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setTitle(trans('admin.city'));
        $this->theme->setSubtitle(trans('admin.city_edit'));

        $city = City::find($id);
        $languages = Language::all();
        $countries = Country::all();
        $countriesList = array();

        foreach ($countries as $country){
            $countriesList[$country->id] = $country->translation(1);
        }
        $view = array(
            'city' => $city,
            'languages' => $languages,
            'countries' => $countriesList
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('city.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'lon' => $request->input('lon'),
            'lat' => $request->input('lat')
        );
        $validator = $this->validator($data);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.update-city-failed'));
            return redirect( route( 'admin.city.update', array($id) ) )->withInput($request->input())->withErrors($validator);
        }else{
            $city = City::find($id);
            $city->status = $request->input('status');
            $city->lon = $request->input('lon');
            $city->lat = $request->input('lat');
            $city->country_id = $request->input('country_id');
            $city->time_zone = $request->input('time_zone');
            $city->post_code = $request->input('post_code');
            $city->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['name'] ) ){
                        CityTrans::where('language_id', $language)->where('city_id', $city->id)->delete();
                        $countryTrans = new CityTrans();
                        $countryTrans->city_id = $city->id;
                        $countryTrans->language_id = $language;
                        $countryTrans->name = $data['name'];
                        $countryTrans->description = $data['description'];
                        $countryTrans->save();
                    }
                }
            }
            Cache::forget('allCities');
            Session::flash('message', trans('admin.update-city-success'));
            return redirect(route('admin.city.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CityTrans::where('city_id', $id)->delete();
        City::destroy($id);
        $response['code'] = 200;
        $response['message'] = trans('admin.deleteSuccess');
        Cache::forget('allCountries');
        echo json_encode($response);
        die();
    }
}
