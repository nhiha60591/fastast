<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use App\Models\Language;
use App\Models\Currency;
use App\Models\Options;
use App\Models\Unit;
use Cache;
use Validator;
use Config;

class DashboardController extends AdminController
{
    protected $mainActive = 'dashboard';
    public function index(){
        $this->theme->asset()->usePath()->add('iCheck-flat-blue', 'plugins/iCheck/flat/blue.css');
        $this->theme->asset()->usePath()->add('morris', 'plugins/morris/morris.css');
        $this->theme->asset()->usePath()->add('jquery-jvectormap', 'plugins/jvectormap/jquery-jvectormap-1.2.2.css');
        $this->theme->asset()->usePath()->add('datepicker3', 'plugins/datepicker/datepicker3.css');
        $this->theme->asset()->usePath()->add('daterangepicker-bs3', 'plugins/daterangepicker/daterangepicker-bs3.css');

        $this->theme->asset()->container('footer')->add('jquery-ui', 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->add('raphael-js', 'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('morris-js', 'plugins/morris/morris.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('jquery.sparkline-js', 'plugins/sparkline/jquery.sparkline.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('jquery-jvectormap', 'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('jquery-jvectormap-world-mill', 'plugins/jvectormap/jquery-jvectormap-world-mill-en.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('jquery.knob', 'plugins/knob/jquery.knob.js', array('jquery'));
        $this->theme->asset()->container('footer')->add('moment-js', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('daterangepicker-js', 'plugins/daterangepicker/daterangepicker.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('bootstrap-datepicker-js', 'plugins/datepicker/bootstrap-datepicker.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('bootstrap3-wysihtml5-js', 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dashboard-js', 'js/pages/dashboard.js', array('jquery'));
        return $this->theme->scope('dashboard.index')->render();
    }
    public function general(){
        $this->theme->setTitle('Settings');
        $this->theme->setSubtitle('General');
        $view = array();
        if( Cache::has( 'allLanguages' ) ){
            $languages = Cache::get( 'allLanguages' );
        }else{
            $languages = Language::all();
            Cache::add( 'allLanguages', $languages, 3600);
        }
        if( Cache::has( 'allCurrencies' ) ){
            $currencies = Cache::get( 'allCurrencies' );
        }else{
            $currencies = Currency::all();
            Cache::add( 'allCurrencies', $currencies, 3600);
        }
      
        if( Cache::has( 'weightUnit' ) ){
            $weightUnit = Cache::get( 'weightUnit' );
        }else{
            $weightUnit = Unit::where('type', 'weight')->get();
            Cache::add( 'weightUnit', $weightUnit, 3600);
        }
        
        if( Cache::has( 'longUnit' ) ){
            $longUnit = Cache::get( 'longUnit' );
        }else{
            $longUnit = Unit::where('type', 'long')->get();
            Cache::add( 'longUnit', $longUnit, 3600);
        }
        $langs = array();
        foreach ($languages as $lang){
            $langs[$lang->id] = $lang->name;
        }

        $currs = array();
        foreach ($currencies as $curr){
            $currs[$curr->id] = $curr->name;
        }
        $weights = array();
        foreach ($weightUnit as $weight){
            $weights[$weight->id] = $weight->name;
        }

        $longs = array();
        foreach ($longUnit as $long){
            $longs[$long->id] = $long->name;
        }
        $view['languages'] = $langs;
        $view['currencies'] = $currs;
        $view['weightUnit'] = $weights;
        $view['longUnit'] = $longs;
        $options = Options::all();
        $ops = array();
        foreach ($options as $op){
            $ops[$op->option_key] = $op->option_value;
        }
        $view['ops'] = $ops;
        return $this->theme->scope('dashboard.general', $view)->render();
    }
    public function generalUpdate(Request $request){
        foreach ($request->input() as $key=>$input){
            if( $key == "_token") continue;
            $this->updateOption($key, $input);
        }
        Cache::forget('allOptions');
        Cache::forget('allOptionData');
        return redirect(url('admin/general'));
    }
    public function updateOption($key, $value){
        $option = Options::where('option_key', $key)->first();
        if( $option ){
            $option = Options::find($option->id);
            $option->option_value = $value;
            $option->save();
        }else{
            $option = Options::create(array(
                'option_key' => $key,
                'option_value' => $value,
                'status' => 'yes'
            ));
        }
        return $option;
    }
}
