<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Models\District;
use App\Models\DistrictTrans;
use App\Models\City;
use App\Models\Language;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Session;

class DistrictController extends AdminController
{
    protected $mainActive = 'package';
    protected $subActive = 'district';

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'lon' => 'required|max:255',
            'lat' => 'required|max:255'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->setTitle(trans('admin.district'));
        $this->theme->setSubtitle(trans('admin.district_list'));
        $districts = District::all();
        $currentLanguage = Language::where('locale', config('app.locale'))->first();
        $view = array(
            'districts'=>$districts,
            'currentLanguage' => $currentLanguage
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('district.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->theme->setTitle(trans('admin.district'));
        $this->theme->setSubtitle(trans('admin.district_create'));
        $languages = Language::all();
        $cities = City::all();
        $citiesList = array();
        foreach ($cities as $city){
            $citiesList[$city->id] = $city->translation(1);
        }
        $view = array(
            'languages' => $languages,
            'cities' => $citiesList
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('district.create', $view)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array(
            'lon' => $request->input('lon'),
            'lat' => $request->input('lat')
        );
        $validator = $this->validator($data);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.create-district-failed'));
            return redirect(route('admin.district.create'))->withInput($request->input())->withErrors($validator);
        }else{
            $district = new District();
            $district->status = $request->input('status');
            $district->lon = $request->input('lon');
            $district->lat = $request->input('lat');
            $district->city_id = $request->input('city_id');
            $district->time_zone = $request->input('time_zone');
            $district->post_code = $request->input('post_code');
            $district->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['name'] ) ){
                        $countryTrans = new DistrictTrans();
                        $countryTrans->district_id = $district->id;
                        $countryTrans->language_id = $language;
                        $countryTrans->name = $data['name'];
                        $countryTrans->description = $data['description'];
                        $countryTrans->save();
                    }
                }
            }
            Cache::forget('allDistricts');
            Session::flash('message', trans('admin.create-district-success'));
            return redirect(route('admin.district.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setTitle(trans('admin.district'));
        $this->theme->setSubtitle(trans('admin.district_edit'));
        $district = District::find($id);
        $languages = Language::all();
        $cities = City::all();
        $citiesList = array();
        foreach ($cities as $city){
            $citiesList[$city->id] = $city->translation(1);
        }
        $view = array(
            'district' => $district,
            'languages' => $languages,
            'cities' => $citiesList
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('district.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'lon' => $request->input('lon'),
            'lat' => $request->input('lat')
        );
        $validator = $this->validator($data);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.update-district-failed'));
            return redirect( route( 'admin.district.update', array($id) ) )->withInput($request->input())->withErrors($validator);
        }else{
            $district = District::find($id);
            $district->status = $request->input('status');
            $district->lon = $request->input('lon');
            $district->lat = $request->input('lat');
            $district->city_id = $request->input('city_id');
            $district->time_zone = $request->input('time_zone');
            $district->post_code = $request->input('post_code');
            $district->save();
            if( !empty( $request->input('trans') ) ){
                foreach ($request->input('trans') as $language=>$data){
                    if( !empty( $data['name'] ) ){
                        $countryTrans = DistrictTrans::where('language_id', $language)->where('district_id', $district->id)->delete();
                        $countryTrans = new DistrictTrans();
                        $countryTrans->district_id = $district->id;
                        $countryTrans->language_id = $language;
                        $countryTrans->name = $data['name'];
                        $countryTrans->description = $data['description'];
                        $countryTrans->save();
                    }
                }
            }
            Cache::forget('allDistricts');
            Session::flash('message', trans('admin.update-district-success'));
            return redirect(route('admin.district.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DistrictTrans::where('district_id', $id)->delete();
        District::destroy($id);
        $response['code'] = 200;
        $response['message'] = trans('admin.deleteSuccess');
        Cache::forget('allDistricts');
        echo json_encode($response);
        die();
    }
}
