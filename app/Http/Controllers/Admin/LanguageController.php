<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Language;
use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Cache;
use Validator;
use Session;

class LanguageController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $rules = array(
        'name' => 'required|max:255',
        'code' => 'required',
        'image' => 'required|max:255',
        'default' => 'required|max:255',
        'status' => 'required',
    );
    protected $rules_create = array(
        'name' => 'required|max:255',
        'code' => 'required|max:255|unique:language',
        'image' => 'required|max:255',
        'default' => 'required|max:255',
        'status' => 'required',
    );
    public function index()
    {
        $this->theme->setSubactive('list');
        $this->theme->setTitle(trans('admin.language'));
        $this->theme->setSubtitle(trans('admin.language_list'));
        if( Cache::has( 'allLanguages' ) ){
            $languages = Cache::get( 'allLanguages' );
        }else{
            $languages = Language::all();
            Cache::add( 'allLanguages', $languages, 3600);
        }
        $view = array(
            'languages' => $languages
        );

        $this->theme->asset()->usePath()->add('dataTables.bootstrap-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('jquery.dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap-js', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        $this->theme->asset()->container('footer')->usePath()->add('user-js', 'js/user.js', array('jquery'));

        return $this->theme->scope('language.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->theme->setSubactive('create');
        $this->theme->setTitle(trans('admin.language'));
        $this->theme->setSubtitle(trans('admin.language_create'));
        $view = array();
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('language.create', $view)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->input(), $this->rules_create);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.create-language-failed'));
            return redirect(route('admin.localization.language.create'))->withInput($request->input())->withErrors($validator);
        }else {
            $language = new Language();
            $language->create($request->input());
            Cache::forget('allLanguages');
            Session::flash('message', trans('admin.create-language-success'));
            return redirect(route('admin.localization.language.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->theme->setSubactive('edit');
        $this->theme->setTitle(trans('admin.language'));
        $this->theme->setSubtitle(trans('admin.language_edit'));
        $language = Language::find($id);
        $view = array(
            'language' => $language
        );
        $this->theme->asset()->usePath()->add('select2-css', 'plugins/select2/select2.min.css');
        $this->theme->asset()->container('footer')->usePath()->add('select2-js', 'plugins/select2/select2.full.min.js', array('jquery'));
        // Dependency with.
        $dependencies = array('jquery', 'select2-js');

        $this->theme->asset()->container('footer')->usePath()->add('user-form', 'js/user-form.js', $dependencies );
        return $this->theme->scope('language.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->input(), $this->rules);
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.update-language-failed'));
            return redirect(route('admin.localization.language.edit', array($id)))->withInput($request->input())->withErrors($validator);
        }else {
            $language = Language::find($id);
            $language->update($request->input());
            Cache::forget('allLanguages');
            Session::flash('message', trans('admin.update-language-success'));
            return redirect(route('admin.localization.language.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = array();
        Language::destroy($id);
        $response['code'] = 200;
        $response['message'] = trans('admin.deleteSuccess');
        Cache::forget( 'allLanguages' );

        echo json_encode($response);
        die();
    }
}
