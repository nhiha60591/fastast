<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests;
use App\Models\ShippingService;
use Illuminate\Http\Request;
use App\Http\Controllers\FrontController;
use App\Models\Language;
use App\Models\Page;
use Illuminate\Support\Facades\Cache;
use App\Models\Delivery;

class HomeController extends FrontController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->theme->asset()->usePath()->add('home', 'css/home.css');
        $this->theme->asset()->usePath()->add('home-js', 'js/home.js', array('jquery'));

        // or just override layout
        $this->theme->layout('welcome');
        $currentLanguage = Language::where('code', config('app.locale'))->first();
        if( empty( $currentLanguage ) ){
            $currentLanguage = Language::find(1)->first();
        }
        if( Cache::has('allShippingServices') ){
            $services = Cache::get('allShippingServices');
        }else{
            $services = ShippingService::all();
            Cache::add('allShippingServices', $services, 3600);
        }

        if( Cache::has('allDeliveries') ){
            $deliveries = Cache::get('allDeliveries');
        }else{
            $deliveries = Delivery::all();
        }
        $view = [
            'currentLanguage' => $currentLanguage,
            'services' => $services,
            'deliveries' => $deliveries
        ];
        //return $this->theme->scope('home.index-'.$currentLanguage->code, $view)->render();
        return $this->theme->scope('home.index-vi', $view)->render();
    }
}
