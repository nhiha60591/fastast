<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\FrontController;
use App\Models\BookImages;
use App\Models\BookShipping;
use App\Models\PackageLocations;
use App\Models\Shipping;
use Illuminate\Http\Request;
use App\Http\Requests;
use Cache;
use App\Models\Language;
use App\Models\ShippingService;
use App\Models\Delivery;
use App\Models\Package;
use App\Models\PackageImages;
use App\Models\PackageService;
use App\Models\UserLimitCount;
use Illuminate\Support\Facades\Auth;
use Teepluss\Theme\Contracts\Theme;
use Illuminate\Support\Facades\Mail;

class PackageController extends FrontController
{
    public function __construct(Theme $theme)
    {
        parent::__construct($theme);
        $this->middleware('auth')->only(['getPostPackage', 'getEdit', 'getSendPackage', 'postSendPackage', 'getShow', 'getShowBooking']);
        $this->theme->asset()->usePath()->add('package-css', 'css/package.css');
    }
    /**
     * Display Index page for Package
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $view = [];
        $package = Package::where('status', 'confirm')->paginate(6);
        $view['packages'] = $package;
        $currentLanguage = Language::where('code', config('app.locale') )->first();
        $view['currentLanguage'] = $currentLanguage;
        if( Cache::has('allShippingServices') ){
            $view['services'] = Cache::get('allShippingServices');
        }else{
            $view['services'] = ShippingService::all();
        }
        if( Cache::has('allDeliveries') ){
            $view['deliveries'] = Cache::get('allDeliveries');
        }else{
            $view['deliveries'] = Delivery::all();
        }
        $this->theme->asset()->usePath()->add('list-package-js', 'js/list-package.js', array('jquery'));
        return $this->theme->scope('package.index', $view)->render();
    }

    /**
     * Search package
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSearch(Request $request)
    {
        $view = [];
        $package = new Package();
        $currentLanguage = Language::where('code', config('app.locale') )->first();
        $view['currentLanguage'] = $currentLanguage;
        if( Cache::has('allShippingServices') ){
            $view['services'] = Cache::get('allShippingServices');
        }else{
            $view['services'] = ShippingService::all();
        }
        if( Cache::has('allDeliveries') ){
            $view['deliveries'] = Cache::get('allDeliveries');
        }else{
            $view['deliveries'] = Delivery::all();
        }
        if( $request->input('service') ){
            $service = $request->input('service');
            $package = $package->whereHas('services', function ($query) use($service) {
                $query->where('id', $service);
            });
        }
        if( $request->input('delivery') ){
            $package = $package->where('shipping_time', $request->input('delivery'));
        }
        if( $request->input('from') ){
            $package = $package->where('origination', 'LIKE', "%".$request->input('from')."%");
        }
        if( $request->input('to') ){
            $package = $package->where('destination', 'LIKE', "%".$request->input('to')."%");
        }
        $inputs = $request->all();
        if( isset($inputs['page'] ) && !empty($inputs['page'])) {
            unset($inputs['page']);
        }
        $url = http_build_query($inputs);
        $package = $package->where('status', 'confirm')->orderBy('created_at', 'desc')->paginate(6)->setPath('find-package/?'.$url);
        $view['packages'] = $package;
        $this->theme->asset()->usePath()->add('list-package-js', 'js/list-package.js', array('jquery'));
        return $this->theme->scope('package.search', $view)->render();
    }

    /**
     * Display create package page
     *
     * @return \Illuminate\Http\Response
     */
    public function getPostPackage($id=0){
        $view = [];
        if( Cache::has('allShippingServices') ){
            $view['services'] = Cache::get('allShippingServices');
        }else{
            $view['services'] = ShippingService::all();
        }
        if( Cache::has('allDeliveries') ){
            $view['deliveries'] = Cache::get('allDeliveries');
        }else{
            $view['deliveries'] = Delivery::all();
        }
        if( $id ){
            $package = Package::find($id);
        }else{
            $package = new Package();
        }
        $currentLanguage = Language::where('code', config('app.locale') )->first();
        $view['currentLanguage'] = $currentLanguage;
        $view['package'] = $package;
        $this->theme->asset()->usePath()->add('post-package-js', 'js/post-package.js', array('jquery', 'tinymce-editor'));
        return $this->theme->scope('package.create', $view)->render();
    }

    /**
     * Save all package data
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postPostPackage(Request $request, $id=0){
        if( $id ){
            $package = Package::find($id);
        }else{
            $package = new Package();
        }
        if( $request->input('originationLocation') ){
            $package->origination_location = $request->input('originationLocation');
        }
        if( $request->input('origination') ){
            $package->origination = $request->input('origination');
        }
        if( $request->input('destinationLocation') ){
            $package->destination_location = $request->input('destinationLocation');
        }
        if( $request->input('destination') ){
            $package->destination = $request->input('destination');
        }
        if( $request->input('shippingCost') ){
            if( $this->defaultCurrency->id != $this->currentCurrency->id ){
                $shippingCostCal = ((double)$request->input('shippingCost') * (double)$this->defaultCurrency->value) / (double)$this->currentCurrency->value;
                $package->shipping_cost = $shippingCostCal;
            }else{
                $package->shipping_cost = $request->input('shippingCost');
            }
        }
        if( $request->input('shippingTime') ){
            $package->shipping_time = $request->input('shippingTime');
        }
        if( $request->input('departureDate') ){
            $package->departure_date = $request->input('departureDate');
        }
        if( $request->input('arrivalDate') ){
            $package->arrival_date = $request->input('arrivalDate');
        }
        if( $request->input('packageOption') ){
            $package->options = implode(',', $request->input('packageOption'));
        }
        if( $request->input('packageContent') ){
            $package->content = $request->input('packageContent');
        }
        if( $request->input('fullName') ){
            $package->contact_name = $request->input('fullName');
        }
        if( $request->input('email') ){
            $package->contact_email = $request->input('email');
        }
        if( $request->input('phone') ){
            $package->contact_phone = $request->input('phone');
        }
        $package->status = 'draft';
        $package->user_id = Auth()->user()->id;
        if( $package->save() ){
            if( $request->input('deliveryService') ){
                PackageService::where('package_id', $package->id)->delete();
                foreach ($request->input('deliveryService') as $service){
                    if( PackageService::where('package_id', $package->id)->where('service_id', $service)->count() ) continue;
                    $saveService = new PackageService();
                    $saveService->package_id = $package->id;
                    $saveService->service_id = $service;
                    $saveService->save();
                }
            }
            if( $request->input('locations') ){
                PackageLocations::where('package_id', $package->id)->delete();
                foreach ($request->input('locations') as $locationItem){
                    $location = new PackageLocations();
                    $location->package_id = $package->id;
                    $location->lng = $locationItem['long'];
                    $location->lat = $locationItem['lat'];
                    $location->save();
                }
            }
            if( $request->hasFile('files') ){
                $files = $request->file('files');
                PackageImages::where('package_id', $package->id)->delete();
                //upload an image to the /img/tmp directory and return the filepath.
                foreach ($files as $file){
                    $packageImages = new PackageImages();
                    $packageImages->package_id = $package->id;
                    $upload = uploadFile($file);
                    $packageImages->image_id = $upload['id'];
                    $packageImages->save();
                }
            }
        }
        if( $request->input('confirm') ){
            $package->status = 'confirm';
            $package->save();
            return redirect(url('package/show', $package->id));
        }
        if( $request->input('preview') ){
            $package->status = 'preview';
            $package->save();
            return redirect(url('package/preview', $package->id));
        }
        if( $request->input('cancel') ){
            $package->status = 'cancel';
            $package->save();
            return redirect(url('/'));
        }
        return redirect(url('package/post-package'));
    }

    /**
     * Show package detail
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getShow($id=0){
        if( !Auth::user()->isSuperUser() || !Auth::user()->package_click_count == -1){
            $count = UserLimitCount::where('user_id', Auth::user()->id)
            ->where('type', 'package')
            ->where('item_id', '<>', $id)
            ->where('month', date('m') )
            ->where('year', date('y') );
            if( $count->count() >= Auth::user()->package_click_count ){
                return $this->theme->scope('package.limit-package')->render();
            }else{
                $userCount = new UserLimitCount();
                $userCount->user_id = Auth::user()->id;
                $userCount->item_id = $id;
                $userCount->type = 'package';
                $userCount->month = date('m');
                $userCount->year = date('Y');
                $userCount->save();
            }
        }
        $this->theme->asset()->usePath()->add('show-package', 'js/show-package.js');
        $view = [];
        $package = Package::find($id);
        $view['package'] = $package;
        $currentLanguage = Language::where('code', config('app.locale') )->first();
        $view['currentLanguage'] = $currentLanguage;
        return $this->theme->scope('package.show', $view)->render();
    }

    /**
     * Show send package page
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getSendPackage($id=0, $bookingid=0){
        $view = ['id'=>$id];
        $shipping = Shipping::find($id);
        $view['shipping'] = $shipping;
        if( $bookingid ){
            $booking = BookShipping::find($bookingid);
            if( $booking->user_id != Auth::user()->id || !Auth::user()->is_admin ){
                return redirect(url('/'));
            }
            if( $booking ){
                $view['booking'] = $booking;
                return $this->theme->scope('package.confirm-send-package', $view)->render();
            }
        }
        return $this->theme->scope('package.send-package', $view)->render();
    }

    public function postSendPackage(Request $request, $id=0, $bookingid = 0){
        if( $request->input('cancel') ){
            return redirect(url('/'));
        }

        $view = ['id'=>$id];
        $searchParams = session('searchParams');
        $shipping = Shipping::find($id);
        $booking = BookShipping::find($bookingid);
        $trackingNumber = BookShipping::where('id', '>', 0)->max('id');
        $trackingNumber = (int)$trackingNumber;
        if( $booking ){
            if( $booking->user_id != Auth::user()->id || !Auth::user()->isSuperUser() ){
                return redirect(url('/'));
            }
        }else{
            $trackingNumber = $trackingNumber + 1;
            switch ($trackingNumber){
                case $trackingNumber < 9:
                    $trackingNumber = '00'.$trackingNumber;
                    break;
                case $trackingNumber < 100:
                    $trackingNumber = '0'.$trackingNumber;
                    break;
            }
            $booking = new BookShipping();
            $booking->user_id = Auth::user()->id;
            $booking->track_number = 'A'.$trackingNumber.'FG';
            $booking->service_options = serialize( @$searchParams['packageType'] );
            $booking->service_info = serialize( @$searchParams );
            $booking->service_type = session('type');
        }
        $booking->shipping_id = $id;
        $booking->containing = $request->input('packageInformation');
        $booking->requirements = serialize( $request->input('packageRequirement') );
        $booking->origination = serialize( $request->input('origination') );
        $booking->destination = serialize( $request->input('destination') );

        if( $request->input('preview') ){
            $booking->status = 'preview';
        }
        if( $request->input('confirm') ){
            $booking->status = 'wait-approve';
            $this->sendNewBookingMail($booking, $shipping);
        }
        $booking->cost = $shipping->costNotCurrencySumboy(session('packageSize'));
        $booking->vat_fee = $shipping->vat;
        $booking->shipper_fee = 0;
        $booking->sender_fee = $shipping->service->transaction_fee;
        $booking->save();
        if( $request->hasFile('files') ){
            BookImages::where('book_id', $booking->id)->delete();
            $files = $request->file('files');
            //upload an image to the /img/tmp directory and return the filepath.
            foreach ($files as $file){
                $bookingImages = new BookImages();
                $bookingImages->book_id = $booking->id;
                $upload = uploadFile($file);
                $bookingImages->media_id = $upload['id'];
                $bookingImages->save();
            }
        }
        if( $request->input('confirm') ){
            return redirect(url('package/show-booking', array('id'=>$booking->id, 'shippingid'=>$id) ));
        }
        $view['shipping'] = $shipping;
        $view['booking'] = $booking;
        $view['inputs'] = $request->all();
        $view['currentLanguage'] = $this->currentLanguage;
        return $this->theme->scope('package.preview-send', $view)->render();
        //return redirect(route('admin.user.create'))->withInput($request->input())->withErrors($validator);
    }
    public function getShowBooking($id=0, $shippingid = 0 ){
        $booking = BookShipping::find($id);
        if( $booking ) {
            $shipping = Shipping::find($shippingid);
            if( $shipping ){
                $view['shipping'] = $shipping;
            }
            if( $booking->status == 'pendding' && $shipping->user()->id == Auth::user()->id ){
                return response()->view("package.limit-booking");
            }
            $view['booking'] = $booking;
            $view['currentLanguage'] = $this->currentLanguage;
            return $this->theme->scope('package.show-send-package', $view)->render();
        }else{
            return response()->view("errors.error");
        }
    }
    public function postConfirmBooking(Request $request, $id=0, $shippingid = 0 ){
        $booking = BookShipping::find($id);
        $shipping = Shipping::find($shippingid);
        if( $booking->user_id != Auth::user()->id || !Auth::user()->isSuperUser() ){
            return redirect(url('/'));
        }

        if( $booking && $shipping ) {
            if( $request->input('confirm') ){
                $booking->status = 'wait-approve';
                $booking->save();
                $this->sendNewBookingMail($booking, $shipping);
                return redirect(url('package/show-booking', array('id'=>$booking->id, 'shippingid'=>$shippingid) ));
            }
            if( $request->input('back') ){
                return redirect(url('package/send-package', array('id'=>$shippingid, 'bookingid'=>$booking->id) ));
            }
            if( $request->input('cancel') ){
                return redirect(url('/' ));
            }
            $view['shipping'] = $shipping;
            $view['booking'] = $booking;
            $view['currentLanguage'] = $this->currentLanguage;
            return $this->theme->scope('package.preview-send', $view)->render();
        }else{
            return response()->view("errors.error");
        }
    }

    public function sendNewBookingMail($booking, $shipping){
        if( !Auth::user()->isSuperUser() || !$shipping->user->package_click_count == -1){
            $count = UserLimitCount::where('user_id', $shipping->user()->id)
            ->where('type', 'booking')
            ->where('item_id', '<>', $booking->id)
            ->where('month', date('m') )
            ->where('year', date('y') );
            if( $count->count() >= Auth::user()->package_click_count ){
                Mail::send('emails.new-booking-limit', ['booking' => $booking, 'shipping'=>$shipping], function ($m) use ($booking, $shipping) {
                    $from = config('mail.from');
                    $m->from($from['address'], $from['address']);
                    $m->to($shipping->user->email, $shipping->user->full_name)->subject('Have new book your shipping service!');
                });
                $booking->status = 'pendding';
                $booking->save();
            }else{
                $userCount = new UserLimitCount();
                $userCount->user_id = $shipping->user()->id;
                $userCount->item_id = $booking->id;
                $userCount->type = 'booking';
                $userCount->month = date('m');
                $userCount->year = date('Y');
                $userCount->save();
                Mail::send('emails.new-booking', ['booking' => $booking, 'shipping'=>$shipping], function ($m) use ($booking, $shipping) {
                    $from = config('mail.from');
                    $m->from($from['address'], $from['address']);
                    $m->to($shipping->user->email, $shipping->user->full_name)->subject('Have new book your shipping service!');
                });
            }
        }
        Mail::send('emails.new-booking-sender', ['booking' => $booking, 'shipping'=>$shipping], function ($m) use ($booking, $shipping) {
            $from = config('mail.from');
            $m->from($from['address'], $from['address']);
            $m->to($booking->user->email, $booking->user->full_name)->subject('Thank you for your booking!');
        });
        Mail::send('emails.new-booking-destination', ['booking' => $booking, 'shipping'=>$shipping], function ($m) use ($booking, $shipping) {
            $from = config('mail.from');
            $m->from($from['address'], $from['address']);
            $destination = unserialize( $booking->destination );
            $m->to($destination['email'], $destination['name'])->subject('Thank you for your booking!');
        });
    }
    public function searchTrackingCode(Request $request){
        $view = [];
        $trackingCode = $request->input('trackingNumber');
        $bookings = BookShipping::where('track_number', 'LIKE' , "%{$trackingCode}%")->get();
        $view['bookings'] = $bookings;
        return $this->theme->scope('package.search-tracking', $view)->render();
    }
    public function getPreview($id=0){
        $this->theme->asset()->usePath()->add('show-package', 'js/show-package.js');
        $view = [];
        $package = Package::find($id);
        $view['package'] = $package;
        $currentLanguage = Language::where('code', config('app.locale') )->first();
        $view['currentLanguage'] = $currentLanguage;
        return $this->theme->scope('package.preview', $view)->render();
    }
    public function postConfirmPackage(Request $request, $id=0){
        $package = Package::find($id);
        if( $request->input('confirm') ){
            $package->status = 'confirm';
            $package->save();
            return redirect(url('package/show', $id));
        }
        if( $request->input('back') ){
            $package->status = 'preview';
            $package->save();
            return redirect(url('package/post-package', $id));
        }
        if( $request->input('cancel') ){
            $package->status = 'cancel';
            $package->save();
        }
    }
    public function getMakeShipping( $id = 0 ){
        $book = BookShipping::find($id);
        $book->status = 'shipping';
        $book->save();
        return redirect(url('user/manage-shipping'));
    }
    public function getMakeCancel( $id = 0 ){
        $book = BookShipping::find($id);
        $book->status = 'cancel';
        $book->save();
        return redirect(url('user/manage-shipping'));
    }
    public function getMakeSuccess( $id = 0 ){
        $book = BookShipping::find($id);
        $book->status = 'success';
        $book->save();
        return redirect(url('user/manage-shipping'));
    }
    public function getUpdateStatus( $id = 0, $status = 'pending' ){
        $package = Package::find($id);
        $package->status = $status;
        $package->save();
        return redirect(url('user/manage-listing-package'));
    }
    public function getDelete( $id = 0 ){
        $package = Package::find($id)->delete();
        return redirect(url('user/manage-listing-package'));
    }
    public function getEdit( $id =0 ){
        $package = Package::find( $id );
        if( $package->user_id == Auth::user()->id || Auth::user()->isSuperUser() ){
            return $this->getPostPackage($id);
        }else{
            return $this->getShow($id);
        }
    }
    public function deleteDelete($id){
        $package = Package::find( $id );
        if( $package->user_id == Auth::user()->id || Auth::user()->isSuperUser() ){
            $package->delete();
            echo json_encode(array('code'=>1, 'message'=>trans('front.remove-success')));
            die();
        }else{
            echo json_encode(array('code'=>0, 'message'=>trans('front.remove-fail')));
            die();
        }
    }
    public function getMakeCopy($id){
        $oldPackage = Package::find( $id );
        $newPackage = $oldPackage->replicate();
        $newPackage->save();
        if( $newPackage->save() ){
            if( $oldPackage->services->count() ){
                foreach ($oldPackage->services as $service) {
                    $saveService = new PackageService();
                    $saveService->package_id = $newPackage->id;
                    $saveService->service_id = $service->id;
                    $saveService->save();
                }
            }
            if( $oldPackage->locations->count() ){
                foreach ($oldPackage->locations as $location) {
                    $locationNew = new PackageLocations();
                    $locationNew->package_id = $newPackage->id;
                    $locationNew->lng = $location->lng;
                    $locationNew->lat = $location->lat;
                    $locationNew->save();
                }
            }
            if( $oldPackage->images->count() ){
                foreach ($oldPackage->images as $image) {
                    $packageImages = new PackageImages();
                    $packageImages->package_id = $newPackage->id;
                    $packageImages->image_id = $image->image_id;
                    $packageImages->save();
                }
            }
        }
        
        return redirect(url('user/manage-listing-package'));
    }
}
