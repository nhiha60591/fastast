<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\FrontController;
use App\Models\PageTrans;
use App\Models\Page;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PageController extends FrontController
{
    public $layout = 'page';
    public function validator(){

    }
    public function getIndex($slug=null)
    {
        $this->theme->asset()->usePath()->add('page-css', 'css/page.css');
        parent::setTheme();
        try{
            $page = PageTrans::where('slug', $slug)->get()->first();
            if( $page ){
                if( $this->currentLanguage->id != $page->language_id ){
                    $page = PageTrans::where('pages_id', $page->pages_id)->where('language_id', $this->currentLanguage->id)->get()->first();
                    return redirect(url($page->slug));
                }
                try{
                    $parentPage = Page::find($page->pages_id);
                    $view = [];
                    $view['page'] = $page;
                    $this->theme->layout($parentPage->layout);
                    $this->theme->setTitle($page->title." - ". $this->theme->get('title'));
                    return $this->theme->scope('pages.'.$parentPage->view, $view)->render();
                }catch (Exception $e){
                    $parentPage = Page::find($page->pages_id);
                    $view = [];
                    $view['page'] = $page;
                    $this->theme->setTitle($page->title." - ". $this->theme->get('title'));
                    return $this->theme->scope('pages.index', $view)->render();
                }

            }else{
                return response()->view("errors.error");
            }
        }catch (Exception $e){
            return response()->view("errors.error");
        }
    }
    public function postIndex(Request $request, $slug){
        $optionData = get_cache('allOptionData');
        Mail::send('emails.contact', [], function ($m) use ($optionData, $request) {
            $from = config('mail.from');
            $m->from($from['address'], $from['address']);
            $m->to($optionData['site_email'], $optionData['site_name'])->subject('Thank you for your booking!');
        });
        return redirect(url($slug))->with('success', 'Thank for your message');
    }
}
