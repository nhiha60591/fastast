<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\FrontController;
use App\Models\Delivery;
use App\Models\ShippingService;
use Illuminate\Http\Request;
use App\Models\Language;
use App\Models\ServiceTypes;
use App\Models\Shipping;
use App\Models\ShippingRate;
use App\Models\ShippingWay;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Validator;
use Illuminate\Support\Facades\Session;
use Teepluss\Theme\Contracts\Theme;
use Illuminate\Support\Facades\Input;
use DB;

class ShippingController extends FrontController
{
    public function __construct(Theme $theme)
    {
        parent::__construct($theme);
        $this->middleware('auth')->only(['getPostShipping', 'getEdit']);
        $this->theme->asset()->usePath()->add('shipping-css', 'css/shipping.css');
//        $this->theme->asset()->usePath()->add('shipping-css', 'plugins/select2-4.0.2/dist/css/select2.css');

        //$this->theme->asset()->usePath()->add('jquery-1-1-1', 'js/jquery-1.11.3.min.js');
        //$this->theme->asset()->usePath()->add('select-2', 'plugins/select2-4.0.2/dist/js/select2.js');
    }

    /**
     * Set validation funtion
     *
     * @param $data
     * @return mixed
     */
    public function validator($data)
    {
        return Validator::make($data, [
            'service' => 'required',
            'serviceType' => 'required',
        ]);
    }

    /**
     * Display index for class
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $view = [];
        $shipping = new Shipping();
        $packageSize = array();
        if( Cache::has('allDeliveries') ){
            $view['deliveries'] = Cache::get('allDeliveries');
        }else{
            $view['deliveries'] = Delivery::all();
        }
        if( Cache::has('allShippingServices') ){
            $view['services'] = Cache::get('allShippingServices');
        }else{
            $view['services'] = ShippingService::all();
        }
        $type = 'weight';
        $currentLanguage = Language::where('code', config('app.locale') )->first();
        $view['currentLanguage'] = $currentLanguage;
        $view['shippings'] = $shipping->paginate(6);
        $view['packageSize'] = $packageSize;
        // Store a piece of data in the session...
        session(['packageSize' => $packageSize]);

        $view['type'] = $type;
        session(['type' => $type]);
        $this->theme->asset()->usePath()->add('list-shipping-js', 'js/list-shipping.js', array('jquery', 'google-maps-api', 'map-marker-label', 'infobox'));
        return $this->theme->scope('shipping.search', $view)->render();
    }

    /**
     * Get all shipping by search params
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSearch(Request $request)
    {
        $searchParams = [];
        $view = [];
        $shipping = new Shipping();
        if( !empty( $request->input('from') ) ){
            $from = $request->input('from');
            $searchParams['from'] = $from;
            $shipping = $shipping->whereHas('ways', function ($query) use($from) {
                $query->where('from', 'like', "%{$from}%");
            });
        }
        if( !empty( $request->input('to') ) ){
            $to = $request->input('to');
            $searchParams['to'] = $to;
            $shipping = $shipping->whereHas('ways', function ($query) use($to) {
                $query->where('to', 'like', "%{$to}%");
            });
        }
        if( !empty( $request->input('service') ) && $request->input('service') != '0'){
            $shipping = $shipping->where('service_id', $request->input('service'));
            $searchParams['service_id'] = $request->input('service');
        }
        if( !empty( $request->input('delivery') ) && $request->input('delivery') != '0'){
            $shipping = $shipping->where('delivery_id', $request->input('delivery'));
            $searchParams['delivery_id'] = $request->input('delivery');
        }
        if( !empty( $request->input('serviceType') ) && $request->input('serviceType') != '0'){
            $shipping = $shipping->whereIn('serviceType', $request->input('serviceType'));
            $searchParams['serviceType'] = $request->input('serviceType');
        }
        if( !empty( $request->input('shipperType') ) && $request->input('shipperType') != '0'){
            $searchParams['shipperType'] = $request->input('shipperType');
        }
        if( !empty( $request->input('packageType') ) && sizeof($request->input('packageType') ) > 0){
            $searchParams['packageType'] = $request->input('packageType');
        }
        $packageSize = array();
        if( !empty( $request->input('packageSize') ) && sizeof( $request->input('packageSize') ) && is_array( $request->input('packageSize') ) ){
            $packageSize = $request->input('packageSize');
            $searchParams['packageSize'] = $request->input('packageSize');
        }
        if( Cache::has('allDeliveries') ){
            $view['deliveries'] = Cache::get('allDeliveries');
        }else{
            $view['deliveries'] = Delivery::all();
        }
        if( Cache::has('allShippingServices') ){
            $view['services'] = Cache::get('allShippingServices');
        }else{
            $view['services'] = ShippingService::all();
        }
        $type = 'weight';
        if( $request->input('searchType') ){
            if( $request->input('searchType') == 'per-trip' ){
                $type= 'trip';
                $shipping = $shipping->whereHas('type', function ($query) use($type) {
                    $query->where('type', $type);
                });
            }elseif($request->input('searchType') == 'per-container'){
                $type= 'container';
                $shipping = $shipping->whereHas('type', function ($query) use($type) {
                    $query->where('type', $type);
                });
            }elseif($request->input('searchType') == 'per-cbmlcl'){
                $type= 'cbmlcl';
                $shipping = $shipping->whereHas('type', function ($query) use($type) {
                    $query->where('type', $type);
                });
            }elseif($request->input('searchType') == 'per-cbmfcl'){
                $type= 'cbmfcl';
                $shipping = $shipping->whereHas('type', function ($query) use($type) {
                    $query->where('type', $type);
                });
            }
        }
        $shipping = $shipping->where('status', 'yes');
        $inputs = $request->all();
        if( isset($inputs['page'] ) && !empty($inputs['page'])) {
            unset($inputs['page']);
        }
        $url = http_build_query($inputs);
        $searchParams['type'] = $type;
        $currentLanguage = Language::where('code', config('app.locale') )->first();
        $view['currentLanguage'] = $currentLanguage;
        $shipping = $shipping->orderBy('created_at', 'desc');
        $shipping = $shipping->paginate(6);
        $shipping = $shipping->setPath('find-shipping/?'.$url);

        foreach ($shipping as $shipping_key => &$shipping_item) {
            $shipper_locations = array();
            $shipper = $shipping_item->user;
            if($shipper)
            {
                $locations = $shipper->locations;
                if($locations)
                {
                    foreach ($locations as $location_key => $location_item) {
                        array_push($shipper_locations,array('lng' => $location_item->lng,'lat' => $location_item->lat));
                    }
                }
            }
            $shipping_item->shipper_locations = $shipper_locations;
        }

        $view['shippings'] = $shipping;
        $view['packageSize'] = $packageSize;
        $view['request'] = $request->all();
        // Store a piece of data in the session...
        session(['packageSize' => $packageSize]);
        session(['searchParams' => $searchParams]);

        $view['type'] = $type;
        session(['type' => $type]);
        $this->theme->asset()->usePath()->add('list-shipping-js', 'js/list-shipping.js', array('jquery', 'google-maps-api', 'map-marker-label', 'infobox'));
    	return $this->theme->scope('shipping.search', $view)->render();
    }

    /**
     * Display post shipping page
     *
     * @return \Illuminate\Http\Response
     */
    public function getPostShipping(){
        $this->middleware('auth');
        $view = [];
        $currentLanguage = Language::where('code', config('app.locale') )->first();
        $view['currentLanguage'] = $currentLanguage;
        if( Cache::has('allShippingServices') ){
            $view['services'] = Cache::get('allShippingServices');
        }else{
            $view['services'] = ShippingService::all();
        }
        if( Cache::has('allShippingServiceTypes') ){
            $view['serviceTypes'] = Cache::get('allShippingServiceTypes');
        }else{
            $view['serviceTypes'] = ServiceTypes::all();
        }
        if( Cache::has('allDeliveries') ){
            $view['deliveries'] = Cache::get('allDeliveries');
        }else{
            $view['deliveries'] = Delivery::all();
        }
        $view['currentCurrency'] = $this->currentCurrency;
        $this->theme->asset()->usePath()->add('post-shipping-js', 'js/post-shipping.js', array('jquery', 'tinymce-editor'));
        return $this->theme->scope('shipping.create', $view)->render();
    }

    /**
     * Save all data from shipping form
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postPostShipping( Request $request){
        $validator = $this->validator($request->all());
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.create-district-failed'));
            return redirect(url('shipping/post-shipping'))->withInput($request->input())->withErrors($validator);
        }else{
            $shipping = new Shipping();
            $shipping->user_id = Auth()->user()->id;
            $shipping->service_id = $request->input('service');
            $shipping->service_type_id = $request->input('serviceType');
            $shipping->delivery_id = $request->input('delivery');
            $shipping->math = 3000;
            if($shipping->service_id==8)
                $shipping->math = 1000000;
            $shipping->vat = 10;
            $shipping->fuel_surcharge = $request->input('fuelSurcharge');
            $shipping->fragile_surcharge = $request->input('fragileSurcharge');
            if( $this->defaultCurrency->id != $this->currentCurrency->id ){
                $loadingCal = ((double)$request->input('loadingUnloadingSurcharge') * (double)$this->defaultCurrency->value) / (double)$this->currentCurrency->value;
                $shipping->loading_unloading_surcharge = $loadingCal;
            }else{
                $shipping->loading_unloading_surcharge = $request->input('loadingUnloadingSurcharge');
            }
            $shipping->status = 'yes';
            if( $request->input('vatSurcharge') && !empty($request->input('vatSurcharge') ) ){
                $shipping->vat = $request->input('vatSurcharge');
            }
            if( $request->input('shippingMath') && !empty($request->input('shippingMath') ) ){
                $shipping->math = $request->input('shippingMath');
            }
            if( $request->input('maxLength') && !empty($request->input('maxLength') ) ){
                $shipping->max_length = $request->input('maxLength');
            }
            if( $request->input('minWeight') && !empty($request->input('minWeight') ) ){
                $shipping->min_weight = $request->input('minWeight');
            }
            if( $request->input('maxHeight') && !empty($request->input('maxHeight') ) ){
                $shipping->max_height = $request->input('maxHeight');
            }
            if( $request->input('maxWidth') && !empty($request->input('maxWidth') ) ){
                $shipping->max_width = $request->input('maxWidth');
            }
            if( $request->input('maxWeight') && !empty($request->input('maxWeight') ) ){
                $shipping->max_weight = $request->input('maxWeight');
            }
            if( $request->input('maxValue') && !empty($request->input('maxValue') ) ){
                $shipping->max_value = $request->input('maxValue');
            }
            if( $request->input('packageDeliveryDate') && !empty($request->input('packageDeliveryDate') ) ){
                $shipping->package_delivery_date = $request->input('packageDeliveryDate');
            }
            if( $request->input('doorToDoor') && !empty($request->input('doorToDoor') ) ){
                $shipping->door_to_door = $request->input('doorToDoor');
            }
            if( $request->hasFile('licenseNumber') ){
                $upload = uploadFile($request->file('licenseNumber'));
                $shipping->license_number = $upload['fileUrl'];
            }
            if( $request->hasFile('insuranceNumber') ){
                $upload = uploadFile($request->file('insuranceNumber'));
                $shipping->insurance_number = $upload['fileUrl'];
            }
            if( $request->hasFile('driverLicenseNumber') ){
                $upload = uploadFile($request->file('driverLicenseNumber'));
                $shipping->driver_license_number = $upload['fileUrl'];
            }
            if( $request->input('title') && !empty($request->input('title') ) ){
                $shipping->title = $request->input('title');
            }
            if( $request->input('description') && !empty($request->input('description') ) ){
                $shipping->description = $request->input('description');
            }
            if( $request->input('guarantee_policy') && !empty($request->input('guarantee_policy') ) ){
                $shipping->guarantee_policy = $request->input('guarantee_policy');
            }
            if( $request->input('requirement') && !empty($request->input('requirement') ) ){
                $shipping->requirement = $request->input('requirement');
            }
            if( $request->hasFile('shipping_image') ){
                $upload = uploadFile($request->file('shipping_image'));
                $shipping->shipping_image = $upload['fileUrl'];
            }
            if( $shipping->save() ){
                if( $request->input('address') && !empty($request->input('address') ) ){
                    foreach ( $request->input('address') as $add ){
                        if( !empty($add['from']) && !empty($add['to']) ){
                            $shippingAddress = new ShippingWay();
                            $shippingAddress->shipping_id = $shipping->id;
                            $shippingAddress->from = $add['from'];
                            $shippingAddress->from_long = $add['from_long'];
                            $shippingAddress->from_lat = $add['from_lat'];
                            $shippingAddress->to = $add['to'];
                            $shippingAddress->to_long = $add['to_long'];
                            $shippingAddress->to_lat = $add['to_lat'];
                            if( isset($add['two_way']) && !empty($add['two_way']) ) {
                                $shippingAddress->two_way = $add['two_way'];
                            }
                            if( isset($add['trip']) && !empty($add['trip']) ) {
                                $shippingAddress->trip = $add['trip'];
                            }
                            if( isset($add['dateGo']) && !empty($add['dateGo']) ) {
                                $shippingAddress->date_go = $add['dateGo'];
                            }
                            if( isset($add['from_location']) && !empty($add['from_location']) ) {
                                $shippingAddress->from_location = $add['from_location'];
                            }
                            if( isset($add['to_location']) && !empty($add['to_location']) ) {
                                $shippingAddress->to_location = $add['to_location'];
                            }
                            $shippingAddress->save();
                        }
                    }
                }
                if( $request->input('shippingRate') && !empty($request->input('shippingRate') ) ){
                    foreach ( $request->input('shippingRate') as $key=>$rate ){
                        $shippingRate = new ShippingRate();
                        $shippingRate->shipping_id = $shipping->id;
                        $shippingRate->weight = 0;
                        $shippingRate->rate = 0;
                        $shippingRate->unit = 0;
                        $shippingRate->trip_type = 0;
                        $shippingRate->trip_rate = 0;
                        $shippingRate->trip_loading = 0;
                        $shippingRate->rate_type = 'weight';
                        $shippingRate->type = 'item';
                        if( isset($rate['weight']) && !empty($rate['weight']) ) {
                            $shippingRate->weight = $rate['weight'];
                        }
                        if( isset($rate['rate']) && !empty($rate['rate']) ) {
                            if( $this->defaultCurrency->id != $this->currentCurrency->id ){
                                $rateCal = ((double)$rate['rate'] * (double)$this->defaultCurrency->value) / (double)$this->currentCurrency->value;
                                $shippingRate->rate = $rateCal;
                            }else{
                                $shippingRate->rate = $rate['rate'];
                            }
                        }
                        if( isset($rate['unit']) && !empty($rate['unit']) ) {
                            $shippingRate->unit = $rate['unit'];
                        }
                        if( isset($rate['trip_type']) && !empty($rate['trip_type']) ) {
                            $shippingRate->trip_type = $rate['trip_type'];
                        }
                        if( isset($rate['trip_rate']) && !empty($rate['trip_rate']) ) {
                            if( $this->defaultCurrency->id != $this->currentCurrency->id ){
                                $rateCal = ((double)$rate['trip_rate'] * (double)$this->defaultCurrency->value) / (double)$this->currentCurrency->value;
                                $shippingRate->trip_rate = $rateCal;
                            } else {
                                $shippingRate->trip_rate = $rate['trip_rate'];
                            }
                        }
                        if( isset($rate['trip_loading']) && !empty($rate['trip_loading']) ) {
                            if( $this->defaultCurrency->id != $this->currentCurrency->id ){
                                $rateCal = ((double)$rate['trip_loading'] * (double)$this->defaultCurrency->value) / (double)$this->currentCurrency->value;
                                $shippingRate->trip_loading = $rateCal;
                            } else {
                                $shippingRate->trip_loading = $rate['trip_loading'];
                            }
                        }
                        if( isset($rate['type']) && !empty($rate['type']) ) {
                            $shippingRate->type = $rate['type'];
                        }
                        if( isset($rate['rate_type']) && !empty($rate['rate_type']) ) {
                            $shippingRate->rate_type = $rate['rate_type'];
                        }
                        if( $key == '20dc' || $key == '40dc' || $key == '40hc'){
                            $shippingRate->trip_type = $key;
                        }
                        $shippingRate->save();
                    }
                }
                return redirect(url('shipping-success'));
            }
        }

    }

    public function getSuccess()
    {
        return $this->theme->scope('shipping.success')->render();
    }

    /**
     * Show shipping detail
     *
     * @param int $id
     */
    public function getShow($id=0, $kltg=0, $type='weight'){
        $shipping = Shipping::find($id);
        return $this->theme->scope('shipping.show', array('shipping'=>$shipping, 'currentLanguage'=> $this->currentLanguage))->render();
    }
    public function getSearchServiceType(){

    }
    public function getMakeCopy($id){
        $oldShipping = Shipping::find( $id );
        $newOrder = $oldShipping->replicate();
        $newOrder->save();
        if( $newOrder->save() ){
            if( $oldShipping->ways->count() ){
                foreach ( $oldShipping->ways as $way ){
                    $shippingAddress = new ShippingWay();
                    $shippingAddress->shipping_id = $newOrder->id;
                    $shippingAddress->from = $way->from;
                    $shippingAddress->from_long = $way->from_long;
                    $shippingAddress->from_lat = $way->from_lat;
                    $shippingAddress->to = $way->to;
                    $shippingAddress->to_long = $way->to_long;
                    $shippingAddress->to_lat = $way->to_lat;
                    if( isset($way->two_way) && !empty($way->two_way) ) {
                        $shippingAddress->two_way = $way->two_way;
                    }
                    if( isset($way->trip) && !empty($way->trip) ) {
                        $shippingAddress->trip = $way->trip;
                    }
                    if( isset($way->date_go) && !empty($way->date_go) ) {
                        $shippingAddress->date_go = $way->date_go;
                    }
                    if( isset($way->from_location) && !empty($way->from_location) ) {
                        $shippingAddress->from_location = $way->from_location;
                    }
                    if( isset($way->to_location) && !empty($way->to_location) ) {
                        $shippingAddress->to_location = $way->to_location;
                    }
                    $shippingAddress->save();
                }
            }
            if( $oldShipping->rates->count() ){
                foreach ( $oldShipping->rates as $rate ){
                    $shippingRate = new ShippingRate();
                    $shippingRate->shipping_id = $newOrder->id;
                    if( isset($rate->weight) && !empty($rate->weight) ) {
                        $shippingRate->weight = $rate->weight;
                    }
                    if( isset($rate->rate) && !empty($rate->rate) ) {
                        $shippingRate->rate = $rate->rate;
                    }
                    if( isset($rate->unit) && !empty($rate->unit) ) {
                        $shippingRate->unit = $rate->unit;
                    }
                    if( isset($rate->trip_type) && !empty($rate->trip_type) ) {
                        $shippingRate->trip_type = $rate->trip_type;
                    }
                    if( isset($rate->trip_rate) && !empty($rate->trip_rate) ) {
                        $shippingRate->trip_rate = $rate->trip_rate;
                    }
                    if( isset($rate->trip_loading) && !empty($rate->trip_loading) ) {
                        $shippingRate->trip_loading = $rate->trip_loading;
                    }
                    if( isset($rate->type) && !empty($rate->type) ) {
                        $shippingRate->type = $rate->type;
                    }
                    if( isset($rate->rate_type) && !empty($rate->rate_type) ) {
                        $shippingRate->rate_type = $rate->rate_type;
                    }
                    if( isset($rate->trip_type) && !empty($rate->trip_type)){
                        $shippingRate->trip_type = $rate->trip_type;
                    }
                    $shippingRate->save();
                }
            }
        }
        return redirect(url('user/manage-listing-shipping'));
    }
    public function getDelete( $id = 0 ){
        $shipping = Shipping::find($id)->delete();
        return redirect(url('user/manage-listing-shipping'));
    }
    public function getUnlistRelist( $id = 0 ){
        $shipping = Shipping::find($id);
        if( !empty( $shipping->status ) && $shipping->status != 'yes'){
            $shipping->status = 'yes';
        }else{
            $shipping->status = 'no';
        }
        $shipping->save();
        return redirect(url('user/manage-listing-shipping'));
    }
    public function getEdit($id = 0){
        $shipping = Shipping::find($id);
        $view = [];
        $currentLanguage = Language::where('code', config('app.locale') )->first();
        $view['currentLanguage'] = $currentLanguage;
        if( Cache::has('allShippingServices') ){
            $view['services'] = Cache::get('allShippingServices');
        }else{
            $view['services'] = ShippingService::all();
        }
        if( Cache::has('allShippingServiceTypes') ){
            $view['serviceTypes'] = Cache::get('allShippingServiceTypes');
        }else{
            $view['serviceTypes'] = ServiceTypes::all();
        }
        if( Cache::has('allDeliveries') ){
            $view['deliveries'] = Cache::get('allDeliveries');
        }else{
            $view['deliveries'] = Delivery::all();
        }
        $view['currentCurrency'] = $this->currentCurrency;
        $view['shipping'] = $shipping;
        $this->theme->asset()->usePath()->add('post-shipping-js', 'js/post-shipping.js', array('jquery', 'tinymce-editor'));
        return $this->theme->scope('shipping.edit', $view)->render();
    }
    public function postEdit(Request $request, $id=0){
        $validator = $this->validator($request->all());
        if ($validator->fails())
        {
            Session::flash('message', trans('admin.edit-shipping-failed'));
            return redirect(url('shipping/post-shipping'))->withInput($request->input())->withErrors($validator);
        }else{
            $shipping = Shipping::find($id);
            $shipping->service_id = $request->input('service');
            $shipping->service_type_id = $request->input('serviceType');
            $shipping->delivery_id = $request->input('delivery');
            $shipping->math = $request->input('shippingMath');
            $shipping->fuel_surcharge = $request->input('fuelSurcharge');
            $shipping->fragile_surcharge = $request->input('fragileSurcharge');
            if( $this->defaultCurrency->id != $this->currentCurrency->id ){
                $loadingCal = ((double)$request->input('loadingUnloadingSurcharge') * (double)$this->defaultCurrency->value) / (double)$this->currentCurrency->value;
                $shipping->loading_unloading_surcharge = $loadingCal;
            }else{
                $shipping->loading_unloading_surcharge = $request->input('loadingUnloadingSurcharge');
            }
            $shipping->status = 'yes';
            if( $request->input('maxLength') && !empty($request->input('maxLength') ) ){
                $shipping->max_length = $request->input('maxLength');
            }
            if( $request->input('minWeight') && !empty($request->input('minWeight') ) ){
                $shipping->min_weight = $request->input('minWeight');
            }
            if( $request->input('maxHeight') && !empty($request->input('maxHeight') ) ){
                $shipping->max_height = $request->input('maxHeight');
            }
            if( $request->input('maxWidth') && !empty($request->input('maxWidth') ) ){
                $shipping->max_width = $request->input('maxWidth');
            }
            if( $request->input('maxWeight') && !empty($request->input('maxWeight') ) ){
                $shipping->max_weight = $request->input('maxWeight');
            }
            if( $request->input('maxValue') && !empty($request->input('maxValue') ) ){
                $shipping->max_value = $request->input('maxValue');
            }
            if( $request->input('packageDeliveryDate') && !empty($request->input('packageDeliveryDate') ) ){
                $shipping->package_delivery_date = $request->input('packageDeliveryDate');
            }
            if( $request->input('doorToDoor') && !empty($request->input('doorToDoor') ) ){
                $shipping->door_to_door = $request->input('doorToDoor');
            }
            if( $request->hasFile('licenseNumber') ){
                $upload = uploadFile($request->file('licenseNumber'));
                $shipping->license_number = $upload['fileUrl'];
            }
            if( $request->hasFile('insuranceNumber') ){
                $upload = uploadFile($request->file('insuranceNumber'));
                $shipping->insurance_number = $upload['fileUrl'];
            }
            if( $request->hasFile('driverLicenseNumber') ){
                $upload = uploadFile($request->file('driverLicenseNumber'));
                $shipping->driver_license_number = $upload['fileUrl'];
            }
            if( $request->input('title') && !empty($request->input('title') ) ){
                $shipping->title = $request->input('title');
            }
            if( $request->input('description') && !empty($request->input('description') ) ){
                $shipping->description = $request->input('description');
            }
            if( $request->input('guarantee_policy') && !empty($request->input('guarantee_policy') ) ){
                $shipping->guarantee_policy = $request->input('guarantee_policy');
            }
            if( $request->input('requirement') && !empty($request->input('requirement') ) ){
                $shipping->requirement = $request->input('requirement');
            }
            if( $request->hasFile('shipping_image') ){
                $upload = uploadFile($request->file('shipping_image'));
                $shipping->shipping_image = $upload['fileUrl'];
            }
            if( $shipping->save() ){
                if( $request->input('address') && !empty($request->input('address') ) ){
                    ShippingWay::where('shipping_id', $shipping->id)->delete();
                    foreach ( $request->input('address') as $add ){
                        if( !empty($add['from']) && !empty($add['to']) ){
                            $shippingAddress = new ShippingWay();
                            $shippingAddress->shipping_id = $shipping->id;
                            $shippingAddress->from = $add['from'];
                            $shippingAddress->from_long = $add['from_long'];
                            $shippingAddress->from_lat = $add['from_lat'];
                            $shippingAddress->to = $add['to'];
                            $shippingAddress->to_long = $add['to_long'];
                            $shippingAddress->to_lat = $add['to_lat'];
                            if( isset($add['two_way']) && !empty($add['two_way']) ) {
                                $shippingAddress->two_way = $add['two_way'];
                            }
                            if( isset($add['trip']) && !empty($add['trip']) ) {
                                $shippingAddress->trip = $add['trip'];
                            }
                            if( isset($add['dateGo']) && !empty($add['dateGo']) ) {
                                $shippingAddress->date_go = $add['dateGo'];
                            }
                            if( isset($add['from_location']) && !empty($add['from_location']) ) {
                                $shippingAddress->from_location = $add['from_location'];
                            }
                            if( isset($add['to_location']) && !empty($add['to_location']) ) {
                                $shippingAddress->to_location = $add['to_location'];
                            }
                            $shippingAddress->save();
                        }
                    }
                }
                if( $request->input('shippingRate') && !empty($request->input('shippingRate') ) ){
                    ShippingRate::where('shipping_id', $shipping->id)->delete();
                    foreach ( $request->input('shippingRate') as $key=>$rate ){
                        $shippingRate = new ShippingRate();
                        $shippingRate->shipping_id = $shipping->id;
                        $shippingRate->weight = 0;
                        $shippingRate->rate = 0;
                        $shippingRate->unit = 0;
                        $shippingRate->trip_type = 0;
                        $shippingRate->trip_rate = 0;
                        $shippingRate->trip_loading = 0;
                        $shippingRate->rate_type = 'weight';
                        $shippingRate->type = 'item';
                        if( isset($rate['weight']) && !empty($rate['weight']) ) {
                            $shippingRate->weight = $rate['weight'];
                        }
                        if( isset($rate['rate']) && !empty($rate['rate']) ) {
                            if( $this->defaultCurrency->id != $this->currentCurrency->id ){
                                $rateCal = ((double)$rate['rate'] * (double)$this->defaultCurrency->value) / (double)$this->currentCurrency->value;
                                $shippingRate->rate = $rateCal;
                            }else{
                                $shippingRate->rate = $rate['rate'];
                            }
                        }
                        if( isset($rate['unit']) && !empty($rate['unit']) ) {
                            $shippingRate->unit = $rate['unit'];
                        }
                        if( isset($rate['trip_type']) && !empty($rate['trip_type']) ) {
                            $shippingRate->trip_type = $rate['trip_type'];
                        }
                        if( isset($rate['trip_rate']) && !empty($rate['trip_rate']) ) {
                            $shippingRate->trip_rate = $rate['trip_rate'];
                        }
                        if( isset($rate['trip_loading']) && !empty($rate['trip_loading']) ) {
                            $shippingRate->trip_loading = $rate['trip_loading'];
                        }
                        if( isset($rate['type']) && !empty($rate['type']) ) {
                            $shippingRate->type = $rate['type'];
                        }
                        if( isset($rate['rate_type']) && !empty($rate['rate_type']) ) {
                            $shippingRate->rate_type = $rate['rate_type'];
                        }
                        if( $key == '20dc' || $key == '40dc' || $key == '40hc'){
                            $shippingRate->trip_type = $key;
                        }
                        $shippingRate->save();
                    }
                }
                return redirect(url('shipping'));
            }
        }
    }
}
