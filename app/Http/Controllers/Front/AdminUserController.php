<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\FrontController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Package;
use App\Models\Shipping;
use App\Models\BookShipping;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Teepluss\Theme\Contracts\Theme;
use Cache;

class AdminUserController extends FrontController
{
    protected $layout = "user";

    public function __construct(Theme $theme)
    {
        parent::__construct($theme);
        $this->middleware('admin.auth');
        $this->theme->asset()->usePath()->add('user-css', 'css/user.css');
        $this->theme->asset()->usePath()->add('user-js', 'js/user.js');
    }

    protected function validator_user(array $data)
    {
        return Validator::make($data, [
            'email' => 'required',
            'first_name' => 'required|max:255',
            'last_name'  => 'required|max:255',
            'full_name'  => 'required|max:255',
            'address'    => 'required|max:255',
            'phone'      => 'required|max:255',
            'gender'     => 'max:255',
            'password'   => 'min:6|confirmed',
        ]);
    }

    public function getMyAccount(){
        $this->theme->setActive('my-account');
        $view = array();
        $this->theme->layout('full-width');
        return $this->theme->scope('user.my-account', $view)->render();
    }

    public function getUpdateProfile(){
        $this->theme->setActive('profile');
        $view = array(
            'user'=> Auth::user(),
        );
        return $this->theme->scope('user.update-profile', $view)->render();
    }

    public  function putUpdateProfile(Request $request){
        $this->theme->setActive('profile');
        $validator = $this->validator_user($request->input());
        if ($validator->fails())
        {
            return redirect(url('user/update-profile'))->withInput($request->input())->withErrors($validator);
        }else{
            $user = User::find(Auth::user()->id);
            $user->email = $request->input('email');
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->full_name = $request->input('full_name');
            $user->address = $request->input('address');
            $user->phone = $request->input('phone');
            $user->gender = $request->input('gender');
            $user->guarantee_policy = $request->input('guarantee_policy');
            $user->password = $request->input('password');
            $user->save();
            return redirect(url('user/update-profile'))->with('message', 'User updated.');
        }
    }

    // Admin Payment
    public function getPaymentView(){
        $this->theme->setActive('admin-payment');
        $view = array();
        return $this->theme->scope('user.admin.payment-view', $view)->render();
    }

    //Admin Ads payment
    public function getPaymentAdsPayment(){
        $this->theme->setActive('admin-payment');
        $view = array();
        return $this->theme->scope('user.admin.payment-ads-payment', $view)->render();
    }

    // Admin Payment
    public function getPaymentStatementView(){
        $this->theme->setActive('admin-payment');
        $this->theme->setSubActive('statement-view');
        $view = array();
        return $this->theme->scope('user.admin.payment-statement-view', $view)->render();
    }

    public function getPaymentTransactionFees(){
        $this->theme->setActive('admin-payment');
        $this->theme->setSubActive('transaction-fees');
        $view = array();
        return $this->theme->scope('user.admin.payment-transaction-fees', $view)->render();
    }

    public function getPaymentMembershipFees(){
        $this->theme->setActive('admin-payment');
        $this->theme->setSubActive('membership-fees');
        $view = array();
        return $this->theme->scope('user.admin.payment-membership-fees', $view)->render();
    }

    public function getPaymentAdvertisementFees(){
        $this->theme->setActive('admin-payment');
        $this->theme->setSubActive('advertisement-fees');
        $view = array();
        return $this->theme->scope('user.admin.payment-advertisement-fees', $view)->render();
    }

    // Admin Manage listing
    public function getManageShipping(Request $request){
        $this->theme->setActive('admin-manage-shipping');
        $view = array();
        $user = User::where('is_admin', 0);
        if( $request->input('from_date') && !empty( $request->input('from_date') ) ){
            $user = $user->where('created_at', '>=', $request->input('from_date') );
        }
        if( $request->input('to_date') && !empty( $request->input('to_date') ) ){
            $user = $user->where('created_at', '<=', $request->input('to_date') );
        }
        $view['shippers'] = $user->paginate(6);
        $view['currentCurrency'] = $this->currentCurrency;
        return $this->theme->scope('user.admin.manage-shipping', $view)->render();
    }

    public function getManageListingPackage(Request $request){
        $this->theme->setActive('admin-listing');
        $this->theme->setSubActive('listing-package');

        $view = array();
        $user = User::where('is_admin', 0);
        if( $request->input('from_date') && !empty( $request->input('from_date') ) ){
            $user = $user->where('created_at', '>=', $request->input('from_date') );
        }
        if( $request->input('to_date') && !empty( $request->input('to_date') ) ){
            $user = $user->where('created_at', '<=', $request->input('to_date') );
        }
        $packages = $user->paginate(6);
        $view['packages'] = $packages;
        return $this->theme->scope('user.admin.manage-listing-package', $view)->render();
    }

    public function getManageListingShipping(Request $request){
        $this->theme->setActive('admin-listing');
        $this->theme->setSubActive('listing-shipping');
        $view = array();
        $user = User::where('is_admin', 0);
        if( $request->input('from_date') && !empty( $request->input('from_date') ) ){
            $user = $user->where('created_at', '>=', $request->input('from_date') );
        }
        if( $request->input('to_date') && !empty( $request->input('to_date') ) ){
            $user = $user->where('created_at', '<=', $request->input('to_date') );
        }
        $shippings = $user->paginate(6);
        $view['shippings'] = $shippings;
        return $this->theme->scope('user.admin.manage-listing-shipping', $view)->render();
    }

    //Manage Message
    public function getManageChatMessage(){
        $this->theme->setActive('admin-chat-message');
        $view = array();
        return $this->theme->scope('user.admin.chat-message', $view)->render();
    }

    public function getManagePayment(){
        $this->theme->setActive('admin-payment');
        $view = array();
        return $this->theme->scope('user.my-account', $view)->render();
    }

    public function getManageAccount(){
        $this->theme->setActive('admin-manage-account');
        $view = array();
        return $this->theme->scope('user.admin.manger-account', $view)->render();
    }

    public function postManageShipping(Request $request, $id = 0){
        $user = User::find($id);
        if( !empty($request->input('transaction_count')) ){
            if( $user->isSuperUser() ){
                $user->transaction_count = -1;
            }else{
                $user->transaction_count = $request->input('transaction_count');
            }
            
        }
        if( !empty($request->input('package_click_count')) ){
            if( $user->isSuperUser() ){
                $user->package_click_count = -1;
            }else{
                $user->package_click_count = $request->input('package_click_count');
            }
        }
        $user->save();
        return redirect(url('user-admin/manage-shipping'));
    }

    public function getMakeStatus($status = 'pending', $id=0){
        $user = User::find( $id );
        $user->status = $status;
        $user->save();
        return redirect(url('user-admin/manage-shipping'));
    }
    public function getListingStatus($status='pending', $id=0){
        return redirect(url('user-admin/manage-listing-shipping'));
    }
    public function getPackageStatus($status='pending', $id=0){
        return redirect(url('user-admin/manage-listing-package'));
    }
    public function getManageExport($type="package", $ext = "xls"){
        $users = User::where('is_admin', 0)->get();
        switch($type){
            case "listing":
                $data = array(
                    'Sheet1' => array(
                        ['ID', $this->trans('front.sender'), $this->trans('front.sender_id'), $this->trans('front.address'), $this->trans('front.number_of_listing'), $this->trans('front.cancelled_listing'), $this->trans('front.number_of_listing_weight'), $this->trans('front.number_of_listing_trip')],
                    )
                );
                if( $users->count() ){
                    foreach( $users as $user){
                        $data['Sheet1'][] = [$user->id, $user->getFullName(), $user->id, $user->address, $user->getListingNumber(), $user->getCancelListing(), $user->getListingWeightNumber(), $user->getListingTripNumber()];
                    }
                }
                break;
            case "shipping":
                $data = array(
                    'Sheet1' => array(
                        ['ID', $this->trans('front.shipper'), $this->trans('front.transaction_allow'), $this->trans('front.package_click'), $this->trans('front.number_of_transaction'), $this->trans('front.fail_transaction'), $this->trans('front.success_transaction'), $this->trans('front.transaction_mount')],
                    )
                );
                if( $users->count() ){
                    foreach( $users as $user){
                        $data['Sheet1'][] = [$user->id, $user->getFullName(), $user->transaction_count, $user->package_click_count, $user->getNumberTransaction(), $user->getFailTransaction(), $user->getSuccessTransaction(), $user->getTransactionAmount()];
                    }
                }
                break;
            default:
                $data = array(
                    'Sheet1' => array(
                        ['ID', $this->trans('front.sender'), $this->trans('front.sender_id'), $this->trans('front.address'), $this->trans('front.service'), $this->trans('front.number_of_listing'), $this->trans('front.cancelled_listing'), $this->trans('front.number_of_listing_weight'), $this->trans('front.number_of_listing_trip')],
                    )
                );
                if( $users->count() ){
                    foreach( $users as $user){
                        $data['Sheet1'][] = [$user->id, $user->getFullName(), $user->id, $user->address, $user->getPackageServiceImage(), $user->getPackageNumber(), $user->getCancelPackage(), $user->getPackageWeightNumber(), $user->getPackageTripNumber()];
                    }
                }
                break;
        }
        $this->exportFile($type.'-export', $data, $ext );
    }
    public function trans($text){
        return strip_tags(trans($text));
    }
}
