<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\FrontController;
use App\Models\BookShipping;
use App\Models\Shipping;
use App\Models\UserLocations;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Package;
use App\Models\MessageConversation;
use App\Models\Message;
use App\Models\MessageAttachments;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class UserController extends FrontController
{
    protected $layout = "user";
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->theme->asset()->usePath()->add('user-css', 'css/user.css');
        $this->theme->asset()->usePath()->add('user-js', 'js/user.js', array('jquery'));
    }
    protected function validator_user(array $data)
    {
        return Validator::make($data, [
            'email' => 'required',
        ]);
    }
    public function getMyAccount(){
        $user = User::find(Auth::user()->id);
        $view = array('user'=>$user);
        return $this->theme->scope('user.profile', $view)->render();
    }
    public function getUpdateProfile(){
        $this->theme->setActive('profile');
        $view = array(
            'user'=> User::find(Auth::user()->id),
        );
        return $this->theme->scope('user.update-profile', $view)->render();
    }
    public  function postUpdateProfile(Request $request){
        $this->theme->setActive('profile');
        $validator = $this->validator_user($request->input());
        if ($validator->fails())
        {
            return redirect(url('user/update-profile'))->withInput($request->input())->withErrors($validator);
        }else{
            $user = User::find(Auth::user()->id);
            if( $request->input('user_type') ){
                $user->user_type = $request->input('user_type');
            }
            if( $request->input('full_name') ){
                $user->full_name = $request->input('full_name');
            }
            if( $request->input('nationality') ){
                $user->nationality = $request->input('nationality');
            }
            if( $request->input('companyName') ){
                $user->company_name = $request->input('companyName');
            }
            if( $request->input('address') ){
                $user->address = $request->input('address');
            }
            if( $request->input('viber') ){
                $user->viber = $request->input('viber');
            }
            if( $request->input('whats_app') ){
                $user->whats_app = $request->input('whats_app');
            }
            if( $request->input('line') ){
                $user->line = $request->input('line');
            }
            if( $request->input('hotline') ){
                $user->hotline = serialize( $request->input('hotline') );
            }
            if( $request->input('fax_number') ){
                $user->fax_number = $request->input('fax_number');
            }
            if( $request->input('email') ){
                $user->email = $request->input('email');
            }
            if( $request->input('phone_number_license') ){
                $user->phone_number_license = $request->input('phone_number_license');
            }
            if( $request->input('website') ){
                $user->website = $request->input('website');
            }
            if( $request->input('company_registered_license') ){
                $user->company_registered_license = $request->input('company_registered_license');
            }
            if( $request->input('facebook') ){
                $user->facebook = $request->input('facebook');
            }
            if( $request->input('zalo') ){
                $user->zalo = $request->input('zalo');
            }
            if( $request->input('skype') ){
                $user->skype = $request->input('skype');
            }
            if( $request->input('introduction') ){
                $user->introduction = $request->input('introduction');
            }
            if( $request->input('service') ){
                $user->service = $request->input('service');
            }
            if( $request->input('other_surcharges') ){
                $user->other_surcharges = $request->input('other_surcharges');
            }
            if( $request->input('requirements') ){
                $user->requirements = $request->input('requirements');
            }
            if( $request->input('guarantee_policy') ){
                $user->guarantee_policy = $request->input('guarantee_policy');
            }
            if( $request->hasFile('banner') ){
                $banner = uploadFile($request->file('banner'));
                $user->banner = $banner['fileUrl'];
            }
            if( $request->hasFile('avatar') ){
                $avatar = uploadFile($request->file('avatar'));
                $user->avatar = $avatar['fileUrl'];
            }
            $user->save();
            if( $request->input('locations') ){
                UserLocations::where('user_id', $user->id)->delete();
                foreach ($request->input('locations') as $locationItem){
                    $location = new UserLocations();
                    $location->user_id = $user->id;
                    $location->lng = $locationItem['long'];
                    $location->lat = $locationItem['lat'];
                    $location->name = $this->getGeo($locationItem['lat'], $locationItem['long']);
                    $location->save();
                }
            }
            return redirect(url('user/update-profile'))->with('message', 'User updated.');
        }

    }
    public  function putUpdateProfile(Request $request){
        return $this->postUpdateProfile($request);
    }
    // Manage Shipping
    public function getManageShipping(Request $request, $id=0){
        $this->theme->setActive('manage-shipping');
        $books = BookShipping::whereIn('status', array('waiting', 'wait-approve', 'shipping', 'cancel', 'success') )->whereHas('shipping', function($q) use($id)
        {
            if( $id && Auth::user()->isSuperUser() ){
                $q->where('user_id', '=', $id );
            }else{
                $q->where('user_id', '=', Auth::user()->id );
            }
        });
        if( $request->input('from_date') && !empty( $request->input('from_date') ) ){
            $books = $books->where('created_at', '>=', $request->input('from_date') );
        }
        if( $request->input('to_date') && !empty( $request->input('to_date') ) ){
            $books = $books->where('created_at', '<=', $request->input('to_date') );
        }
        $view = array('books'=>$books->paginate(6));
        return $this->theme->scope('user.manage-shipping', $view)->render();
    }

    // Manage Listing
    public function getManageListingPackage(Request $request, $id = 0){
        $this->theme->setActive('listing');
        $this->theme->setSubActive('manage-listing-package');
        $view = array();
        if( $id && Auth::user()->isSuperUser() ){
            $packages = Package::where('user_id', $id);
        }else{
            $packages = Package::where('user_id', Auth::user()->id);
        }
        if( $request->input('from_date') && !empty( $request->input('from_date') ) ){
            $packages = $packages->where('created_at', '>=', $request->input('from_date') );
        }
        if( $request->input('to_date') && !empty( $request->input('to_date') ) ){
            $packages = $packages->where('created_at', '<=', $request->input('to_date') );
        }
        $view['packages'] = $packages->paginate(6);
        $view['currentLanguage'] = $this->currentLanguage;
        return $this->theme->scope('user.manage-listing-package', $view)->render();
    }

    public function getManageListingShipping(Request $request, $id=0){
        $this->theme->setActive('listing');
        $this->theme->setSubActive('manage-listing-shipping');
        $view = array();
        if( $id && Auth::user()->isSuperUser() ){
            $shippings = Shipping::where('user_id', $id);
        }else{
            $shippings = Shipping::where('user_id', Auth::user()->id);
        }
        if( $request->input('from_date') && !empty( $request->input('from_date') ) ){
            $shippings = $shippings->where('created_at', '>=', $request->input('from_date') );
        }
        if( $request->input('to_date') && !empty( $request->input('to_date') ) ){
            $shippings = $shippings->where('created_at', '<=', $request->input('to_date') );
        }
        $view['shippings'] = $shippings->paginate(6);
        $view['currentLanguage'] = $this->currentLanguage;
        return $this->theme->scope('user.manage-listing-shipping', $view)->render();
    }

    //Manage Message
    public function getChatMessage($id=0){
        $this->theme->setActive('chat');
        $view = array();
        $mailChat = new Message();
        $users = User::where('is_admin', 0)->where('id', '<>', Auth::user()->id )->get();
        /**
         * Inbox
         */
        $inbox = $mailChat->where(function ($query) {
            $query->where('from', '=', Auth::user()->id)
                ->orWhere('to', '=', Auth::user()->id);
        })->orderBy('id', 'desc')->paginate(6)->unique('conversation_id');
        $view['inboxCount'] = $inbox->count();
        $view['users'] = $users;

        if( $id ){
            $messages = $mailChat->where('conversation_id', $id)->orderBy('id', 'desc')->paginate(6);
            $view['messages'] = $messages;
            $view['id'] = $id;
            $mailChat->where('conversation_id', $id)->update(['read' => 1]);
            return $this->theme->scope('user.message-detail', $view)->render();
        }
        $view['inbox'] = $inbox;

        /**
         * Trash
         */
        $trashCount = $mailChat->where('status', 'trash')->where('read', 0)->count();
        $trash = $mailChat->where('status', 'trash')->get();
        $view['trashCount'] = $trashCount;
        $view['trash'] = $trash;

        return $this->theme->scope('user.message', $view)->render();
    }
    //Manage Message
    public function postChatMessage(Request $request, $id=0){
        $mailChat = new Message();
        $toUser = $mailChat->where(function ($query) {
            $query->where('from', '<>', Auth::user()->id)
                ->orWhere('to', '<>', Auth::user()->id);
        })->first();
        $mailChat->body = $request->input('message');
        $mailChat->conversation_id = $id;
        $mailChat->from = Auth::user()->id;
        $mailChat->to = $toUser->id;
        $mailChat->read = 0;
        $mailChat->save();
        return redirect(url('user/chat-message', $id));
    }
    //Payment
    public function getPayment(){
        $this->theme->setActive('payment');
        $view = array();
        return $this->theme->scope('user.payment', $view)->render();
    }

    public function getAllStatementView($id = 0){
        $books = BookShipping::whereIn('status', array('success') )->whereHas('shipping', function($q) use($id)
        {
            if( $id && Auth::user()->isSuperUser() ){
                $q->where('user_id', '=', $id );
            }else{
                $q->where('user_id', '=', Auth::user()->id );
            }
        });
        $this->theme->setActive('payment');
        $this->theme->setSubActive('all-statement-view');
        $view = array();

        /**
         * Delivery Package
         */
        $DeliveredPackage = $books->sum('cost');
        $DeliveredPackage = ($DeliveredPackage * $this->currentCurrency->value) / $this->defaultCurrency->value;
        $view['deliveredPackage'] = showPricePosition($DeliveredPackage, $this->currentCurrency->position, $this->currentCurrency->symbol);

        /**
         * Sender's fee
         */
        $ShipperFee = $books->sum('sender_fee');
        $ShipperFee = ($ShipperFee * $this->currentCurrency->value) / $this->defaultCurrency->value;
        $view['shipperFee'] = showPricePosition($ShipperFee, $this->currentCurrency->position, $this->currentCurrency->symbol);

        /**
         * Shipper's fee
         */
        $SenderFee = $books->sum('shipper_fee');
        $SenderFee = ($SenderFee * $this->currentCurrency->value) / $this->defaultCurrency->value;
        $view['senderFee'] = showPricePosition($SenderFee, $this->currentCurrency->position, $this->currentCurrency->symbol);

        /**
         * Tax fee
         */
        $TaxFee = $books->sum('vat_fee');
        $TaxFee = ($TaxFee * $this->currentCurrency->value) / $this->defaultCurrency->value;
        $view['taxFee'] = showPricePosition($TaxFee, $this->currentCurrency->position, $this->currentCurrency->symbol);

        /**
         * Close Balance
         */
        $CloseBalance = $DeliveredPackage - $ShipperFee - $SenderFee - $TaxFee;
        $view['closeBalance'] = showPricePosition($CloseBalance, $this->currentCurrency->position, $this->currentCurrency->symbol);

        return $this->theme->scope('user.all-statement-view', $view)->render();
    }

    public function getTransactionView(){
        $this->theme->setActive('payment');
        $this->theme->setSubActive('transaction-view');
        $view = array();
        return $this->theme->scope('user.transaction-view', $view)->render();
    }
    public function getMembershipFees(){
        $this->theme->setActive('payment');
        $this->theme->setSubActive('membership-fees');
        $view = array();
        return $this->theme->scope('user.membership-fees', $view)->render();
    }

    public function getGeo($lat=10.754919, $lng=106.643336){
        $file = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng={$lat},{$lng}&sensor=false");
        return json_decode($file)->results[0]->formatted_address;
    }

    public function getProfile($id=0){
        $user = User::find($id);
        $view = array('user'=>$user);
        return $this->theme->scope('user.profile', $view)->render();
    }

    public function getManageExport($type="package", $ext = "xls"){
        switch($type){
            case "listing":
                $data = array(
                    'Sheet1' => array(
                        ['ID', $this->trans('front.date'), $this->trans('front.groupt_listing'), $this->trans('front.from'), $this->trans('front.to'), $this->trans('front.shipping_time'), $this->trans('front.minimun_rate'), $this->trans('front.status_rate')],
                    )
                );
                $shippings = Shipping::where('user_id', Auth::user()->id)->get();
                if( $shippings->count() ){
                    foreach( $shippings as $shipping){
                        $data['Sheet1'][] = [$shipping->id, date('M, d Y', strtotime($shipping->created_at)), 'Zone 1', @$shipping->ways()->first()->from, @$shipping->ways()->first()->to, @$shipping->delivery()->first()->translation($this->currentLanguage->id, 'name'), 'Enverlope', 'Negotiation'];
                    }
                }
                break;
            case "shipping":
                $data = array(
                    'Sheet1' => array(
                        ['ID', $this->trans('front.date'), $this->trans('front.tracking_number'), $this->trans('front.from'), $this->trans('front.to'), $this->trans('front.shipping_time'), $this->trans('front.sender'), $this->trans('front.shipping_package_information'), $this->trans('front.status')],
                    )
                );
                $this->theme->setActive('manage-shipping');
                $books = BookShipping::whereIn('status', array('waiting', 'shipping', 'cancel', 'success') )->whereHas('shipping', function($q)
                {
                    $q->where('user_id', '=', Auth::user()->id );

                })->get();
                if( $books->count() ){
                    foreach( $books as $book){
                        $origination = unserialize($book->origination);
                        $serviceData = unserialize( $book->service_info);
                        $data['Sheet1'][] = [$book->id, date('M, d Y', strtotime($book->created_at)), $book->track_number, @$serviceData['from'], @$serviceData['to'], date('M, d Y', strtotime($book->departure_date)), $origination['name'], 'Clothes', $book->status(true)];
                    }
                }
                break;
            default:
                $data = array(
                    'Sheet1' => array(
                        ['ID', $this->trans('front.date'), $this->trans('front.tracking_number'), $this->trans('front.from'), $this->trans('front.to'), $this->trans('front.shipping_time'), $this->trans('front.sender'), $this->trans('front.shipping_package_information'), $this->trans('front.status')],
                    )
                );
                $this->theme->setActive('manage-shipping');
                $books = BookShipping::whereIn('status', array('waiting', 'shipping', 'cancel', 'success') )->whereHas('shipping', function($q)
                {
                    $q->where('user_id', '=', Auth::user()->id );

                })->get();
                if( $books->count() ){
                    foreach( $books as $book){
                        $origination = unserialize($book->origination);
                        $serviceData = unserialize( $book->service_info);
                        $data['Sheet1'][] = [$book->id, date('M, d Y', strtotime($book->created_at)), $book->track_number, @$serviceData['from'], @$serviceData['to'], date('M, d Y', strtotime($book->departure_date)), $origination['name'], 'Clothes', $book->status(true)];
                    }
                }
                break;
        }
        $this->exportFile($type.'-export', $data, $ext );
    }
    public function trans($text){
        return strip_tags(trans($text));
    }
    public function postMakeMessage(Request $request){
        if( Message::where('from', '=', Auth::user()->id)->where('to', '=', $request->input('to'))->count() ){
            $conversationID = Message::where('from', '=', Auth::user()->id)->where('to', '=', $request->input('to'))->first()->conversation_id;
            $message = new Message();
            $message->body = $request->input('body');
            $message->conversation_id = $conversationID;
            $message->from = Auth::user()->id;
            $message->to = $request->input('to');
            $message->read = 0;
            $message->save();
        }else{
            $conversation = new MessageConversation();
            $conversation->status = 'inbox';
            $conversation->from = Auth::user()->id;
            $conversation->read = 0;
            $conversation->count = 1;
            $conversation->save();
            $message = new Message();
            $message->body = $request->input('body');
            $message->conversation_id = $conversation->id;
            $message->from = Auth::user()->id;
            $message->to = $request->input('to');
            $message->read = 0;
            $message->save();
        }
        return redirect( redirect()->getUrlGenerator()->previous() );
    }
    public function postManageListingUpdate(Request $request, $id=0){
        $shipping = Shipping::find($id);
        $shipping->title = $request->input('title');
        $shipping->save();
    }
    public function postManagePackageUpdate(Request $request, $id=0){
        $package = Package::find($id);
        $package->title = $request->input('title');
        $package->save();
    }
}
