<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\FrontController;
use App\Models\User;
use App\Models\RoleUser;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;

class AuthController extends FrontController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:user',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        RoleUser::create([
            'role_id' => 2,
            'user_id' => $user->id
        ]);
        return $user;
    }
    public function showLoginForm(){
        $this->theme->layout('auth');
        $currentTitle = $this->theme->get('title');
        $this->theme->setTitle(trans('front.login')." - ".$currentTitle);
        $this->theme->asset()->usePath()->add('bootstrap-social-css', 'plugins/bootstrap-social/bootstrap-social.css');
        return $this->theme->scope('auth.login')->render();
    }
    public function showRegistrationForm(){
        $this->theme->layout('auth');
        $currentTitle = $this->theme->get('title');
        $this->theme->setTitle(trans('front.register')." - ".$currentTitle);
        $this->theme->asset()->usePath()->add('bootstrap-social-css', 'plugins/bootstrap-social/bootstrap-social.css');
        return $this->theme->scope('auth.register')->render();
    }
    public function redirectSocial($social='facebook'){
        return Socialite::driver($social)->redirect();
    }
    public function loginSocial($social='facebook'){
        $socialUser = Socialite::driver($social)->user();
        if( $socialUser ) {
            $user = User::where('social', $social)->where('social_id', $socialUser->getId())->first();
            if( !empty( $socialUser->getEmail() ) ){
                $user = User::where('email', $socialUser->getEmail())->first();
            }
            if ($user) {
                $user->avatar = $socialUser->getAvatar();
                $user->save();
                Auth::login($user);
            } else {
                $userBanner = image_url('images/banner-default.jpg');
                if( $social = ''){
                    $userBanner = $socialUser->user['profile_banner_url'];
                }
                $user = User::create(array(
                    'email' => !empty($socialUser->getEmail()) ? $socialUser->getEmail() : "auto-".md5(time())."@yopmail.com",
                    'first_name' => $socialUser->getName(),
                    'last_name' => $socialUser->getName(),
                    'full_name' => $socialUser->getName(),
                    'password' => bcrypt($socialUser->id),
                    'avatar' => $socialUser->getAvatar(),
                    'banner' => $userBanner,
                    'social' => $social,
                    'social_id' => $socialUser->getId(),
                ));
                Auth::login($user);
            }
            return redirect('/');
        }else{
            echo "You can not access to this page";
        }
    }
}
