<?php
namespace App\Http\Controllers;
use Session;
use Teepluss\Theme\Contracts\Theme;
class AdminController extends Controller
{
    /**
     * Theme instance.
     *
     * @var \Teepluss\Theme\Theme
     */
    protected $theme;
    protected $title = "Dash Board";
    protected $subtitle = "Dash Board";
    protected $mainActive = "dashboard";
    protected $subActive = "general";
    protected $items = [];


    /**
     * Construct
     *
     * @return void
     */
    public function __construct(Theme $theme)
    {
        // Using theme as a global.
        $this->theme = $theme->uses('admin')->layout('default');
        $this->theme->setTitle($this->title);
        $this->theme->setSubtitle($this->subtitle);
        $this->theme->setMainactive($this->mainActive);
        $this->theme->setSubactive($this->subActive);
        if (Session::has('message')){
            $this->theme->asset()->container('footer')->usePath()->add('js-bootstrap-growl', 'js/jquery.bootstrap-growl.min.js', array('jquery'));
            // Writing an in-line script.
            $this->theme->asset()->container('footer')->writeContent('custom-inline-script', '
                <script type="text/javascript">
                    $(document).ready(function() {
                        $.bootstrapGrowl("'.Session::get('message').'", { type: "success" });
                    });
                </script>
            ', array('jquery','js-bootstrap-growl'));
        }
    }

    /**
     * Get columns count
     *
     * @return int
     */
    public function getColumnCount(){
        return 2;
    }

    /**
     * Check user permission
     *
     * @param string $name
     * @param null $id
     * @return bool
     */
    public function checkPermission($name = 'read', $id = null ){
        return true;
    }

    /**
     * Get all items
     *
     * @return array
     */
    public function getItems(){
        return $this->items;
    }

    /**
     * Get current item
     *
     * @param int $id
     * @return null
     */
    public function getItem($id=0){
        return null;
    }

    /**
     * Get view
     *
     * @param array $data
     * @param string $view
     * @param string $type
     * @return \Illuminate\Http\Response
     */
    public function view( $data = array(), $view = 'data.index', $type='index'){
        switch ($type){
            case "index":
                $this->theme_index();
                break;
            case "edit":
                $this->theme_edit();
                break;
            case "create":
                $this->theme_create();
                break;
            default:
                $this->theme_index();
                break;
        }
        $data['controllerUrl'] = $this->controlerUrl;
        $data['dis'] = $this;
        return $this->theme->scope($view, $data)->render();
    }

    /**
     * Get view for list items page
     */
    protected function theme_index(){
        $this->theme->asset()->usePath()->add('dataTables-css', 'plugins/datatables/dataTables.bootstrap.css');
        $this->theme->asset()->usePath()->add('dataTables-js', 'plugins/datatables/jquery.dataTables.min.js', array('jquery', 'jquery-ui', 'bootstrap-js') );
        $this->theme->asset()->container('footer')->usePath()->add('datatables', 'plugins/datatables/jquery.dataTables.min.js', array('jquery', 'jquery-ui', 'bootstrap-js'));
        $this->theme->asset()->container('footer')->usePath()->add('dataTables.bootstrap', 'plugins/datatables/dataTables.bootstrap.min.js', array('jquery', 'jquery-ui', 'bootstrap-js'));
        $this->theme->asset()->container('footer')->usePath()->add('jquery.slimscroll', 'plugins/slimScroll/jquery.slimscroll.min.js', array('jquery', 'jquery-ui', 'bootstrap-js'));
        $this->theme->asset()->container('footer')->usePath()->add('fastclick', 'plugins/fastclick/fastclick.js', array('jquery', 'jquery-ui', 'bootstrap-js'));
        $this->theme->asset()->container('footer')->usePath()->add('settings', 'js/settings.js', array('jquery', 'jquery-ui', 'bootstrap-js'));
        $this->theme->asset()->container('footer')->usePath()->add('list-items', 'js/list-items.js', array('jquery', 'jquery-ui', 'bootstrap-js'));
        $this->theme->asset()->container('footer')->usePath()->add('customs', 'js/customs.js');
    }

    /**
     * Get view for edit item page
     */
    protected function theme_edit(){

    }

    /**
     * Get view for edit item page
     */
    protected function theme_create(){

    }

    /**
     * Delete item
     *
     * @param $id
     */
    public function deleteDestroy($id){
        $response = array(
            'status' => 404,
            'message' => trans('Function not found')
        );
        echo json_encode($response);
        die();
    }

    /**
     * Check has items or not
     *
     * @return bool
     */
    public function has_items(){
        return !empty( $this->items );
    }

    /**
     * Print no items text
     */
    public function no_items(){
        echo trans('admin.no-items');
    }

    /**
     * Get columns
     */
    public function getColumns(){
        $this->columns = array(
            'cb' => '<input type="checkbox" class="check-all" onClick="toggle(this)" />',
            'actions' => trans('admin.actions')
        );
    }

    /**
     * Checkbox for list
     *
     * @param $item
     */
    public function column_cb( $item ) {}

    /**
     * Show column default
     *
     * @param $item
     * @param $columnName
     */
    public function columnDefault($item, $columnName){}

    /**
     * Display list items
     */
    public function display(){
        $this->display_rows_or_placeholder();
    }

    /**
     * Check has item or not to show
     */
    public function display_rows_or_placeholder(){
        if ( $this->has_items() ) {
            $this->display_rows();
        } else {
            echo '<tr class="no-items"><td class="colspanchange" colspan="' . $this->getColumnCount() . '">';
            $this->no_items();
            echo '</td></tr>';
        }
    }

    /**
     * Display list all rows
     */
    public function display_rows(){
        foreach ( $this->getItems() as $item )
            $this->single_row( $item );
    }

    /**
     * Display single row
     *
     * @param $item
     */
    public function single_row($item){
        echo '<tr>';
        $this->single_row_columns( $item );
        echo '</tr>';
    }

    /**
     * Show single row columns
     *
     * @param $item
     */
    public function single_row_columns( $item ) {
        list( $columns, $hidden, $sortable, $primary ) = $this->get_column_info();

        foreach ( $columns as $column_name => $column_display_name ) {
            $classes = "$column_name column-$column_name";
            if ( $primary === $column_name ) {
                $classes .= ' has-row-actions column-primary';
            }

            if ( in_array( $column_name, $hidden ) ) {
                $classes .= ' hidden';
            }

            // Comments column uses HTML in the display name with screen reader text.
            // Instead of using esc_attr(), we strip tags to get closer to a user-friendly string.
            $data = 'data-colname="' . wp_strip_all_tags( $column_display_name ) . '"';

            $attributes = "class='$classes' $data";

            if ( 'cb' === $column_name ) {
                echo '<th scope="row" class="check-column">';
                echo $this->column_cb( $item );
                echo '</th>';
            } elseif ( method_exists( $this, 'column_' . $column_name ) ) {
                echo "<td $attributes>";
                echo call_user_func( array( $this, 'column_' . $column_name ), $item );
                echo "</td>";
            } else {
                echo "<td $attributes>";
                echo $this->columnDefault( $item, $column_name );
                echo "</td>";
            }
        }
    }
}
