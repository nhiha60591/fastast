<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Front\HomeController@index');
// Authentication routes...
Route::get('webmaster/login', 'Admin\AdminAuthController@getLogin');
Route::post('webmaster/login', 'Admin\AdminAuthController@postLogin');
Route::get('webmaster/logout', 'Admin\AdminAuthController@getLogout');

// Registration routes...
Route::get('webmaster/register', 'Admin\AdminAuthController@getRegister');
Route::post('webmaster/register', 'Admin\AdminAuthController@postRegister');

Route::group(['prefix' => 'admin', 'middleware' => 'admin.auth'], function () {
    
    Route::get('/', 'Admin\DashboardController@index');
    Route::get('/dashboard', 'Admin\DashboardController@index');
    Route::get('general', 'Admin\DashboardController@general');
    Route::post('general/update', 'Admin\DashboardController@generalUpdate');
    Route::resource('user', 'Admin\AdminUserController');
    Route::resource('media', 'Admin\AdminMediaController');
    
    Route::resource('localization/language', 'Admin\LanguageController');
    Route::resource('localization/currency', 'Admin\CurrencyController');

    Route::resource('shipping-service', 'Admin\ShippingServiceController');
    Route::resource('unit', 'Admin\UnitController');
    Route::resource('shipping', 'Admin\ShippingController');
    Route::resource('page', 'Admin\PageController');
});

Route::group(['middleware' => 'auth'], function ($route) {
   $route->controller('user', 'Front\UserController');
   $route->controller('user-admin', 'Front\AdminUserController');
});

Route::auth();
Route::get('login/{social}', 'Auth\AuthController@loginSocial');
Route::get('social-redirect/{social}', 'Auth\AuthController@redirectSocial');
Route::get('update/settings/{type}/{id}', 'FrontController@updateSettings');


Route::group(['namespace' => 'Front'], function ($route) {
    $route->controller('shipping', 'ShippingController');
    $route->controller('package', 'PackageController');
    $route->get('find-shipping', 'ShippingController@getSearch');
    $route->get('find-package', 'PackageController@getSearch');
    $route->get('search', 'PackageController@searchTrackingCode');
    $route->get('{slug}', 'PageController@getIndex');
    $route->post('{slug}', 'PageController@postIndex');
});