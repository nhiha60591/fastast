@extends('layouts.email')

@section('content')
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">
            <!-- content -->
            <div class="content">
                <table>
                    <tr>
                        <td>

                            <h1>Hello, {{ unserialize($booking->destination)['email'] }}</h1>
                            <p>Thank have an package prepare to send to you <a href="{{ url('/') }}">Fastast</a></p>
                            <p>Please click <a href="{{ url('package/show-booking', array('id'=>$booking->id, 'shippingid'=>$shipping->id)) }}">here</a> to see more detail about that booking</p>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td></td>
    </tr>
</table>
@endsection
