@extends('layouts.email')

@section('content')
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">

                <!-- content -->
                <div class="content">
                    <table>
                        <tr>
                            <td>
                                <h1>Hello , {{ $shipping->user->full_name }}</h1>
                                <p>We would like to congratulate you have a sender was booked your shipping on <a href="{{ url('/') }}">Fastast</a></p>
                                <p>But you can see information about this booking. Because you've exceeded the number of Bookings over a month. Please upgrade your memebership level to can get more booking per month. </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
@endsection
