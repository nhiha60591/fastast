@extends('layouts.email')

@section('content')
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">

                <!-- content -->
                <div class="content">
                    <table>
                        <tr>
                            <td>
                                <p>Thank for your contact. We will check and let you know as soon as possible.</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
@endsection
