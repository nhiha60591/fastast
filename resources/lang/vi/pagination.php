<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'previous' => '&laquo; Trở lại',
    'next'     => 'Tiếp theo &raquo;',
    'sSortAscending'     => 'kích hoạt để sắp xếp cột tăng dần',
    'sSortDescending'     => 'kích hoạt để sắp xếp cột giảm dần',
    'sFirst'     => 'Đầu',
    'sLast'     => 'Cuối',
    'sNext'     => 'Tiếp theo',
    'sPrevious'     => 'Trở lại',
    'sEmptyTable'     => 'Không có dữ liệu trong bảng',
    'sInfo'     => 'Hiển thị _START_ tới _END_ của _TOTAL_ mục',
    'sInfoEmpty'     => 'Hiển thị 0 tới 0 của 0 mục',
    'sInfoFiltered'     => '(đã lọc từ _MAX_ tất cả mục)',
    'sLengthMenu'     => 'Hiển thị _MENU_ mục',
    'sLoadingRecords'     => 'Đang tải...',
    'sProcessing'     => 'Đang xử lý...',
    'sSearch'     => 'Tìm kiếm:',
    'sZeroRecords'     => 'Không tìm thấy kết quả',

];
