<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Những thông tin không phù hợp với hồ sơ của chúng tôi.',
    'throttle' => 'Quá nhiều lần đăng nhập. Vui lòng thử lại :seconds giây.',
    'email' => 'Email',
    'password' => 'Mật khẩu',
    'remember' => 'Ghi nhớ tôi',
    'forgot_password' => 'Quên mật khẩu?',
    'first_name' => 'Họ lót',
    'last_name' => 'Tên',
    'confirm_password' => 'Xác nhận mật khẩu',
    'log_in_with_your_email' => 'Đăng nhập bằng email',
    'sign_up_with_your_email' => 'Đăng ký bằng email',
    'register' => 'Đăng ký',
    'login' => 'Đăng nhập',
    'register_caption' => 'Bắt đầu hoàn toàn MIỄN PHÍ',
    'login_caption' => 'Đăng nhập tài khoản của bạn',
    'log_in_my_account' => 'Đăng nhập tài khoản của tôi',
    'create_my_account' => 'Tạo tài khoản của tôi',
];
