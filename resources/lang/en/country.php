<?php
return [
    'country' => 'Countries',
    'list' => 'List countries',
    'create' => 'Create country',
    'edit' => 'Edit country',
    'status' => 'Status',
    'no' => 'No',
    'name' => 'Country name',
    'lon' => 'Google Long',
    'lat' => 'Google Lat',
    'actions' => 'Actions',
    'time_zone' => 'Time Zone',
    'post_code' => 'Post code',
    'description' => 'Description'
];