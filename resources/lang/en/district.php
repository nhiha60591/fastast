<?php
return [
    'district' => 'District',
    'list' => 'List District',
    'create' => 'Create',
    'edit' => 'Edit',
    'remove' => 'Remove',
    'delete' => 'Delete',
    'actions' => 'Actions',
    'status' => 'Status',
    'update' => 'Update',
    'no' => 'No',
    'name' => 'Name',
    'description' => 'Description',
    'lon' => 'Google Long',
    'lat' => 'Google Lat',
];