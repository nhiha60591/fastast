<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'previous' => '&laquo; Previous',
    'next'     => 'Next &raquo;',
    'sSortAscending'     => 'activate to sort column ascending',
    'sSortDescending'     => 'activate to sort column descending',
    'sFirst'     => 'Fist',
    'sLast'     => 'Last',
    'sNext'     => 'Next',
    'sPrevious'     => 'Previous',
    'sEmptyTable'     => 'No data available in table',
    'sInfo'     => 'Showing _START_ to _END_ of _TOTAL_ entries',
    'sInfoEmpty'     => 'Showing 0 to 0 of 0 entries',
    'sInfoFiltered'     => '(filtered from _MAX_ total entries)',
    'sLengthMenu'     => 'Show _MENU_ entries',
    'sLoadingRecords'     => 'Loading...',
    'sProcessing'     => 'Processing...',
    'sSearch'     => 'Search:',
    'sZeroRecords'     => 'No matching records found',

];
