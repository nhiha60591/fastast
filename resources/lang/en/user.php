<?php
return [
    'user' => 'User',
    'create' => 'Create new user',
    'edit' => 'Edit',
    'list' => 'List all users',
    'no' => 'No',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'full_name' => 'Full Name',
    'email' => 'Email',
    'status' => 'Status',
    'role' => 'Role',
    'actions' => 'Actions',
    'active' => 'Active',
    'deactivate' => 'Deactivate',
    'your-name' => 'Your name',
    'you-are' => 'You\'re',
    'company' => 'Company',
    'individual' => 'Individual',
    'full-name' => 'First - Last Name',
    'nationality' => 'Nationality',
    'company-name' => 'Company Name',
    'address' => 'Address',
    'hotline' => 'Hotline',
    'fax_number' => 'Fax Number',
    'phone-number-license' => 'Phone Number <br /> License',
    'website' => 'Website',
    'company-registered-license' => 'Company Registered <br /> License',
    'Let-locate-your-network-on-Map' => 'Let locate your network on Map',
    'introduction' => 'Introduction',
    'service' => 'Service',
    'other-surcharges' => 'Other surcharges',
    'requirements' => 'Requirements',
    'guarantee-policy' => 'Guarantee Policy',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'enter' => 'Enter',
    'list_package' => 'List package',
    'make-pending' => 'Do you want to pending this user?',
    'make-delete' => 'Do you want to delete this user?',
    'message' => 'Message',
    'send' => 'Send',
    'user-first-last-name' => 'First - Last Name',
    'individual-driver-license' => 'Passport license',
    'individual-passport-license' => 'Driver license',
    'individual-car-license' => 'Car license',
];