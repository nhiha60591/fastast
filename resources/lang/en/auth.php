<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email' => 'Email',
    'password' => 'Password',
    'remember' => 'Remember me',
    'forgot_password' => 'Forgot your password?',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'confirm_password' => 'Confirm password',
    'log_in_with_your_email' => 'Log in with your email',
    'sign_up_with_your_email' => 'Sign up with your email',
    'register' => 'Register',
    'login' => 'Login',
    'register_caption' => 'Get started absolutely FREE',
    'login_caption' => 'Login to your account',
    'log_in_my_account' => 'Log in my account',
    'create_my_account' => 'Create my account',
];
