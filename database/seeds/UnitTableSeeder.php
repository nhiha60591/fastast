<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unit')->insert([
                'name'      => 'Kilogram',
                'slug'      => 'kg',
                'value'    => 1,
                'type'     => 'weight',
                'status'   => 'yes',
                'position'    => 1
            ]
        );
        DB::table('unit')->insert([
                'name'      => 'Centimeter',
                'slug'      => 'cm',
                'value'    => 1,
                'type'     => 'long',
                'status'   => 'yes',
                'position'    => 2
            ]
        );
    }
}
