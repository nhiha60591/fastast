<?php

use Illuminate\Database\Seeder;

class InstallData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Languages
         */
        $viID = DB::table('language')->insertGetId([
                'name'      => 'Tiếng Việt',
                'code'      => 'vi',
                'locale'    => 'vi',
                'image'     => 'vi-flag.png',
                'default'   => 1,
                'status'    => 'yes',
                'position'  => 1
            ]
        );
        $endID = DB::table('language')->insertGetId([
                'name'      => 'English',
                'code'      => 'en',
                'locale'    => 'en_US',
                'image'     => 'en-US-flag.png',
                'default'   => 0,
                'status'    => 'yes',
                'position'  => 2
            ]
        );
        /**
         * Users and Roles
         */
        DB::table('user')->insert(
            array(
                'first_name' => 'Ho',
                'last_name' => 'Huu Hien',
                'full_name' => 'Ho Huu Hien',
                'gender' => 1,
                'address' => 'Phong 1, K81/52 Pham Nhu Xuong, Hoa Khanh Nam, Lien Chieu, Da Nang',
                'phone' => '01649787224',
                'email' => 'nhiha60591@gmail.com',
                'password' => bcrypt('123123123'),
                'status' => 1,
                'is_admin' => 1,
                'notify' => 1,
            )
        );
        // Insert default roles
        $admin = DB::table('role')->insertGetId(
            array(
                'status' => 1,
            )
        );
        DB::table('role_trans')->insert(
            array(
                'role_id' => $admin,
                'language_id' => $viID,
                'name' => 'Quản trị viên',
                'display_name' => 'Quản trị viên',
                'description' => 'Quản trị viên',
            )
        );
        $member = DB::table('role')->insertGetId(
            array(
                'status' => 1,
            )
        );
        DB::table('role_trans')->insert(
            array(
                'role_id' => $member,
                'language_id' => $viID,
                'name' => 'Thành viên',
                'display_name' => 'Thành viên',
                'description' => 'Thành viên',
            )
        );
        DB::table('role_trans')->insert(
            array(
                'role_id' => $admin,
                'language_id' => $endID,
                'name' => 'Administrator',
                'display_name' => 'Administrator',
                'description' => 'Administrator',
            )
        );
        DB::table('role_trans')->insert(
            array(
                'role_id' => $member,
                'language_id' => $endID,
                'name' => 'Member',
                'display_name' => 'Member',
                'description' => 'Member',
            )
        );
        /**
         * Options
         */
        $fields = array(
            'site_name' => 'Fastast',
            'site_owner' => 'Fastast',
            'site_title' => 'Fastast',
        );
        foreach ($fields as $k=>$v){
            DB::table('option')->insert([
                'option_key' => $k,
                'option_value' => $v,
                'status' => 1,
            ]);
        }
        /**
         * Currencies
         */
        DB::table('currency')->insert([
                'name'              => 'Việt Nam Đồng',
                'code'              => 'VND',
                'default'           => 1,
                'status'            => 'yes',
                'position'          => 'right_space',
                'decimal_place'     => 0,
                'value'             => 22000,
                'symbol'            => 'đ'
            ]
        );
        DB::table('currency')->insert([
                'name'              => 'US Dollar',
                'code'              => 'USD',
                'default'           => 0,
                'status'            => 'yes',
                'position'          => 'left',
                'decimal_place'     => 0,
                'value'             => 1,
                'symbol'            => '$'
            ]
        );
        /**
         * Insert all shipping service type to database
         * @author: Hien(Hamilton) H.HO
         * @date: 09-Aug-2016
         */
        $shippingServiceTypes = [
            [
                $viID => [
                    'name' => 'Trong nước',
                    'type' => 'weight'
                ],
                $endID => [
                    'name' => 'Domestic express'
                ]
            ],
            [
                $viID => [
                    'name' => 'Ngoài nước',
                    'type' => 'weight'
                ],
                $endID => [
                    'name' => 'Worldwide express'
                ]
            ],
            [
                $viID => [
                    'name' => 'Giá theo khối lượng (VAT)',
                    'type' => 'weight'
                ],
                $endID => [
                    'name' => 'Rates per weight (VAT)'
                ]
            ],
            [
                $viID => [
                    'name' => 'Giá theo chuyến',
                    'type' => 'trip'
                ],
                $endID => [
                    'name' => 'Rates per trip'
                ]
            ],
            [
                $viID => [
                    'name' => 'Giá theo CBM (LCL)',
                    'type' => 'cbmlcl'
                ],
                $endID => [
                    'name' => 'Rates per CBM (LCL)'
                ]
            ],
            [
                $viID => [
                    'name' => 'Giá theo Container (FCL)',
                    'type' => 'container'
                ],
                $endID => [
                    'name' => 'Rates per Container (FCL)'
                ]
            ]
        ];
        foreach ( $shippingServiceTypes as $type ){
            $typeID = DB::table('service_types')->insertGetId([
                    'status'    => 1
                ]
            );
            $typeService = 'weight';
            foreach ($type as $language=>$data){
                DB::table('service_types_trans')->insertGetId([
                        'service_types_id'      => $typeID,
                        'language_id'           => $language,
                        'name'                  => $data['name']
                    ]
                );
                if( isset( $data['type'] ) && !empty($data['type']) ){
                    $typeService = $data['type'];
                }
            }
            DB::table('service_types')->where('id', $typeID)
                ->update(['type' => $typeService]);
        }

        /**
         * Insert all shipping services to database
         * @author: Hien(Hamilton) H.HO
         * @date: 09-Aug-2016
         */
        $shippingServices = [
            [
                $viID => [
                    'name' => 'Chuyển phát nhanh tài liệu/văn bản'
                ],
                $endID => [
                    'name' => 'Express Envelope/Document',
                    'type' => array(1,2)
                ]

            ],
            [
                $viID => [
                    'name' => 'Chuyển phát đường bộ'
                ],
                $endID => [
                    'name' => 'Road express',
                    'type' => array(3)
                ]

            ],
            [
                $viID => [
                    'name' => 'Vận chuyển nhà, văn phòng'
                ],
                $endID => [
                    'name' => 'Home/office moving',
                    'type' => array(4)
                ]

            ],
            [
                $viID => [
                    'name' => 'Vận chuyển hàng nhẹ bằng ôtô'
                ],
                $endID => [
                    'name' => 'Car freight',
                    'type' => array(3,4)
                ]

            ],
            [
                $viID => [
                    'name' => 'Vận chuyển bằng xe tải nhẹ'
                ],
                $endID => [
                    'name' => 'Light truck freight',
                    'type' => array(3,4)
                ]

            ],
            [
                $viID => [
                    'name' => 'Vận chuyển bằng xe tải nặng/container'
                ],
                $endID => [
                    'name' => 'Container/heavy truck freight',
                    'type' => array(3,4)
                ]

            ],
            [
                $viID => [
                    'name' => 'Vận chuyển hàng không'
                ],
                $endID => [
                    'name' => 'Air freight',
                    'type' => array(1,2)
                ]

            ],
            [
                $viID => [
                    'name' => 'Vận chuyển đường biển'
                ],
                $endID => [
                    'name' => 'Sea freight',
                    'type' => array(5,6)
                ]

            ]
        ];
        $i=1;
        foreach( $shippingServices as $data){
            $serviceID = DB::table('shipping_service')->insertGetId([
                    'status'        => 1,
                    'image'         => "uploads/2016/08/08/icon-service-{$i}.png"
                ]
            );
            $typeArray = array();
            foreach ($data as $lang=>$subData){
                DB::table('shipping_service_trans')->insert([
                        'shipping_service_id'          => $serviceID,
                        'language_id'               => $lang,
                        'name'                      => $subData['name']
                    ]
                );
                if( isset( $subData['type'] ) && sizeof($subData['type']) > 0 ){
                    $typeArray = $subData['type'];
                }
            }
            if( sizeof( $typeArray ) ){
                foreach ($typeArray as $type){
                    DB::table('service_type')->insert([
                            'service_id'          => $serviceID,
                            'type_id'             => $type
                        ]
                    );
                }
            }
            $i++;
        }

        /**
         * Insert Delivery Date
         */
        $deliveryDate = [
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 6 giờ'
                ],
                $endID => [
                    'name' => 'Delivery within 6 hours'
                ]
            ],
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 12 giờ'
                ],
                $endID => [
                    'name' => 'Delivery within 12 hours'
                ]
            ],
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 24 giờ'
                ],
                $endID => [
                    'name' => 'Delivery within 24 hours'
                ]
            ],
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 1 - 2 ngày'
                ],
                $endID => [
                    'name' => 'Delivery within 1 - 2 days'
                ]
            ],
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 2 - 4 ngày'
                ],
                $endID => [
                    'name' => 'Delivery within 2 - 4 days'
                ]
            ],
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 4 - 7 ngày'
                ],
                $endID => [
                    'name' => 'Delivery within 4 - 7 days'
                ]
            ],
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 1 - 1.5 tuần'
                ],
                $endID => [
                    'name' => 'Delivery within 1 – 1.5 weeks'
                ]
            ],
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 1.5 - 2 tuần'
                ],
                $endID => [
                    'name' => 'Delivery within 1.5 - 2 weeks'
                ]
            ],
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 2 - 3 tuần'
                ],
                $endID => [
                    'name' => 'Delivery within 2 - 3 weeks'
                ]
            ],
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 3 - 4 tuần'
                ],
                $endID => [
                    'name' => 'Delivery within 3 - 4 weeks'
                ]
            ],
            [
                $viID => [
                    'name' => 'Vận chuyển trong vòng 1 - 1.5 tháng'
                ],
                $endID => [
                    'name' => 'Delivery within 1 – 1.5 months'
                ]
            ]
        ];
        $i=1;
        foreach( $deliveryDate as $data){
            $deliveryID = DB::table('delivery')->insertGetId([
                    'status'        => 'yes'
                ]
            );
            foreach ($data as $lang=>$subData){
                DB::table('delivery_trans')->insert([
                        'delivery_id'          => $deliveryID,
                        'language_id'               => $lang,
                        'name'                      => $subData['name']
                    ]
                );
            }
            $i++;
        }
    }
}
