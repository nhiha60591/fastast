<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('packages')) {
            Schema::create('packages', function (Blueprint $table) {
                $table->increments('id');
                $table->string('status')->default('yes');
                $table->integer('user_id')->unsigned();
                $table->string('origination');
                $table->string('origination_location');
                $table->string('destination');
                $table->string('destination_location');
                $table->double('shipping_cost')->nullable();
                $table->integer('shipping_time')->unsigned()->nullable();
                $table->dateTime('departure_date')->nullable();
                $table->dateTime('arrival_date')->nullable();
                $table->string('options')->nullable();
                $table->longText('content')->nullable();
                $table->string('contact_name');
                $table->string('contact_email');
                $table->string('contact_phone');
                $table->foreign('user_id')->references('id')->on('user')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('package_service')) {
            Schema::create('package_service', function (Blueprint $table) {
                $table->integer('package_id')->unsigned();
                $table->integer('service_id')->unsigned();
                $table->foreign('package_id')->references('id')->on('packages')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('service_id')->references('id')->on('shipping_service')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('package_images')) {
            Schema::create('package_images', function (Blueprint $table) {
                $table->integer('package_id')->unsigned();
                $table->integer('image_id')->unsigned();
                $table->foreign('package_id')->references('id')->on('packages')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('image_id')->references('id')->on('media')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_images');
        Schema::dropIfExists('package_service');
        Schema::dropIfExists('packages');
    }
}
