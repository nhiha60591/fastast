<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookShipping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('book_shipping')) {
            Schema::create('book_shipping', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('shipping_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->string('track_number');
                $table->string('status')->default('draft');
                $table->longText('containing')->nullable();
                $table->longText('requirements')->nullable();
                $table->longText('origination')->nullable();
                $table->longText('destination')->nullable();
                $table->dateTime('departure_date')->nullable();
                $table->dateTime('arrival_date')->nullable();
                $table->longText('service_info')->nullable();
                $table->longText('service_options')->nullable();
                $table->string('service_type')->nullable();
                $table->foreign('shipping_id')->references('id')->on('shipping')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('user')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('book_images')) {
            Schema::create('book_images', function (Blueprint $table) {
                $table->integer('book_id')->unsigned();
                $table->integer('media_id')->unsigned();
                $table->foreign('book_id')->references('id')->on('book_shipping')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('media_id')->references('id')->on('media')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
                $table->primary(['book_id', 'media_id']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_images');
        Schema::dropIfExists('book_shipping');
    }
}
