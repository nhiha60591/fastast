<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', ['yes', 'no'])->default('yes');
            $table->timestamps();
        });
        Schema::create('delivery_trans', function (Blueprint $table) {
            $table->integer('delivery_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->enum('status', ['yes', 'no'])->default('yes');
            $table->timestamps();
        });
        Schema::create('shipping', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->integer('service_type_id')->unsigned();
            $table->integer('delivery_id')->unsigned();
            $table->string('math');
            $table->float('fuel_surcharge')->default(0);
            $table->float('fragile_surcharge')->default(0);
            $table->float('loading_unloading_surcharge')->default(0);
            $table->enum('status',['yes', 'no'])->default('yes');
            /**
             * Package requirement
             */
            $table->float('max_length')->default(0);
            $table->float('max_width')->default(0);
            $table->float('max_height')->default(0);
            $table->float('min_weight')->default(0);
            $table->float('max_weight')->default(0);
            $table->float('max_value')->default(0);
            $table->string('package_delivery_date')->nullable();
            $table->enum('door_to_door', ['yes', 'no'])->default('no');
            /**
             * More shipping detail
             */
            $table->string('license_number');
            $table->string('insurance_number');
            $table->string('driver_license_number');
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->longText('guarantee_policy')->nullable();
            $table->longText('requirement')->nullable();
            $table->string('shipping_image')->nullable();

            $table->foreign('user_id')->references('id')->on('user')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('shipping_service')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('service_type_id')->references('id')->on('service_types')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('delivery_id')->references('id')->on('delivery')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('shipping_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shipping_id')->unsigned();
            $table->double('weight')->default(0);
            $table->double('rate')->default(0);
            $table->string('unit')->default('0');
            $table->string('trip_type')->nullable();
            $table->double('trip_rate')->default(0);
            $table->double('trip_loading')->default(0);
            $table->enum('type', ['min', 'last', 'over', 'item'])->default('item');
            $table->enum('rate_type', ['weight', 'trip', 'container', 'cbmlcl', 'cbmfcl'])->default('weight');
            $table->foreign('shipping_id')->references('id')->on('shipping')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('shipping_ways', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shipping_id')->unsigned();
            $table->integer('trip')->default(0);
            $table->string('date_go')->default('per_day');
            $table->string('from');
            $table->double('from_long');
            $table->double('from_lat');
            $table->string('to');
            $table->double('to_long');
            $table->double('to_lat');
            $table->string('from_location');
            $table->string('to_location');
            $table->tinyInteger('two_way')->default(0);
            $table->foreign('shipping_id')->references('id')->on('shipping')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_ways');
        Schema::dropIfExists('shipping_rates');
        Schema::dropIfExists('shipping');
        Schema::dropIfExists('delivery_trans');
        Schema::dropIfExists('delivery');
    }
}
