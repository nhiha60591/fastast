<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('package_locations')) {
            Schema::create('package_locations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('package_id')->unsigned();
                $table->double('lng')->default(0);
                $table->double('lat')->default(0);
                $table->string('name')->nullable();
                $table->string('type')->nullable();
                $table->longText('description')->nullable();
                $table->foreign('package_id')->references('id')->on('packages')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_locations');
    }
}
