<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatMessageTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Create Message Conversation Table
         */
        if (!Schema::hasTable('message_conversation')) {
            Schema::create('message_conversation', function (Blueprint $table) {
                $table->increments('id');
                $table->string('status');
                $table->integer('from')->unsigned();
                $table->integer('read')->unsigned();
                $table->integer('count')->nullable();
                $table->string('subject');
                $table->foreign('from')->references('id')->on('user')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }
        /**
         * Create Message Table
         */
        if (!Schema::hasTable('message')) {
            Schema::create('message', function (Blueprint $table) {
                $table->increments('id');
                $table->string('status');
                $table->integer('conversation_id')->unsigned();
                $table->integer('from')->unsigned();
                $table->integer('to')->unsigned();
                $table->integer('read');
                $table->string('subject');
                $table->longText('body');
                $table->foreign('conversation_id')->references('id')->on('message_conversation')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('from')->references('id')->on('user')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('to')->references('id')->on('user')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }
        /**
         * Create Message Attachments Table
         */
        if (!Schema::hasTable('message_attachments')) {
            Schema::create('message_attachments', function (Blueprint $table) {
                $table->increments('id');
                $table->string('status');
                $table->integer('message_id')->unsigned();
                $table->integer('media_id')->unsigned();
                $table->string('media_url');
                $table->foreign('message_id')->references('id')->on('message')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('media_id')->references('id')->on('media')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_attachments');
        Schema::dropIfExists('message');
        Schema::dropIfExists('message_conversation');
    }
}
