<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableForPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Update table book_shipping
         */
        Schema::table('book_shipping', function ($table) {
            if (Schema::hasColumn('book_shipping', 'sender_fee'))
            {
                $table->double('sender_fee')->default(0)->change();
            }else{
                $table->double('sender_fee')->default(0);
            }
            if (Schema::hasColumn('book_shipping', 'shipper_fee'))
            {
                $table->double('shipper_fee')->default(0)->change();
            }else{
                $table->double('shipper_fee')->default(0);
            }
            if (Schema::hasColumn('book_shipping', 'vat_fee'))
            {
                $table->double('vat_fee')->default(0)->change();
            }else{
                $table->double('vat_fee')->default(0);
            }
        });
        /**
         * Update table book_shipping
         */
        Schema::table('shipping_service', function ($table) {
            if (Schema::hasColumn('shipping_service', 'transaction_fee'))
            {
                $table->double('transaction_fee')->default(0)->change();
            }else{
                $table->double('transaction_fee')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
