<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Update table user
         */
        Schema::table('user', function ($table) {
            if (!Schema::hasColumn('user', 'guarantee_policy'))
            {
                $table->text('guarantee_policy')->nullable();
            }
        });

        /**
         * Update table service_types
         */
        Schema::table('service_types', function ($table) {
            if (Schema::hasColumn('service_types', 'type'))
            {
                $table->enum('type', ['trip', 'weight', 'container', 'cbmlcl', 'cbmfcl'])->default('weight')->change();
            }

        });

        /**
         * Update table shipping
         */
        Schema::table('shipping', function ($table) {
            if (Schema::hasColumn('shipping', 'title'))
            {
                $table->string('title')->nullable()->change();
            }
            if (!Schema::hasColumn('shipping', 'guarantee_policy'))
            {
                $table->longText('guarantee_policy')->nullable();
            }
            if (!Schema::hasColumn('shipping', 'requirement'))
            {
                $table->longText('requirement')->nullable();
            }
            if (!Schema::hasColumn('shipping', 'shipping_image'))
            {
                $table->string('shipping_image')->nullable();
            }

        });

        /**
         * Update table service_types
         */
        Schema::table('shipping_rates', function ($table) {
            if (Schema::hasColumn('shipping_rates', 'rate_type'))
            {
                $table->enum('rate_type', ['weight', 'trip', 'container', 'cbmlcl', 'cbmfcl'])->default('weight')->change();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
