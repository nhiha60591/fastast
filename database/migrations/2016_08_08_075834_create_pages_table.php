<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->enum('status',['yes', 'no']);
            $table->foreign('user_id')->references('id')->on('user')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('pages_trans', function (Blueprint $table) {
            $table->integer('pages_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('slug')->unique();
            $table->string('title');
            $table->longText('content');
            $table->foreign('pages_id')->references('id')->on('pages')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('language')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->primary(['pages_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages_trans');
        Schema::drop('pages');
    }
}
