<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableToUpdateSomethings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Update table package
         */
        Schema::table('packages', function ($table) {
            if (Schema::hasColumn('packages', 'title'))
            {
                $table->string('title')->nullable()->change();
            }else{
                $table->string('title')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
