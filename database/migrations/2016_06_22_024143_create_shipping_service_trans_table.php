<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingServiceTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_service_trans', function (Blueprint $table) {
            $table->integer('shipping_service_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->foreign('shipping_service_id')->references('id')->on('shipping_service')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('language')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->primary(['shipping_service_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_service_trans');
    }
}
