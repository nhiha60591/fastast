<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_types', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['trip', 'weight', 'container', 'cbmlcl', 'cbmfcl'])->default('weight');
            $table->integer('status')->default('1');
            $table->timestamps();
        });
        Schema::create('service_types_trans', function (Blueprint $table) {
            $table->integer('service_types_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->foreign('service_types_id')->references('id')->on('service_types')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('language')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->primary(['service_types_id', 'language_id']);
        });
        Schema::create('shipping_service', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->integer('status')->default('1');
            $table->timestamps();
        });
        Schema::create('service_type', function (Blueprint $table) {
            $table->integer('service_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->foreign('service_id')->references('id')->on('shipping_service')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('service_types')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->primary(['service_id', 'type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_type');
        Schema::dropIfExists('service_types_trans');
        Schema::dropIfExists('service_types');
        Schema::dropIfExists('shipping_service');
    }
}
