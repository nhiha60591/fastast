<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Update table user
         */
        Schema::table('user', function ($table) {
            if (!Schema::hasColumn('user', 'user_type'))
            {
                $table->string('user_type')->nullable();
            }
            if (!Schema::hasColumn('user', 'nationality'))
            {
                $table->string('nationality')->nullable();
            }
            if (!Schema::hasColumn('user', 'company_name'))
            {
                $table->string('company_name')->nullable();
            }
            if (!Schema::hasColumn('user', 'viber'))
            {
                $table->string('viber')->nullable();
            }
            if (!Schema::hasColumn('user', 'whats_app'))
            {
                $table->string('whats_app')->nullable();
            }
            if (!Schema::hasColumn('user', 'line'))
            {
                $table->string('line')->nullable();
            }
            if (!Schema::hasColumn('user', 'hotline'))
            {
                $table->string('hotline')->nullable();
            }
            if (!Schema::hasColumn('user', 'fax_number'))
            {
                $table->string('fax_number')->nullable();
            }
            if (!Schema::hasColumn('user', 'phone_number_license'))
            {
                $table->string('phone_number_license')->nullable();
            }
            if (!Schema::hasColumn('user', 'website'))
            {
                $table->string('website')->nullable();
            }
            if (!Schema::hasColumn('user', 'company_registered_license'))
            {
                $table->string('company_registered_license')->nullable();
            }
            if (!Schema::hasColumn('user', 'facebook'))
            {
                $table->string('facebook')->nullable();
            }
            if (!Schema::hasColumn('user', 'zalo'))
            {
                $table->string('zalo')->nullable();
            }
            if (!Schema::hasColumn('user', 'skype'))
            {
                $table->string('skype')->nullable();
            }
            if (!Schema::hasColumn('user', 'introduction'))
            {
                $table->longText('introduction')->nullable();
            }
            if (!Schema::hasColumn('user', 'service'))
            {
                $table->longText('service')->nullable();
            }
            if (!Schema::hasColumn('user', 'other_surcharges'))
            {
                $table->longText('other_surcharges')->nullable();
            }
            if (!Schema::hasColumn('user', 'requirements'))
            {
                $table->longText('requirements')->nullable();
            }
            if (!Schema::hasColumn('user', 'transaction_count'))
            {
                $table->integer('transaction_count')->default(10);
            }
            if (!Schema::hasColumn('user', 'package_click_count'))
            {
                $table->integer('package_click_count')->default(10);
            }
        });
        /**
         * Update table user
         */
        Schema::table('book_shipping', function ($table) {
            if (!Schema::hasColumn('book_shipping', 'cost'))
            {
                $table->double('cost')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
