<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserLimitCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Create Package Click User
         */
        if (!Schema::hasTable('user_limit_count')) {
            Schema::create('user_limit_count', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('item_id')->unsigned();
                $table->string('type')->default('package');
                $table->integer('month')->unsigned();
                $table->integer('year')->unsigned();
                $table->foreign('user_id')->references('id')->on('user')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_limit_count')) {
            Schema::drop('user_limit_count');
        }
    }
}
