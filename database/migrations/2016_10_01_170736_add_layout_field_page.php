<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLayoutFieldPage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Update table book_shipping
         */
        Schema::table('pages', function ($table) {
            if (Schema::hasColumn('pages', 'layout'))
            {
                $table->string('layout')->default('page')->change();
            }else{
                $table->string('layout')->default('page');
            }
            if (Schema::hasColumn('pages', 'view'))
            {
                $table->string('view')->default('index')->change();
            }else{
                $table->string('view')->default('index');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
